package com.eeventoo.app.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.eeventoo.app.CustomRequest;
import com.eeventoo.app.class_.Constants;
import com.eeventoo.app.class_.MyRequestQueue;
import com.eeventoo.app.class_.UserSessionManager;
import com.eeventoo.app.interfaceListener.CustomItemClickListener;
import com.eeventoo.app.class_.Evenement;
import com.eeventoo.app.R;
import com.bumptech.glide.Glide;

import org.json.JSONArray;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;
import static com.bumptech.glide.request.RequestOptions.centerCropTransform;

/**
 * Created by isma on 07/09/2016.
 */

public class AdapterFav extends RecyclerView.Adapter<AdapterFav.MyviewHolder_> {
    Activity mContext;
    private ArrayList<Evenement> evenements =new ArrayList<>();
    CustomItemClickListener listener;
    AlertDialog.Builder builder;
    int id_user;
    String email_user;
    UserSessionManager session;


    private ImageLoader imageLoader;
    private Context context;


    public AdapterFav(Activity mContext, ArrayList<Evenement> evenements,int id_user,String email_user,CustomItemClickListener listener ) {
        this.context = mContext;
        this.evenements = evenements;
        this.listener=listener;
        this.id_user=id_user;
        this.email_user=email_user;
        session  = new UserSessionManager(mContext);
        session.retrieveIntListFav();
        session.retrieveIntListLike();
    }
    @Override
    public MyviewHolder_ onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.cardview, parent,false);
        return  new MyviewHolder_(view);
    }

    @Override
    public void onBindViewHolder(MyviewHolder_ holder, final int position) {


        Calendar calDeb = Calendar.getInstance();
        calDeb.set(Calendar.DAY_OF_MONTH,Integer.valueOf(evenements.get(position).getDatedeb().substring(0,2)) );
        calDeb.set(Calendar.MONTH,Integer.valueOf(evenements.get(position).getDatedeb().substring(3,5))-1);
        calDeb.set(Calendar.YEAR,Integer.valueOf(evenements.get(position).getDatedeb().substring(6,10)));
        calDeb.set(Calendar.HOUR_OF_DAY,Integer.valueOf(evenements.get(position).getDatedeb().substring(11,13)));
        calDeb.set(Calendar.MINUTE,Integer.valueOf(evenements.get(position).getDatedeb().substring(14,16)));




        Calendar calFin = Calendar.getInstance();
        calFin.set(Calendar.DAY_OF_MONTH,Integer.valueOf(evenements.get(position).getDatefin().substring(0,2)));
        calFin.set(Calendar.MONTH,Integer.valueOf(evenements.get(position).getDatefin().substring(3,5))-1);
        calFin.set(Calendar.YEAR,Integer.valueOf(evenements.get(position).getDatefin().substring(6,10)));
        calFin.set(Calendar.HOUR_OF_DAY,Integer.valueOf(evenements.get(position).getDatefin().substring(11,13)));
        calFin.set(Calendar.MINUTE,Integer.valueOf(evenements.get(position).getDatefin().substring(14,16)));

        holder.vhTitre.setText(evenements.get(position).getNomEvenement());
        holder.vhLieu.setText(evenements.get(position).getAdresse());
        holder.vhTarif.setText(translateTypeTarif(evenements.get(position).getTypeTarif()));
        holder.vhType.setText(translateTypeEvenement(evenements.get(position).getType()));
        holder.vhDateDeb.setText(context.getResources().getString(R.string.affiche_debut) + outPutDate(calDeb)+ " "+ outPutTime(calDeb));
        holder.vhDateFin.setText(context.getResources().getString(R.string.affiche_fin) +outPutDate(calFin)+" "+ outPutTime(calFin));
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true);
        Glide.with(context).load(evenements.get(position).getImageUrl()).apply(requestOptions).apply(centerCropTransform()).transition(withCrossFade()).into(holder.imageView);
        holder.tsLike.setCurrentText(evenements.get(position).getNbLike()+"");
        holder.tsFavoris.setCurrentText(evenements.get(position).getNbFavoris()+"");
        holder.tsAvis.setCurrentText(evenements.get(position).getNbAvis()+"");
        holder.buttonSupprimerFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                supFav(evenements.get(position).getId(),""+session.getIdUser());
                removeAt(position);

            }
        });
       // holder.vhAjouterpar.setText("Ajouté par"+evenements.get(position).getPrenomUser()+" "+evenements.get(position).getNomUser());
        //je conserve la position de la vue
        final Evenement item =evenements.get(position);
        // holder.imageView.setImageResource(R.drawable.logo_android);

        //Clique et long clique
        holder.setItemClickListener(new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                //Snackbar.make(v, evenements.get(position).getNomEvenement(),Snackbar.LENGTH_SHORT).show();
                //final MyviewHolder myviewHolder = new MyviewHolder(view);
                listener.onItemClick(v, pos);
            }


        });

    }

    @Override
    public int getItemCount() {
        return evenements.size();
    }

    class MyviewHolder_ extends RecyclerView.ViewHolder implements View.OnClickListener{
        public ImageView imageView;
       // ImageView imageView;
        TextView vhTitre;
        TextView vhDateDeb;
        TextView vhTarif;
        TextView vhLieu;
        TextView vhAjouterpar;
        //ImageView imageFavoris;
        CustomItemClickListener customItemClickListener;
        TextView vhType;
        TextView vhDateFin;
ImageButton buttonSupprimerFav;
        TextSwitcher tsLike;
        TextSwitcher tsFavoris;
        TextSwitcher tsAvis;

        MyviewHolder_(View view){
            super(view);
            vhTitre = (TextView) view.findViewById(R.id.titreEvenement);
            vhDateDeb =(TextView) view.findViewById(R.id.datedeb);
            vhTarif=(TextView) view.findViewById(R.id.tarif);
            vhLieu=(TextView) view.findViewById(R.id.lieu);
            //vhAjouterpar = (TextView) view.findViewById(R.id.ajouterpar);
            imageView= (ImageView) view.findViewById(R.id.eventImage);
           // imageFavoris= (ImageView) view.findViewById(R.id.buttonFavorite);
            //imageFavoris.setBackgroundResource(R.drawable.ico_star_border_black_24dp);
            vhType= (TextView) view.findViewById(R.id.type);
            vhDateFin = (TextView) view.findViewById(R.id.datefin);
            view.setOnClickListener(this);
            buttonSupprimerFav = (ImageButton) view.findViewById(R.id.buttonSupprimerFav);
            tsLike = (TextSwitcher) view.findViewById(R.id.tsLikes);
            tsFavoris = (TextSwitcher) view.findViewById(R.id.tsInterest);
            tsAvis = (TextSwitcher) view.findViewById(R.id.tsCommentaires);


        }



        @Override
        public void onClick(View view) {
            this.customItemClickListener.onItemClick(view,getLayoutPosition());
        }
        void setItemClickListener(CustomItemClickListener ic){
            this.customItemClickListener=ic;
        }








  /*  public void display(FakeNews nouvelle){
        currentNews = nouvelle;
        title.setText(nouvelle.title);
    }*/

    }
    public void showDialog(String title, String message,int id_user, String email_user) {

        builder.setTitle(title);
        builder.setMessage(message+ "idEvent: "+id_user);
        builder.setPositiveButton("OUI", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialogInterface.dismiss();
            }
        });
        builder.setNegativeButton("NON", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public ArrayList<Evenement> inverseList(ArrayList<Evenement> event){
        int sizeOfList=event.size();
        ArrayList<Evenement> listInverse = new ArrayList<Evenement>();
        for(int i=sizeOfList;i>0;i--){
            listInverse.add((event.get(i)));
        }
        return listInverse;

    }

    public void removeAt(int position) {
        evenements.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, evenements.size());
    }

    private void supFav(String idEvent, String idUser){
        MyRequestQueue.getInstance(context).addTorequestqueue(supFavFromServer(idEvent,idUser));
    }

    private CustomRequest supFavFromServer(String idEvent, String idUser){
        HashMap<String,String> hashMapPostValue = new HashMap<>();
        hashMapPostValue.put("idEvent",idEvent);
        hashMapPostValue.put("idUser", idUser);
        final String url = Constants.SUP_FAV_URL;
        CustomRequest customRequest= new CustomRequest(Request.Method.POST,url, hashMapPostValue,new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                Log.i("FavSERVER",response.toString() );
                Log.i("FAvSERVER",url);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

            }
        });
        return customRequest;
    }

    public String typeTarifText(String typeTarif, String tarif){
        String res = "";
        if (typeTarif.equals("Gratuit")){
            res= typeTarif;
        }
        else if (typeTarif.equals("Payant")) {
            res= typeTarif+"/"+tarif+"€";
        }
        return  res;
    }
    public String translateTypeEvenement(String type){
        String strReturn = null;


        if (type.equals("Soirées")){
            strReturn=  context.getResources().getString(R.string.soirees);
        }
        else if (type.equals("Concerts")){
            strReturn=context.getResources().getString(R.string.concerts);
        }
        else if (type.equals("Films")){
            strReturn=context.getResources().getString(R.string.film);
        }
        else if (type.equals("Spectacles")){
            strReturn=context.getResources().getString(R.string.spectacles);
        }
        else if (type.equals("Festivals")){
            strReturn=context.getResources().getString(R.string.festivals);
        }

        return strReturn;


    }

    public String translateTypeTarif(String typeTarif) {
        String strReturn = null;


        if (typeTarif.contains("y")) {
            strReturn = context.getResources().getString(R.string.payant);

        } else if (typeTarif.contains("G")) {
            strReturn = context.getResources().getString(R.string.gratuit);
        }

        return strReturn;
    }

    public String outPutDate(Calendar calendar){

        int style = DateFormat.MEDIUM;

        DateFormat df =  DateFormat.getDateInstance(style,context.getResources().getConfiguration().locale);;
        return df.format(calendar.getTime());
    }

    public String outPutTime(Calendar calendar){
        String pattern = "HH:mm";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat (pattern);
        return   simpleDateFormat.format(calendar.getTime());
    }

}
