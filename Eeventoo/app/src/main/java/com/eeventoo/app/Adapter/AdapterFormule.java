package com.eeventoo.app.Adapter;

/**
 * Created by isma on 14/10/2018.
 */


        import android.app.Activity;
        import android.content.Context;
        import androidx.recyclerview.widget.RecyclerView;
        import android.text.Editable;
        import android.text.TextWatcher;
        import android.util.Log;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.EditText;
import android.widget.ImageButton;

        import com.eeventoo.app.class_.Formule;
        import com.eeventoo.app.R;

        import java.util.ArrayList;

        import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * Created by isma on 11/03/2017.
 */
public class AdapterFormule extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private int itemsCount = 0;
    private int avatarSize;
    Activity mContext;
    private static ArrayList<Formule> formuleArrayList = new ArrayList<>();

    public AdapterFormule(Context context, ArrayList<Formule> formules) {
        this.context = context;
        this.formuleArrayList =formules;
        itemsCount=formuleArrayList.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(context).inflate(R.layout.item_formule, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int position) {

        final ViewHolder holder = (ViewHolder) viewHolder;



        Log.i("OnBInd",formuleArrayList.get(position).getPrix() + " position: "+ position);
        if(formuleArrayList.get(position).getPrix()==0){
              holder.prix.setText("");

        }
        else
        {
            holder.prix.setText(String.valueOf(formuleArrayList.get(position).getPrix()));

        }
        holder.description.setText(formuleArrayList.get(position).getDescription());

        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.i("REMOVEFORMULE","position"+position+ "arraySize" + formuleArrayList.size());
          //   holder.prix.setText("");
          //  holder.description.setText("");
                removeAt(position);

            }
        });

    }


    @Override
    public int getItemCount() {
        return formuleArrayList.size();
    }


    public void removeAt(int position) {
        formuleArrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, formuleArrayList.size());
    }

    public void updateItems() {

        notifyDataSetChanged();
    }

    public void addItem() {
        formuleArrayList.add(new Formule());
        notifyItemInserted(formuleArrayList.size() -1 );
    }

    public ArrayList<Formule> getListFormules() {
        ArrayList<Formule> formules = new ArrayList<>();
        for( int i =0 ; i< getItemCount();i++){
            Formule formule = new Formule(formuleArrayList.get(i).getPrix(),formuleArrayList.get(i).getDescription());
            formules.add(formule);
        }
        return formules;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder  {
        //@BindView(R.id.ivUserAvatar)
        EditText prix;
        EditText description;
        ImageButton btnDelete;
        public Formule formuleItem;
        public  View mView;



        public ViewHolder(View view) {
            super(view);


            prix = (EditText) view.findViewById(R.id.tarifE);
            description= (EditText) view.findViewById(R.id.descriptiontarifE);
            btnDelete = (ImageButton) view.findViewById(R.id.btnRemoveFormule);
            prix.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if(!prix.getText().toString().equals("")){
                        formuleArrayList.get(getAdapterPosition()).setPrix(Integer.valueOf(prix.getText().toString()));
                    }
                    else
                    {
                        formuleArrayList.get(getAdapterPosition()).setPrix(0);
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
            description.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    formuleArrayList.get(getAdapterPosition()).setDescription(description.getText().toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
        }


    }
}