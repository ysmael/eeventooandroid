package com.eeventoo.app.Adapter;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.eeventoo.app.BuildConfig;
import com.eeventoo.app.InterfaceRetrofit.IDeleteEventOfUser;
import com.eeventoo.app.R;
import com.eeventoo.app.activity.UpdateEventActivity;
import com.eeventoo.app.class_.Evenement;
import com.eeventoo.app.class_.Example;
import com.eeventoo.app.class_.ServiceGenerator;
import com.eeventoo.app.class_.UserSessionManager;
import com.eeventoo.app.interfaceListener.CustomItemClickListener;
import com.bumptech.glide.Glide;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;
import static com.bumptech.glide.request.RequestOptions.centerCropTransform;

/**
 * Created by isma on 22/05/2017.
 */

public class AdapterProfil extends  RecyclerView.Adapter<AdapterProfil.MyviewHolder> {
    private ArrayList<Evenement> evenements =new ArrayList<>();
    private Context context;
    CustomItemClickListener listener;
    private ImageLoader imageLoader;
    private UserSessionManager session;

    public  AdapterProfil(Context context, ArrayList<Evenement> evenements,CustomItemClickListener listener){
        this.context = context;
        this.evenements = evenements;
        this.listener=listener;
        session = new UserSessionManager(context);
    }

    @Override
    public MyviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.cell_profil, parent, false);
        return new MyviewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyviewHolder holder, final int position) {


        Calendar calDeb = Calendar.getInstance();
        calDeb.set(Calendar.DAY_OF_MONTH,Integer.valueOf(evenements.get(position).getDatedeb().substring(0,2)));
        calDeb.set(Calendar.MONTH,Integer.valueOf(evenements.get(position).getDatedeb().substring(3,5))-1);
        calDeb.set(Calendar.YEAR,Integer.valueOf(evenements.get(position).getDatedeb().substring(6,10)));
        calDeb.set(Calendar.HOUR_OF_DAY,Integer.valueOf(evenements.get(position).getDatedeb().substring(11,13)));
        calDeb.set(Calendar.MINUTE,Integer.valueOf(evenements.get(position).getDatedeb().substring(14,16)));




        Calendar calFin = Calendar.getInstance();
        calFin.set(Calendar.DAY_OF_MONTH,Integer.valueOf(evenements.get(position).getDatefin().substring(0,2)));
        calFin.set(Calendar.MONTH,Integer.valueOf(evenements.get(position).getDatefin().substring(3,5))-1);
        calFin.set(Calendar.YEAR,Integer.valueOf(evenements.get(position).getDatefin().substring(6,10)));
        calFin.set(Calendar.HOUR_OF_DAY,Integer.valueOf(evenements.get(position).getDatefin().substring(11,13)));
        calFin.set(Calendar.MINUTE,Integer.valueOf(evenements.get(position).getDatefin().substring(14,16)));


        holder.vhTitre.setText(evenements.get(position).getNomEvenement());

        holder.vhLieu.setText(evenements.get(position).getAdresse());
        holder.vhTarif.setText(translateTypeTarif(evenements.get(position).getTypeTarif()));
        holder.vhType.setText(translateTypeEvenement(evenements.get(position).getType()));
        holder.vhDatedeb.setText(context.getResources().getString(R.string.affiche_debut) + outPutDate(calDeb)+ " "+ outPutTime(calDeb));
        holder.vhDatefin.setText(context.getResources().getString(R.string.affiche_fin) +outPutDate(calFin)+" "+ outPutTime(calFin));
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true);
        Glide.with(context).load(evenements.get(position).getImageUrl()).apply(requestOptions).apply(centerCropTransform()).transition(withCrossFade()).into(holder.imageView);
        holder.tsLike.setCurrentText(evenements.get(position).getNbLike()+"");
        holder.tsFavoris.setCurrentText(evenements.get(position).getNbFavoris()+"");
        holder.tsAvis.setCurrentText(evenements.get(position).getNbAvis()+"");

        //Clique et long clique
        holder.setItemClickListener(new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                listener.onItemClick(v, pos);
            }


        });
        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteEvent(session.getIdUser()+"",evenements.get(position).getId(),position);

            }
        });

        holder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(context, UpdateEventActivity.class);
                myIntent.putExtra("id", "" + evenements.get(position).getId());
                myIntent.putExtra("nom", evenements.get(position).getNomEvenement());
                myIntent.putExtra("type", evenements.get(position).getType() + "");
                myIntent.putExtra("adresse", evenements.get(position).getAdresse() + "");
                myIntent.putExtra("adresseComplet", evenements.get(position).getAdresseComplet()) ;
                myIntent.putExtra("codepostal", evenements.get(position).getCodePostale() + "");
                myIntent.putExtra("ville", evenements.get(position).getVille() + "");
                myIntent.putExtra("departement", evenements.get(position).getDepartement());
                myIntent.putExtra("region", evenements.get(position).getRegion());
                myIntent.putExtra("paysNom", evenements.get(position).getPaysNom());
                myIntent.putExtra("paysCode", evenements.get(position).getPaysCode());
                myIntent.putExtra("latitude", evenements.get(position).getLatitude());
                myIntent.putExtra("lontitude", evenements.get(position).getLontitude());
                myIntent.putExtra("datedeb", evenements.get(position).getDatedeb() + "");
                myIntent.putExtra("datefin", evenements.get(position).getDatefin());
                myIntent.putExtra("telephone", evenements.get(position).getTelephone());
                myIntent.putExtra("typeTarif", evenements.get(position).getTypeTarif());
                myIntent.putExtra("description", evenements.get(position).getDescription());
                myIntent.putExtra("image", evenements.get(position).getImageUrl());
                context.startActivity(myIntent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return evenements.size();
    }



    private void deleteEvent(String idUser, String idEvent, final int position){
        IDeleteEventOfUser iDeleteEventOfUser = ServiceGenerator.createService(IDeleteEventOfUser.class);

        Call<Example> call = iDeleteEventOfUser.deleteEventUser(idUser,idEvent);
        call.enqueue(new Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                if(response.body() != null){

                    if(BuildConfig.DEBUG) {
                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    if (response.body().isSuccess()) {
                        removeAt(position);
                    }
                }

                else {
                    Toast.makeText(context, context.getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
                if(BuildConfig.DEBUG) {
                    Toast.makeText(context, "Erreur lors de la suppression  " + t.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        });
    }


    class MyviewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView vhTitre;
        TextView vhDatedeb;
        TextView vhTarif;
        TextView vhLieu;
        CustomItemClickListener customItemClickListener;
        ImageView imageView;
        TextView vhType;
        TextView vhDatefin;
        ImageButton imgDelete;
        TextSwitcher tsLike;
        TextSwitcher tsFavoris;
        TextSwitcher tsAvis;
        ImageButton btnEdit;

        MyviewHolder(View view) {
            super(view);
            vhTitre = (TextView) view.findViewById(R.id.titreEvenementA);
            vhDatedeb = (TextView) view.findViewById(R.id.datedebA);
            vhTarif = (TextView) view.findViewById(R.id.tarifA);
            vhLieu = (TextView) view.findViewById(R.id.lieuA);
            imageView = (ImageView) view.findViewById(R.id.eventImageA);

            vhType = (TextView) view.findViewById(R.id.typeA);
            vhDatefin = (TextView) view.findViewById(R.id.datefinA);
            view.setOnClickListener(this);
            imgDelete = (ImageButton) view.findViewById(R.id.btnDelete);
           btnEdit = (ImageButton) view.findViewById(R.id.btnmodifier);
            tsLike = (TextSwitcher) view.findViewById(R.id.tsLikes);
            tsFavoris = (TextSwitcher) view.findViewById(R.id.tsInterest);
            tsAvis = (TextSwitcher) view.findViewById(R.id.tsCommentaires);


        }

        @Override
        public void onClick(View view) {

            this.customItemClickListener.onItemClick(view, getLayoutPosition());

        }

        void setItemClickListener(CustomItemClickListener ic) {
            this.customItemClickListener = ic;
        }
    }
    public String typeTarifText(String typeTarif, String tarif){
        String res = "";
        if (typeTarif.equals("Gratuit")){
            res= typeTarif;
        }
        else if (typeTarif.equals("Payant")) {
            res= typeTarif+"/"+tarif+"€";
        }
        return  res;
    }

    public void removeAt(int position) {
        evenements.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, evenements.size());
    }

    public String translateTypeEvenement(String type){
        String strReturn = null;


        if (type.equals("Soirées")){
            strReturn=  context.getResources().getString(R.string.soirees);
        }
        else if (type.equals("Concerts")){
            strReturn=context.getResources().getString(R.string.concerts);
        }
        else if (type.equals("Films")){
            strReturn=context.getResources().getString(R.string.film);
        }
        else if (type.equals("Spectacles")){
            strReturn=context.getResources().getString(R.string.spectacles);
        }
        else if (type.equals("Festivals")){
            strReturn=context.getResources().getString(R.string.festivals);
        }

        return strReturn;


    }

    public String translateTypeTarif(String typeTarif) {
        String strReturn = null;


        if (typeTarif.contains("y")) {
            strReturn = context.getResources().getString(R.string.payant);

        } else if (typeTarif.contains("G")) {
            strReturn = context.getResources().getString(R.string.gratuit);
        }

        return strReturn;
    }

    public String outPutDate(Calendar calendar){

        int style = DateFormat.MEDIUM;

        DateFormat df =  DateFormat.getDateInstance(style,context.getResources().getConfiguration().locale);;
        return df.format(calendar.getTime());
    }

    public String outPutTime(Calendar calendar){
        String pattern = "HH:mm";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat (pattern);
        return   simpleDateFormat.format(calendar.getTime());
    }
}
