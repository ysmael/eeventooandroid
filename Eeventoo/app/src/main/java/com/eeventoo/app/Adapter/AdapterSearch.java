package com.eeventoo.app.Adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eeventoo.app.interfaceListener.CustomItemClickListener;
import com.eeventoo.app.class_.Evenement;
import com.eeventoo.app.R;

import java.util.ArrayList;

/**
 * Created by isma on 13/01/2017.
 */

public class AdapterSearch extends RecyclerView.Adapter<AdapterSearch.MyviewHolder> {
    private Context context;
    private ArrayList<Evenement> evenements =new ArrayList<>();
    CustomItemClickListener listener;


    //constructor
   public AdapterSearch( ArrayList<Evenement> evenements,CustomItemClickListener listener) {

        this.evenements = evenements;
        this.listener=listener;

    }


    @Override
    public AdapterSearch.MyviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_search, parent,false);
        return  new AdapterSearch.MyviewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterSearch.MyviewHolder holder, int position) {
        holder.vhTitre.setText(evenements.get(position).getNomEvenement());

        //Clique et long clique
        holder.setItemClickListener(new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                //Snackbar.make(v, evenements.get(position).getNomEvenement(),Snackbar.LENGTH_SHORT).show();
                //final MyviewHolder myviewHolder = new MyviewHolder(view);
                listener.onItemClick(v, pos);
            }


        });
    }



    @Override
    public int getItemCount() {
        return evenements.size();
    }

    class MyviewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView vhTitre;

        CustomItemClickListener customItemClickListener;


        MyviewHolder(View view) {
            super(view);
            vhTitre = (TextView) view.findViewById(R.id.SearchTitreEvenement);


            view.setOnClickListener(this);


        }

        @Override
        public void onClick(View view) {
            this.customItemClickListener.onItemClick(view, getLayoutPosition());
        }

        void setItemClickListener(CustomItemClickListener ic) {
            this.customItemClickListener = ic;
        }
    }

    public void setFilter(ArrayList<Evenement> listFiltre){
        evenements = new ArrayList<>();
        evenements.addAll(listFiltre);
        notifyDataSetChanged();

    }
}
