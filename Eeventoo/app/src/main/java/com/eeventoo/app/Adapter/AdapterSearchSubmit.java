package com.eeventoo.app.Adapter;

import android.app.Activity;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.eeventoo.app.interfaceListener.CustomItemClickListener;
import com.eeventoo.app.class_.Evenement;
import com.eeventoo.app.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;
import static com.bumptech.glide.request.RequestOptions.centerCropTransform;

/**
 * Created by isma on 13/01/2017.
 */

public class AdapterSearchSubmit  extends RecyclerView.Adapter<AdapterSearchSubmit.MyviewHolder>{

    private ArrayList<Evenement> evenements =new ArrayList<>();
    CustomItemClickListener listener;
    private Context context;



    //constructor
    public AdapterSearchSubmit(Activity mContext,ArrayList<Evenement> evenements, CustomItemClickListener listener) {
        this.context = mContext;
        this.evenements = evenements;
        this.listener=listener;

    }


    @Override
    public MyviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_search_submit, parent,false);
        return  new AdapterSearchSubmit.MyviewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyviewHolder holder, int position) {

        Log.i("IMGurlAcc",evenements.get(position).getImageUrl() );
        Glide.with(context).load(evenements.get(position).getImageUrl()).apply(centerCropTransform()).transition(withCrossFade()).into(holder.imageView);
        holder.vhTitre.setText(evenements.get(position).getNomEvenement());
        holder.vhDate.setText("Du " + evenements.get(position).getDatedeb()+" au "+evenements.get(position).getDatefin());
        holder.vhLieu.setText(evenements.get(position).getDepartement()+"/"+evenements.get(position).getVille());
        holder.vhTarif.setText(evenements.get(position).getTypeTarif());

        //Clique et long clique
        holder.setItemClickListener(new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

                listener.onItemClick(v, pos);
            }


        });
    }

    @Override
    public int getItemCount() {
        return evenements.size();
    }

    class MyviewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView vhTitre;
        TextView vhDate;
        TextView vhTarif;
        TextView vhLieu;
        ImageView imageView;

        CustomItemClickListener customItemClickListener;


        MyviewHolder(View view) {
            super(view);
            vhTitre = (TextView) view.findViewById(R.id.titreEvenementS);
            vhDate=(TextView) view.findViewById(R.id.dateS);
            vhTarif=(TextView) view.findViewById(R.id.tarifS);
            vhLieu=(TextView) view.findViewById(R.id.lieuS);
            imageView= (ImageView) view.findViewById(R.id.eventImageS);

            view.setOnClickListener(this);


        }

        @Override
        public void onClick(View view) {
            this.customItemClickListener.onItemClick(view, getLayoutPosition());
        }

        void setItemClickListener(CustomItemClickListener ic) {
            this.customItemClickListener = ic;
        }
    }

    public String typeTarifText(String typeTarif, String tarif){
        String res = "";
        if (typeTarif.equals("Gratuit")){
            res= typeTarif;
        }
        else if (typeTarif.equals("Payant")) {
            res= typeTarif+"/"+tarif+"€";
        }
        return  res;
    }
}
