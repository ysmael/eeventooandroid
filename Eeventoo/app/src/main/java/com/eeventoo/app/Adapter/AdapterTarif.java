package com.eeventoo.app.Adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.eeventoo.app.R;
import com.eeventoo.app.activity.PaymentActivity;
import com.eeventoo.app.class_.Formule;

import java.util.ArrayList;

/**
 * Created by isma on 18/10/2018.
 */

public class AdapterTarif extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private static ArrayList<Formule> formuleArrayList = new ArrayList<>();


    public AdapterTarif(Context context, ArrayList<Formule> formules) {
        this.context = context;
        this.formuleArrayList =formules;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(context).inflate(R.layout.cell_formule_event_item, parent, false);
        return new AdapterTarif.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {

        final AdapterTarif.ViewHolder holder = (AdapterTarif.ViewHolder) viewHolder;

        holder.description.setText(formuleArrayList.get(position).getDescription());
        holder.btnBuy.setText(context.getString(R.string.acheter)+" "+formuleArrayList.get(position).getPrix()+ formuleArrayList.get(position).getDevise());
        holder.prixF.setText(formuleArrayList.get(position).getPrix()+ formuleArrayList.get(position).getDevise());
        holder.btnBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PaymentActivity.class);
                intent.putExtra("idEvent",formuleArrayList.get(position).getIdEvent());
                intent.putExtra("idformule",formuleArrayList.get(position).getId());
                intent.putExtra("description",formuleArrayList.get(position).getDescription());
                intent.putExtra("prix",formuleArrayList.get(position).getPrix());
                intent.putExtra("devise",formuleArrayList.get(position).getDevise());

                context.startActivity(intent);


            }
        });


    }

    @Override
    public int getItemCount() {
        return formuleArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {


        TextView description;
        TextView prixF;
        Button btnBuy;


        public ViewHolder(View view) {
            super(view);


            description = (TextView) view.findViewById(R.id.descriptiontarifE);
            btnBuy = (Button) view.findViewById(R.id.buttonBuy);
            prixF =  view.findViewById(R.id.prixF);


        }
    }
}
