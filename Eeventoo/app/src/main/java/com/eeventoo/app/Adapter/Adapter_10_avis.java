package com.eeventoo.app.Adapter;

import android.app.Activity;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.eeventoo.app.class_.Avis;
import com.eeventoo.app.interfaceListener.CustomItemClickListener;
import com.eeventoo.app.R;

import java.util.ArrayList;

/**
 * Created by isma on 11/09/2016.
 */

public class Adapter_10_avis extends RecyclerView.Adapter<Adapter_10_avis.MyviewHolderAvis> {

    Activity mContext;
    private ArrayList<Avis> arrayListAvis = new ArrayList<>();
    CustomItemClickListener listener;
    AlertDialog.Builder builder;

    public Adapter_10_avis(Activity mContext, ArrayList<Avis> arrayListAvis) {
        this.mContext = mContext;
        this.arrayListAvis = arrayListAvis;

    }

    @Override
    public MyviewHolderAvis onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_cell_avis, parent, false);
        return new MyviewHolderAvis(view);
    }

    @Override
    public void onBindViewHolder(MyviewHolderAvis holder, int position) {
        holder.vhPrenom.setText(arrayListAvis.get(position).getDisplayNameUserAvis());
        holder.vhDate.setText(arrayListAvis.get(position).getDateAvis());
        holder.vhAvis.setText(arrayListAvis.get(position).getTextAvis());
        holder.ratingBar.setRating(Float.valueOf(arrayListAvis.get(position).getNbStarAvis()));
    }

    @Override
    public int getItemCount() {
        return arrayListAvis.size();
    }

    class MyviewHolderAvis extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView vhNom;
        TextView vhPrenom;
        TextView vhDate;
        TextView vhAvis;
        RatingBar ratingBar;


        MyviewHolderAvis(View view) {
            super(view);
            vhDate = (TextView) view.findViewById(R.id.dateAvis);
            vhPrenom = (TextView) view.findViewById(R.id.prenomAvis);
            vhAvis = (TextView) view.findViewById(R.id.text_avis);
            ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);


        }
    }
}
