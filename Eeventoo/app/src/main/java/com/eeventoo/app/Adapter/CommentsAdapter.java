package com.eeventoo.app.Adapter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.eeventoo.app.class_.Avis;
import com.eeventoo.app.interfaceListener.CustomItemClickListener;
import com.eeventoo.app.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;
import static com.bumptech.glide.request.RequestOptions.circleCropTransform;

/**
 * Created by isma on 11/03/2017.
 */
public class CommentsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private int itemsCount = 0;
    private int lastAnimatedPosition = -1;
    private int avatarSize;
    Activity mContext;
    private ArrayList<Avis> arrayListAvis = new ArrayList<>();
    CustomItemClickListener listener;
    private boolean animationsLocked = false;
    private boolean delayEnterAnimation = true;

    public CommentsAdapter(Context context, ArrayList<Avis> arrayListAvis) {
        this.context = context;
        avatarSize = context.getResources().getDimensionPixelSize(R.dimen.comment_avatar_size);
        this.arrayListAvis=arrayListAvis;
        itemsCount=arrayListAvis.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(context).inflate(R.layout.item_comment, parent, false);
        return new CommentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        runEnterAnimation(viewHolder.itemView, position);
        CommentViewHolder holder = (CommentViewHolder) viewHolder;
        holder.vhPrenom.setText(arrayListAvis.get(position).getDisplayNameUserAvis());
        holder.vhDate.setText(arrayListAvis.get(position).getDateAvis());
        holder.vhAvis.setText(arrayListAvis.get(position).getTextAvis());
        holder.ratingBar.setRating(Float.valueOf(arrayListAvis.get(position).getNbStarAvis()));

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true);
        Glide.with(context).load(arrayListAvis.get(position).getUserPP()).apply(requestOptions).apply(circleCropTransform()).transition(withCrossFade()).into(holder.ivUserAvatar);
    }

    private void runEnterAnimation(View view, int position) {
        if (animationsLocked) return;

        if (position > lastAnimatedPosition) {
            lastAnimatedPosition = position;
            view.setTranslationY(100);
            view.setAlpha(0.f);
            view.animate()
                    .translationY(0).alpha(1.f)
                    .setStartDelay(delayEnterAnimation ? 20 * (position) : 0)
                    .setInterpolator(new DecelerateInterpolator(2.f))
                    .setDuration(300)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            animationsLocked = true;
                        }
                    })
                    .start();
        }
    }

    @Override
    public int getItemCount() {
        return arrayListAvis.size();
    }

    public void updateItems() {

        notifyDataSetChanged();
    }

    public void addItem() {
        itemsCount++;
        notifyItemInserted(arrayListAvis.size() -1 );
    }

    public void setAnimationsLocked(boolean animationsLocked) {
        this.animationsLocked = animationsLocked;
    }

    public void setDelayEnterAnimation(boolean delayEnterAnimation) {
        this.delayEnterAnimation = delayEnterAnimation;
    }

    public static class CommentViewHolder extends RecyclerView.ViewHolder {
        //@BindView(R.id.ivUserAvatar)
        ImageView ivUserAvatar;
        TextView vhPrenom;
        TextView vhDate;
        TextView vhAvis;
        RatingBar ratingBar;


        public CommentViewHolder(View view) {
            super(view);
            ivUserAvatar = (ImageView) view.findViewById(R.id.ivUserAvatar);
            vhAvis= (TextView) view.findViewById(R.id.tvComment);
            vhDate = (TextView) view.findViewById(R.id.dateAvis);
            vhPrenom = (TextView) view.findViewById(R.id.prenomAvis);
            ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
        }
    }
}