package com.eeventoo.app.Adapter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Path;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.eeventoo.app.BuildConfig;
import com.eeventoo.app.CustomRequest;
import com.eeventoo.app.InterfaceRetrofit.I_Like;
import com.eeventoo.app.activity.LoginActivity;
import com.eeventoo.app.backgroundTask.BackgroundTaskAdd;
import com.eeventoo.app.class_.Constants;
import com.eeventoo.app.class_.MyRequestQueue;
import com.eeventoo.app.class_.ReponseServeur;
import com.eeventoo.app.class_.ServiceGenerator;
import com.eeventoo.app.class_.UserSessionManager;
import com.eeventoo.app.interfaceListener.CustomItemClickListener;
import com.eeventoo.app.class_.Evenement;
import com.eeventoo.app.interfaceListener.OnFeedItemClickListener;
import com.eeventoo.app.R;
import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONArray;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * Created by isma on 20/06/2016.
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyviewHolder> implements View.OnClickListener {

    private static final AccelerateInterpolator ACCELERATE_INTERPOLATOR = new AccelerateInterpolator();

    private static final OvershootInterpolator OVERSHOOT_INTERPOLATOR = new OvershootInterpolator(4);


    private ArrayList<Evenement> evenements = new ArrayList<>();
    CustomItemClickListener listener;
    AlertDialog.Builder builder;
    int id_user;
    String email_user;
    private ImageLoader imageLoader;
    private Context context;
    OnFeedItemClickListener onFeedItemClickListener;
    UserSessionManager session;
    private final Map<RecyclerView.ViewHolder, AnimatorSet> likeAnimations = new HashMap<>();



    private final Map<RecyclerView.ViewHolder, AnimatorSet> interestAnimations = new HashMap<>();


    @Override
    public int getItemCount() {
        return evenements.size();
    }

    public MyAdapter(Context mContext, ArrayList<Evenement> evenements) {
        this.context = mContext;
        this.evenements = evenements;
        session  = new UserSessionManager(mContext);
        session.retrieveIntListFav();


    }

    public MyAdapter(Activity mContext, ArrayList<Evenement> evenements, int id_user, String email_user, CustomItemClickListener listener, OnFeedItemClickListener onFeedItemClickListener) {
        this.context = mContext;
        this.evenements = evenements;
        this.listener = listener;
        this.id_user = id_user;
        this.email_user = email_user;
        this.onFeedItemClickListener = onFeedItemClickListener;
        session  = new UserSessionManager(mContext);
        int i = session.getIdUser();
        session.retrieveIntListFav();
        session.retrieveIntListLike();
    }

    @Override
    public MyviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.cell_home, parent, false);
        return new MyviewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyviewHolder holder, int position) {
        // imageLoader = CustomVolleyRequest.getInstance(context).getImageLoader();
        Log.i("IMGurlAcc", evenements.get(position).getImageUrl());
        //imageLoader.get(evenements.get(position).getImageUrl(), ImageLoader.getImageListener(holder.imageView, 0, 0));
        //holder.imageView.setImageUrl(evenements.get(position).getImageUrl(), imageLoader);

        Calendar calDeb = Calendar.getInstance();
        calDeb.set(Calendar.DAY_OF_MONTH,Integer.valueOf(evenements.get(position).getDatedeb().substring(0,2)));
        calDeb.set(Calendar.MONTH,Integer.valueOf(evenements.get(position).getDatedeb().substring(3,5))-1);
        calDeb.set(Calendar.YEAR,Integer.valueOf(evenements.get(position).getDatedeb().substring(6,10)));
        calDeb.set(Calendar.HOUR_OF_DAY,Integer.valueOf(evenements.get(position).getDatedeb().substring(11,13)));
        calDeb.set(Calendar.MINUTE,Integer.valueOf(evenements.get(position).getDatedeb().substring(14,16)));




        Calendar calFin = Calendar.getInstance();
        calFin.set(Calendar.DAY_OF_MONTH,Integer.valueOf(evenements.get(position).getDatefin().substring(0,2)));
        calFin.set(Calendar.MONTH,Integer.valueOf(evenements.get(position).getDatefin().substring(3,5))-1);
        calFin.set(Calendar.YEAR,Integer.valueOf(evenements.get(position).getDatefin().substring(6,10)));
        calFin.set(Calendar.HOUR_OF_DAY,Integer.valueOf(evenements.get(position).getDatefin().substring(11,13)));
        calFin.set(Calendar.MINUTE,Integer.valueOf(evenements.get(position).getDatefin().substring(14,16)));

        holder.vhTitre.setText(evenements.get(position).getNomEvenement());
        holder.vhDatedeb.setText(context.getResources().getString(R.string.affiche_debut) + outPutDate(calDeb)+ " "+ outPutTime(calDeb));
        holder.vhDatefin.setText(context.getResources().getString(R.string.affiche_fin) +outPutDate(calFin)+" "+ outPutTime(calFin));
        //holder.vhDatefin.setText(evenements.get(position).getDatefin().substring(0,2));
        holder.vhLieu.setText(evenements.get(position).getAdresse());
        String typeT=translateTypeTarif(evenements.get(position).getTypeTarif());
        holder.vhTarif.setText(typeT);
        holder.vhType.setText(translateTypeEvenement(evenements.get(position).getType()));

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
        Glide.with(context).load(evenements.get(position).getImageUrl()).apply(requestOptions.placeholder(R.mipmap.ic_app)).into(holder.imageView);
        holder.idEvent = Integer.valueOf(evenements.get(position).getId());
        holder.tsLike.setCurrentText(evenements.get(position).getNbLike()+"");
        holder.tsFavoris.setCurrentText(evenements.get(position).getNbFavoris()+"");
        holder.tsAvis.setCurrentText(evenements.get(position).getNbAvis()+"");
        holder.nbLike=evenements.get(position).getNbLike();
        holder.nbFavoris=evenements.get(position).getNbFavoris();
        holder.nbAvis=evenements.get(position).getNbAvis();




        //je conserve la position de la vue
        final Evenement item = evenements.get(position);
        // holder.imageView.setImageResource(R.drawable.logo_android);

        //Clique et long clique
        holder.setItemClickListener(new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                //Snackbar.make(v, evenements.get(position).getNomEvenement(),Snackbar.LENGTH_SHORT).show();
                //final MyviewHolder myviewHolder = new MyviewHolder(view);
                listener.onItemClick(v, pos);
            }


        });

        if (!session.getLikedPositions().contains(Integer.valueOf(evenements.get(position).getId()))) {
            resetLikeClick(holder);
            Log.i("EVENT_CONTAINT", "onBindViewHolder: TEST");
        }

        if (session.getLikedPositions().contains(Integer.valueOf(evenements.get(position).getId()))) {

            holder.imgLike.setImageResource(R.drawable.ic_heart_red);

        } else {

            holder.imgLike.setImageResource(R.drawable.ic_favorite_border_black_24dp);
        }

        if (!session.getInterstPositions().contains(Integer.valueOf(evenements.get(position).getId()))) {

            holder.imageFavoris.setImageResource(R.drawable.ic_interest);
            Log.e("myadapter", "interestpos : "+ session.getInterstPositions().toString()+ "event id " + evenements.get(position).getId());


        } else {

            holder.imageFavoris.setImageResource(R.drawable.ic_click_interest);

        }


        holder.imgComments.setOnClickListener(this);
        holder.imgComments.setTag(position);

        holder.imgLike.setOnClickListener(this);
        holder.imgLike.setTag(holder);

        holder.imageFavoris.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickInterest(view,item);

            }
        });
        holder.imageFavoris.setTag(holder);



    }


    class MyviewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView vhTitre;
        TextView vhDatedeb;
        TextView vhTarif;
        TextView vhLieu;
        ImageButton imageFavoris;
        CustomItemClickListener customItemClickListener;
        ImageView imageView;
        TextView vhType;
        TextView vhDatefin;
        ImageButton imgLike;
        ImageButton imgComments;
        Integer idEvent;
        TextSwitcher tsLike;
        TextSwitcher tsFavoris;
        TextSwitcher tsAvis;
        int nbLike;
        int nbFavoris;
        int nbAvis;
        MyviewHolder(View view) {
            super(view);
            vhTitre = (TextView) view.findViewById(R.id.titreEvenementA);
            vhDatedeb = (TextView) view.findViewById(R.id.datedebA);
            vhTarif = (TextView) view.findViewById(R.id.tarifA);
            vhLieu = (TextView) view.findViewById(R.id.lieuA);
            imageView = (ImageView) view.findViewById(R.id.eventImageA);

            imageFavoris = (ImageButton) view.findViewById(R.id.buttonFavorite);
            //imageFavoris.setBackgroundResource(R.drawable.ico_star_border_black_24dp);
            vhType = (TextView) view.findViewById(R.id.typeA);
            vhDatefin = (TextView) view.findViewById(R.id.datefinA);
            view.setOnClickListener(this);
            imgLike = (ImageButton) view.findViewById(R.id.btnLike);
            imgComments = (ImageButton) view.findViewById(R.id.btnComments);
            tsLike = (TextSwitcher) view.findViewById(R.id.tsLikes);
            tsFavoris = (TextSwitcher) view.findViewById(R.id.tsInterest);
            tsAvis = (TextSwitcher) view.findViewById(R.id.tsCommentaires);


        }


        @Override
        public void onClick(View view) {

            this.customItemClickListener.onItemClick(view, getLayoutPosition());

        }

        void setItemClickListener(CustomItemClickListener ic) {
            this.customItemClickListener = ic;
        }


    }

    public void onClick(View v) {
        if (session.isUserLoggedIn()) {
            if (v.getId() == R.id.btnComments) {
                if (onFeedItemClickListener != null) {
                    onFeedItemClickListener.onCommentsClick(v, (Integer) v.getTag());
                }
            } else if (v.getId() == R.id.btnLike) {

                MyviewHolder holder = (MyviewHolder) v.getTag();

                if (!session.getLikedPositions().contains(holder.idEvent)) {

                    session.getLikedPositions().add(holder.idEvent);
                    Log.i("LikePOSITION", session.getLikedPositions().toString());
                    holder.nbLike++;
                    holder.tsLike.setCurrentText(""+holder.nbLike);
                    updateHeartButton(holder, true);
                    addLike(session.getIdUser()+"",holder.idEvent+"");


                } else {

                    updateHeartButton(holder, false);
                    holder.nbLike--;
                    holder.tsLike.setCurrentText(holder.nbLike+"");
                    suppLike(session.getIdUser()+"",holder.idEvent+"");

                }

            }

        }else
        {
            Intent intent = new Intent(context, LoginActivity.class);
            context.startActivity(intent);
        }
    }

    public void  onClickInterest(View v , Evenement event){
        if (session.isUserLoggedIn()) {
            if (v.getId()==R.id.buttonFavorite){

                MyviewHolder holder = (MyviewHolder) v.getTag();

                if (!session.getInterstPositions().contains(Integer.valueOf(event.getId()))) {

                    session.getInterstPositions().add(Integer.valueOf( event.getId()));
                    Log.i("LikePOSITION", session.getInterstPositions().toString());

                    updateInterestBuuton(holder, true);

                    holder.nbFavoris++;

                    holder.tsFavoris.setCurrentText(holder.nbFavoris+"");
                    if(BuildConfig.DEBUG) {
                        Toast.makeText(context, "nbFav: " + holder.nbFavoris, Toast.LENGTH_SHORT).show();
                    }
                    BackgroundTaskAdd backgroundTaskAddFav = new BackgroundTaskAdd(context);

                    backgroundTaskAddFav.execute("favoris", "null", "" + session.getIdUser(), "" + event.getId());
                    if(BuildConfig.DEBUG) {
                        Toast.makeText(context, "evenement id" + event.getId(), Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    holder.nbFavoris--;
                    holder.tsFavoris.setCurrentText(holder.nbFavoris+"");
                    if(BuildConfig.DEBUG) {
                        Toast.makeText(context, "nbFav: " + holder.nbFavoris, Toast.LENGTH_SHORT).show();
                    }
                    updateInterestBuuton(holder, false);
                    supFav(event.getId(),""+session.getIdUser());

                }

            }
        }
        else{
            Intent intent = new Intent(context, LoginActivity.class);
            context.startActivity(intent);
        }
    }

    public void setOnFeedItemClickListener(OnFeedItemClickListener onFeedItemClickListener) {
        this.onFeedItemClickListener = onFeedItemClickListener;
    }

    public void showDialog(String title, String message, int id_user, String email_user) {

        builder.setTitle(title);
        builder.setMessage(message + "idEvent: " + id_user);
        builder.setPositiveButton("OUI", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialogInterface.dismiss();
            }
        });
        builder.setNegativeButton("NON", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }




    public static Bitmap GetBitmapClippedCircle(Bitmap bitmap) {

        final int width = bitmap.getWidth();
        final int height = bitmap.getHeight();
        final Bitmap outputBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        final Path path = new Path();
        path.addCircle(
                (float) (width / 2)
                , (float) (height / 2)
                , (float) Math.min(width, (height / 2))
                , Path.Direction.CCW);

        final Canvas canvas = new Canvas(outputBitmap);
        canvas.clipPath(path);
        canvas.drawBitmap(bitmap, 0, 0, null);
        return outputBitmap;
    }

    private void updateHeartButton(final MyviewHolder holder, boolean animated) {

        if (animated) {

            if (!likeAnimations.containsKey(holder)) {

                AnimatorSet animatorSet = new AnimatorSet();

                likeAnimations.put(holder, animatorSet);


                ObjectAnimator rotationAnim = ObjectAnimator.ofFloat(holder.imgLike, "rotation", 0f, 360f);

                rotationAnim.setDuration(300);

                rotationAnim.setInterpolator(ACCELERATE_INTERPOLATOR);


                ObjectAnimator bounceAnimX = ObjectAnimator.ofFloat(holder.imgLike, "scaleX", 0.2f, 1f);

                bounceAnimX.setDuration(300);

                bounceAnimX.setInterpolator(OVERSHOOT_INTERPOLATOR);


                ObjectAnimator bounceAnimY = ObjectAnimator.ofFloat(holder.imgLike, "scaleY", 0.2f, 1f);

                bounceAnimY.setDuration(300);

                bounceAnimY.setInterpolator(OVERSHOOT_INTERPOLATOR);

                bounceAnimY.addListener(new AnimatorListenerAdapter() {

                    @Override

                    public void onAnimationStart(Animator animation) {

                        holder.imgLike.setImageResource(R.drawable.ic_heart_red);

                    }

                });


                animatorSet.play(rotationAnim);

                animatorSet.play(bounceAnimX).with(bounceAnimY).after(rotationAnim);


                animatorSet.addListener(new AnimatorListenerAdapter() {

                    @Override

                    public void onAnimationEnd(Animator animation) {

                        resetLikeAnimationState(holder);

                    }

                });


                animatorSet.start();

            }

        } else {

            if (session.getLikedPositions().contains(holder.idEvent)) {

                holder.imgLike.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                session.getLikedPositions().remove(holder.idEvent);

                Log.i("FALSELikePOSITION", session.getLikedPositions().toString() + holder.idEvent);

            } else {

                holder.imgLike.setImageResource(R.drawable.ic_heart_red);

            }

        }


    }

    private void resetLikeAnimationState(MyviewHolder holder) {

        likeAnimations.remove(holder);



    }

    private void resetLikeClick(MyviewHolder holder){
        holder.imgLike.setImageResource(R.drawable.ic_favorite_border_black_24dp);
    }

    public void updateInterestBuuton (final MyviewHolder holder, boolean animated) {

        if (animated) {

            if (!interestAnimations.containsKey(holder)) {

                AnimatorSet animatorSet = new AnimatorSet();

                interestAnimations.put(holder, animatorSet);


                ObjectAnimator rotationAnim = ObjectAnimator.ofFloat(holder.imageFavoris, "rotation", 0f, 360f);

                rotationAnim.setDuration(300);

                rotationAnim.setInterpolator(ACCELERATE_INTERPOLATOR);


                ObjectAnimator bounceAnimX = ObjectAnimator.ofFloat(holder.imageFavoris, "scaleX", 0.2f, 1f);

                bounceAnimX.setDuration(300);

                bounceAnimX.setInterpolator(OVERSHOOT_INTERPOLATOR);


                ObjectAnimator bounceAnimY = ObjectAnimator.ofFloat(holder.imageFavoris, "scaleY", 0.2f, 1f);

                bounceAnimY.setDuration(300);

                bounceAnimY.setInterpolator(OVERSHOOT_INTERPOLATOR);

                bounceAnimY.addListener(new AnimatorListenerAdapter() {

                    @Override

                    public void onAnimationStart(Animator animation) {

                        holder.imageFavoris.setImageResource(R.drawable.ic_click_interest);


                    }

                });


                animatorSet.play(rotationAnim);

                animatorSet.play(bounceAnimX).with(bounceAnimY).after(rotationAnim);


                animatorSet.addListener(new AnimatorListenerAdapter() {

                    @Override

                    public void onAnimationEnd(Animator animation) {

                        resetInterestAnimationState(holder);

                    }

                });


                animatorSet.start();

            }

        } else {

            if (session.getInterstPositions().contains(holder.idEvent)) {

                holder.imageFavoris.setImageResource(R.drawable.ic_interest);
                session.getInterstPositions().remove(holder.idEvent);

                Log.i("FALSELikePOSITION", session.getInterstPositions().toString() + holder.idEvent);

            } else {

                holder.imageFavoris.setImageResource(R.drawable.ic_click_interest);


            }

        }


    }

    private void resetInterestAnimationState(MyviewHolder holder) {
        interestAnimations.remove(holder);
    }
    /*public String typeTarifText(String typeTarif, String tarif){
        String res = "";
        if (typeTarif.equals("Gratuit")){
            res= typeTarif;
        }
        else if (typeTarif.equals("Payant")) {
            res= typeTarif+"/"+tarif+"€";
        }
        return  res;
    }*/

    private  void addLike(String idUser,String idEvent){
        I_Like i_addLike = ServiceGenerator.createService(I_Like.class);

        Call<ReponseServeur> call = i_addLike.addLike(idUser,idEvent);
        call.enqueue(new Callback<ReponseServeur>() {
            @Override
            public void onResponse(Call<ReponseServeur> call, retrofit2.Response<ReponseServeur> response) {

                  if(response.body() != null){
                      if(BuildConfig.DEBUG) {
                          Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                          Log.e("myadapter", response.body().getMessage().toString());
                      }
                }

                else {
                    Toast.makeText(context, context.getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<ReponseServeur> call, Throwable t) {
                Toast.makeText(context, context.getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();

                if(BuildConfig.DEBUG) {
                    Toast.makeText(context, "Erreur d'ajout  " + t.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.e("myadapter", "Erreur suppression  " + t.getMessage());
                }
            }
        });



    }


    private void suppLike(String idUser,String idEvent){
        I_Like i_like=  ServiceGenerator.createService(I_Like.class);

        Call<ReponseServeur> call = i_like.supLike(idUser,idEvent);
        call.enqueue(new Callback<ReponseServeur>() {
            @Override
            public void onResponse(Call<ReponseServeur> call, retrofit2.Response<ReponseServeur> response) {
                  if(response.body() != null){
                      if(BuildConfig.DEBUG) {
                          Toast.makeText(context, response.body().getMessage().toString(), Toast.LENGTH_SHORT).show();
                          Log.e("myadapter", response.body().getMessage().toString());
                      }

                }

                else {
                    Toast.makeText(context, context.getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<ReponseServeur> call, Throwable t) {
                Toast.makeText(context, context.getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();

                if(BuildConfig.DEBUG) {
                    Toast.makeText(context, "Erreur suppression  " + t.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.e("myadapter", "Erreur suppression  " + t.getMessage());
                }
            }
        });

    }

    private void supFav(String idEvent, String idUser){
        MyRequestQueue.getInstance(context).addTorequestqueue(supFavFromServer(idEvent,idUser));
    }

    private CustomRequest supFavFromServer(String idEvent, String idUser){
        HashMap<String,String> hashMapPostValue = new HashMap<>();
        hashMapPostValue.put("idEvent",idEvent);
        hashMapPostValue.put("idUser", idUser);
        final String url = Constants.SUP_FAV_URL;
        CustomRequest customRequest= new CustomRequest(Request.Method.POST,url, hashMapPostValue,new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                Log.i("FavSERVER",response.toString() );
                Log.i("FAvSERVER",url);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

            }
        });
        return customRequest;
    }

    private void updateLikeCount(MyviewHolder holder){

    }
    public  UserSessionManager  getSession(){
        return session;
    }

    public String translateTypeEvenement(String type){
        String strReturn = null;


        if (type.equals("Soirées")){
            strReturn=  context.getResources().getString(R.string.soirees);
        }
        else if (type.equals("Concerts")){
            strReturn=context.getResources().getString(R.string.concerts);
        }
        else if (type.equals("Films")){
            strReturn=context.getResources().getString(R.string.film);
        }
        else if (type.equals("Spectacles")){
            strReturn=context.getResources().getString(R.string.spectacles);
        }
        else if (type.equals("Festivals")){
            strReturn=context.getResources().getString(R.string.festivals);
        }

        return strReturn;


    }

    public String translateTypeTarif(String typeTarif) {
        String strReturn = null;


        if (typeTarif.contains("y")) {
            strReturn = context.getResources().getString(R.string.payant);

        } else if (typeTarif.contains("G")) {
            strReturn = context.getResources().getString(R.string.gratuit);
        }

        return strReturn;
    }

    public String outPutDate(Calendar calendar){

        int style = DateFormat.MEDIUM;

        DateFormat df =  DateFormat.getDateInstance(style,context.getResources().getConfiguration().locale);;
        return df.format(calendar.getTime());
    }

    public String outPutTime(Calendar calendar){
        String pattern = "HH:mm";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat (pattern);
        return   simpleDateFormat.format(calendar.getTime());
    }



    private int dpToPx(int dp) {
        float density = context.getResources().getDisplayMetrics().density;
        return Math.round((float)dp * density);
    }

}