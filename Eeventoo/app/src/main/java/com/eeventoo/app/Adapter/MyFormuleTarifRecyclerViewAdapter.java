package com.eeventoo.app.Adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import com.eeventoo.app.R;
import com.eeventoo.app.class_.Formule;
import com.eeventoo.app.fragment.FormuleTarifFragment.OnListFragmentInteractionListener;
import com.eeventoo.app.fragment.dummy.DummyContent.FormuleItem;

import java.util.ArrayList;
import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link FormuleItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyFormuleTarifRecyclerViewAdapter extends RecyclerView.Adapter<MyFormuleTarifRecyclerViewAdapter.ViewHolder> {

    private final ArrayList<Formule> mValues;
    private final OnListFragmentInteractionListener mListener;
    private final ArrayList<ViewHolder> listViewHolder= new ArrayList<>();

    public MyFormuleTarifRecyclerViewAdapter(ArrayList<Formule> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_formuletarif, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);

holder.mRevoveButton.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {

        Log.i("REMOVEFORMULE","position"+position+ "arraySize" + mValues.size());
       /* holder.mTarifE.setText("");
        holder.mDescriptionTarifE.setText("");*/
        removeAt(position);
refreshList();

    }
});

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                    Log.i("HOLDERVIEW", "onClick: ");
                }
            }
        });

        listViewHolder.add(holder);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void addItem() {

        mValues.add(new Formule());
        notifyItemInserted(mValues.size() -1 );
        notifyDataSetChanged();
        Log.i("ADDITEM", mValues.toString());
        Log.i("FORMULELIST", getListFormule().toString());
//        refreshList();
    }

    public void removeAllFormule() {
        mValues.clear();
        notifyDataSetChanged();
    }

    public void removeAt(int position) {
       mValues.remove(position);
       // listViewHolder.remove(position);
        //notifyDataSetChanged();
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mValues.size());
    }
 public List<Formule> getListFormule(){
     return mValues;
 }

 public void refreshList(){
     for (Formule d: mValues
          ) {
        int posFormule =mValues.indexOf(d);


        d.setPrix(Integer.valueOf(listViewHolder.get(posFormule).mTarifE.getText().toString()));
        d.setDescription(listViewHolder.get(posFormule).mDescriptionTarifE.getText().toString());
         Log.i("REFRESH", "refreshList:"+ d.toString());
     }
 }
    public class ViewHolder extends RecyclerView.ViewHolder {
        public  View mView;
        public  EditText mTarifE;
        public  EditText mDescriptionTarifE;
        public Formule mItem;
        public ImageButton mRevoveButton;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mTarifE = (EditText) view.findViewById(R.id.tarifE);
            mDescriptionTarifE = (EditText) view.findViewById(R.id.descriptiontarifE);
            mRevoveButton = (ImageButton) view.findViewById(R.id.btnRemoveFormule);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTarifE.getText() +" "+ mDescriptionTarifE.getText() + "'";
        }
    }

}
