package com.eeventoo.app.InterfaceRetrofit;

import com.eeventoo.app.class_.Constants;
import com.eeventoo.app.class_.Example;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by isma on 20/05/2017.
 */

public interface EventOfUser {

    @FormUrlEncoded
    @POST(Constants.EVENTOFUSER)
    Call<Example> getEventUser(
            @Field("idUser") String idUser
    );

}
