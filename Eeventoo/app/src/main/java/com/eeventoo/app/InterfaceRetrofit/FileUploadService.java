package com.eeventoo.app.InterfaceRetrofit;

import com.eeventoo.app.class_.ReponseServeur;
import com.eeventoo.app.class_.ResponseServeurAddEvent;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by isma on 24/05/2017.
 */

public interface FileUploadService {
    @Multipart
    @POST("upload.php/")
    Call<ResponseServeurAddEvent> upload(
            @Part MultipartBody.Part file,
            @Part("idUser") RequestBody idUser,
            @Part("nom") RequestBody nom,
            @Part("type") RequestBody type,
            @Part("adresse") RequestBody adresse,
            @Part("adresseComplet") RequestBody adresseComplet,
            @Part("codepostal") RequestBody codepostal,
            @Part("ville") RequestBody ville,
            @Part("departement") RequestBody departement,
            @Part("region") RequestBody region,
            @Part("paysNom") RequestBody paysNom,
            @Part("paysCode") RequestBody paysCode,
            @Part("latitude") RequestBody latitude,
            @Part("lontitude") RequestBody lontitude,
            @Part("datedeb") RequestBody datedeb,
            @Part("datefin") RequestBody datefin,
            @Part("telephone") RequestBody telephone,
            @Part("typeTarif") RequestBody typeTarif,
            @Part("description") RequestBody description,
            @Part("intTypeEvent") RequestBody intType,
            @Part("intTousLes") RequestBody intTousLes,
            @Part("nbjoursRestant") RequestBody nbjoursRestant,
            @Part("jourLundi") RequestBody jourLundi,
            @Part("jourMardi") RequestBody jourMardi,
            @Part("jourMercredi") RequestBody jourMercredi,
            @Part("jourJeudi") RequestBody jourJeudi,
            @Part("jourVendredi") RequestBody jourVendredi,
            @Part("jourSamedi") RequestBody jourSamedi,
            @Part("jourDimanche") RequestBody jourDimanche,
            @Part("nbjourDiffDebFin") RequestBody nbjourDiffDebFin,
            @Part("intTypeEventMensuel") RequestBody intTypeEventMensuel,
            @Part("datejusqua") RequestBody dateJusqua,
            @Part("niemeJour") RequestBody niemeJour,
            @Part("jourSemaine") RequestBody jourSemaine



            );

    @Multipart
    @POST("updateEvent.php/")
    Call<ReponseServeur> updateEvent(
            @Part MultipartBody.Part file,
            @Part("idEvent") RequestBody idEvent,
            @Part("idUser") RequestBody idUser,
            @Part("nom") RequestBody nom,
            @Part("type") RequestBody type,
            @Part("adresse") RequestBody adresse,
            @Part("adresseComplet") RequestBody adresseComplet,
            @Part("codepostal") RequestBody codepostal,
            @Part("ville") RequestBody ville,
            @Part("departement") RequestBody departement,
            @Part("region") RequestBody region,
            @Part("paysNom") RequestBody paysNom,
            @Part("paysCode") RequestBody paysCode,
            @Part("latitude") RequestBody latitude,
            @Part("lontitude") RequestBody lontitude,
            @Part("datedeb") RequestBody datedeb,
            @Part("datefin") RequestBody datefin,
            @Part("telephone") RequestBody telephone,
            @Part("typeTarif") RequestBody typeTarif,
            @Part("description") RequestBody description

    );

    @Multipart
    @POST("updateEventSansImage.php/")
    Call<ReponseServeur> updateEventSansImage(
            @Part("idEvent") RequestBody idEvent,
            @Part("idUser") RequestBody idUser,
            @Part("nom") RequestBody nom,
            @Part("type") RequestBody type,
            @Part("adresse") RequestBody adresse,
            @Part("adresseComplet") RequestBody adresseComplet,
            @Part("codepostal") RequestBody codepostal,
            @Part("ville") RequestBody ville,
            @Part("departement") RequestBody departement,
            @Part("region") RequestBody region,
            @Part("paysNom") RequestBody paysNom,
            @Part("paysCode") RequestBody paysCode,
            @Part("latitude") RequestBody latitude,
            @Part("lontitude") RequestBody lontitude,
            @Part("datedeb") RequestBody datedeb,
            @Part("datefin") RequestBody datefin,
            @Part("telephone") RequestBody telephone,
            @Part("typeTarif") RequestBody typeTarif,
            @Part("description") RequestBody description

    );

    @Multipart
    @POST("addEventWithoutImage.php/")
    Call<ReponseServeur> addEventWithoutImage(
            @Part("idUser") RequestBody idUser,
            @Part("nom") RequestBody nom,
            @Part("type") RequestBody type,
            @Part("adresse") RequestBody adresse,
            @Part("adresseComplet") RequestBody adresseComplet,
            @Part("codepostal") RequestBody codepostal,
            @Part("ville") RequestBody ville,
            @Part("departement") RequestBody departement,
            @Part("region") RequestBody region,
            @Part("paysNom") RequestBody paysNom,
            @Part("paysCode") RequestBody paysCode,
            @Part("latitude") RequestBody latitude,
            @Part("lontitude") RequestBody lontitude,
            @Part("datedeb") RequestBody datedeb,
            @Part("datefin") RequestBody datefin,
            @Part("telephone") RequestBody telephone,
            @Part("typeTarif") RequestBody typeTarif,
            @Part("description") RequestBody description,
            @Part("intTypeEvent") RequestBody intType,
            @Part("intTousLes") RequestBody intTousLes,
            @Part("nbjoursRestant") RequestBody nbjoursRestant,
            @Part("jourLundi") RequestBody jourLundi,
            @Part("jourMardi") RequestBody jourMardi,
            @Part("jourMercredi") RequestBody jourMercredi,
            @Part("jourJeudi") RequestBody jourJeudi,
            @Part("jourVendredi") RequestBody jourVendredi,
            @Part("jourSamedi") RequestBody jourSamedi,
            @Part("jourDimanche") RequestBody jourDimanche,
            @Part("nbjourDiffDebFin") RequestBody nbjourDiffDebFin,
            @Part("intTypeEventMensuel") RequestBody intTypeEventMensuel,
            @Part("datejusqua") RequestBody dateJusqua,
            @Part("niemeJour") RequestBody niemeJour,
            @Part("jourSemaine") RequestBody jourSemaine

    );

}