package com.eeventoo.app.InterfaceRetrofit;

import com.eeventoo.app.class_.Constants;
import com.eeventoo.app.class_.Example;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by isma on 28/05/2017.
 */

public interface IDeleteEventOfUser {
    @FormUrlEncoded
    @POST(Constants.DELETE_EVENTOFUSER)
    Call<Example> deleteEventUser(
            @Field("idUser") String idUser,
            @Field("idEvent") String idEvent
    );
}
