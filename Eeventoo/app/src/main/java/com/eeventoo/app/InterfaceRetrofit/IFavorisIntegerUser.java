package com.eeventoo.app.InterfaceRetrofit;

import com.eeventoo.app.class_.Constants;
import com.eeventoo.app.class_.ResponseListFavoris;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by isma on 31/05/2017.
 */

public interface IFavorisIntegerUser {

    @FormUrlEncoded
    @POST(Constants.INTEGER_FAVORIS_OF_USER)
    Call<ResponseListFavoris> getIntegerFavoris(
            @Field("idUser") String idUser

    );
}
