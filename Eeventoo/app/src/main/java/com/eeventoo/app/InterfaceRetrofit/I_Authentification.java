package com.eeventoo.app.InterfaceRetrofit;

import com.eeventoo.app.class_.Constants;
import com.eeventoo.app.class_.ReponseLogin;
import com.eeventoo.app.class_.ReponseRegister;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by isma on 12/06/2017.
 */

public interface I_Authentification {

    @FormUrlEncoded
    @POST(Constants.LOGIN_URL)
    Call<ReponseLogin> checkLogin(

            @Field("email") String email,
            @Field("motdepasse") String mp
    );

    @FormUrlEncoded
    @POST(Constants.REGISTER_URL)
    Call<ReponseRegister> registration(


            @Field("email") String email,
            @Field("displayName") String displayName


    );

    @FormUrlEncoded
    @POST(Constants.REGISTER_USER_GOOGLE_IN_MYSQL_URL)
    Call<ReponseLogin> registrationUserGoogleInMysql(

            @Field("idUser") String id,
            @Field("nom")  String  nom,
            @Field("email") String email,
            @Field("typeInscription") String typeInscription
    );


    @FormUrlEncoded
    @POST(Constants.INIT_USER_FIREBASE_URL)
    Call<ReponseRegister> initUserFirebase(


            @Field("email") String email

    );

    @FormUrlEncoded
    @POST(Constants.INIT_USER_FIREBASE_GOOGLE_FACEBOOK_URL)
    Call<ReponseRegister> initUserFirebaseGoogleFacebook(


            @Field("email") String email,
            @Field("displayName") String displayName,
            @Field("photoUrl") String photoUrl

    );
}
