package com.eeventoo.app.InterfaceRetrofit;

import com.eeventoo.app.class_.Constants;
import com.eeventoo.app.class_.ReponseServeur;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by isma on 10/11/2017.
 */

public interface I_Avis {

    @FormUrlEncoded
    @POST(Constants.ADDAVIS)
    Call<ReponseServeur> addLike(
            @Field("idUser") String idUser,
            @Field("idEvent") String idEvent,
            @Field("avis") String avis,
            @Field("date_avis") String date_avis,
            @Field("nbetoiles") String nbetoiles
    );
}
