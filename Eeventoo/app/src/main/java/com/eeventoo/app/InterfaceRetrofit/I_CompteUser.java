package com.eeventoo.app.InterfaceRetrofit;

import com.eeventoo.app.class_.Constants;
import com.eeventoo.app.class_.ReponseServeur;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by isma on 05/06/2017.
 */

public interface I_CompteUser {

    @Multipart
    @POST(Constants.UPDATE_PP)
    Call<ReponseServeur> idUserupdatePP(
            @Part MultipartBody.Part file,
            @Part("emailUser") RequestBody emailUser
    );

    @FormUrlEncoded
    @POST(Constants.UPDATE_EMAIL)
    Call<ReponseServeur> updateEmail(
            @Field("idUser") String idUser,
            @Field("email") String email
    );

    @FormUrlEncoded
    @POST(Constants.UPDATE_DISPLAY_NAME)
    Call<ReponseServeur> updateDisplayName(
            @Field("idUser") String idUser,
            @Field("displayName") String displayName
    );

    @FormUrlEncoded
    @POST(Constants.UPDATE_TEL)
    Call<ReponseServeur> updateTel(
            @Field("emailUser") String emailUser,
            @Field("telephone") String telephone
    );

    @FormUrlEncoded
    @POST(Constants.UPDATE_MODIF)
    Call<ReponseServeur> updateModif(
            @Field("idUser") String idUser,
            @Field("prenom") String prenom,
            @Field("nom")    String nom,
            @Field("adresse") String adresse,
            @Field("adresseComplet") String adresseComplet,
            @Field("codepostal") String codepostal,
            @Field("ville") String ville,
            @Field("departement") String departement,
            @Field("region") String region,
            @Field("paysNom") String paysNom,
            @Field("paysCode") String paysCode,
            @Field("latitude") String latitude,
            @Field("lontitude") String lontitude
    );
}
