package com.eeventoo.app.InterfaceRetrofit;

import com.eeventoo.app.class_.Constants;
import com.eeventoo.app.class_.ReponseServeur;
import com.eeventoo.app.class_.ResponseListFavoris;
import com.eeventoo.app.class_.ResponseListIdEvent;
import com.eeventoo.app.class_.ResponselistFormule;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by isma on 17/10/2018.
 */

public interface I_Event {

    @FormUrlEncoded
    @POST(Constants.GET_ID_LIST_EVENT)
    Call<ResponseListIdEvent> getIdEvent(
            @Field("idUser") String idUser,
            @Field("idGroupEvent") String idGroupEvent

    );

    @FormUrlEncoded
    @POST(Constants.ADD_FORMULE_EVENT)
    Call<ReponseServeur> setFormuleEvent(
            @Field("requete") String requete

    );


    @FormUrlEncoded
    @POST(Constants.GET_FORMULES_EVENT)
    Call<ResponselistFormule> getFomulesEvent(
            @Field("idEvent") String idEvent

    );

}
