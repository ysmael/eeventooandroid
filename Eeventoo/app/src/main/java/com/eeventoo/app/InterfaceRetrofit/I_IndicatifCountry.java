package com.eeventoo.app.InterfaceRetrofit;

import com.eeventoo.app.class_.Constants;
import com.eeventoo.app.class_.Example;
import com.eeventoo.app.class_.ReponseServeur;
import com.eeventoo.app.class_.ResponseIndicatif;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by isma on 26/02/2018.
 */

public interface I_IndicatifCountry {


    @POST(Constants.GET_INDICATIF)
    Call<ResponseIndicatif> getIndicatif(

    );
}
