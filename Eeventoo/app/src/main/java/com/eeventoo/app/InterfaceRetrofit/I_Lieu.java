package com.eeventoo.app.InterfaceRetrofit;

import com.eeventoo.app.class_.Constants;
import com.eeventoo.app.class_.ReponseAllDepartement;

import retrofit2.Call;
import retrofit2.http.POST;

/**
 * Created by isma on 14/06/2017.
 */

public interface I_Lieu {

    @POST(Constants.GET_ALL_DEP_URL)
    Call<ReponseAllDepartement> getAllDepartement(
    );
}
