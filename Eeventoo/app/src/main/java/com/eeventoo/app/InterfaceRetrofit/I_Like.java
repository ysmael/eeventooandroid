package com.eeventoo.app.InterfaceRetrofit;

import com.eeventoo.app.class_.Constants;
import com.eeventoo.app.class_.ReponseServeur;
import com.eeventoo.app.class_.ResponseListFavoris;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by isma on 03/06/2017.
 */

public interface I_Like {

    @FormUrlEncoded
    @POST(Constants.ADDLIKE)
    Call<ReponseServeur> addLike(
            @Field("idUser") String idUser,
            @Field("idEvent") String idEvent
    );

    @FormUrlEncoded
    @POST(Constants.SUPLIKE)
    Call<ReponseServeur> supLike(
            @Field("idUser") String idUser,
            @Field("idEvent") String idEvent
    );

    @FormUrlEncoded
    @POST(Constants.INTEGER_LIKES_OF_USER)
    Call<ResponseListFavoris> getIntegerLikes(
            @Field("idUser") String idUser

    );

}
