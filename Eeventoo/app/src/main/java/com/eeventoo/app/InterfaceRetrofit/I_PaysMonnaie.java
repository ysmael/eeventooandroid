package com.eeventoo.app.InterfaceRetrofit;

import com.eeventoo.app.class_.Constants;
import com.eeventoo.app.class_.ResponseIndicatif;
import com.eeventoo.app.class_.ResponsePaysMonnaie;

import retrofit2.Call;
import retrofit2.http.POST;

/**
 * Created by isma on 28/02/2018.
 */

public interface I_PaysMonnaie {



    @POST(Constants.GET_PAYS_MONNAIE)
    Call<ResponsePaysMonnaie> getMonnaie(

    );
}
