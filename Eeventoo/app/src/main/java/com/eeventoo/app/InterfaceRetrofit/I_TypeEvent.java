package com.eeventoo.app.InterfaceRetrofit;

import com.eeventoo.app.class_.Constants;
import com.eeventoo.app.class_.ResponseTypeEvent;

import retrofit2.Call;
import retrofit2.http.POST;

/**
 * Created by isma on 02/03/2018.
 */

public interface I_TypeEvent {


    @POST(Constants.GET_TYPE_EVENT)
    Call<ResponseTypeEvent> getTypeEvent(

    );
}
