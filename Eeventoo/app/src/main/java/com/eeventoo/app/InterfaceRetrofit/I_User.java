package com.eeventoo.app.InterfaceRetrofit;

import com.eeventoo.app.class_.Constants;
import com.eeventoo.app.class_.ReponseUser;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by isma on 06/06/2017.
 */

public interface I_User {
    @FormUrlEncoded
    @POST(Constants.INFOS_USER)
    Call<ReponseUser> getInfosUser(
            @Field("idUser") String idUser
    );
}
