package com.eeventoo.app.InterfaceRetrofit;

import com.eeventoo.app.class_.ReponseServeur;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by isma on 30/05/2017.
 */

public interface IprofilImageUser {

    @Multipart
    @POST("upload.php/")
    Call<ReponseServeur> upload(
            @Part MultipartBody.Part file,
            @Part("idUser") RequestBody idUser);
}
