package com.eeventoo.app;

import android.graphics.drawable.Drawable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by isma on 22/06/2016.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {
ArrayList<Fragment> fragments = new ArrayList<>();
    ArrayList<String> tabtitle=new ArrayList<>();
    ArrayList<Drawable> tabicon = new ArrayList<>();
    public void addFragment(Fragment fragments, String title){
        this.fragments.add(fragments);
        this.tabtitle.add(title);

    }
    public void addIcon(Fragment fragments, Drawable drawable){
        this.fragments.add(fragments);
        this.tabicon.add(drawable);

    }
    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabtitle.get(position) ;
    }

    public ArrayList<Fragment> getFragments() {
        return fragments;
    }

    public void setFragments(ArrayList<Fragment> fragments) {
        this.fragments = fragments;
    }

    public ArrayList<String> getTabtitle() {
        return tabtitle;
    }

    public void setTabtitle(ArrayList<String> tabtitle) {
        this.tabtitle = tabtitle;
    }
}
