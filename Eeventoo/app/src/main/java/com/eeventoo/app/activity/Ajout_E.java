package com.eeventoo.app.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import androidx.annotation.RequiresApi;

import com.eeventoo.app.class_.ReponseServeur;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.request.RequestOptions;
import com.eeventoo.app.BuildConfig;
import com.eeventoo.app.InterfaceRetrofit.FileUploadService;
import com.eeventoo.app.InterfaceRetrofit.I_IndicatifCountry;

import com.eeventoo.app.class_.FileUtils;
import com.eeventoo.app.R;
import com.eeventoo.app.class_.Formule;
import com.eeventoo.app.class_.PaysIndicatif;
import com.eeventoo.app.class_.ResponseIndicatif;
import com.eeventoo.app.class_.ResponseServeurAddEvent;
import com.eeventoo.app.class_.ServiceGenerator;
import com.eeventoo.app.class_.UserSessionManager;
import com.bumptech.glide.Glide;
import com.eeventoo.app.fragment.FormuleTarifFragment;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class Ajout_E extends AppCompatActivity implements  FormuleTarifFragment.OnListFragmentInteractionListener {
    private static final int TAKE_PICTURE_CAMERA_REQUEST = 2;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    private ImageView imageEvent;
    private static final int EVENEMENT_UNIQUE= 1;
    private static final int EVENEMENT_QUOTIDIEN= 2;
    private static final int EVENEMENT_HEBDOMADAIRE= 3;
    private static final int EVENEMENT_MENSUEL= 4;
    private static final int EVENEMENT_ANNUEL= 5;
    private static final int EVENEMENT_MENSUEL_TOUS_LES_X_DU_MOIS=1;
    private static final int EVENEMENT_MENSUEL_TOUS_LES_Xeme_JOURS_DU_MOIS=2;
    private static final int EVENEMENT_MENSUEL_DERNIER_JOURS_DU_MOIS=3;
    private static final int EVENEMENT_MENSUEL_DERNIER_JOURS_X_DU_MOIS =4;

    //variable pour le boutton date debut et heure debut
    int year_x, month_x, day_x, heure_x, min_x;
    //variable pour le boutton date fin et heure fin
    int year_df, month_df, day_df, heure_df, min_df;
    static final int DIALOG_ID = 0;
    static final int DIALOG_ID_DATE_FIN = 1;
    static final int DIALOG_ID_HEURE_DEB = 2;
    static final int DIALOG_ID_HEURE_FIN = 3;
    static final int DIALOG_DATE_JUSQUA =4;
    private String cp ,ville,departement,region;
    private String paysNom;
    private String adresseNom;
    private String adresseComplet;
    private String paysCode;
    private double latitude;
    private double lontitude;
    private String textAdress;
    private String tarif_type_inserer;
    private String  type_event_inserer;
    public String jourLundi="0";
    String jourMardi="0";
    String jourMercredi ="0";
    String jourJeudi="0";
    String jourVendredi="0";
    String jourSamedi="0";String jourDimanche = "0";
    int niemeJour ;
    int jourSemaine;
    CheckedTextView ctLundi, ctMardi,ctMercredi,ctJeudi,ctVendredi,ctSamedi, ctDimanche;
    boolean dateDebSet=false;
    boolean timeDebSet=false;
    boolean dateFinSet=false;
    boolean timeFinSet=false;
    boolean checkDatebool =false;

    private boolean bool_payant;
    private ArrayList <PaysIndicatif> listeIndicatif = new ArrayList() ;
    private   ArrayAdapter<PaysIndicatif> indicatifMonnaieArrayAdapter;
    LayoutInflater inflater;
    FormuleTarifFragment formuleTarifFragment;

    //Editext
    EditText nom_E, tel_E,lieu_E, description_E;
    Spinner type_E;
    EditText  buttonDateDeb, buttonHeureDeb, buttonDateFin, buttonHeureFin;
    Button buttonSave , buttonAddPictureE;
    AlertDialog.Builder builder;
    String dateDebString, dateJusquaString,dateFinString, heureDebString, heureFinString;
    CoordinatorLayout coordinatorLayout;
    UserSessionManager session;
    // String imageData= "";
    Calendar calendarChoisieDateFin = Calendar.getInstance();
    Calendar calendarChoisieDateDebut = Calendar.getInstance();
    Calendar calendarChoisieDatejusqua = Calendar.getInstance();
    private Geocoder mGeocoder;
    private int PICK_IMAGE_REQUEST = 1;
    private Spinner tel_indicatif;

    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 30;
    private Uri filePath;
    private int intTypeEvent,intTousLes,nbjoursRestant,intTypeEventMensuel;
    CollapsingToolbarLayout collapsingToolbarLayout;
    private static final int CONTENT_VIEW_ID = 10101010;

    @BindView(R.id.radioGroupE)
    RadioGroup radioGroup;

    private RadioButton radioButton;
    ProgressDialog progressDialog;
    Calendar calendar = Calendar.getInstance(Locale.FRANCE);
    private CheckBox checkBoxAddTel;

    AlertDialog dialog;
    Button btnAddTarif;
    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private Button btnRepeterEvent;
    private Button btnDateJusqua;
    private String idGroupEvent;

    RadioButton rbtousLesJoursX ;
    RadioButton rbtousXjoursdumois;
    RadioButton rbtousDernierXjoursdumois;
    RadioButton rbtousDernierjourdumois;



    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ajout_event);
        //Calendar calendar = Calendar.getInstance(Locale.FRANCE);

        //tester la connection reseau

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if(networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()) {
        }
        else{
            Toast toast = Toast.makeText(this, getString(R.string.aucuneConnexion), Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }



        intTypeEvent=EVENEMENT_UNIQUE;
        year_x = calendar.get(Calendar.YEAR);
        month_x = calendar.get(Calendar.MONTH);
        day_x = calendar.get(Calendar.DAY_OF_MONTH);
        heure_x = calendar.get(Calendar.HOUR_OF_DAY);
        min_x = calendar.get(Calendar.MINUTE);
        year_df = calendar.get(Calendar.YEAR);
        month_df = calendar.get(Calendar.MONTH);
        day_df = calendar.get(Calendar.DAY_OF_MONTH);
        heure_df = calendar.get(Calendar.HOUR_OF_DAY);
        min_df = calendar.get(Calendar.MINUTE);
        nom_E = (EditText) findViewById(R.id.nomE);
        type_E = (Spinner) findViewById(R.id.typeE);
        buttonDateDeb = (EditText) findViewById(R.id.Buttondatedeb);
        buttonHeureDeb = (EditText) findViewById(R.id.Buttonheuredeb);
        buttonDateFin = (EditText) findViewById(R.id.ButtondatefinE);
        buttonHeureFin = (EditText) findViewById(R.id.ButtonheurefinE);
        session = new UserSessionManager(getApplicationContext());
        tel_E = (EditText) findViewById(R.id.telephoneE);
        description_E = (EditText) findViewById(R.id.descriptionE);
        buttonSave = (Button) findViewById(R.id.buttonSaveE);
        buttonAddPictureE = findViewById(R.id.buttonAddPictureE);
        imageEvent = (ImageView) findViewById(R.id.imageEvenement);
        lieu_E= (EditText) findViewById(R.id.lieuE);
        ButterKnife.bind(this);
        tel_indicatif = (Spinner) findViewById(R.id.telephoneIndicatif);
        checkBoxAddTel = findViewById(R.id.checkboxAddTel);
        btnRepeterEvent=findViewById(R.id.btnRepeterEvent);
        btnAddTarif= findViewById(R.id.btnAddTarif);
        mGeocoder = new Geocoder(this, Locale.getDefault());


        if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (shouldShowRequestPermissionRationale(
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                // Explain to the user why we need to read the contacts
            }

            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

            // MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE is an
            // app-defined int constant that should be quite unique

            return;
        }

        inflater=Ajout_E.this.getLayoutInflater();
        ArrayAdapter<CharSequence> typeAdapter = ArrayAdapter.createFromResource(this, R.array.type_eventStringAddEvent, android.R.layout.simple_spinner_item);
        typeAdapter.setDropDownViewResource(R.layout.spinner_ajout_e);
        type_E.setAdapter(typeAdapter);

        type_E.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                //Pour cacher le clavier
                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                return false;
            }
        });
        type_E.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                typeEventAInserer(i);
                ((TextView) adapterView.getChildAt(0)).setTextColor(Color.WHITE);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btnAddTarif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Ajout_E.this,FormuleActivity.class);
                startActivity(intent);
            }
        });


//btnRepeter onClick
        btnRepeterEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(dateDebSet && dateFinSet && timeDebSet && timeFinSet){
                    if (!calendarChoisieDateDebut.before(calendar)) {
                        if (calendarChoisieDateDebut.before(calendarChoisieDateFin)) {
                            repeterEventAction();
                        }

                        else {
                            Toast.makeText(Ajout_E.this, getString(R.string.checkDebFin), Toast.LENGTH_LONG).show();

                        }
                    } else {
                        Toast.makeText(Ajout_E.this, getString(R.string.checkDebCourante), Toast.LENGTH_LONG).show();

                    }
                }

                else{
                    Toast.makeText(Ajout_E.this,getString(R.string.checkInputDateEvent) , Toast.LENGTH_LONG).show();
                }


            }
        });
        //ajouter numero de tel
        checkBoxAddTel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkAddTel();
            }
        });
        //indicatif adapter
        recupererIndicatif();
        tel_indicatif.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                //Pour cacher le clavier
                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                return false;
            }
        });

        tel_indicatif.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) adapterView.getChildAt(0)).setTextColor(Color.WHITE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorAddE);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_AddE);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar_AddE);

        buttonAddPictureE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSelectImage();

            }
        });
        showDialogOnButtonDateDebClick();
        showDialogOnButtonDateFinClick();
        showDialogOnButtonheureDebClick();
        showDialogOnButtonheureFinClick();


        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(getString(R.string.patientez));
        progressDialog.setMessage(getString(R.string.enregistrementEvenement));

        lieu_E.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lancerAutocomplete();
            }
        });

        description_E.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() ==R.id.descriptionE) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction()&MotionEvent.ACTION_MASK){
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }

        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                String titre = getString(R.string.quelqueChoseMalPasse);
                String str_tout_champ = getString(R.string.remplissezToutChamp);
                String str_datef_inf_dateD = getString(R.string.checkDebFin);
                String str_dateD_inf_dateC = getString(R.string.checkDebCourante);
                String str_tel = getString(R.string.numeroTelError);
                String str_imageEvenement = getString(R.string.ajouterImagesvp);
                if (nom_E.getText().toString().equals("") ||
                        textAdress.equals("") ||
                        description_E.getText().toString().equals("") ||
                        radioGroup.getCheckedRadioButtonId() == -1) {
                    showDialog(titre, str_tout_champ);
                } else {

                    if (!calendarChoisieDateDebut.before(calendar)) {
                        if (calendarChoisieDateDebut.before(calendarChoisieDateFin)) {
                            if (filePath == null) {
                                showDialogImage();
                            } else {
                                uploadFile(filePath,
                                        session.getIdUser() + "",
                                        replaceApostrophe(nom_E.getText().toString()),
                                        replaceApostrophe(type_event_inserer),
                                        replaceApostrophe(adresseNom),
                                        replaceApostrophe(adresseComplet),
                                        replaceApostrophe(cp),
                                        replaceApostrophe(ville),
                                        replaceApostrophe(departement),
                                        replaceApostrophe(region),
                                        replaceApostrophe(paysNom),
                                        replaceApostrophe(paysCode),
                                        replaceApostrophe(String.valueOf(latitude)),
                                        replaceApostrophe(String.valueOf(lontitude)),
                                        replaceApostrophe(dateDebString + " " + heureDebString),
                                        replaceApostrophe(dateFinString + " " + heureFinString),
                                        checkTel(recupererIndicatifSpinner(tel_indicatif.getSelectedItem().toString()), tel_E.getText().toString()),
                                        replaceApostrophe(tarif_type_inserer),
                                        replaceApostrophe(description_E.getText().toString()),
                                        String.valueOf(intTypeEvent),
                                        String.valueOf(intTousLes),
                                        String.valueOf(getDiffDates()),
                                        jourLundi,
                                        jourMardi,
                                        jourMercredi,
                                        jourJeudi,
                                        jourVendredi,
                                        jourSamedi,
                                        jourDimanche,
                                        String.valueOf(getDiffDatesDebFin()),
                                        String.valueOf(intTypeEventMensuel),
                                        outPutDateSql(calendarChoisieDatejusqua),
                                        String.valueOf(niemeJour),
                                        String.valueOf(jourSemaine)
                                );

                            }
                        } else {
                            showDialog(titre, str_datef_inf_dateD + outPutDate(calendarChoisieDateDebut)+" : "+outPutDate(calendarChoisieDateFin) );
                        }
                    } else {

                        showDialog(titre, str_dateD_inf_dateC);


                    }




                }
            }
        });


    }

    private String recupererIndicatifSpinner(String s){
        return s.substring(s.lastIndexOf("("), s.lastIndexOf(")")+1);
    }

    private String recupererSymboleMonnaieSpinner(String s){

        String res ="";
        if(bool_payant) {
            res= s.substring(s.lastIndexOf("("));
        }
        return  res;
    }

    private void getInformationByCoordinates(double lat, double lon) throws IOException {

        List<android.location.Address> addresses = mGeocoder.getFromLocation(lat, lon, 1);
        if (addresses != null && addresses.size() > 0) {

            cp = checkNullString(addresses.get(0).getPostalCode());
            ville = checkNullString(addresses.get(0).getLocality());
            departement = checkNullString(addresses.get(0).getSubAdminArea());
            region = checkNullString(addresses.get(0).getAdminArea());
            paysNom = checkNullString(addresses.get(0).getCountryName());
            paysCode = checkNullString(addresses.get(0).getCountryCode());
            latitude = lat;
            lontitude = lon;
            adresseComplet = adresseNom + "," + cp + "," + ville + "," + departement + "," + region + "," + paysNom + "," + paysCode + "," + latitude + "," + lontitude;
            if (BuildConfig.DEBUG) {
                Log.i("ADRESSE", "getInformationByCoordinates: " + adresseComplet);
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        //tester la connection reseau

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if(networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()) {
        }
        else{
            Toast toast = Toast.makeText(this, getString(R.string.aucuneConnexion), Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }

    private String checkNullString(String str) {
        if (str != null) {
            return str;
        } else {
            return "";
        }
    }

    private boolean checkNullStringBoolean(String str) {
        if (str != null) {
            return false;
        } else {
            return true;
        }
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        /*Intent intent = new Intent(Intent.ACTION_PICK,
        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);*/
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    public void takePictureCamera() {

        Intent pickPhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(pickPhoto, TAKE_PICTURE_CAMERA_REQUEST);

    }

    private void dialogSelectImage() {
        final CharSequence[] items = {getString(R.string.prendrePhoto),getString(R.string.selectPhoto) };
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);

        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals(getString(R.string.prendrePhoto))) {
                    takePictureCamera();
                } else if (items[item].equals(getString(R.string.selectPhoto))) {

                    showFileChooser();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            filePath = data.getData();
            Glide.with(this).load(filePath).apply(new RequestOptions()).transition(withCrossFade()).into(imageEvent);
        }

        if (requestCode == TAKE_PICTURE_CAMERA_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            filePath = data.getData();
            Glide.with(this).load(filePath).apply(new RequestOptions()).transition(withCrossFade()).into(imageEvent);
        }
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE && resultCode == RESULT_OK && data != null ) {


                Place place = PlaceAutocomplete.getPlace(this, data);

                if(place.getAddress() != null) {
                    adresseNom = place.getAddress().toString();


                    try {

                        getInformationByCoordinates(place.getLatLng().latitude, place.getLatLng().longitude);
                        place.getId();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    textAdress = place.getName() + "";
                    lieu_E.setText(place.getName());
                }
                else{
                    Toast.makeText(this, getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                }

        }

    }






    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void uploadFile(Uri fileUri,
                            String idUser,
                            String nom,
                            String type,
                            String adresse,
                            String adresseComplet,
                            String codepostal,
                            String ville,
                            String departement,
                            String region,
                            String paysNom,
                            String paysCode,
                            String latitude,
                            String lontitude,
                            String datedeb,
                            String datefin,
                            String telephone,
                            String typeTarif,
                            String description,
                            String intTypeEvent,
                            String intTousLes,
                            String nbjoursRestant,
                            String jourLundi,
                            String jourMardi,
                            String jourMercredi,
                            String jourJeudi,
                            String jourVendredi,
                            String jourSamedi,
                            String jourDimanche,
                            String nbjourDiffDebFin,
                            String intTypeEventMensuel,
                            String dateJusqua, String niemeJour, String jourSemaine) {

        String str = "idUser:"+idUser+" nom:"+nom+" adresse:"+adresse+" adresseComplet:"+ adresseComplet+" codepostal:"+codepostal+" ville:"+ville+" departement:"+departement+" region:"+region+
                "paysNom:"+paysNom+" paysCode:"+ paysCode+" latitude :"+latitude+" lontitue:"+ lontitude+" datedeb:"+ datedeb +" datefin:"+datefin + " telephone"+ telephone;
        if(BuildConfig.DEBUG){
            Log.i("UPLOAD", "je suis dans  upload");
            Log.i("STRINGUPLOAD", str);
        }

        progressDialog.show();
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        // create upload service client
        FileUploadService service =
                ServiceGenerator.createService(FileUploadService.class);

        // https://github.com/iPaulPro/aFileChooser/blob/master/aFileChooser/src/com/ipaulpro/afilechooser/utils/FileUtils.java
        // use the FileUtils to get the actual file by uri
        File file = FileUtils.getFile(this, fileUri);

        // create RequestBody instance from file
        final RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse(getContentResolver().getType(fileUri)),
                        file
                );
        if(BuildConfig.DEBUG){
            Log.i("UPLOAD", "2 je suis dans  upload");
        }
        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file", file.getName(), requestFile);

        RequestBody r_idUser =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, idUser);
        RequestBody r_nom =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, nom);

        RequestBody r_type =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, type);
        RequestBody r_adresse =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, adresse);

        RequestBody r_adresseComplet =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, adresseComplet);

        RequestBody r_codepostal =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, codepostal);

        RequestBody r_departement =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, departement);
        RequestBody r_region =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, region);
        RequestBody r_paysNom =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, paysNom);
        RequestBody r_paysCode =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, paysCode);
        RequestBody r_latitude =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, latitude);
        RequestBody r_lontitude =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, lontitude);

        RequestBody r_ville =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, ville);
        RequestBody r_datedeb =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, datedeb);
        RequestBody r_datefin =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, datefin);
        RequestBody r_telephone =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, telephone);
        RequestBody r_typeTarif =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, typeTarif);


        RequestBody r_description =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, description);

        RequestBody r_IntType =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, intTypeEvent);

        RequestBody r_intTousLes =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, intTousLes);

        RequestBody r_nbjoursRestant =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, nbjoursRestant);

        RequestBody r_jourLundi =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, jourLundi);
        RequestBody r_jourMardi =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, jourMardi);

        RequestBody r_jourMercredi =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, jourMercredi);

        RequestBody r_jourJeudi =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, jourJeudi);

        RequestBody r_jourVendredi =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, jourVendredi);

        RequestBody r_jourSamedi =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, jourSamedi);

        RequestBody r_jourDimanche =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, jourDimanche);
        RequestBody r_nbDiffDebFin =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, nbjourDiffDebFin);
        RequestBody r_intTypeEventMensuel =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, intTypeEventMensuel);
        RequestBody r_datejusqua =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, dateJusqua);

        RequestBody r_niemeJour =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, niemeJour);

        RequestBody r_jourSemaine =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, jourSemaine);

        verifyStoragePermissions(Ajout_E.this);
        if(BuildConfig.DEBUG){
            Log.i("UPLOAD", "3 je suis dans  upload");
        }
        // finally, execute the request
        Call<ResponseServeurAddEvent> call = service.upload(body,
                r_idUser,
                r_nom,
                r_type,
                r_adresse,
                r_adresseComplet,
                r_codepostal,
                r_ville,
                r_departement,
                r_region,
                r_paysNom,
                r_paysCode,
                r_latitude,
                r_lontitude,
                r_datedeb,
                r_datefin,
                r_telephone,
                r_typeTarif,
                r_description,
                r_IntType,
                r_intTousLes,
                r_nbjoursRestant,
                r_jourLundi,
                r_jourMardi,
                r_jourMercredi,
                r_jourJeudi,
                r_jourVendredi,
                r_jourSamedi,
                r_jourDimanche,
                r_nbDiffDebFin,
                r_intTypeEventMensuel,
                r_datejusqua,
                r_niemeJour,
                r_jourSemaine
        );
        call.enqueue(new Callback<ResponseServeurAddEvent>() {
            @Override
            public void onResponse(Call<ResponseServeurAddEvent> call, retrofit2.Response<ResponseServeurAddEvent> response) {

                if(response.body() != null){

                    if(response.body().isSuccess()) {

                        if(BuildConfig.DEBUG){
                            Log.i("UPLOAD", "4 je suis dans  upload");
                        }
                        if (BuildConfig.DEBUG) {
                            Toast.makeText(Ajout_E.this, response.body().getMessage().toString(), Toast.LENGTH_SHORT).show();


                        }

                        if(typeTarif.equals("Payant")){
                            Toast.makeText(Ajout_E.this, getString(R.string.finaliserAjout), Toast.LENGTH_LONG).show();

                        }

                        progressDialog.dismiss();

                        idGroupEvent=response.body().getIdGroupEvent();
                        if(bool_payant) {
                            Intent intent = new Intent(Ajout_E.this, FormuleActivity.class);
                            intent.putExtra("idGroupEvent", response.body().getIdGroupEvent());
                            intent.putExtra("idUser", String.valueOf(session.getIdUser()));
                            startActivity(intent);
                        }
                        else {
                            Intent intent = new Intent(Ajout_E.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        if (BuildConfig.DEBUG) {
                            Log.i("Upload", " message :" + response.body().getMessage().toString());
                        }
                    }
                    else{

                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            Toast.makeText(Ajout_E.this, jObjError.getString("message"), Toast.LENGTH_LONG).show();
                            if(BuildConfig.DEBUG){
                                Log.i("UPLOAD5", "5 je suis dans  upload  "+ response.errorBody().toString() + response.message()+ jObjError.getString("message") );
                            }
                        } catch (Exception e) {
                            Toast.makeText(Ajout_E.this, e.getMessage(), Toast.LENGTH_LONG).show();
                        }


                        progressDialog.dismiss();
                        String mes = response.body().getMessage();
                        Toast.makeText(Ajout_E.this, getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Toast.makeText(Ajout_E.this, getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseServeurAddEvent> call, Throwable t) {
                if(BuildConfig.DEBUG){
                    Log.i("UPLOAD", "6 je suis dans  upload");
                }
                progressDialog.dismiss();
                Toast.makeText(Ajout_E.this, getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                if (BuildConfig.DEBUG) {
                    Log.e("Upload error:", t.getMessage() + " " + call.toString());
                }
            }
        });


    }

    private void viewImage() {
        //startActivity(new Intent(this, ImageListView.class));
    }

    private String radioButtonSelectString() {
        int selectedId = radioGroup.getCheckedRadioButtonId();

        // find the radiobutton by returned id
        radioButton = (RadioButton) findViewById(selectedId);
        return radioButton.getText().toString();
    }



    public void lancerAutocomplete() {

        try

        {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch(
                GooglePlayServicesRepairableException e)

        {
            // TODO: Handle the error.
        } catch(
                GooglePlayServicesNotAvailableException e)

        {
            // TODO: Handle the error.
        }

    }



    public void showDialogOnButtonDateJusqua(Button btn){
        btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                hideKeyboard();
                showDialog(DIALOG_DATE_JUSQUA);
            }
        });
    }
    public void showDialogOnButtonDateDebClick(){
        buttonDateDeb.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                hideKeyboard();
                showDialog(DIALOG_ID);
            }
        });
    }
    //selection de la date de fin
    public void showDialogOnButtonDateFinClick(){
        buttonDateFin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
                showDialog(DIALOG_ID_DATE_FIN);
            }
        });
    }
    //selection heure et min de debut
    public void showDialogOnButtonheureDebClick(){
        buttonHeureDeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
                showDialog(DIALOG_ID_HEURE_DEB);
            }
        });
    }
    //selection heure et minute de fin
    public void showDialogOnButtonheureFinClick(){
        buttonHeureFin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
                showDialog(DIALOG_ID_HEURE_FIN);
            }
        });
    }
    @Override
    protected Dialog onCreateDialog(int id){
        if (id == DIALOG_ID){
            DatePickerDialog datePickerDialog =new DatePickerDialog(this,datePickerListener,year_x,month_x,day_x);

            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            return  datePickerDialog;
        }
        else if (id==DIALOG_ID_DATE_FIN){
            DatePickerDialog datePickerDialog=  new DatePickerDialog(this,datePickerDateFinListener,year_df,month_df,day_df);

            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            return  datePickerDialog;
        }
        else if (id==DIALOG_DATE_JUSQUA){
            DatePickerDialog datePickerDialog=  new DatePickerDialog(this,datePickerDateJusquaListener,year_df,month_df,day_df);

            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            return  datePickerDialog;
        }
        else if (id==DIALOG_ID_HEURE_DEB){
            TimePickerDialog timePickerDialog =new TimePickerDialog(this,timePickerHeureDebListener,heure_x,min_x,true);


            return  timePickerDialog;
        }
        else if (id==DIALOG_ID_HEURE_FIN){
            TimePickerDialog timePickerDialog = new TimePickerDialog(this,timePickerHeureFinListener,heure_df,min_df,true);


            return  timePickerDialog;
        }
        return null;
    }

    //Datepicker de la date de debut
    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {

            calendarChoisieDateDebut.set(Calendar.YEAR,year);
            calendarChoisieDateDebut.set(Calendar.MONTH,monthOfYear);
            calendarChoisieDateDebut.set(Calendar.DAY_OF_MONTH,dayOfMonth);


            if (calendarChoisieDateDebut.before(calendar)){

                buttonDateDeb.setText(outPutDate(calendar));
                dateDebString = outPutDateSql(calendar);
                if(BuildConfig.DEBUG) {
                    Toast.makeText(Ajout_E.this, dateDebString, Toast.LENGTH_SHORT).show();
                }

            }else {
                dateDebSet=true;
                buttonDateDeb.setText(outPutDate(calendarChoisieDateDebut));
                dateDebString = outPutDateSql(calendarChoisieDateDebut);
                if(BuildConfig.DEBUG) {
                    Toast.makeText(Ajout_E.this, dateDebString, Toast.LENGTH_SHORT).show();
                    Log.i("DATEDEB", dateDebString);
                }
            }
        }
    };
    //Datepicker de la date de fin
    private DatePickerDialog.OnDateSetListener datePickerDateFinListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {

            calendarChoisieDateFin.set(Calendar.YEAR,year);
            calendarChoisieDateFin.set(Calendar.MONTH,monthOfYear);
            calendarChoisieDateFin.set(Calendar.DAY_OF_MONTH,dayOfMonth);


            if (calendarChoisieDateFin.before(calendar)) {
                year_df = calendar.get(Calendar.YEAR);
                month_df=calendar.get(Calendar.MONTH)+1;
                day_df=calendar.get(Calendar.DAY_OF_MONTH);

                buttonDateFin.setText(outPutDate(calendar));
                dateFinString = outPutDateSql(calendar);
                if(BuildConfig.DEBUG) {
                    Toast.makeText(Ajout_E.this, outPutDateSql(calendar), Toast.LENGTH_SHORT).show();
                }
            }
            else {

                dateFinSet=true;
                buttonDateFin.setText(outPutDate(calendarChoisieDateFin));
                dateFinString = outPutDateSql(calendarChoisieDateFin);
                if(BuildConfig.DEBUG) {
                    Toast.makeText(Ajout_E.this, outPutDateSql(calendarChoisieDateFin), Toast.LENGTH_SHORT).show();
                }
            }
        }
    };


    //dateJusqua

    private DatePickerDialog.OnDateSetListener datePickerDateJusquaListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {

            calendarChoisieDatejusqua.set(Calendar.YEAR,year);
            calendarChoisieDatejusqua.set(Calendar.MONTH,monthOfYear);
            calendarChoisieDatejusqua.set(Calendar.DAY_OF_MONTH,dayOfMonth);


            if (calendarChoisieDatejusqua.before(calendarChoisieDateDebut)) {


                dateJusquaString = outPutDateSql(calendarChoisieDateDebut);

                Toast.makeText(Ajout_E.this, getString(R.string.date_ulterieur_date_debut), Toast.LENGTH_SHORT).show();

            }
            else {


                btnDateJusqua.setText(outPutDate(calendarChoisieDatejusqua));
                dateJusquaString = outPutDateSql(calendarChoisieDatejusqua);
                if(BuildConfig.DEBUG) {
                    Toast.makeText(Ajout_E.this, dateJusquaString, Toast.LENGTH_SHORT).show();
                }
            }
        }
    };
    //Timepicker de la heure de debut
    private TimePickerDialog.OnTimeSetListener timePickerHeureDebListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int heure, int min) {
            timeDebSet=true;
            calendarChoisieDateDebut.set(Calendar.HOUR_OF_DAY,heure);
            calendarChoisieDateDebut.set(Calendar.MINUTE,min);
            buttonHeureDeb.setText(outPutTime(calendarChoisieDateDebut));
            heureDebString=outPutTimeSql(calendarChoisieDateDebut);

            if(BuildConfig.DEBUG) {
                Toast.makeText(Ajout_E.this, heureDebString, Toast.LENGTH_SHORT).show();
            }
        }
    };



    //Timepicker de la heure de fin
    private TimePickerDialog.OnTimeSetListener timePickerHeureFinListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int heure, int min) {

            timeFinSet=true;
            calendarChoisieDateFin.set(Calendar.HOUR_OF_DAY,heure);
            calendarChoisieDateFin.set(Calendar.MINUTE,min);
            buttonHeureFin.setText(outPutTime(calendarChoisieDateFin));
            heureFinString=outPutTimeSql(calendarChoisieDateFin);

            if(BuildConfig.DEBUG) {
                Toast.makeText(Ajout_E.this, heureFinString, Toast.LENGTH_SHORT).show();
            }

        }
    };
    private void hideKeyboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }





    public void showDialog(String title, String message){
        builder = new AlertDialog.Builder(Ajout_E.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void showDialogImage(){
        builder = new AlertDialog.Builder(Ajout_E.this);
        builder.setTitle(getString(R.string.ooops));
        builder.setMessage(getString(R.string.event_without_image));
        builder.setPositiveButton(getString(R.string.ajouter_quand_meme), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                addEventWithoutImage(
                        session.getIdUser() + "",
                        replaceApostrophe(nom_E.getText().toString()),
                        replaceApostrophe(type_event_inserer),
                        replaceApostrophe(adresseNom),
                        replaceApostrophe(adresseComplet),
                        replaceApostrophe(cp),
                        replaceApostrophe(ville),
                        replaceApostrophe(departement),
                        replaceApostrophe(region),
                        replaceApostrophe(paysNom),
                        replaceApostrophe(paysCode),
                        replaceApostrophe(String.valueOf(latitude)),
                        replaceApostrophe(String.valueOf(lontitude)),
                        replaceApostrophe(dateDebString + " " + heureDebString),
                        replaceApostrophe(dateFinString + " " + heureFinString),
                        checkTel(recupererIndicatifSpinner(tel_indicatif.getSelectedItem().toString()), tel_E.getText().toString()),
                        replaceApostrophe(tarif_type_inserer),
                        replaceApostrophe(description_E.getText().toString()),
                        String.valueOf(intTypeEvent),
                        String.valueOf(intTousLes),
                        String.valueOf(getDiffDates()),
                        jourLundi,
                        jourMardi,
                        jourMercredi,
                        jourJeudi,
                        jourVendredi,
                        jourSamedi,
                        jourDimanche,
                        String.valueOf(getDiffDatesDebFin()),
                        String.valueOf(intTypeEventMensuel),
                        outPutDateSql(calendarChoisieDatejusqua),
                        String.valueOf(niemeJour),
                        String.valueOf(jourSemaine)      );
            }
        });
        builder.setNegativeButton(getString(R.string.ajouterImage), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogSelectImage();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }



    public String replaceApostrophe(String str){

        String str_=str.replaceAll("'", "\\\\'");
        return str_;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_desc_event, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();

        }

        return super.onOptionsItemSelected(item);
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radioGratuit:
                if (checked)
                    btnAddTarif.setVisibility(View.GONE);
                buttonSave.setText(getString(R.string.affiche_save));
                bool_payant=false;
                tarif_type_inserer="Gratuit";
                break;
            case R.id.radioPayant:
                if (checked)
                    bool_payant=true;
                buttonSave.setText(getString(R.string.affiche_continuer));

                tarif_type_inserer="Payant";
                break;
        }
    }

    public void typeEventAInserer(int i){
        switch (i){
            case 0:
                type_event_inserer ="Soirées";
                break;
            case 1:
                type_event_inserer ="Concerts";
                break;
            case 2:
                type_event_inserer ="Films";
                break;
            case 3:
                type_event_inserer ="Spectacles";
                break;
            case 4:
                type_event_inserer ="Festivals";
                break;
        }

    }


    private void recupererIndicatif(){
        I_IndicatifCountry service =
                ServiceGenerator.createService(I_IndicatifCountry.class);

        Call<ResponseIndicatif> call = service.getIndicatif();

        call.enqueue(new Callback<ResponseIndicatif>() {
            @Override
            public void onResponse(Call<ResponseIndicatif> call, Response<ResponseIndicatif> response) {
  if(response.body() != null){

      if (response.body().isSuccess()){
          listeIndicatif=response.body().getData();

          indicatifMonnaieArrayAdapter = new ArrayAdapter<PaysIndicatif>(Ajout_E.this,android.R.layout.simple_spinner_item,listeIndicatif);
          indicatifMonnaieArrayAdapter.setDropDownViewResource(R.layout.spinner_ajout_e);


          tel_indicatif.setAdapter(indicatifMonnaieArrayAdapter);
          if(BuildConfig.DEBUG){
              Toast.makeText(Ajout_E.this, listeIndicatif.size()+"", Toast.LENGTH_SHORT).show();
              Log.i("LIST_INDICATIF", listeIndicatif.toString());


          }
      }
      else{
          if(BuildConfig.DEBUG){
              Toast.makeText(Ajout_E.this, "error Indicatif succes False", Toast.LENGTH_SHORT).show();
          }
      }
  }

                else {
                    Toast.makeText(Ajout_E.this, getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                }
                         }

            @Override
            public void onFailure(Call<ResponseIndicatif> call, Throwable t) {
                if(BuildConfig.DEBUG) {
                    Toast.makeText(Ajout_E.this, "Error Failure", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }




    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    public void checkAddTel(){
        if (checkBoxAddTel.isChecked()){
            tel_E.setVisibility(View.VISIBLE);
            tel_indicatif.setVisibility(View.VISIBLE);
        }
        else {
            tel_E.setVisibility(View.GONE);
            tel_indicatif.setVisibility(View.GONE);
            tel_E.setText("");
        }
    }

    public String checkTel(String indicatif, String num){
        if(checkBoxAddTel.isChecked()) {
            if (checkNullStringBoolean(num)) {
                return "NR";

            } else {
                return indicatif + num;

            }
        }
        else{
            return "NR";
        }
    }

    public void repeterEventAction(){
        View view=inflater.inflate(R.layout.repeter_dialog,null);
        builder=new AlertDialog.Builder(Ajout_E.this);
        builder.setView(view);
        builder.setTitle(getString(R.string.affiche_repeter));
        RadioGroup radioGroupEvent;
        radioGroupEvent = view.findViewById(R.id.radioGroupRepeter);
        /////////////////////////
        RadioButton rbEvenementUnique = view.findViewById(R.id.rbEvenementUnique);
        rbEvenementUnique.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                actionEventUnique();
            }
        });
        ///////////////////////////////
        RadioButton rbToutLesJours = view.findViewById(R.id.rbToutLesJours);
        rbToutLesJours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                actionEventQuotidien();
            }
        });
        ////////////////////
        RadioButton rbHebdomadaire = view.findViewById(R.id.rbHebdomadaire);
        rbHebdomadaire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                actionEventHebdomadaire();
            }
        });
        //////////////////
        RadioButton rbMensuel = view.findViewById(R.id.rbMensuel);
        rbMensuel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                actionEventMensuel();
            }
        });
        /////////////
        RadioButton rbAnnuel = view.findViewById(R.id.rbAnnuel);
        rbAnnuel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                actionEventAnnuel();
            }
        });
        ////
        builder.setNegativeButton(getString(R.string.annuler), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        dialog = builder.create();
        dialog.show();
    }

    public void actionEventUnique(){

        dialog.dismiss();
        intTypeEvent=EVENEMENT_UNIQUE;

    }

    public void actionEventQuotidien(){
        View view=inflater.inflate(R.layout.dialog_evenement_quotidien,null);
        builder=new AlertDialog.Builder(Ajout_E.this);
        calendarChoisieDatejusqua.set(Calendar.YEAR,calendarChoisieDateDebut.get(Calendar.YEAR)+1);
        calendarChoisieDatejusqua.set(Calendar.MONTH,calendarChoisieDateDebut.get(Calendar.MONTH));
        calendarChoisieDatejusqua.set(Calendar.DAY_OF_MONTH,calendarChoisieDateDebut.get(Calendar.DAY_OF_MONTH));

        final EditText eTnbJours= view.findViewById(R.id.eTnbJours);
        btnDateJusqua = view.findViewById(R.id.btnDateJusqua);
        showDialogOnButtonDateJusqua(btnDateJusqua);

        final TextView tvJusqua = view.findViewById(R.id.tvJusqua);
        btnDateJusqua.setText(outPutDate(calendarChoisieDatejusqua));
        final CheckBox checkBoxToujours =view.findViewById(R.id.checkBoxToujours);

        checkBoxToujours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkBoxToujours.isChecked()){
                    btnDateJusqua.setClickable(false);
                    btnDateJusqua.setTextColor(Color.GRAY);
                    tvJusqua.setTextColor(Color.GRAY);


                }
                else {
                    btnDateJusqua.setClickable(true);
                    btnDateJusqua.setTextColor(Color.BLACK);
                    tvJusqua.setTextColor(Color.BLACK);

                }
            }
        });

        builder.setView(view);
        builder.setTitle(getString(R.string.affiche_repeter));
        builder.setPositiveButton(getString(R.string.affiche_termine), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                intTypeEvent=EVENEMENT_QUOTIDIEN;
                intTousLes= Integer.valueOf(eTnbJours.getText().toString());
                nbjoursRestant= getDiffDates();
            }
        });
        builder.setNegativeButton(getString(R.string.annuler), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        dialog = builder.create();
        dialog.show();
    }

    public void actionEventHebdomadaire(){
        View view=inflater.inflate(R.layout.dialog_evenement_hebdomadaire,null);
        builder=new AlertDialog.Builder(Ajout_E.this);
        final EditText eTnbJours= view.findViewById(R.id.eTnbJours);
        builder.setView(view);
        builder.setTitle(getString(R.string.affiche_repeter));

        calendarChoisieDatejusqua.set(Calendar.YEAR,calendarChoisieDateDebut.get(Calendar.YEAR)+1);
        calendarChoisieDatejusqua.set(Calendar.MONTH,calendarChoisieDateDebut.get(Calendar.MONTH));
        calendarChoisieDatejusqua.set(Calendar.DAY_OF_MONTH,calendarChoisieDateDebut.get(Calendar.DAY_OF_MONTH));
        builder.setPositiveButton(getString(R.string.affiche_termine), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getDayOfWeek();
                intTypeEvent=EVENEMENT_HEBDOMADAIRE;
                intTousLes= Integer.valueOf(eTnbJours.getText().toString());
                nbjoursRestant= getDiffDates();

            }
        });
        builder.setNegativeButton(getString(R.string.annuler), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        dialog = builder.create();
        dialog.show();


        ctLundi=view.findViewById(R.id.ctLundi);
        ctLundi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkCheckTextView(ctLundi);
            }
        });

        ctMardi=view.findViewById(R.id.ctMardi);
        ctMardi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkCheckTextView(ctMardi);
            }
        });


        ctMercredi=view.findViewById(R.id.ctMercredi);
        ctMercredi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkCheckTextView(ctMercredi);
            }
        });



        ctJeudi=view.findViewById(R.id.ctJeudi);
        ctJeudi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkCheckTextView(ctJeudi);
            }
        });

        ctJeudi.setChecked(true);
        ctJeudi.setBackground(getResources().getDrawable(R.drawable.roud_green_inside));

        ctVendredi=view.findViewById(R.id.ctVendredi);
        ctVendredi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkCheckTextView(ctVendredi);
            }
        });


        ctSamedi=view.findViewById(R.id.ctSamedi);
        ctSamedi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkCheckTextView(ctSamedi);
            }
        });

        ctDimanche=view.findViewById(R.id.ctDimanche);
        ctDimanche.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkCheckTextView(ctDimanche);
            }
        });

        btnDateJusqua = view.findViewById(R.id.btnDateJusqua);
        btnDateJusqua.setText(outPutDate(calendarChoisieDatejusqua));
        showDialogOnButtonDateJusqua(btnDateJusqua);

        final TextView tvJusqua = view.findViewById(R.id.tvJusqua);
        final CheckBox checkBoxToujours =view.findViewById(R.id.checkBoxToujours);

        checkBoxToujours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkBoxToujours.isChecked()){
                    btnDateJusqua.setClickable(false);
                    btnDateJusqua.setTextColor(Color.GRAY);
                    tvJusqua.setTextColor(Color.GRAY);

                }
                else {
                    btnDateJusqua.setClickable(true);
                    btnDateJusqua.setTextColor(Color.BLACK);
                    tvJusqua.setTextColor(Color.BLACK);

                }
            }
        });



    }

    public void actionEventMensuel(){
        View view=inflater.inflate(R.layout.dialog_evenement_mensuel,null);
        builder=new AlertDialog.Builder(Ajout_E.this);
        builder.setView(view);
        builder.setTitle(getString(R.string.affiche_repeter));
        final EditText eTnbJours= view.findViewById(R.id.eTnbJours);
        final TextView tvJusqua = view.findViewById(R.id.tvJusqua);
        final CheckBox checkBoxToujours =view.findViewById(R.id.checkBoxToujours);
        rbtousLesJoursX = view.findViewById(R.id.rbtousLesJoursX);
        rbtousXjoursdumois= view.findViewById(R.id.rbtousXjoursdumois);
        rbtousDernierXjoursdumois= view.findViewById(R.id.rbtousDernierXjoursdumois);
        rbtousDernierjourdumois= view.findViewById(R.id.rbtousDernierjourdumois);


        int jour = calendarChoisieDateDebut.get(Calendar.DAY_OF_MONTH);
        rbtousLesJoursX.setText(getString(R.string.affiche_tous_les)+" "+jour+" "+ getString(R.string.affiche_du_mois));
        int jourIeme= calendarChoisieDateDebut.get(Calendar.DAY_OF_WEEK_IN_MONTH);
        rbtousXjoursdumois.setText(getString(R.string.affiche_tous_les)+" "+ getXemeJourDuMois(calendarChoisieDateDebut)+" "+ getJourDeLaSemaine(calendarChoisieDateDebut)+" "+getString(R.string.affiche_du_mois));

        if(!getLastDayOfMonth(calendarChoisieDateDebut)){
            rbtousDernierjourdumois.setVisibility(View.GONE);
        }

        if(getLastXemeDayOfMonth(calendarChoisieDateDebut)){
            rbtousDernierXjoursdumois.setText(getString(R.string.affiche_tous_les_derniers)+" "+getJourDeLaSemaine(calendarChoisieDateDebut)+" "+getString(R.string.affiche_du_mois));
        }
        else
        {
            rbtousDernierXjoursdumois.setVisibility(View.GONE);

        }
        calendarChoisieDatejusqua.set(Calendar.YEAR,calendarChoisieDateDebut.get(Calendar.YEAR)+1);
        calendarChoisieDatejusqua.set(Calendar.MONTH,calendarChoisieDateDebut.get(Calendar.MONTH));
        calendarChoisieDatejusqua.set(Calendar.DAY_OF_MONTH,calendarChoisieDateDebut.get(Calendar.DAY_OF_MONTH));



        builder.setPositiveButton(getString(R.string.affiche_termine), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if(!rbtousLesJoursX.isChecked() && !rbtousXjoursdumois.isChecked()){
                    Toast.makeText(Ajout_E.this, getString(R.string.remplissezToutChamp), Toast.LENGTH_LONG).show();
                }
                else
                {
                    intTypeEvent=EVENEMENT_MENSUEL;
                    intTousLes= Integer.valueOf(eTnbJours.getText().toString());
                    Toast.makeText(Ajout_E.this, "Tout les " +intTousLes, Toast.LENGTH_SHORT).show();
                    nbjoursRestant= getDiffDates();
                }


            }
        });
        builder.setNegativeButton(getString(R.string.annuler), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        dialog = builder.create();
        dialog.show();

        btnDateJusqua = view.findViewById(R.id.btnDateJusqua);
        btnDateJusqua.setText(outPutDate(calendarChoisieDatejusqua));
        showDialogOnButtonDateJusqua(btnDateJusqua);


        rbtousLesJoursX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intTypeEventMensuel = EVENEMENT_MENSUEL_TOUS_LES_X_DU_MOIS;

            }
        });
        rbtousXjoursdumois.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intTypeEventMensuel = EVENEMENT_MENSUEL_TOUS_LES_Xeme_JOURS_DU_MOIS;

            }
        });

        rbtousDernierjourdumois.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intTypeEventMensuel =EVENEMENT_MENSUEL_DERNIER_JOURS_DU_MOIS;
            }
        });

        rbtousDernierXjoursdumois.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intTypeEventMensuel =EVENEMENT_MENSUEL_DERNIER_JOURS_X_DU_MOIS;
            }
        });

        checkBoxToujours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkBoxToujours.isChecked()){
                    btnDateJusqua.setClickable(false);
                    btnDateJusqua.setTextColor(Color.GRAY);
                    tvJusqua.setTextColor(Color.GRAY);

                }
                else {
                    btnDateJusqua.setClickable(true);
                    btnDateJusqua.setTextColor(Color.BLACK);
                    tvJusqua.setTextColor(Color.BLACK);

                }
            }
        });

    }


    public void actionEventAnnuel(){
        View view=inflater.inflate(R.layout.dialog_evenement_annuel,null);
        builder=new AlertDialog.Builder(Ajout_E.this);
        final EditText eTnbJours= view.findViewById(R.id.eTnbJours);
        calendarChoisieDatejusqua.set(Calendar.YEAR,calendarChoisieDateDebut.get(Calendar.YEAR)+2);
        calendarChoisieDatejusqua.set(Calendar.MONTH,calendarChoisieDateDebut.get(Calendar.MONTH));
        calendarChoisieDatejusqua.set(Calendar.DAY_OF_MONTH,calendarChoisieDateDebut.get(Calendar.DAY_OF_MONTH));
        builder.setView(view);
        builder.setTitle(getString(R.string.affiche_repeter));
        builder.setPositiveButton(getString(R.string.affiche_termine), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                intTypeEvent=EVENEMENT_ANNUEL;
                intTousLes= Integer.valueOf(eTnbJours.getText().toString());
                nbjoursRestant= getDiffDates();

            }
        });
        builder.setNegativeButton(getString(R.string.annuler), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        dialog = builder.create();
        dialog.show();

        btnDateJusqua = view.findViewById(R.id.btnDateJusqua);
        btnDateJusqua.setText(outPutDate(calendarChoisieDatejusqua));
        showDialogOnButtonDateJusqua(btnDateJusqua);

        final TextView tvJusqua = view.findViewById(R.id.tvJusqua);
        final CheckBox checkBoxToujours =view.findViewById(R.id.checkBoxToujours);

        checkBoxToujours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkBoxToujours.isChecked()){
                    btnDateJusqua.setClickable(false);
                    btnDateJusqua.setTextColor(Color.GRAY);
                    tvJusqua.setTextColor(Color.GRAY);

                }
                else {
                    btnDateJusqua.setClickable(true);
                    btnDateJusqua.setTextColor(Color.BLACK);
                    tvJusqua.setTextColor(Color.BLACK);

                }
            }
        });

    }

    public  void checkCheckTextView(CheckedTextView c){
        if (c.isChecked()){
            c.setChecked(false);
            c.setBackground(getResources().getDrawable(R.drawable.round));
        }
        else {
            c.setChecked(true);
            c.setBackground(getResources().getDrawable(R.drawable.roud_green_inside));
        }
    }
    public int getDiffDates(){
        long diff = calendarChoisieDatejusqua.getTimeInMillis() - calendarChoisieDateDebut.getTimeInMillis();
        int res = (int) (diff / (1000*60*60*24));
        if (BuildConfig.DEBUG) {
            Log.i("getDiffDates","difference de dates entre " +outPutDateTimeSql(calendarChoisieDateDebut) +" et "+ outPutDateTimeSql(calendarChoisieDatejusqua)+" = "+  res);
        }
        return res ;
    }

    public int getDiffDatesDebFin(){
        long diff = calendarChoisieDateFin.getTimeInMillis() - calendarChoisieDateDebut.getTimeInMillis();
        int res = (int) (diff / (1000*60*60*24));
        if (BuildConfig.DEBUG) {
            Log.i("getDiffDates","difference de dates entre " +outPutDateTimeSql(calendarChoisieDateDebut) +" et "+ outPutDateTimeSql(calendarChoisieDateFin)+" = "+  res);        }
        return res ;
    }

    public void getDayOfWeek(){

        int dayofWeek= calendarChoisieDateDebut.get(Calendar.DAY_OF_WEEK)-1;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR,calendarChoisieDateDebut.get(Calendar.YEAR));
        cal.set(Calendar.MONTH,calendarChoisieDateDebut.get(Calendar.MONTH));
        cal.set(Calendar.DAY_OF_MONTH,calendarChoisieDateDebut.get(Calendar.DAY_OF_MONTH));

        cal.set(Calendar.HOUR,calendarChoisieDateDebut.get(Calendar.HOUR));
        cal.set(Calendar.MINUTE,calendarChoisieDateDebut.get(Calendar.MINUTE));
        cal.set(Calendar.SECOND,calendarChoisieDateDebut.get(Calendar.SECOND));



        switch (dayofWeek){
            case 0:
                //dimanche


                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctLundi.isChecked()){jourLundi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctMardi.isChecked()){jourMardi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctMercredi.isChecked()){jourMercredi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctJeudi.isChecked()){jourJeudi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctVendredi.isChecked()){jourVendredi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctSamedi.isChecked()){jourSamedi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctDimanche.isChecked()){jourDimanche= outPutDateTimeSql(cal);}

                if (BuildConfig.DEBUG) {
                    Log.i("dayofWeek","calendarChoisieDateDebut " +outPutDateTimeSql(calendarChoisieDateDebut) +" et calendar : "+ outPutDateTimeSql(cal));        }


                break;
            case 1:
                //lundi

                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctMardi.isChecked()){jourMardi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctMercredi.isChecked()){jourMercredi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctJeudi.isChecked()){jourJeudi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctVendredi.isChecked()){jourVendredi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctSamedi.isChecked()){jourSamedi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctDimanche.isChecked()){jourDimanche= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctLundi.isChecked()){jourLundi= outPutDateTimeSql(cal);}


                break;
            case 2:
                //mardi


                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctMercredi.isChecked()){jourMercredi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctJeudi.isChecked()){jourJeudi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctVendredi.isChecked()){jourVendredi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctSamedi.isChecked()){jourSamedi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctDimanche.isChecked()){jourDimanche= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctLundi.isChecked()){jourLundi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctMardi.isChecked()){jourMardi= outPutDateTimeSql(cal);}
                break;
            case 3:
                //mercredi

                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctJeudi.isChecked()){jourJeudi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctVendredi.isChecked()){jourVendredi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctSamedi.isChecked()){jourSamedi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctDimanche.isChecked()){jourDimanche= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctLundi.isChecked()){jourLundi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctMardi.isChecked()){jourMardi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctMercredi.isChecked()){jourMercredi= outPutDateTimeSql(cal);}

                break;
            case 4:
                //jeudi


                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctVendredi.isChecked()){jourVendredi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctSamedi.isChecked()){jourSamedi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctDimanche.isChecked()){jourDimanche= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctLundi.isChecked()){jourLundi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctMardi.isChecked()){jourMardi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctMercredi.isChecked()){jourMercredi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctJeudi.isChecked()){jourJeudi= outPutDateTimeSql(cal);}
                break;
            case 5:
                //vendredi

                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctSamedi.isChecked()){jourSamedi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctDimanche.isChecked()){jourDimanche= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctLundi.isChecked()){jourLundi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctMardi.isChecked()){jourMardi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctMercredi.isChecked()){jourMercredi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctJeudi.isChecked()){jourJeudi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctVendredi.isChecked()){jourVendredi= outPutDateTimeSql(cal);}
                break;
            case 6:
                //samedi
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctDimanche.isChecked()){jourDimanche= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctLundi.isChecked()){jourLundi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctMardi.isChecked()){jourMardi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctMercredi.isChecked()){jourMercredi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctJeudi.isChecked()){jourJeudi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctVendredi.isChecked()){jourVendredi= outPutDateTimeSql(cal);}
                cal.add(Calendar.DAY_OF_MONTH,1);
                if(ctSamedi.isChecked()){jourSamedi= outPutDateTimeSql(cal);}


                break;
        }

    }


    public String outPutDate(Calendar calendar){

        int style =DateFormat.MEDIUM;

        DateFormat df =  DateFormat.getDateInstance(style,getResources().getConfiguration().locale);;
        return df.format(calendar.getTime());
    }

    public String outPutDateSql(Calendar calendar){
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat (pattern);
        return   simpleDateFormat.format(calendar.getTime());
    }
    public String outPutDateTimeSql(Calendar calendar){
        String pattern = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat (pattern);
        return   simpleDateFormat.format(calendar.getTime());
    }

    public String outPutTime(Calendar calendar){
        String pattern = "HH:mm";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat (pattern);
        return   simpleDateFormat.format(calendar.getTime());
    }

    public String outPutTimeSql(Calendar calendar){
        String pattern = "HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat (pattern);
        return   simpleDateFormat.format(calendar.getTime());
    }

    public String getXemeJourDuMois(Calendar calendar){
        int jour =calendar.get(Calendar.DAY_OF_MONTH);

        String res = "";
        if(jour<=7){
            res = getString(R.string.affichePremiers);
            niemeJour=1;
        }
        else if(jour>7 && jour<=14)
        {
            res = getString(R.string.afficheDeuxieme);
            niemeJour=2;
        }
        else if(jour>14 && jour<=21)
        {
            res = getString(R.string.afficheTroisieme);
            niemeJour=3;
        }
        else if (jour>21 && jour<=28){
            res = getString(R.string.afficheQuatrieme);
            niemeJour=4;
        }
        else {
            res = jour + getString(R.string.afficheCinquieme);
            rbtousDernierXjoursdumois.setVisibility(View.GONE);

        }
        return res;
    }


    public boolean getLastXemeDayOfMonth(Calendar calendar){
        int nbJourMois = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        int jour =calendar.get(Calendar.DAY_OF_MONTH);

        if (nbJourMois==28){
            if(jour>=22 && jour<=28) {
                return true;
            }
        }
        else if (nbJourMois==29){
            if(jour>=23 && jour<=29) {
                return true;
            }
        }
        else if (nbJourMois==30){
            if(jour>=24 && jour<=30) {
                return true;
            }
        }
        else if (nbJourMois==31){
            if(jour>=25 && jour<=31) {
                return true;
            }
        }


        return false;




    }

    public boolean getLastDayOfMonth(Calendar calendar){
        int nbJourMois = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        int jour =calendar.get(Calendar.DAY_OF_MONTH);

        if (jour == nbJourMois){
            return true;
        }
        return false;
    }

    public String getJourDeLaSemaine(Calendar calendar) {
        int jour = calendar.get(Calendar.DAY_OF_WEEK)-1;;
        String res = "";
        switch (jour) {
            case 0:
                //dimanche
                res=getString(R.string.affiche_dimanche_entier);
                jourSemaine=6;
                break;

            case 1:
                //Lundi
                res=getString(R.string.affiche_Lundi_entier);
                jourSemaine=0;

                break;

            case 2:
                //Mardi
                res=getString(R.string.affiche_Mardi_entier);
                jourSemaine=1;

                break;

            case 3:
                //Mercredi
                res=getString(R.string.affiche_Mercredi_entier);
                jourSemaine=2;

                break;

            case 4:
                //Jeudi
                res=getString(R.string.affiche_Jeudi_entier);
                jourSemaine=3;

                break;

            case 5:
                //vendredi
                res=getString(R.string.affiche_Vendredi_entier);
                jourSemaine=4;

                break;

            case 6:
                //samedi
                res=getString(R.string.affiche_Samedi_entier);
                jourSemaine=5;

                break;

        }
        return  res;
    }

    @Override
    public void onListFragmentInteraction(Formule item) {

    }



    private void addEventWithoutImage(
            String idUser,
            String nom,
            String type,
            String adresse,
            String adresseComplet,
            String codepostal,
            String ville,
            String departement,
            String region,
            String paysNom,
            String paysCode,
            String latitude,
            String lontitude,
            String datedeb,
            String datefin,
            String telephone,
            String typeTarif,
            String description,
            String intTypeEvent,
            String intTousLes,
            String nbjoursRestant,
            String jourLundi,
            String jourMardi,
            String jourMercredi,
            String jourJeudi,
            String jourVendredi,
            String jourSamedi,
            String jourDimanche,
            String nbjourDiffDebFin,
            String intTypeEventMensuel,
            String dateJusqua, String niemeJour, String jourSemaine) {

        progressDialog.show();
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        // create upload service client
        FileUploadService service =
                ServiceGenerator.createService(FileUploadService.class);

        RequestBody r_idUser =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, idUser);
        RequestBody r_nom =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, nom);

        RequestBody r_type =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, type);
        RequestBody r_adresse =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, adresse);

        RequestBody r_adresseComplet =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, adresseComplet);

        RequestBody r_codepostal =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, codepostal);

        RequestBody r_departement =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, departement);
        RequestBody r_region =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, region);
        RequestBody r_paysNom =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, paysNom);
        RequestBody r_paysCode =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, paysCode);
        RequestBody r_latitude =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, latitude);
        RequestBody r_lontitude =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, lontitude);

        RequestBody r_ville =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, ville);
        RequestBody r_datedeb =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, datedeb);
        RequestBody r_datefin =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, datefin);
        RequestBody r_telephone =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, telephone);
        RequestBody r_typeTarif =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, typeTarif);


        RequestBody r_description =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, description);

        RequestBody r_IntType =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, intTypeEvent);

        RequestBody r_intTousLes =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, intTousLes);

        RequestBody r_nbjoursRestant =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, nbjoursRestant);

        RequestBody r_jourLundi =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, jourLundi);
        RequestBody r_jourMardi =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, jourMardi);

        RequestBody r_jourMercredi =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, jourMercredi);

        RequestBody r_jourJeudi =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, jourJeudi);

        RequestBody r_jourVendredi =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, jourVendredi);

        RequestBody r_jourSamedi =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, jourSamedi);

        RequestBody r_jourDimanche =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, jourDimanche);
        RequestBody r_nbDiffDebFin =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, nbjourDiffDebFin);
        RequestBody r_intTypeEventMensuel =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, intTypeEventMensuel);
        RequestBody r_datejusqua =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, dateJusqua);

        RequestBody r_niemeJour =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, niemeJour);

        RequestBody r_jourSemaine =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, jourSemaine);


        Call<ReponseServeur> call = service.addEventWithoutImage(  r_idUser,
                r_nom,
                r_type,
                r_adresse,
                r_adresseComplet,
                r_codepostal,
                r_ville,
                r_departement,
                r_region,
                r_paysNom,
                r_paysCode,
                r_latitude,
                r_lontitude,
                r_datedeb,
                r_datefin,
                r_telephone,
                r_typeTarif,
                r_description,
                r_IntType,
                r_intTousLes,
                r_nbjoursRestant,
                r_jourLundi,
                r_jourMardi,
                r_jourMercredi,
                r_jourJeudi,
                r_jourVendredi,
                r_jourSamedi,
                r_jourDimanche,
                r_nbDiffDebFin,
                r_intTypeEventMensuel,
                r_datejusqua,
                r_niemeJour,
                r_jourSemaine
        );

        call.enqueue(new Callback<ReponseServeur>() {
            @Override
            public void onResponse(Call<ReponseServeur> call, Response<ReponseServeur> response) {
  if(response.body() != null){

      if (response.body().isSuccess()){
          if (BuildConfig.DEBUG) {
              Toast.makeText(Ajout_E.this, response.body().getMessage().toString(), Toast.LENGTH_SHORT).show();

              Snackbar.make(coordinatorLayout, response.body().getMessage().toString(), Snackbar.LENGTH_LONG);
          }
          progressDialog.dismiss();
          Intent intent = new Intent(Ajout_E.this, MainActivity.class);
          startActivity(intent);
          finish();
          overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
          if (BuildConfig.DEBUG) {
              Log.i("Upload", " message :" + response.body().getMessage().toString());
          }
      }
                }

                else {
                    Toast.makeText(Ajout_E.this, getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ReponseServeur> call, Throwable t) {
                if (BuildConfig.DEBUG) {
                    Log.e("Upload error:", t.getMessage() + " " + call.toString());
                }
            }

        });
    }

  /*  public void checkDateEvent(boolean dateDebSet,boolean dateFinSet,boolean timeDebSet,boolean timeFinSet){



        if(dateDebSet && dateFinSet && timeDebSet && timeFinSet){
            if (!calendarChoisieDateDebut.before(calendar)) {
                if (calendarChoisieDateDebut.before(calendarChoisieDateFin)) {
                    btnRepeterEvent.setVisibility(View.VISIBLE);
                    checkDatebool =true;
                }

                else {
                    Toast.makeText(this, getString(R.string.checkDebFin), Toast.LENGTH_LONG).show();

                }
            } else {
                Toast.makeText(this, getString(R.string.checkDebCourante), Toast.LENGTH_LONG).show();

            }

        }
    }*/
}

