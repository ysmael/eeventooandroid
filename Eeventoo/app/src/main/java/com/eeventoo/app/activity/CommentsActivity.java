package com.eeventoo.app.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.eeventoo.app.BuildConfig;
import com.eeventoo.app.CustomRequest;
import com.eeventoo.app.InterfaceRetrofit.I_Avis;
import com.eeventoo.app.Adapter.CommentsAdapter;
import com.eeventoo.app.R;
import com.eeventoo.app.SendCommentButton;

import com.eeventoo.app.class_.Avis;
import com.eeventoo.app.class_.Constants;
import com.eeventoo.app.class_.ReponseServeur;
import com.eeventoo.app.class_.ServiceGenerator;
import com.eeventoo.app.class_.UserSessionManager;
import com.eeventoo.app.class_.Utils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;
import static com.bumptech.glide.request.RequestOptions.circleCropTransform;

/**
 * Created by isma on 11/03/2017.
 */
public class CommentsActivity extends AppCompatActivity{
    public static final String ARG_DRAWING_START_LOCATION = "arg_drawing_start_location";



UserSessionManager session;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.contentRoot)
    LinearLayout contentRoot;
    @BindView(R.id.rvComments)
    RecyclerView rvComments;
    @BindView(R.id.llAddComment)
    LinearLayout llAddComment;
    @BindView(R.id.etComment)
    EditText etComment;
    float rating;
    @BindView(R.id.btnSendComment)
    SendCommentButton btnSendComment;

    String idEvent;
    RatingBar ratingBar;
    int year_x,month_x, day_x,heure_x, min_x;
    private CommentsAdapter commentsAdapter;
    private int drawingStartLocation;
    String idOfUser;
    private ArrayList<Avis> listAvis = new ArrayList<>();
    private RequestQueue requestQueue;
    //The request counter to send ?page=1, ?page=2  requests
    private int requestCount = 1;
    TextView textViewListeVide;
    private FirebaseAuth mAuth;
    Uri photoUrl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {


       // BackgroundTaskGet_10_avis backgroundTaskGet_10_avis;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);
        drawingStartLocation = getIntent().getIntExtra(ARG_DRAWING_START_LOCATION, 0);
        ratingBar= (RatingBar) findViewById(R.id.ratingBar);
        ButterKnife.bind(this);
        idEvent= getIntent().getExtras().getString("id");
        //tester la connection reseau

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if(networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()) {
        }
        else{
            Toast toast = Toast.makeText(this, getString(R.string.aucuneConnexion), Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }

        if (savedInstanceState == null) {
            contentRoot.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    contentRoot.getViewTreeObserver().removeOnPreDrawListener(this);
                    startIntroAnimation();
                    return true;
                }
            });
        }

        mAuth = FirebaseAuth.getInstance();

        if(mAuth.getCurrentUser() != null){


            for (UserInfo profile : mAuth.getCurrentUser().getProviderData()) {

                photoUrl = profile.getPhotoUrl();




            }


        }


        setupComments();

        final Calendar calendar = Calendar.getInstance();

        year_x = calendar.get(Calendar.YEAR);
        month_x=calendar.get(Calendar.MONTH)+1;
        day_x=calendar.get(Calendar.DAY_OF_MONTH);
        heure_x=calendar.get(Calendar.HOUR_OF_DAY);
        min_x=calendar.get(Calendar.MINUTE);

        session = new UserSessionManager(getApplicationContext());
        idOfUser = ""+session.getIdUser();

        textViewListeVide = (TextView) findViewById(R.id.textViewListVide);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // back button pressed
                onBackPressed();
            }
        });

        etComment.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() ==R.id.etComment) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction()&MotionEvent.ACTION_MASK){
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }

        });

    }

    private void startIntroAnimation() {
        contentRoot.setScaleY(0.1f);
        contentRoot.setPivotY(drawingStartLocation);
        llAddComment.setTranslationY(100);

        contentRoot.animate()
                .scaleY(1)
                .setDuration(200)
                .setInterpolator(new AccelerateInterpolator())
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        animateContent();
                    }
                })
                .start();
    }

    private void animateContent() {
        commentsAdapter.updateItems();
        llAddComment.animate().translationY(0)
                .setInterpolator(new DecelerateInterpolator())
                .setDuration(300)
                .start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //tester la connection reseau

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if(networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()) {
        }
        else{
            Toast toast = Toast.makeText(this, getString(R.string.aucuneConnexion), Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }
    private void setupComments() {

        requestQueue = Volley.newRequestQueue(CommentsActivity.this);
        getData(idEvent);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.scrollToPositionWithOffset(0, 0);
        rvComments.setLayoutManager(linearLayoutManager);
        rvComments.setHasFixedSize(true);

        commentsAdapter = new CommentsAdapter(this,listAvis);
        rvComments.setAdapter(commentsAdapter);
        rvComments.setOverScrollMode(View.OVER_SCROLL_NEVER);
        rvComments.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                    commentsAdapter.setAnimationsLocked(true);
                }
            }
        });
        rvComments.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(isFirstItemDisplaying(recyclerView)){
                getData(idEvent);
                }
            }
        });
        btnSendComment.setOnSendClickListener(new SendCommentButton.OnSendClickListener() {
            @Override
            public void onSendClickListener(View v) {
                onSendCommentClick();
            }
        });
    }

    //This method would check that the recyclerview scroll has reached the bottom or not
    private boolean isFirstItemDisplaying(RecyclerView recyclerView) {
        if (recyclerView.getAdapter().getItemCount() != 0) {
            int firstVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
            if (firstVisibleItemPosition != RecyclerView.NO_POSITION && firstVisibleItemPosition == recyclerView.getAdapter().getItemViewType(0))
                return true;
        }
        return false;
    }

    //This method will get data from the web api
    public void getData(String idEvent){

        //Adding the method to the queue by calling the method getDataFromServer
        requestQueue.add(getDataFromServer(idEvent,requestCount));
        //Incrementing the request counter
        requestCount++;
    }

    private CustomRequest getDataFromServer(String idEvent,int requestCount){
        HashMap<String,String> hashMapPostValue = new HashMap<>();
        hashMapPostValue.put("idEvent",idEvent);
        hashMapPostValue.put("page",String.valueOf(requestCount));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, Constants.GET_AVIS_URL, hashMapPostValue, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                parseData(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Toast.makeText(CommentsActivity.this, "Plus d'avis disponible", Toast.LENGTH_SHORT).show();
            }
        });
        return  customRequest;
    }

    private void parseData(JSONArray array) {
        Log.i("CommentActivity",array.toString());
        for (int i = 0; i < array.length(); i++) {
            Avis avis = new Avis();
            JSONObject json = null;
            try {
                //Getting json
                json = array.getJSONObject(i);
                avis.setId(json.getString("idAvis"));
                avis.setDisplayNameUserAvis(json.getString("displayName"));
                avis.setTextAvis(json.getString("avis"));
                avis.setDateAvis(json.getString("date_avis"));
                avis.setNbStarAvis(json.getString("nbetoiles"));
                avis.setUserPP(json.getString("photoprofil"));
            } catch (JSONException e) {
                Log.i("CommentActivity", e.getMessage().toString());
                e.printStackTrace();

            }

            listAvis.add(avis);
        }

        if(listAvis.isEmpty()){
            textViewListeVide.setVisibility(View.VISIBLE);
        }
        else {
            textViewListeVide.setVisibility(View.GONE);
        }

        commentsAdapter.notifyDataSetChanged();
    }


    public void onSendCommentClick()  {
        if (session.isUserLoggedIn()) {
            if (validateComment()) {
                rating = ratingBar.getRating();
                if(BuildConfig.DEBUG) {
                    Toast.makeText(getApplicationContext(), "Your Selected Ratings  : " + String.valueOf(rating), Toast.LENGTH_LONG).show();

                    Log.i("BOUTTON AVIS", "idOfUser " + idOfUser + " id :" + "" + idEvent + "date : " + year_x + "/" + month_x + "/" + day_x + " " + heure_x + ":" + min_x + ":00 " + "nb etoiles: " + String.valueOf(rating));
                }

                addAvis( idOfUser, "" + idEvent, etComment.getText().toString(), year_x + "/" + month_x + "/" + day_x + " " + heure_x + ":" + min_x + ":00", String.valueOf(rating));



            }
        }
            else
            {
                Intent intent = new Intent(CommentsActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        }



    @Override
    public void onBackPressed() {
        contentRoot.animate()
                .translationY(Utils.getScreenHeight(this))
                .setDuration(400)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        CommentsActivity.super.onBackPressed();
                        overridePendingTransition(0, 0);
                    }
                })
                .start();
    }




    private boolean validateComment() {
        if ((TextUtils.isEmpty(etComment.getText())) ) {
            btnSendComment.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake_error));
            return false;
        }

        return true;
    }

    //permet de cacher le clavier du portable
    private void hideKeyboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    private  void addAvis(String idUser,String idEvent,String avis,String date_avis, String nbetoiles) {
        I_Avis i_avis = ServiceGenerator.createService(I_Avis.class);

        Call<ReponseServeur> call = i_avis.addLike(idUser,idEvent,avis,date_avis,nbetoiles);
        call.enqueue(new Callback<ReponseServeur>() {
            @Override
            public void onResponse(Call<ReponseServeur> call, retrofit2.Response<ReponseServeur> response) {
                  if(response.body() != null){

                      if (response.body().isSuccess()) {
                          textViewListeVide.setVisibility(View.GONE);
                          rating = ratingBar.getRating();


                          commentsAdapter.setAnimationsLocked(false);
                          commentsAdapter.setDelayEnterAnimation(false);
                          if (!listAvis.isEmpty()) {
                              rvComments.smoothScrollBy(0, rvComments.getChildAt(0).getHeight() * commentsAdapter.getItemCount());

                          }
                          if(mAuth.getCurrentUser()!=null) {
                              Avis a = new Avis(idOfUser, mAuth.getCurrentUser().getDisplayName(), etComment.getText().toString(),
                                      day_x + "/" + month_x + "/" + year_x, String.valueOf(rating),  photoUrl.toString());
                              listAvis.add(a);

                          }
                          commentsAdapter.addItem();
                          rating = ratingBar.getRating();

                          etComment.setText(null);
                          ratingBar.setRating(0);
                          btnSendComment.setCurrentState(SendCommentButton.STATE_DONE);
                          hideKeyboard();
                          Toast.makeText(CommentsActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                      }
                      else
                      {
                          Toast.makeText(CommentsActivity.this, getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                      }
                }

                else {
                    Toast.makeText(CommentsActivity.this, getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ReponseServeur> call, Throwable t) {
                Toast.makeText(CommentsActivity.this, "Erreur d'ajout  "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);


        /*MenuItem searchItem = menu.findItem(R.id.action_search);
// with MenuItemCompat instead of your MenuItem
        SearchView mSearchView = (SearchView) MenuItemCompat.getActionView(searchItem);
mSearchView.setOnQueryTextListener(this);*/
        return true;
    }
}