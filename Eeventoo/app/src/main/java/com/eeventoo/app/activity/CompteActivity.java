package com.eeventoo.app.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.request.RequestOptions;
import com.eeventoo.app.BuildConfig;
import com.eeventoo.app.InterfaceRetrofit.I_CompteUser;
import com.eeventoo.app.InterfaceRetrofit.I_IndicatifCountry;
import com.eeventoo.app.R;
import com.eeventoo.app.class_.FileUtils;
import com.eeventoo.app.class_.PaysIndicatif;
import com.eeventoo.app.class_.ReponseServeur;
import com.eeventoo.app.class_.ResponseIndicatif;
import com.eeventoo.app.class_.ServiceGenerator;
import com.eeventoo.app.class_.UserSessionManager;
import com.eeventoo.app.outils.RoundedTransformation;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;
import static com.bumptech.glide.request.RequestOptions.circleCropTransform;

public class CompteActivity extends AppCompatActivity {
    private static int avatarSize ;
    UserSessionManager session;
    private ImageView imagePP;
    private EditText  e_displayName ,e_tel;
    private TextView e_email;
    int maxLength_tel = 9;
    private int PICK_IMAGE_REQUEST = 1;
    private static final int TAKE_PICTURE_CAMERA_REQUEST =2 ;
    private Uri filePath;
    private Button btnMAJpp,btnMAJtel,btnMAJModif, btnLogout,btnResetPassword ;
    AlertDialog.Builder builder;
    private Spinner tel_indicatif;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 30;
    String providerId, name,email, phone;
    Uri photoUrl;

    private ArrayList <PaysIndicatif> listeIndicatif = new ArrayList() ;
    private   ArrayAdapter<PaysIndicatif> indicatifMonnaieArrayAdapter;
    private String TAG = "CompteActivity";

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compte);

        //tester la connection reseau
        mAuth = FirebaseAuth.getInstance();

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if(networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()) {
        }
        else{
            Toast toast = Toast.makeText(this, getString(R.string.aucuneConnexion), Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }

        session = new UserSessionManager(getApplicationContext());
        Log.e(TAG, "onCreate: " + session.getKeyEmail() + session.getUserNom() );
        avatarSize =this.getResources().getDimensionPixelSize(R.dimen.pp_size_compte);
        imagePP= (ImageView) findViewById(R.id.compteImageProfil);
        e_displayName = (EditText) findViewById(R.id.comptePrenom);

        e_email= (TextView) findViewById(R.id.compteEmail);
        e_tel= (EditText) findViewById(R.id.compteTel);
        btnMAJpp = (Button) findViewById(R.id.btnChangePhoto);
        btnMAJModif= (Button) findViewById(R.id.btnModif);
        btnMAJtel = (Button) findViewById(R.id.btnChangeTel);
        tel_indicatif= (Spinner) findViewById(R.id.telephoneIndicatif);
        btnLogout = findViewById(R.id.btnLogout);
        btnResetPassword = findViewById(R.id.resetPassword);


        //recupererIndicatif();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        checkUserLog();
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
                Intent i = new Intent(CompteActivity.this, LoginActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                // Add new Flag to start new Activity
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
        });

        btnResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetPassword(mAuth.getCurrentUser().getEmail());
            }
        });



        btnMAJpp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogSelectImage();
            }
        });


        btnMAJtel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateTel(mAuth.getCurrentUser().getEmail(),recupererIndicatifSpinner(tel_indicatif.getSelectedItem().toString())+e_tel.getText().toString());

            }
        });

        btnMAJModif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String erreur=getString(R.string.quelqueChoseMalPasse);
                if (e_displayName.getText().toString().isEmpty()){
                    showDialog(erreur,getString(R.string.champs_nom_non_rempli));
                }

                else{
                    if(mAuth.getCurrentUser() != null){

                        updateDisplayName(String.valueOf(session.getIdUser()),e_displayName.getText().toString());


                    }
                }


            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        //tester la connection reseau

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if(networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()) {
            checkUserLog();

        }
        else{
            Toast toast = Toast.makeText(this, getString(R.string.aucuneConnexion), Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }
    private String recupererIndicatifSpinner(String s){
        return s.substring(s.lastIndexOf("("), s.lastIndexOf(")")+1);
    }

    private String tranformTel(String keyTelephone) {
        String res="";
        if (!keyTelephone.isEmpty()){
            if (!keyTelephone.equals("null")) {
                res = keyTelephone.substring(keyTelephone.lastIndexOf(")")+1,keyTelephone.length());
            }
        }
        return res;
    }
    public void lancerAutocomplete() {

        try

        {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch(
                GooglePlayServicesRepairableException e)

        {
            // TODO: Handle the error.
        } catch(
                GooglePlayServicesNotAvailableException e)

        {
            // TODO: Handle the error.
        }

    }





    private String checkNullString(String str) {
        if (str != null) {
            return str;
        } else {
            return "";
        }
    }

    public void showDialog(String title, String message){
        builder = new AlertDialog.Builder(CompteActivity.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }









    public String transformS(String string){
        String res="";
        if(!string.isEmpty()){
            if(!string.equals("null")) {

                res = string;
            }
        }
        return res;
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }
    public void takePictureCamera(){

        Intent pickPhoto =  new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(pickPhoto , TAKE_PICTURE_CAMERA_REQUEST);

    }

    private  void dialogSelectImage(){
        final CharSequence[] items = {"Prendre une photo", "Selectionner une de mes photos"};
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);

        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Prendre une photo")) {
                    takePictureCamera();
                } else if  (items[item].equals("Selectionner une de mes photos")) {

                    showFileChooser();
                }
            }
        });
        builder.show();
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            filePath = data.getData();

            Glide.with(this).load(filePath).apply(circleCropTransform().placeholder(R.drawable.btn_capture)).transition(withCrossFade()).into(imagePP);
            updatePP(filePath,mAuth.getCurrentUser().getEmail());
            session.refreshSession();

            Picasso.with(this)
                    .load(filePath)
                    .centerCrop()
                    .resize(avatarSize, avatarSize)
                    .placeholder(R.drawable.btn_capture)
                    .transform(new RoundedTransformation())
                    .into(MainActivity.imagePP);

            //Glide.with(this).load(filePath).bitmapTransform(new CropCircleTransformation(this)).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).placeholder(R.drawable.btn_capture).crossFade().into(MainActivity.imagePP);
            if(BuildConfig.DEBUG) {
                Log.e("UPDATEPP", session.getIdUser()+""+session.getUserpp());
            }


        }

        if (requestCode == TAKE_PICTURE_CAMERA_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            filePath = data.getData();

            Glide.with(this).load(filePath).apply(circleCropTransform().placeholder(R.drawable.btn_capture)).transition(withCrossFade()).into(imagePP);
            updatePP(filePath,mAuth.getCurrentUser().getEmail());
            session.refreshSession();

            Picasso.with(this)
                    .load(filePath)
                    .centerCrop()
                    .resize(MainActivity.avatarSize, MainActivity.avatarSize)
                    .placeholder(R.drawable.btn_capture)
                    .transform(new RoundedTransformation())
                    .into(MainActivity.imagePP);
            if(BuildConfig.DEBUG) {
                Log.e("UPDATEPP", session.getIdUser() + "" + session.getUserpp());
            }

        }



    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void updatePP(Uri fileUri, String email){
        I_CompteUser service =
                ServiceGenerator.createService(I_CompteUser.class);

        // https://github.com/iPaulPro/aFileChooser/blob/master/aFileChooser/src/com/ipaulpro/afilechooser/utils/FileUtils.java
        // use the FileUtils to get the actual file by uri
        File file = FileUtils.getFile(this, fileUri);

        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse(getContentResolver().getType(fileUri)),
                        file
                );

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file", file.getName(), requestFile);

        RequestBody r_emailUser =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM,email );

        Call<ReponseServeur> call = service.idUserupdatePP(body,r_emailUser);

        call.enqueue(new Callback<ReponseServeur>() {
            @Override
            public void onResponse(Call<ReponseServeur> call, retrofit2.Response<ReponseServeur> response) {
                if(response.body() != null){
                    if(response.body().isSuccess()) {
                        if(mAuth.getCurrentUser() != null){
                            FirebaseUser user = mAuth.getCurrentUser();
                            Log.i("COPIEIMAGE", "onResponse: "+response.body().getMessage());

                            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                    .setPhotoUri(Uri.parse(response.body().getMessage()))
                                    .build();
                            user.updateProfile(profileUpdates)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                Toast.makeText(CompteActivity.this, getString(R.string.userProfileUpdate), Toast.LENGTH_SHORT).show();
                                            }
                                            else {
                                                Toast.makeText(CompteActivity.this, getString(R.string.userProfileNotUpdate), Toast.LENGTH_SHORT).show();

                                            }
                                        }
                                    });



                        }

                        session.refreshSession();
                    }
                    else {
                        Toast.makeText(CompteActivity.this, getString(R.string.majEchouePP), Toast.LENGTH_SHORT).show();
                    }
                }

                else {
                    Toast.makeText(CompteActivity.this, getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ReponseServeur> call, Throwable t) {

                if(BuildConfig.DEBUG) {
                    Log.e("Upload error:", t.getMessage()+ " "+call.toString());
                }
            }
        });
    }

    private void updateEmail(String idUser, final String email){
        I_CompteUser  iCompteUser = ServiceGenerator.createService(I_CompteUser.class);
        Call<ReponseServeur> call = iCompteUser.updateEmail(idUser,email);
        call.enqueue(new Callback<ReponseServeur>() {
            @Override
            public void onResponse(Call<ReponseServeur> call, retrofit2.Response<ReponseServeur> response) {
                if(response.body() != null){
                    if (response.body().isSuccess()) {
                        Toast.makeText(CompteActivity.this, getString(R.string.majEffectueEmail), Toast.LENGTH_SHORT).show();
                        session.refreshSession();
                    } else {
                        Toast.makeText(CompteActivity.this, getString(R.string.majEchoueEmail), Toast.LENGTH_SHORT).show();
                    }
                }

                else {
                    Toast.makeText(CompteActivity.this, getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ReponseServeur> call, Throwable t) {
                if(BuildConfig.DEBUG) {
                    Log.e("Upload error:", t.getMessage()+ " "+call.toString());
                }
            }
        });
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    private void updateTel(String emailUser, String tel){
        I_CompteUser  iCompteUser = ServiceGenerator.createService(I_CompteUser.class);
        Call<ReponseServeur> call = iCompteUser.updateTel(emailUser,tel);
        call.enqueue(new Callback<ReponseServeur>() {
            @Override
            public void onResponse(Call<ReponseServeur> call, retrofit2.Response<ReponseServeur> response) {
                if(response.body() != null){
                    if(response.body().isSuccess()) {
                        Toast.makeText(CompteActivity.this, getString(R.string.majEffectueTel), Toast.LENGTH_SHORT).show();
                        session.refreshSession();
                    }
                    else {
                        Toast.makeText(CompteActivity.this, getString(R.string.majEchoueTel), Toast.LENGTH_SHORT).show();
                    }
                }

                else {
                    Toast.makeText(CompteActivity.this, getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ReponseServeur> call, Throwable t) {
                if(BuildConfig.DEBUG) {
                    Log.e("Upload error:", t.getMessage()+ " "+call.toString());
                }
            }
        });
    }

    private void updateModif(String idUser,
                             String prenom,
                             String nom,
                             String adresse,
                             String adresseComplet,
                             String codepostal,
                             String ville,
                             String departement,
                             String region,
                             String paysNom,
                             String paysCode,
                             String latitude,
                             String lontitude){
        I_CompteUser  iCompteUser = ServiceGenerator.createService(I_CompteUser.class);
        Call<ReponseServeur> call = iCompteUser.updateModif(idUser,
                prenom,
                nom,
                adresse,
                adresseComplet,
                codepostal,
                ville,
                departement,
                region,
                paysNom,
                paysCode,
                latitude,
                lontitude);

        call.enqueue(new Callback<ReponseServeur>() {
            @Override
            public void onResponse(Call<ReponseServeur> call, retrofit2.Response<ReponseServeur> response) {
                if(response.body() != null){
                    if(response.body().isSuccess()) {
                        Toast.makeText(CompteActivity.this, getString(R.string.majEffectue), Toast.LENGTH_SHORT).show();
                        session.refreshSession();
                    }
                    else {
                        Toast.makeText(CompteActivity.this, getString(R.string.majEchoue), Toast.LENGTH_SHORT).show();
                    }
                }

                else {
                    Toast.makeText(CompteActivity.this, getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ReponseServeur> call, Throwable t) {
                if(BuildConfig.DEBUG) {
                    Log.e("Upload error:", t.getMessage()+ " "+call.toString());
                }
            }
        });

    }
    public String replaceApostrophe(String str){

        String str_=str.replaceAll("'", "\\\\'");
        return str_;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();

        }

        return super.onOptionsItemSelected(item);
    }

    private void recupererIndicatif(){
        I_IndicatifCountry service =
                ServiceGenerator.createService(I_IndicatifCountry.class);

        Call<ResponseIndicatif> call = service.getIndicatif();

        call.enqueue(new Callback<ResponseIndicatif>() {
            @Override
            public void onResponse(Call<ResponseIndicatif> call, retrofit2.Response<ResponseIndicatif> response) {
                if(response.body() != null){

                    if (response.body().isSuccess()){
                        listeIndicatif=response.body().getData();

                        indicatifMonnaieArrayAdapter = new ArrayAdapter<PaysIndicatif>(CompteActivity.this,android.R.layout.simple_spinner_item,listeIndicatif);
                        indicatifMonnaieArrayAdapter.setDropDownViewResource(R.layout.spinner_ajout_e);



                        tel_indicatif.setAdapter(indicatifMonnaieArrayAdapter);
                        tel_indicatif.setSelection(recupererIndexIndicatif());

                        if(BuildConfig.DEBUG){
                            Toast.makeText(CompteActivity.this, listeIndicatif.size()+"", Toast.LENGTH_SHORT).show();


                        }
                    }
                    else{
                        if(BuildConfig.DEBUG){
                            Toast.makeText(CompteActivity.this, "error Indicatif succes False", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                else {
                    Toast.makeText(CompteActivity.this, getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseIndicatif> call, Throwable t) {
                if(BuildConfig.DEBUG) {
                    Toast.makeText(CompteActivity.this, "Error Failure", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private  int recupererIndexIndicatif(){
        int res=0;
        if (!session.getKeyTelephone().isEmpty()){
            for (int i=0; i<listeIndicatif.size();i++){
                if (listeIndicatif.get(i).toString().contains(recupererIndicatifSpinner(session.getKeyTelephone()))){
                    res=i;
                }
            }
        }

        return res;
    }



    private void logout() {

        session.logoutUser();
        if(mAuth.getCurrentUser() != null){
            mAuth.signOut();

        }

    }


    private void resetPassword(String email){
        mAuth.sendPasswordResetEmail(email).addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(CompteActivity.this, getString(R.string.emailResetPasswordSend), Toast.LENGTH_SHORT).show();

                }
                else {
                    Toast.makeText(CompteActivity.this, getString(R.string.erreurServeur), Toast.LENGTH_SHORT).show();

                }
            }
        });
    }


    private void updateDisplayName(String idUser, String displayName){

        I_CompteUser  iCompteUser = ServiceGenerator.createService(I_CompteUser.class);
        Call<ReponseServeur> call = iCompteUser.updateDisplayName(idUser,displayName);
        call.enqueue(new Callback<ReponseServeur>() {
            @Override
            public void onResponse(Call<ReponseServeur> call, retrofit2.Response<ReponseServeur> response) {
                if(response.body() != null){

                    if (response.body().isSuccess()) {

                        UserProfileChangeRequest.Builder u = new UserProfileChangeRequest.Builder()
                                .setDisplayName(e_displayName.getText().toString());
                        mAuth.getCurrentUser().updateProfile(u.build());

                        Toast.makeText(CompteActivity.this, getString(R.string.majEffectue), Toast.LENGTH_SHORT).show();
//                    session.refreshSession();
                    } else {
                        Toast.makeText(CompteActivity.this, getString(R.string.errorUpdate), Toast.LENGTH_SHORT).show();
                    }
                }

                else {
                    Toast.makeText(CompteActivity.this, getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ReponseServeur> call, Throwable t) {
                if(BuildConfig.DEBUG) {
                    Log.e("Upload error:", t.getMessage()+ " "+call.toString());
                }
            }
        });

    }


    private void checkUserLog() {

        if(mAuth.getCurrentUser() != null){


            for (UserInfo profile : mAuth.getCurrentUser().getProviderData()) {
                // Id of the provider (ex: google.com)
                providerId = profile.getProviderId();

                // Name, email address, and profile photo Url
                name = profile.getDisplayName();
                email = profile.getEmail();
                photoUrl = profile.getPhotoUrl();
                phone= profile.getPhoneNumber();




            }

            if((providerId.equals("password"))){
                btnResetPassword.setVisibility(View.VISIBLE);
            }
            if(BuildConfig.DEBUG) {
                Toast.makeText(this, providerId, Toast.LENGTH_LONG).show();
                Log.i("TYPEPROVIDER", providerId);
            }
            RequestOptions requestOptions = new RequestOptions();

            Glide.with(this).load(photoUrl).apply(requestOptions).apply(circleCropTransform()).transition(withCrossFade()).into(imagePP);

            e_displayName.setText(mAuth.getCurrentUser().getDisplayName());

            e_email.setText(mAuth.getCurrentUser().getEmail());

            e_tel.setText(session.getKeyTelephone());

        }
    }
}
