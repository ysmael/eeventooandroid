package com.eeventoo.app.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.eeventoo.app.BuildConfig;
import com.eeventoo.app.InterfaceRetrofit.I_Avis;
import com.eeventoo.app.R;
import com.eeventoo.app.ViewPagerAdapter;
import com.eeventoo.app.class_.ReponseServeur;
import com.eeventoo.app.class_.ServiceGenerator;
import com.eeventoo.app.fragment.FragmentAvis;
import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class DescriptionEvent extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {
    private TextView Type;
    private TextView Adresse;
    private TextView CodePostal;
    private TextView Ville;
    private TextView DateDeb;
    private TextView DateFin;
    private TextView Telephone;
    private TextView Tarif;
    private TextView Description;
    private ImageView imageView;
    FloatingActionButton fab, fabScan;
    LayoutInflater inflater;
    private EditText text_avis;
    CoordinatorLayout coordinatorLayout;
    private FloatingActionButton fab_dialog_avis;
    private RatingBar ratingBar;
    View view;
    private static Context mContext;
    String nom;
    String idOfUser;
    float rating = 0;
    String idEvent;
    int year_x,month_x, day_x,heure_x, min_x;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    String nbetoiles;
    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;
    private ImageLoader imageLoader;
    private GoogleMap mMap;
    double latitude;
    double  lontitude;
    private   String type;
    private String adresse;
    private String codepostal;
    private String ville;
    private String datedeb;
    private String datefin;
    private String telephone;
    private String typeTarif;
    private String description;
    private Button btnVoirPrix;
    private Boolean bool_Payant;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description_event);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fabScan = findViewById( R.id.fabScan);
        inflater=DescriptionEvent.this.getLayoutInflater();
        FragmentAvis fragmentAvis=new FragmentAvis();
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorDesc);

        /*
        * recupere la date et lheure actuelle
        * */

        ButterKnife.bind(this);
        final Calendar calendar = Calendar.getInstance();

        //tester la connection reseau

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if(networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()) {
        }
        else{
            Toast toast = Toast.makeText(this, getString(R.string.aucuneConnexion), Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }

        year_x = calendar.get(Calendar.YEAR);
        month_x=calendar.get(Calendar.MONTH)+1;
        day_x=calendar.get(Calendar.DAY_OF_MONTH);
        heure_x=calendar.get(Calendar.HOUR_OF_DAY);
        min_x=calendar.get(Calendar.MINUTE);

        /*
        * Recupere les extras donner par le recyclerview
        * */
        idEvent =getIntent().getExtras().getString("id");
        nom  =getIntent().getExtras().getString("nom");
        type =getIntent().getExtras().getString("type");
        adresse=getIntent().getExtras().getString("adresse");
        codepostal=getIntent().getExtras().getString("codepostal");
        ville=getIntent().getExtras().getString("ville");
        datedeb=getIntent().getExtras().getString("datedeb");
        datefin=getIntent().getExtras().getString("datefin");
        telephone =getIntent().getExtras().getString("telephone");
        typeTarif =getIntent().getExtras().getString("typeTarif");
        description=getIntent().getExtras().getString("description");
        latitude =getIntent().getExtras().getDouble("latitude");
        lontitude=getIntent().getExtras().getDouble("lontitude");
        final String imageURL=getIntent().getExtras().getString("image");
        idOfUser=getIntent().getExtras().getString("idOfUser");
        //bool_Payant =getIntent().getExtras().getBoolean("boolTypeTarif");

        Calendar calDeb = Calendar.getInstance();
        calDeb.set(Calendar.DAY_OF_MONTH,Integer.valueOf(datedeb.substring(0,2)));
        calDeb.set(Calendar.MONTH,Integer.valueOf(datedeb.substring(3,5))-1);
        calDeb.set(Calendar.YEAR,Integer.valueOf(datedeb.substring(6,10)));
        calDeb.set(Calendar.HOUR_OF_DAY,Integer.valueOf(datedeb.substring(11,13)));
        calDeb.set(Calendar.MINUTE,Integer.valueOf(datedeb.substring(14,16)));




        Calendar calFin = Calendar.getInstance();
        calFin.set(Calendar.DAY_OF_MONTH,Integer.valueOf(datefin.substring(0,2)));
        Log.i("TEST", "onCreate: "+Integer.valueOf(datefin.substring(3,5)));
        calFin.set(Calendar.MONTH,Integer.valueOf(datefin.substring(3,5))-1);
        calFin.set(Calendar.YEAR,Integer.valueOf(datefin.substring(6,10)));
        calFin.set(Calendar.HOUR_OF_DAY,Integer.valueOf(datefin.substring(11,13)));
        calFin.set(Calendar.MINUTE,Integer.valueOf(datefin.substring(14,16)));

        btnVoirPrix = findViewById(R.id.btnVoirPrix);

        btnVoirPrix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(DescriptionEvent.this, TarifActivity.class);
                intent.putExtra("idEvent",idEvent);
                intent.putExtra("nomEvent",nom);
                startActivity(intent);
            }
        });

        Toolbar toolbar =   (Toolbar) findViewById(R.id.toolbar_desc);
        Type = (TextView) findViewById(R.id.TypeE_desc);
        Type.setText(getString(R.string.typeDescription)+" "+translateTypeEvenement(type));

        Adresse= (TextView) findViewById(R.id.Adresse_Desc);
        Adresse.setText(adresse);

        Ville=(TextView) findViewById(R.id.villeE_Desc);
        Ville.setText(getString(R.string.affiche_villeDescription)+" "+ville);

        CodePostal=(TextView) findViewById(R.id.CodeP_Desc);
        CodePostal.setText(getString(R.string.affiche_cpDescription)+codepostal);

        DateDeb=(TextView) findViewById(R.id.dateDebE_Desc);
        DateDeb.setText(getString(R.string.affiche_duDescription)+" "+outPutDate(calDeb)+ " "+ outPutTime(calDeb));

        DateFin=(TextView) findViewById(R.id.dateFinE_Desc);
        DateFin.setText(getString(R.string.affiche_auDescription)+" "+outPutDate(calFin)+ " "+ outPutTime(calFin));

        Telephone=(TextView) findViewById(R.id.telephoneE_Desc);

        if (!telephone.equals("NR")){
        Telephone.setText(getString(R.string.affiche_telDescription)+" "+telephone);
        }
        else{
            Telephone.setVisibility(View.GONE);
        }
        Tarif=(TextView) findViewById(R.id.tarifE_Desc);
        Tarif.setText(getString(R.string.affiche_tarifDescription)+" "+translateTypeTarif(typeTarif));

        if (BuildConfig.DEBUG){
            Log.i("TYPETARIF", typeTarif);

        }

        Description=(TextView) findViewById(R.id.descE_Desc);
        Description.setText(getString(R.string.affiche_descDescription)+" "+ description);

        //imageLoader = CustomVolleyRequest.getInstance(this).getImageLoader();
        // imageLoader.get(imageURL, ImageLoader.getImageListener(imageView,0,0));

        imageView= (ImageView) this.findViewById(R.id.backgroundImageView_desc);
        //imageView.setImageUrl(imageURL,imageLoader);



        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //scan fab

        fabScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(DescriptionEvent.this, ScanReader.class);
                startActivity(i);
            }
        });

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true);
        Glide.with(this).load(imageURL).apply(requestOptions).transition(withCrossFade()).into(imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(DescriptionEvent.this, activity_image_event.class);
                myIntent.putExtra("nom", nom);
                myIntent.putExtra("image",imageURL);
                startActivity(myIntent);
            }
        });
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar_desc);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.CollapsedAppBar);
        collapsingToolbarLayout.setTitle(nom);

        // collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(DescriptionEvent.this,CommentsActivity.class);
                i.putExtra("id",idEvent);
                startActivity(i);
            }
        });





        // BackgroundTaskGet_10_avis backgroundTaskGet_10_avis2 = new BackgroundTaskGet_10_avis(fragmentAvis.getActivity());
        //backgroundTaskGet_10_avis2.execute("getavis");
      /*  viewPager =(ViewPager) findViewById(R.id.viewPageAvis);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(fragmentAvis,"");
        viewPager.setAdapter(viewPagerAdapter);
*/
        //evoie des extras au fragment Fragment_avis
        Bundle extraToFragment_Avis = new Bundle();
        extraToFragment_Avis.putString("IdEvent", idEvent);

        fragmentAvis.setArguments(extraToFragment_Avis);


    }

    @Override
    protected void onResume() {
        super.onResume();
        //tester la connection reseau

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if(networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()) {
        }
        else{
            Toast toast = Toast.makeText(this, getString(R.string.aucuneConnexion), Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_desc_event, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        this.view=view;
        switch (view.getId()){
            case R.id.fab:

                view=inflater.inflate(R.layout.dialog_add_avis,null);

                builder=new AlertDialog.Builder(DescriptionEvent.this);
                ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);

                text_avis= (EditText) view.findViewById(R.id.editext_add_avis);
                ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                    @Override
                    public void onRatingChanged(RatingBar ratingBar, float rating, boolean b) {
                        //Toast.makeText(getApplicationContext(),"Your Selected Ratings  : " + String.valueOf(rating),Toast.LENGTH_LONG).show();
                    }
                });


                builder.setView(view);
                builder.setTitle("Ajoutez votre avis");
                builder.setPositiveButton("Ajouter ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {


                    }
                });
                builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                dialog = builder.create();
                dialog.show();
                // builder.show();

                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Boolean wantToCloseDialog = false;

                        if(wantToCloseDialog)
                            dialog.dismiss();
                        if (!text_avis.getText().toString().equals("")) {
                            rating=ratingBar.getRating();
                            if(BuildConfig.DEBUG) {
                                Toast.makeText(getApplicationContext(), "Your Selected Ratings  : " + String.valueOf(rating), Toast.LENGTH_LONG).show();
                                Log.i("BOUTTON AVIS", "idOfUser " + idOfUser + " id :" + "" + idEvent + "date : " + year_x + "/" + month_x + "/" + day_x + " " + heure_x + ":" + min_x + ":00 " + "nb etoiles: " + String.valueOf(rating));
                            }
                            addAvis(idOfUser,""+idEvent,text_avis.getText().toString(),year_x+"/"+month_x+"/"+day_x+" "+heure_x+":"+min_x+":00",String.valueOf(rating));

                        }
                        else {
                            // Snackbar snackbar = Snackbar.make(getCurrentFocus(),"Remplissez tous les champs svp ...",1500);
                            // snackbar.show();
                            Toast.makeText(DescriptionEvent.this, "Remplissez tous les champs svp ...", Toast.LENGTH_SHORT).show();
                        }

                    }
                });
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        Boolean wantToCloseDialog = true;

                        if(wantToCloseDialog)
                            dialog.dismiss();
                        //else dialog stays open. Make sure you have an obvious way to close the dialog especially if you set cancellable to false.
                    }
                });


                break;
        }
    }
    public void effacer(AlertDialog.Builder bld){
        bld.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                dialogInterface.dismiss();
            }
        });
    }

    /*
    @OnClick(R.id.fab_record)
    public void recordClick(){
        int[] startingLocation = new int[2];
        fab_record.getLocationOnScreen(startingLocation);
        startingLocation[0] += fab_record.getWidth() / 2;
        TakePhotoActivity.startCameraFromLocation(startingLocation, this);

    }
    */
    public String typeTarifText(String typeTarif, String tarif){
        String res = "";
        if (typeTarif.equals("Gratuit")){
            res= typeTarif;
        }
        else if (typeTarif.equals("Payant")) {
            res= typeTarif+"/"+tarif;
        }
        return  res;
    }

    private  void addAvis(String idUser,String idEvent,String avis,String date_avis, String nbetoiles) {
        I_Avis i_avis = ServiceGenerator.createService(I_Avis.class);

        Call<ReponseServeur> call = i_avis.addLike(idUser,idEvent,avis,date_avis,nbetoiles);
        call.enqueue(new Callback<ReponseServeur>() {
            @Override
            public void onResponse(Call<ReponseServeur> call, retrofit2.Response<ReponseServeur> response) {
                  if(response.body() != null){

                      if (response.body().isSuccess()) {
                          dialog.dismiss();
                          Toast.makeText(DescriptionEvent.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                      }
                      else
                      {
                          if(BuildConfig.DEBUG){
                              Toast.makeText(DescriptionEvent.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                          }
                          Toast.makeText(DescriptionEvent.this, getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();

                      }
                }

                else {
                    Toast.makeText(DescriptionEvent.this, getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ReponseServeur> call, Throwable t) {
                Toast.makeText(DescriptionEvent.this, "Erreur d'ajout  "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public String translateTypeEvenement(String type){
        String strReturn = null;


        if (type.equals("Soirées")){
            strReturn=  getString(R.string.soirees);
        }
        else if (type.equals("Concerts")){
            strReturn=getString(R.string.concerts);
        }
        else if (type.equals("Films")){
            strReturn=getString(R.string.film);
        }
        else if (type.equals("Spectacles")){
            strReturn=getResources().getString(R.string.spectacles);
        }
        else if (type.equals("Festivals")){
            strReturn=getResources().getString(R.string.festivals);
        }

        return strReturn;


    }

    public String translateTypeTarif(String typeTarif) {
        String strReturn = null;
        if (BuildConfig.DEBUG){
            Log.i("TYPETARIF2", typeTarif);

        }
        if (typeTarif.contains("y")) {
            strReturn = getString(R.string.payant);
            btnVoirPrix.setVisibility(View.VISIBLE);

        } else if (typeTarif.contains("G")) {
            strReturn = getResources().getString(R.string.gratuit);
        }

        return strReturn;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney, Australia, and move the camera.
        LatLng lieu = new LatLng(latitude, lontitude);
        //LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(lieu));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(lieu));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(lieu, 12.0f));
    }

    public String outPutDate(Calendar calendar){

        int style = DateFormat.MEDIUM;

        DateFormat df =  DateFormat.getDateInstance(style,getResources().getConfiguration().locale);;
        return df.format(calendar.getTime());
    }

    public String outPutTime(Calendar calendar){
        String pattern = "HH:mm";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat (pattern);
        return   simpleDateFormat.format(calendar.getTime());
    }
}
