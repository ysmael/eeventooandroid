package com.eeventoo.app.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.eeventoo.app.Adapter.AdapterFormule;
import com.eeventoo.app.BuildConfig;
import com.eeventoo.app.InterfaceRetrofit.I_Event;
import com.eeventoo.app.InterfaceRetrofit.I_PaysMonnaie;
import com.eeventoo.app.R;
import com.eeventoo.app.class_.Formule;
import com.eeventoo.app.class_.PaysMonnaie;
import com.eeventoo.app.class_.ReponseServeur;
import com.eeventoo.app.class_.ResponseListIdEvent;
import com.eeventoo.app.class_.ResponsePaysMonnaie;
import com.eeventoo.app.class_.ServiceGenerator;
import com.eeventoo.app.fragment.FormuleTarifFragment;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FormuleActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private FormuleTarifFragment.OnListFragmentInteractionListener mListener;
    private ArrayList<Formule> formules = new ArrayList<>();
    public AdapterFormule adapterFormule;

    private  ArrayList<Integer> listIdEvent = new ArrayList<>();
    private String idGroupEvent;
    private String idUser;
    private boolean bool_payant;
    private Spinner  spinner_devise;
    private ArrayList <PaysMonnaie> listeMonnaie = new ArrayList() ;
    private   ArrayAdapter<PaysMonnaie> paysMonnaieArrayAdapter;
    String requete = "";
    String requeteUpdateActive = "";
    ProgressBar progressBar;
    AlertDialog.Builder builder;
    private int idOperation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formule);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.ajouterTarif));

        idGroupEvent =getIntent().getExtras().getString("idGroupEvent");
        idUser =getIntent().getExtras().getString("idUser");

        retrieveIdEvent(idUser,idGroupEvent);

        formules = populateList();
        adapterFormule= new AdapterFormule(FormuleActivity.this,formules);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        progressBar = (ProgressBar) findViewById(R.id.progressBar1);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adapterFormule.addItem();
            }
        });


        recyclerView=findViewById(R.id.recyclerFormule);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);


        recyclerView.setAdapter(adapterFormule);
        spinner_devise= (Spinner)findViewById(R.id.spinner_devise);
        recupererMonnaie();


        spinner_devise.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                return false;
            }
        });

        spinner_devise.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) adapterView.getChildAt(0)).setTextColor(Color.WHITE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.saveButton) {
            formules=adapterFormule.getListFormules();
            Log.i("FormuleActivity", "onCreate: formules size : "+ formules.size() + "list Event: "+ listIdEvent.size());
            for (int i = 0; i< listIdEvent.size();i++){
                requeteUpdateActive += "UPDATE `evenement_tb` SET `active`=1 where id="+listIdEvent.get(i).toString()+"; ";
                for (int j=0; j<formules.size();j++){

                    requete += "INSERT INTO `formulesEvent`(`idEvent`, `description`, `prix`, `devise`) VALUES ("+listIdEvent.get(i).toString()+",'"+
                            formules.get(j).getDescription()+"','"+
                            formules.get(j).getPrix()+"','"+
                            recupererSymboleMonnaieSpinner(spinner_devise.getSelectedItem().toString())+"') ;";
                }
            }

            addFormules(requeteUpdateActive + requete);
        }
        Log.i("FORMULEREQUETE", requeteUpdateActive + requete );
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_save_formules, menu);

        return true;
    }

    private ArrayList<Formule> populateList(){

        ArrayList<Formule> list = new ArrayList<>();

        for(int i = 0; i < 1; i++){
            Formule formule = new Formule();
            list.add(formule);
        }

        return list;
    }

    public void retrieveIdEvent(String idUser,String idGroupEvent){

        I_Event i_event = ServiceGenerator.createService(I_Event.class);
        Call<ResponseListIdEvent> call = i_event.getIdEvent(idUser,idGroupEvent);

        call.enqueue(new Callback<ResponseListIdEvent>() {
            @Override
            public void onResponse(Call<ResponseListIdEvent> call, Response<ResponseListIdEvent> response) {
                if(response.body() != null){
                    if(response.body().isSuccess()){
                        //Toast.makeText(FormuleActivity.this, "Response Success", Toast.LENGTH_SHORT).show();
                        listIdEvent=response.body().getData();

                    }
                    else
                    {
                        Toast.makeText(FormuleActivity.this,getString(R.string.recupDeviseEchoue) , Toast.LENGTH_SHORT).show();

                    }
                }

                else {
                    Toast.makeText(FormuleActivity.this, getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                }



            }

            @Override
            public void onFailure(Call<ResponseListIdEvent> call, Throwable t) {
                if(BuildConfig.DEBUG) {
                    Log.e("LikeLIST", t.getMessage());
                }
            }
        });
    }

    private String recupererSymboleMonnaieSpinner(String s){

        String res= s.substring(s.lastIndexOf("("));

        return  res;
    }

    public void hideProgressLoad(){
        progressBar.setVisibility(View.GONE);
    }

    public void showProgressLoad(){
        progressBar.setVisibility(View.VISIBLE);
    }

    private void recupererMonnaie(){
        I_PaysMonnaie service = ServiceGenerator.createService(I_PaysMonnaie.class);
        Call<ResponsePaysMonnaie> call = service.getMonnaie();

        call.enqueue(new Callback<ResponsePaysMonnaie>() {
            @Override
            public void onResponse(Call<ResponsePaysMonnaie> call, Response<ResponsePaysMonnaie> response) {
                if(response.body() != null){
                    if (response.body().isSuccess()){
                        listeMonnaie=response.body().getData();

                        paysMonnaieArrayAdapter = new ArrayAdapter<PaysMonnaie>(FormuleActivity.this,android.R.layout.simple_spinner_item,listeMonnaie);
                        paysMonnaieArrayAdapter.setDropDownViewResource(R.layout.spinner_ajout_e);


                        spinner_devise.setAdapter(paysMonnaieArrayAdapter);
                        if(BuildConfig.DEBUG){
                            Toast.makeText(FormuleActivity.this, listeMonnaie.size()+"", Toast.LENGTH_SHORT).show();
                            Log.i("LIST_INDICATIF", listeMonnaie.toString());


                        }
                    }
                    else{
                        if(BuildConfig.DEBUG){
                            Toast.makeText(FormuleActivity.this, "error Indicatif succes False", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                else {
                    Toast.makeText(FormuleActivity.this, getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponsePaysMonnaie> call, Throwable t) {
                if(BuildConfig.DEBUG) {
                    Toast.makeText(FormuleActivity.this, "Error Failure", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void addFormules(String requete){
        I_Event i_event= ServiceGenerator.createService(I_Event.class);
        Call<ReponseServeur>  call = i_event.setFormuleEvent(requete);
        //Displaying Progressbar
        showProgressLoad();
        call.enqueue(new Callback<ReponseServeur>() {
                         @Override
                         public void onResponse(Call<ReponseServeur> call, Response<ReponseServeur> response) {
                             hideProgressLoad();
                             if(response.body() != null){
                                 if(response.body().isSuccess()){

                                     Toast.makeText(FormuleActivity.this, getString(R.string.evenement_ajoute), Toast.LENGTH_SHORT).show();


                                     Intent intent = new Intent(FormuleActivity.this, MainActivity.class);
                                     intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                     startActivity(intent);
                                 }
                                 else {
                                     Toast.makeText(FormuleActivity.this, getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();

                                 }
                             }

                             else {
                                 Toast.makeText(FormuleActivity.this, getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                             }

                         }

                         @Override
                         public void onFailure(Call<ReponseServeur> call, Throwable t) {
                             hideProgressLoad();
                             if(BuildConfig.DEBUG) {
                                 Toast.makeText(FormuleActivity.this, "Error Failure", Toast.LENGTH_SHORT).show();
                             }
                         }
                     }
        );
    }

    @Override
    public void onBackPressed() {
        builder = new AlertDialog.Builder(FormuleActivity.this);
        builder.setTitle(getString(R.string.ooops));
        builder.setMessage(getString(R.string.vosEvenementsSupprimes));
        builder.setPositiveButton(getString(R.string.affiche_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(FormuleActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
        builder.setNegativeButton(getString(R.string.annuler), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
