package com.eeventoo.app.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.eeventoo.app.BuildConfig;
import com.eeventoo.app.InterfaceRetrofit.I_Authentification;
import com.eeventoo.app.R;
import com.eeventoo.app.class_.ReponseLogin;
import com.eeventoo.app.class_.ReponseRegister;
import com.eeventoo.app.class_.ServiceGenerator;
import com.eeventoo.app.class_.UserSessionManager;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;

import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;
import static com.bumptech.glide.request.RequestOptions.circleCropTransform;
import static com.eeventoo.app.class_.Constants.USER_EEVENTOO;

public class LoginActivity extends Activity {
    private static final int RC_SIGN_IN = 1 ;
    private static final int FACEBOOK_SIGN_IN = 2;
    SharedPreferences mPreferences;
    EditText mp,email;
    TextView signup_text,passwordForgot;
    Button buttonLogin;
    Button buttonGoogleLogin;
    Button buttonFacebookLogin;
    AlertDialog.Builder builder;
    UserSessionManager session;
    ProgressDialog progressDialog;
    SignInButton signInButton ;
    private GoogleSignInClient mGoogleSignInClient;
    private FirebaseAuth mAuth;
    FirebaseAuth.AuthStateListener mAuthStateListener;
    private CallbackManager mCallbackManager;
    private static final  String TAG = "FACELOG";
    String providerId,displayName;
    Uri photoUrl;


    @Override
    protected void onStart() {
        super.onStart();

        mAuth.addAuthStateListener(mAuthStateListener);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email = (EditText) findViewById(R.id.email_login);
        mp = (EditText) findViewById(R.id.password_login);
        signup_text = (TextView) findViewById(R.id.signup);
        buttonLogin = (Button) findViewById(R.id.login_button);
        buttonGoogleLogin = findViewById(R.id.sign_in_button);
        buttonFacebookLogin = findViewById(R.id.login_fb_button);
        passwordForgot = findViewById(R.id.passwordForgot);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getString(R.string.affiche_connection));

        mAuth = FirebaseAuth.getInstance();

        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {



                if(firebaseAuth.getCurrentUser() != null){

                    for (UserInfo profile : firebaseAuth.getCurrentUser().getProviderData()) {
                        // Id of the provider (ex: google.com)
                        providerId = profile.getProviderId();

                        // Name, email address, and profile photo Url
                        String name = profile.getDisplayName();
                        String email = profile.getEmail();
                        photoUrl = profile.getPhotoUrl();

                    }


                    if( providerId.equals("password")){


                        if(firebaseAuth.getCurrentUser().isEmailVerified()){
                            Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                        else
                        {
                            Toast.makeText(LoginActivity.this, getString(R.string.verifierVotreEmail), Toast.LENGTH_LONG).show();
                        }

                    }







                }
            }
        };

        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();


        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        buttonGoogleLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showProgressLoad();
                signIn();
            }
        });
        // User Session Manager
        session = new UserSessionManager(getApplicationContext());

        if(session.isUserLoggedIn()){
            Intent i = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(i);
        }


 

        signup_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = (new Intent(LoginActivity.this, Register.class));
                startActivity(intent);
            }
        });

        passwordForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = (new Intent(LoginActivity.this, PasswordResetActivity.class));
                startActivity(intent);
            }
        });

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mp.getText().toString().equals("") || email.getText().toString().equals("")) {

                    builder = new AlertDialog.Builder(LoginActivity.this);
                    builder.setTitle(getString(R.string.quelqueChoseMalPasse));
                    builder.setMessage(getString(R.string.remplissezToutChamp));
                    builder.setPositiveButton(getString(R.string.affiche_ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                } else {
                    String emailSansEspace=email.getText().toString().replaceAll(" ","");
                    if (isValidEmail(emailSansEspace)) {
                        loginEmailFirebase(emailSansEspace,mp.getText().toString());
                        showProgressLoad();

                    }else {
                        builder = new AlertDialog.Builder(LoginActivity.this);
                        builder.setTitle(getString(R.string.quelqueChoseMalPasse));
                        builder.setMessage(getString(R.string.emailIncorrect));
                        builder.setPositiveButton(getString(R.string.affiche_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                    }


                }
            }
        });


        mCallbackManager = CallbackManager.Factory.create();
        buttonFacebookLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("email","public_profile"));
                LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "facebook:onSuccess:" + loginResult);
                        }
                        showProgressLoad();
                        handleFacebookAccessToken(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {
                        hideProgressLoad();

                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "facebook:onCancel");
                        }
                    }

                    @Override
                    public void onError(FacebookException error) {
                        hideProgressLoad();

                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "facebook:onError", error);

                        }
                        // ...
                    }
                });
            }
        });




    }


    @Override
    public void onBackPressed() {
    }

    private void handleFacebookAccessToken(AccessToken token) {


        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "signInWithCredential:success" );
                            }
                            FirebaseUser user = mAuth.getCurrentUser();


                            updateUI(user);


                        } else {

                            hideProgressLoad();
                            // If sign in fails, display a message to the user.
                            if (BuildConfig.DEBUG) {
                                Log.w(TAG, "signInWithCredential:failure", task.getException());
                            }
                            Toast.makeText(LoginActivity.this, getString(R.string.authenticationFailed),
                                    Toast.LENGTH_LONG).show();

                        }

                        // ...
                    }
                });
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    //    private void login(String email, String mp){
//        progressDialog = new ProgressDialog(LoginActivity.this);
//        progressDialog.show();
//        progressDialog.setMessage(getString(R.string.affiche_connection));
//        progressDialog.setCancelable(false);
//        progressDialog.setCanceledOnTouchOutside(false);
//
//        I_Authentification i_authentification= ServiceGenerator.createService(I_Authentification.class);
//        Call<ReponseLogin> call = i_authentification.checkLogin(email,mp);
//        call.enqueue(new Callback<ReponseLogin>() {
//            @Override
//            public void onResponse(Call<ReponseLogin> call, Response<ReponseLogin> response) {
//                progressDialog.dismiss();
//                Log.i("Login", "onResponse/ "+ response.body().getMessage());
//                if (response.body().isSuccess()) {
//                    int id = response.body().getId();
//                    String prenomU =  response.body().getPrenomU();
//                    String nomU= response.body().getNomU();
//                    String ppU= response.body().getPpU();
//                    String emailOfUser= response.body().getEmailOfUser();
//                    String adresseU= response.body().getAdresseU();
//                    String cpU= response.body().getCpU();
//                    String villeU= response.body().getVilleU();
//                    String telU= response.body().getTelU();
//
//                    session.createUserLoginSession(id,emailOfUser,nomU,prenomU,ppU,adresseU,cpU,villeU,telU,USER_EEVENTOO);
//                    Intent intent = new Intent(LoginActivity.this,MainActivity.class);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(intent);
//
//                }
//                else  {
//                    String msg ;
//                    Integer status =response.body().getStatus();
//                    if (status==0){
//                        msg=getString(R.string.emailNonConfirmer);
//                    }
//                    else{
//
//                        msg=getString(R.string.loginEchoue);
//                    }
//                    showDialog(getString(R.string.loginEchoue),msg);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ReponseLogin> call, Throwable t) {
//                String title=getString(R.string.ooops);
//                String message =getString(R.string.erreurServeur);
//                builder = new AlertDialog.Builder(LoginActivity.this);
//                builder.setTitle(title);
//                builder.setMessage(message);
//                builder.setPositiveButton(getString(R.string.affiche_ok), new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        dialogInterface.dismiss();
//                        progressDialog.dismiss();
//                    }
//                });
//                AlertDialog alertDialog = builder.create();
//                alertDialog.show();
//            }
//        });
//    }
    public void showDialog(String title, String message){
        builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.affiche_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }



    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                hideProgressLoad();

                // Google Sign In failed, update UI appropriately
                Log.w("TAG", "Google sign in failed", e);
                Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());

                Toast.makeText(this, getString(R.string.quelqueChoseMalPasseAuth), Toast.LENGTH_SHORT).show();

            }
        }

        mCallbackManager.onActivityResult(requestCode, resultCode, data);

    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("TAG", "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();



                            updateUI(user);


                        } else {
                            hideProgressLoad();
                            // If sign in fails, display a message to the user.
                            Log.w("TAG", "signInWithCredential:failure", task.getException());
                            Toast.makeText(LoginActivity.this, getString(R.string.authenticationFailed), Toast.LENGTH_LONG).show();

                        }

                    }
                });

    }

    private void updateUI(FirebaseUser user) {

        if( mAuth.getCurrentUser() != null){

            for (UserInfo profile : mAuth.getCurrentUser().getProviderData()) {
                // Id of the provider (ex: google.com)
                providerId = profile.getProviderId();

                // Name, email address, and profile photo Url
                displayName = profile.getDisplayName();
                photoUrl = profile.getPhotoUrl();

            }

            if(providerId.equals("google.com") || providerId.equals("facebook.com")){

                initUserFirebaseGoogleFacebook(user.getEmail(), user.getDisplayName(), photoUrl.toString());

            }
            else{
                if( mAuth.getCurrentUser().isEmailVerified()){
                    initUserFirebase(user.getEmail());

                }
            }


        }




    }


    private void loginEmailFirebase(String email,String password){

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            if( user != null){
                                if (user.isEmailVerified()){
                                    updateUI(user);
                                }
                                else{
                                    hideProgressLoad();
                                    Toast.makeText(LoginActivity.this, getText(R.string.verifierVotreEmail), Toast.LENGTH_SHORT).show();
                                }
                            }

                        } else {
                            hideProgressLoad();
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(LoginActivity.this, getString(R.string.authenticationFailed),
                                    Toast.LENGTH_LONG).show();
                        }

                        // ...
                    }
                });

    }





    private void checkLinkEmail (){


        Intent intent = getIntent();
        String emailLink = intent.getData().toString();

// Confirm the link is a sign-in with email link.
        if (mAuth.isSignInWithEmailLink(emailLink)) {
            // Retrieve this from wherever you stored it
            String email = "someemail@domain.com";

            // The client SDK will parse the code from the link for you.
            mAuth.signInWithEmailLink(email, emailLink)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Log.d(TAG, "Successfully signed in with email link!");
                                AuthResult result = task.getResult();
                                // You can access the new user via result.getUser()
                                // Additional user info profile *not* available via:
                                // result.getAdditionalUserInfo().getProfile() == null
                                // You can check if the user is new or existing:
                                // result.getAdditionalUserInfo().isNewUser()
                            } else {
                                Log.e(TAG, "Error signing in with email link", task.getException());
                            }
                        }
                    });
        }
    }


    private void initUserFirebase(  String email){



        I_Authentification iAuthentification =ServiceGenerator.createService(I_Authentification.class);
        Call<ReponseRegister> call = iAuthentification.initUserFirebase(email);
        call.enqueue(new Callback<ReponseRegister>() {
            @Override
            public void onResponse(Call<ReponseRegister> call, Response<ReponseRegister> response) {
                  if(response.body() != null){
                      if (response.body().isSuccess()) {

                          Toast.makeText(LoginActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                          String idUser = response.body().getMessage();
                          session.createUserLoginSession(Integer.parseInt(idUser),email );

                          hideProgressLoad();

                          Toast.makeText(LoginActivity.this, getString(R.string.affiche_bienvenue)+" "+ mAuth.getCurrentUser().getEmail(), Toast.LENGTH_LONG).show();

                          Intent intent = new Intent(LoginActivity.this,MainActivity.class);

                          intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                          intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                          startActivity(intent);



                      }
                      else {
                          showDialog(getString(R.string.inscriptionEchoue),response.body().getMessage());
                      }
                }

                else {
                    Toast.makeText(LoginActivity.this, getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ReponseRegister> call, Throwable t) {
                String title=getString(R.string.ooops);
                String message =getString(R.string.erreurServeur);

            }
        });

    }


    private void initUserFirebaseGoogleFacebook(  String email, String displayName , String photoUrl){



        I_Authentification iAuthentification =ServiceGenerator.createService(I_Authentification.class);
        Call<ReponseRegister> call = iAuthentification.initUserFirebaseGoogleFacebook(email,displayName,photoUrl);
        call.enqueue(new Callback<ReponseRegister>() {
            @Override
            public void onResponse(Call<ReponseRegister> call, Response<ReponseRegister> response) {
                  if(response.body() != null){

                      if (response.body().isSuccess()) {

                          if(BuildConfig.DEBUG) {

                              Toast.makeText(LoginActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                          }
                          String idUser = response.body().getMessage();
                          session.createUserLoginSession(Integer.parseInt(idUser),email );

                          hideProgressLoad();

                          Toast.makeText(LoginActivity.this, getString(R.string.affiche_bienvenue)+" "+ mAuth.getCurrentUser().getEmail(), Toast.LENGTH_LONG).show();

                          Intent intent = new Intent(LoginActivity.this,MainActivity.class);

                          intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                          intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                          startActivity(intent);



                      }
                      else {
                          hideProgressLoad();
                          if(mAuth.getCurrentUser() != null){
                              mAuth.signOut();

                          }
                          showDialog(getString(R.string.inscriptionEchoue),response.body().getMessage());
                      }
                }

                else {
                      hideProgressLoad();

                      Toast.makeText(LoginActivity.this, getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ReponseRegister> call, Throwable t) {
                String title=getString(R.string.ooops);
                String message =getString(R.string.erreurServeur);

            }
        });

    }


    public void hideProgressLoad(){
        progressDialog.dismiss();
    }

    public void showProgressLoad(){

        progressDialog.show();    }


}
