package com.eeventoo.app.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;

import com.eeventoo.app.class_.ReponseRegister;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import androidx.core.view.GravityCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.request.RequestOptions;
import com.eeventoo.app.BuildConfig;
import com.eeventoo.app.InterfaceRetrofit.I_Authentification;
import com.eeventoo.app.class_.ReponseLogin;
import com.eeventoo.app.class_.ServiceGenerator;
import com.eeventoo.app.fragment.FavorisFragment;
import com.eeventoo.app.fragment.Fragment_Accueil;
import com.eeventoo.app.R;
import com.eeventoo.app.class_.User;
import com.eeventoo.app.class_.UserSessionManager;
import com.eeventoo.app.ViewPagerAdapter;
import com.eeventoo.app.outils.RoundedTransformation;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.AccessToken;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;
import static com.bumptech.glide.request.RequestOptions.circleCropTransform;
import static com.eeventoo.app.class_.Constants.USER_EEVENTOO;
import static com.eeventoo.app.class_.Constants.USER_FACEBOOK;
import static com.eeventoo.app.class_.Constants.USER_GOOGLE;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {


    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;
    private RecyclerView rv;

    private DrawerLayout mDrawerLayout;
    User user;
    String nomUser;
    // User Session Manager Class
    UserSessionManager session;
    private Spinner spinnerTrierPar;
    private Spinner spinnerType;
    private int numberSaveTrierPar = 0;
    private int numberSaveType = 0;
    private Button buttonSearchDrawer;
    private EditText searchText;
    NavigationView navigationView;
    private TextView drawerloginText;
    private RelativeLayout relativeHeaderDrawer;
    private int idOfUser ;
    private  String email_ ;
    private String motDepasse_;
    private AlertDialog.Builder builder;
    private String url_pp;
    public static int avatarSize ;
    public static ImageView imagePP;
    MenuItem deconnexionItem;
    MenuItem mesEventsItem, monCompteItem;
    private String cp;
    private String ville;
    private String departement;
    private String region;
    private String paysNom;
    private String adresseNom;
    private String adresseComplet;
    private String paysCode;
    private double latitude;
    private double lontitude;
    private String textAdress;
    private Geocoder mGeocoder;

    private ImageButton btnCancelDateDebut;
    private ImageButton btnCancelDateFin;
    private ImageButton btnCancelLieu;
    FloatingActionButton fab;

    @BindView(R.id.Buttondatedeb)
    EditText buttonDateDeb;
    @BindView(R.id.progressBarLoad)
    ProgressBar progressBar;

    @BindView(R.id.ButtondatefinE)
    EditText buttonDateFin;

    @BindView(R.id.btnCancelRecherche)
    ImageButton  btnCancelRecherche;

    FirebaseUser firebaseUser;
    EditText lieu_E;

    //variable pour le boutton date debut et heure debut
    int year_x,month_x, day_x,heure_x, min_x;
    //variable pour le boutton date fin et heure fin
    int year_df,month_df,day_df, heure_df,min_df;
    static final int DIALOG_ID =0;
    static final int DIALOG_ID_DATE_FIN =1;
    static final int DIALOG_ID_HEURE_DEB=2;
    static final int DIALOG_ID_HEURE_FIN=3;
    String dateDebString, dateFinString;
    final Fragment_Accueil fragment_accueil =  new Fragment_Accueil();

    final FavorisFragment favorisFragment = new FavorisFragment();
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 30;
    private FirebaseAuth mAuth;
    FirebaseAuth.AuthStateListener mAuthStateListener;
    private  int typeUser;
    private  AccessToken  accessToken;
    boolean isLoggedIn;
    String providerId;
    Uri photoUrl;
    Menu menuNav;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onStart() {
        super.onStart();



        mAuth.addAuthStateListener(mAuthStateListener);




    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        //tester la connection reseau

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);


        setSupportActionBar(toolbar);
        final Calendar calendar = Calendar.getInstance(Locale.FRANCE);
        mAuth = FirebaseAuth.getInstance();

        year_x = calendar.get(Calendar.YEAR);
        month_x=calendar.get(Calendar.MONTH);
        day_x=calendar.get(Calendar.DAY_OF_MONTH);
        heure_x=calendar.get(Calendar.HOUR_OF_DAY);
        min_x=calendar.get(Calendar.MINUTE);
        year_df = calendar.get(Calendar.YEAR);
        month_df=calendar.get(Calendar.MONTH);
        day_df=calendar.get(Calendar.DAY_OF_MONTH);
        heure_df=calendar.get(Calendar.HOUR_OF_DAY);
        min_df=calendar.get(Calendar.MINUTE);

        //Initiallisation variable
        //CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar_main);
        tabLayout = (TabLayout) findViewById(R.id.tablelayout);
        viewPager = (ViewPager) findViewById(R.id.viewPage);




        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView= (NavigationView) findViewById(R.id.nav_view);

        navigationView.setNavigationItemSelectedListener(this);
        session = new UserSessionManager(getApplicationContext());
        avatarSize =this.getResources().getDimensionPixelSize(R.dimen.ppUserHeader);
        View header = navigationView.getHeaderView(0);
        btnCancelDateDebut = (ImageButton) findViewById(R.id.btnCancelDateDebut);
        btnCancelDateFin = (ImageButton) findViewById(R.id.btnCancelDateFin);
        btnCancelLieu = (ImageButton) findViewById(R.id.btnCancelLieu);

        lieu_E= (EditText) findViewById(R.id.lieuE);
        region ="";
        departement ="";
        ville ="";

        menuNav = navigationView.getMenu();
        deconnexionItem = menuNav.findItem(R.id.nav_quit);
        mesEventsItem = menuNav.findItem(R.id.nav_MesEvents);
        monCompteItem= menuNav.findItem(R.id.nav_mon_compte);


        if(networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()) {




        }
        else{
            Toast toast = Toast.makeText(this, getString(R.string.aucuneConnexion), Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }

        btnCancelDateDebut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateDebString="";
                buttonDateDeb.setText(getResources().getString(R.string.date_debut));
                btnCancelDateDebut.setVisibility(View.GONE);
            }
        });

        btnCancelLieu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lieu_E.setText(getResources().getString(R.string.lieu));
                region ="";
                departement ="";
                ville ="";
                btnCancelLieu.setVisibility(View.GONE);
            }
        });
        btnCancelDateFin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateFinString="";
                buttonDateFin.setText(getResources().getString(R.string.date_fin));
                btnCancelDateFin.setVisibility(View.GONE);
            }
        });

        btnCancelRecherche.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchText.setText("");
                btnCancelRecherche.setVisibility(View.GONE);
            }
        });

        //recupère les extras emis par login activity

        drawerloginText = (TextView) header.findViewById(R.id.textView);
        relativeHeaderDrawer = (RelativeLayout) header.findViewById(R.id.relat_header);
        imagePP = (ImageView) header.findViewById(R.id.imageView);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        builder = new AlertDialog.Builder(this);



        //police
 

        lieu_E.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lancerAutocomplete();
            }
        });



        checkUserLog();
        FloatingActionButton fabSearch = (FloatingActionButton) findViewById(R.id.fab_search);
        fabSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDrawerSearch();
            }
        });
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        actionBarDrawerToggle.syncState();



        buttonSearchDrawer= (Button) findViewById(R.id.buttonSearchDrawer);
        searchText= (EditText) findViewById(R.id.drawerRecherche);
        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length()>=1){
                    btnCancelRecherche.setVisibility(View.VISIBLE);
                }
                else {
                    btnCancelRecherche.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        buttonSearchDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                fragment_accueil.setRequestCount(1);
                fragment_accueil.getData(searchText.getText().toString(),
                        getSpinnerTrierValue(spinnerTrierParFrenchForRequest(spinnerTrierPar.getSelectedItemPosition())) ,
                        spinnerTypeFrenchForRequest(spinnerType.getSelectedItemPosition()),
                        bonneSyntaxe(region),
                        bonneSyntaxe(departement),
                        bonneSyntaxe(ville),
                        dateAuBonFormat(dateDebString),
                        dateAuBonFormat(dateFinString));
                closeDrawerSearch();

            }
        });




        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            if(BuildConfig.DEBUG) {

                Toast.makeText(this, query
                        , Toast.LENGTH_SHORT).show();

            }
        }


        spinnerTrierPar = (Spinner) findViewById(R.id.spinner_Trier_Par);
        ArrayAdapter<CharSequence> dataAdapter = ArrayAdapter.createFromResource(this, R.array.trierParString, android.R.layout.simple_spinner_item);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinnerTrierPar.setAdapter(dataAdapter);
        spinnerTrierPar.setSelection(numberSaveTrierPar);
        spinnerTrierPar.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                numberSaveTrierPar = i;
                if(BuildConfig.DEBUG) {
                    Toast.makeText(MainActivity.this, i + "", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                if(BuildConfig.DEBUG) {
                    Toast.makeText(MainActivity.this, "nothing selected", Toast.LENGTH_SHORT).show();
                }
            }
        });

        spinnerTrierPar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                //Pour cacher le clavier
                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                return false;
            }
        });

        spinnerType = (Spinner) findViewById(R.id.spinner_Type_drawer);
        ArrayAdapter<CharSequence> typeAdapter = ArrayAdapter.createFromResource(this, R.array.type_eventString, android.R.layout.simple_spinner_item);

        typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerType.setAdapter(typeAdapter);
        spinnerType.setSelection(numberSaveType);

        spinnerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                numberSaveType = i;
                if(BuildConfig.DEBUG) {
                    Toast.makeText(MainActivity.this, i + "", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                if(BuildConfig.DEBUG) {
                    Toast.makeText(MainActivity.this, "nothing selected", Toast.LENGTH_SHORT).show();
                }
            }
        });

        spinnerType.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                //Pour cacher le clavier
                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                return false;
            }
        });






        showDialogOnButtonDateDebClick();
        showDialogOnButtonDateFinClick();
        accessToken = AccessToken.getCurrentAccessToken();




        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

                firebaseUser = firebaseAuth.getCurrentUser();


                if(firebaseUser != null){


//                    isLoggedIn = accessToken != null && !accessToken.isExpired();



                    for (UserInfo profile : firebaseUser.getProviderData()) {
                        // Id of the provider (ex: google.com)
                        providerId = profile.getProviderId();

                        // Name, email address, and profile photo Url
                        String name = profile.getDisplayName();
                        String email = profile.getEmail();
                        photoUrl = profile.getPhotoUrl();

                    }
                    if(BuildConfig.DEBUG) {
                        Toast.makeText(MainActivity.this, providerId, Toast.LENGTH_LONG).show();
                        Log.i("TYPEPROVIDERMAIN", providerId);
                    }
                    if(providerId.equals("google.com") || providerId.equals("facebook.com")){


                        actionUserConnected();

                        Glide.with(getApplicationContext()).load(photoUrl).apply(circleCropTransform().placeholder(R.drawable.btn_capture)).transition(withCrossFade()).into(imagePP);


                    }
                    else{
                        if(  firebaseUser.isEmailVerified()  ){
                            actionUserConnected();

                        }
                    }













                }
            }
        };






        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(fragment_accueil,getResources().getString(R.string.accueil));
        viewPagerAdapter.addFragment(favorisFragment, getResources().getString(R.string.favoris));
        // viewPagerAdapter.addFragment(fragmentMyspace, "My Space");
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        mGeocoder = new Geocoder(this, Locale.getDefault());

        //pour que le l'appli ne refresh pas à chaque fois que je change de tab
        viewPager.setOffscreenPageLimit(2);



    }




    public void lancerAutocomplete() {

        try

        {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch(
                GooglePlayServicesRepairableException e)

        {
            // TODO: Handle the error.
        } catch(
                GooglePlayServicesNotAvailableException e)

        {
            // TODO: Handle the error.
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                try {
                    getInformationByCoordinates(place.getLatLng().latitude, place.getLatLng().longitude);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                textAdress = place.getName() + "";
                lieu_E.setText(place.getName());
                btnCancelLieu.setVisibility(View.VISIBLE);

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i("PLACEERROR", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    private void getInformationByCoordinates(double lat, double lon) throws IOException {

        List<Address> addresses = mGeocoder.getFromLocation(lat, lon, 1);
        if (addresses != null && addresses.size() > 0) {

            adresseNom = checkNullString(addresses.get(0).getFeatureName()) + " " + checkNullString(addresses.get(0).getThoroughfare());
            cp = checkNullString(addresses.get(0).getPostalCode());
            ville = checkNullString(addresses.get(0).getLocality());
            departement = checkNullString(addresses.get(0).getSubAdminArea());
            region = checkNullString(addresses.get(0).getAdminArea());
            paysNom = checkNullString(addresses.get(0).getCountryName());
            paysCode = checkNullString(addresses.get(0).getCountryCode());
            latitude = lat;
            lontitude = lon;
            adresseComplet = adresseNom + "," + cp + "," + ville + "," + departement + "," + region + "," + paysNom + "," + paysCode + "," + latitude + "," + lontitude;
            if (BuildConfig.DEBUG) {
                Log.i("ADRESSE", "getInformationByCoordinates: " + adresseComplet);
            }
        }

    }

    private String checkNullString(String str) {
        if (str != null) {
            return str;
        } else {
            return "";
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        //tester la connection reseau


        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if(BuildConfig.DEBUG) {
            Toast.makeText(this, "OnResume", Toast.LENGTH_SHORT).show();
            Log.i("SESSION", "onResume: " + session.isUserLoggedIn());

        }

        checkUserLog();



        if(!(networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected())) {

            Toast toast = Toast.makeText(this, getString(R.string.aucuneConnexion), Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }




    }

    public  UserSessionManager  getSession(){
        return session;
    }

    public void showDialogOnButtonDateDebClick(){
        buttonDateDeb.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                hideKeyboard();
                showDialog(DIALOG_ID);
            }
        });
    }
    //selection de la date de fin
    public void showDialogOnButtonDateFinClick(){
        buttonDateFin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
                showDialog(DIALOG_ID_DATE_FIN);
            }
        });
    }

    @Override
    protected Dialog onCreateDialog(int id){
        if (id == DIALOG_ID){
            DatePickerDialog datePickerDialog =new DatePickerDialog(this,datePickerListener,year_x,month_x,day_x);
            datePickerDialog.setButton(DialogInterface.BUTTON_NEUTRAL, getResources().getString(R.string.aujourdhui), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Calendar calendar = Calendar.getInstance(Locale.FRANCE);
                    year_x = calendar.get(Calendar.YEAR);
                    month_x=calendar.get(Calendar.MONTH)+1;
                    day_x=calendar.get(Calendar.DAY_OF_MONTH);
                    buttonDateDeb.setText(day_x+"-"+month_x+"-"+year_x);
                    dateDebString=year_x+"-"+month_x+"-"+day_x;

                    if(BuildConfig.DEBUG) {
                        Toast.makeText(MainActivity.this, dateDebString, Toast.LENGTH_SHORT).show();

                    }
                    btnCancelDateDebut.setVisibility(View.VISIBLE);

                }
            });
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            return  datePickerDialog;
        }
        else if (id==DIALOG_ID_DATE_FIN){
            DatePickerDialog datePickerDialog=  new DatePickerDialog(this,datePickerDateFinListener,year_df,month_df,day_df);
            datePickerDialog.setButton(DialogInterface.BUTTON_NEUTRAL, getResources().getString(R.string.aujourdhui), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Calendar calendar = Calendar.getInstance(Locale.FRANCE);
                    year_df = calendar.get(Calendar.YEAR);
                    month_df=calendar.get(Calendar.MONTH)+1;
                    day_df=calendar.get(Calendar.DAY_OF_MONTH);
                    buttonDateFin.setText(day_df+"-"+month_df+"-"+year_df);
                    dateFinString=year_df+"-"+month_df+"-"+day_df;

                    if(BuildConfig.DEBUG) {
                        Toast.makeText(MainActivity.this, dateFinString, Toast.LENGTH_SHORT).show();
                    }
                    btnCancelDateFin.setVisibility(View.VISIBLE);

                }
            });
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            return  datePickerDialog;
        }

        return null;
    }

    //Datepicker de la date de debut
    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
            Calendar calendarChoisie = Calendar.getInstance();
            calendarChoisie.set(Calendar.YEAR,year);
            calendarChoisie.set(Calendar.MONTH,monthOfYear);
            calendarChoisie.set(Calendar.DAY_OF_MONTH,dayOfMonth);

            Calendar calendar= Calendar.getInstance(Locale.FRANCE);
            if (calendarChoisie.before(calendar)){
                year_x = calendar.get(Calendar.YEAR);
                month_x=calendar.get(Calendar.MONTH)+1;
                day_x=calendar.get(Calendar.DAY_OF_MONTH);
                buttonDateDeb.setText(day_x + "-" + month_x + "-" + year_x);
                dateDebString = year_x + "-" + month_x + "-" + day_x;

                if(BuildConfig.DEBUG) {

                    Toast.makeText(MainActivity.this, dateDebString, Toast.LENGTH_SHORT).show();

                }
                btnCancelDateDebut.setVisibility(View.VISIBLE);
            }else {
                year_x = year;
                month_x = monthOfYear + 1;
                day_x = dayOfMonth;
                buttonDateDeb.setText(day_x + "-" + month_x + "-" + year_x);

                dateDebString = year_x + "-" + month_x + "-" + day_x;
                if(BuildConfig.DEBUG) {
                    Toast.makeText(MainActivity.this, dateDebString, Toast.LENGTH_SHORT).show();
                }
                btnCancelDateDebut.setVisibility(View.VISIBLE);
            }
        }
    };

    //Datepicker de la date de fin
    private DatePickerDialog.OnDateSetListener datePickerDateFinListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
            Calendar calendarChoisie = Calendar.getInstance();
            calendarChoisie.set(Calendar.YEAR,year);
            calendarChoisie.set(Calendar.MONTH,monthOfYear);
            calendarChoisie.set(Calendar.DAY_OF_MONTH,dayOfMonth);

            Calendar calendar= Calendar.getInstance(Locale.FRANCE);
            if (calendarChoisie.before(calendar)) {
                year_df = calendar.get(Calendar.YEAR);
                month_df=calendar.get(Calendar.MONTH)+1;
                day_df=calendar.get(Calendar.DAY_OF_MONTH);

                buttonDateFin.setText(day_df + "-" + month_df + "-" + year_df);
                dateFinString = year_df + "-" + month_df + "-" + day_df;
                btnCancelDateFin.setVisibility(View.VISIBLE);
                if(BuildConfig.DEBUG) {
                    Toast.makeText(MainActivity.this, dateFinString, Toast.LENGTH_SHORT).show();
                }
            }
            else {

                year_df = year;
                month_df = monthOfYear + 1;
                day_df = dayOfMonth;
                buttonDateFin.setText(day_df + "-" + month_df + "-" + year_df);
                dateFinString = year_df + "-" + month_df + "-" + day_df;
                btnCancelDateFin.setVisibility(View.VISIBLE);
                if(BuildConfig.DEBUG) {
                    Toast.makeText(MainActivity.this, dateFinString, Toast.LENGTH_SHORT).show();}
            }
        }
    };



    //SHOW INSERT DIALOG
    private void showDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.test_dialog, null);
        dialogBuilder.setView(dialogView);

        AlertDialog d = dialogBuilder.create();

        d.show();
    }




    private void hideKeyboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void openDrawerSearch() {
        mDrawerLayout.openDrawer(GravityCompat.END);
    }
    public void openDrawerMenu() {
        mDrawerLayout.openDrawer(GravityCompat.START);
    }
    public  void closeDrawerSearch(){
        mDrawerLayout.closeDrawer(GravityCompat.END);
    }
    public  void closeDrawerMenu(){
        mDrawerLayout.closeDrawer(GravityCompat.START);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        /*MenuItem itemCart = menu.findItem(R.id.panierButton);
        LayerDrawable icon = (LayerDrawable) itemCart.getIcon();
        Utils2.setBadgeCount(this, icon, "9");*/
        /*MenuItem searchItem = menu.findItem(R.id.action_search);
// with MenuItemCompat instead of your MenuItem
        SearchView mSearchView = (SearchView) MenuItemCompat.getActionView(searchItem);
mSearchView.setOnQueryTextListener(this);*/
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();






        return super.onOptionsItemSelected(item);
    }

    public boolean onPrepareOptionsMenu(Menu item)
    {
        if(!session.isUserLoggedIn()){
            deconnexionItem.setVisible(false);
            monCompteItem.setVisible(false);

        }
        return true;
    }


    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            selectPage(0);
            closeDrawerMenu();
        } else if (id == R.id.nav_AddEvent) {
            if(mAuth.getCurrentUser() !=null) {
                Intent myIntent = new Intent(MainActivity.this, Ajout_E.class);
                startActivity(myIntent);
                closeDrawerMenu();
            }
            else{
                demandeLogin();
            }

        } else if (id == R.id.nav_quit) {
            //finish();
            logout();
            fragment_accueil.refresh();
            favorisFragment.refresh();

            Intent i = new Intent(MainActivity.this, LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);


//            Picasso.with(this)
//                    .load(R.drawable.defaultphoto)
//                    .centerCrop()
//                    .resize(avatarSize, avatarSize)
//                    .transform(new RoundedTransformation())
//                    .into(imagePP);
//
//            drawerloginText.setText(getResources().getString(R.string.affiche_se_connecter));
//            relativeHeaderDrawer.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
//                    startActivity(intent);
//                }
//
//            });
//            fab.setOnClickListener(new View.OnClickListener() {
//
//                @Override
//                public void onClick(View view) {
//
//                    Intent myIntent = new Intent(MainActivity.this, LoginActivity.class);
//                    startActivity(myIntent);
//
//
//                }
//            });
//            deconnexionItem.setVisible(false);
//            mesEventsItem.setVisible(false);
//            monCompteItem.setVisible(false);

        }
        else if (id==R.id.nav_MesEvents){
            if(mAuth.getCurrentUser()!=null) {

                Intent i = new Intent(MainActivity.this, ProfilActivity.class);
                startActivity(i);

            }
            else{
                demandeLogin();
            }
        }
        else if(id==R.id.nav_mon_compte){
            Intent intent = new Intent(MainActivity.this,SettingsActivity.class);
            startActivity(intent);
        }
        return true;
    }



    private void logout() {

        session.logoutUser();
        if(mAuth.getCurrentUser() != null){
            mAuth.signOut();

        }

    }



    void selectPage(int pageIndex){

        tabLayout.setScrollPosition(pageIndex,0f,true);
        viewPager.setCurrentItem(pageIndex);

    }



    public String getSpinnerTrierValue(String s){
        if (s.equals("Date d'ajout")){
            return  "id";
        }
        else if (s.equals("Date de début")){
            return "datedebut";
        }
        else {
            return "datefin";
        }
    }



    public void demandeLogin(){
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
    }
    public String replaceApostrophe(String str){

        String str_=str.replaceAll("'", "\\\\'");
        return str_;
    }

    public String dateAuBonFormat(String date){
        String datefinal;

        if (date!= null){
            datefinal=date;
        }
        else {
            datefinal="";
        }
        return  datefinal;
    }

    public void hideProgressLoad(){
        progressBar.setVisibility(View.GONE);
    }

    public void showProgressLoad(){
        progressBar.setVisibility(View.VISIBLE);
    }







    private String bonneSyntaxe(String s){


        String str= s.replaceAll(" ", "%").replaceAll("'", "\\\\'").replaceAll("'", "\\\\'").replaceAll("'", "\\\\'");
        return str;
    }

    //utilisateur connecté
    public void actionUserConnected(){

        if(session.isUserLoggedIn()){

            drawerloginText.setText(firebaseUser.getDisplayName());
            firebaseUser = mAuth.getCurrentUser();
            deconnexionItem.setVisible(true);
            mesEventsItem.setVisible(true);
            monCompteItem.setVisible(true);



            fab.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    Intent myIntent = new Intent(MainActivity.this, Ajout_E.class);
                    startActivity(myIntent);


                }
            });
        }

        else  {

            actionNoUser();
        }




    }



    private void actionNoUser(){
        Picasso.with(this)
                .load(R.drawable.defaultphoto)
                .centerCrop()
                .resize(avatarSize, avatarSize)
                .transform(new RoundedTransformation())
                .into(imagePP);

        drawerloginText.setText(getResources().getString(R.string.affiche_se_connecter));
        relativeHeaderDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
            }

        });
        fab.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                Intent myIntent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(myIntent);


            }
        });
        deconnexionItem.setVisible(false);
        mesEventsItem.setVisible(false);
        monCompteItem.setVisible(false);

    }


    private String spinnerTypeFrenchForRequest(int selectInt){
        String str="";
        switch (selectInt){
            case 0: str="Tout";
                break;
            case 1: str="Soirées";
                break;
            case 2 : str="Concerts";
                break;
            case 3 : str="Films";
                break;
            case 4: str="Spectacles";
                break;
            case 5 : str= "Festivals";
                break;
        }
        return str;
    }


    private String spinnerTrierParFrenchForRequest(int selectInt){
        String str="";
        switch (selectInt){
            case 0: str="datedebut";
                break;
            case 1: str="id";
                break;
            case 2 : str="datefin";
                break;

        }
        return str;
    }

    private void checkUserLog() {
        if (firebaseUser != null) {
            firebaseUser = mAuth.getCurrentUser();


//                    isLoggedIn = accessToken != null && !accessToken.isExpired();


            for (UserInfo profile : firebaseUser.getProviderData()) {
                // Id of the provider (ex: google.com)
                providerId = profile.getProviderId();

                // Name, email address, and profile photo Url
                String name = profile.getDisplayName();
                String email = profile.getEmail();
                photoUrl = profile.getPhotoUrl();

            }
            if (BuildConfig.DEBUG) {
                Toast.makeText(MainActivity.this, providerId, Toast.LENGTH_LONG).show();
                Log.i("TYPEPROVIDERMAIN", providerId);
            }
            if (providerId.equals("google.com") || providerId.equals("facebook.com")) {


                actionUserConnected();

                Glide.with(getApplicationContext()).load(photoUrl).apply(circleCropTransform().placeholder(R.drawable.btn_capture)).transition(withCrossFade()).into(imagePP);


            } else {
                if (firebaseUser.isEmailVerified()) {
                    actionUserConnected();

                }
            }
        }
    }



}
