package com.eeventoo.app.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.eeventoo.app.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class PasswordResetActivity extends AppCompatActivity {
    EditText email;
    private FirebaseAuth mAuth;
    Button resetPassword ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_reset);

        email = (EditText) findViewById(R.id.email_reset);
        resetPassword = findViewById(R.id.resetPassword);
        mAuth = FirebaseAuth.getInstance();


        resetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(email.getText().toString().equals("")){
                    Toast.makeText(PasswordResetActivity.this, getString(R.string.indiquerEmail), Toast.LENGTH_SHORT).show();
                }
                else
                {
                    resetPassword(email.getText().toString());
                }
            }
        });



    }

    private void resetPassword(String email){
        mAuth.sendPasswordResetEmail(email).addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(PasswordResetActivity.this, getString(R.string.emailResetPasswordSend), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(PasswordResetActivity.this,LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
                else {
                    Toast.makeText(PasswordResetActivity.this, getString(R.string.erreurResetPasswordServeur), Toast.LENGTH_LONG).show();

                }
            }
        });
    }
}
