package com.eeventoo.app.activity;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.eeventoo.app.R;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardMultilineWidget;


import java.util.ArrayList;


public class PaymentActivity extends AppCompatActivity {
    CardMultilineWidget cardWidget ;
    Card card;
    private String idEvent, idFormule, description, devise;
    private Spinner spinner;
    ArrayList<Integer> listQuantite = new ArrayList<>();
    private TextView descriptiontarifE, prixE;
    private int quantiteFormule;

    private int prixDefinitif;
    private Button buttonBuy ;
    private int prix;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        idEvent=  getIntent().getExtras().getString("idEvent");
        idFormule= getIntent().getExtras().getString(      "idformule");
        description= getIntent().getExtras().getString(  "description");
        prix =getIntent().getExtras().getInt(  "prix");
        Log.i("PRIX", "prix"+prix);
        devise =getIntent().getExtras().getString( "devise");
        prixDefinitif =prix;
        //mCardInputWidget= (CardInputWidget) findViewById(R.id.card_input_widget);
        spinner = (Spinner) findViewById(R.id.spinnerQuantite);
        descriptiontarifE = findViewById(R.id.descriptiontarifE);
        prixE = findViewById(R.id.prixFormule);
        prixE.setText(prix+" "+devise);
        buttonBuy = findViewById(R.id.buttonBuy);
        buttonBuy.setText(getString(R.string.acheter) + " "+ prixDefinitif+ devise);
        descriptiontarifE.setText(description);
        populateListQuantite();

        ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_item, listQuantite);
        spinner.setAdapter(adapter);
        spinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                //Pour cacher le clavier
                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                return false;
            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                quantiteFormule = (int)adapterView.getSelectedItem();
                prixDefinitif= quantiteFormule * Integer.valueOf(prix);
                buttonBuy.setText(getString(R.string.acheter) + " "+ prixDefinitif+ devise);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        buttonBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCardSaved();
            }
        });


//        Stripe stripe = new Stripe(PaymentActivity.this, "pk_test_DVbknVWPLHq7hhYrjLu5LRW8");
//        stripe.createToken(
//                card,
//                new TokenCallback() {
//                    public void onSuccess(Token token) {
//                        // Send token to your server
//                    }
//                    public void onError(Exception error) {
//                        // Show localized error message
//                        Toast.makeText(PaymentActivity.this,
//                                error.getLocalizedMessage(),
//                                Toast.LENGTH_LONG
//                        ).show();
//                    }
//                }
//        );


    }


    public  void populateListQuantite(){
        for (int i =1; i<=100;i++){
            listQuantite.add(i);
        }
    }

     private void onCardSaved() {
        final Card cardToSave = cardWidget.getCard();

        if (cardToSave != null) {
            // tokenize card (see below)
            tokenizeCard(cardToSave);
        }
    }

     private  void tokenizeCard(@NonNull Card card) {
        Stripe stripe = new Stripe(PaymentActivity.this, "pk_test_DVbknVWPLHq7hhYrjLu5LRW8");
        stripe.createToken(
                card,
                new TokenCallback() {
                    public void onSuccess(Token token) {
                        // Send token to your server
                    }
                    public void onError(Exception error) {
                        // Show localized error message
                        Toast.makeText(PaymentActivity.this,
                                error.getLocalizedMessage(),
                                Toast.LENGTH_LONG
                        ).show();
                    }
                }
        );
    }
}


