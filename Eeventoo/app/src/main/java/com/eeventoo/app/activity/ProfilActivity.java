package com.eeventoo.app.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.bumptech.glide.request.RequestOptions;
import com.eeventoo.app.Adapter.AdapterProfil;
import com.eeventoo.app.BuildConfig;
import com.eeventoo.app.InterfaceRetrofit.EventOfUser;
import com.eeventoo.app.R;
import com.eeventoo.app.class_.Constants;
import com.eeventoo.app.class_.Evenement;
import com.eeventoo.app.class_.Example;
import com.eeventoo.app.class_.UserSessionManager;
import com.eeventoo.app.interfaceListener.CustomItemClickListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserInfo;


import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;
import static com.bumptech.glide.request.RequestOptions.circleCropTransform;

public class ProfilActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    @BindView(R.id.recyclerProfil)
    RecyclerView recyclerView;
    private ImageLoader imageLoader;
    private ImageView imageView;
    //creer retrofit instance
    private static Retrofit.Builder builder = new Retrofit.Builder().baseUrl(Constants.EVENTOFUSER).addConverterFactory(GsonConverterFactory.create());

    private FirebaseAuth mAuth;

    private  SwipeRefreshLayout swipeRefresh;
    public static  Retrofit retrofit = builder.build();
    private ArrayList <Evenement> evenements ;
    private UserSessionManager session;
    Uri photoUrl;

    @BindView(R.id.textViewListVide)
    TextView textViewListeVide;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);





        //tester la connection reseau

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if(networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()) {
        }
        else{
            Toast toast = Toast.makeText(this, getString(R.string.aucuneConnexion), Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.mesEvenements);
        ButterKnife.bind(this);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabAddPhotoProfil);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfilActivity.this, CompteActivity.class);
                startActivity(intent);
            }
        });

        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar_desc);

        if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (shouldShowRequestPermissionRationale(
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                // Explain to the user why we need to read the contacts
            }

            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

            // MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE is an
            // app-defined int constant that should be quite unique

            return;
        }

        session = new UserSessionManager(getApplicationContext());




        imageView= (ImageView) this.findViewById(R.id.backgroundImageView_profil);


        if(BuildConfig.DEBUG) {
            Toast.makeText(this, session.getIdUser() + "", Toast.LENGTH_SHORT).show();
        }
        executeGetEventOfUser(session.getIdUser()+"");

        swipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swipeRefrehProfil);

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {



                refresh();

            }
        });
        mAuth = FirebaseAuth.getInstance();


        if(mAuth.getCurrentUser() != null){


            for (UserInfo profile : mAuth.getCurrentUser().getProviderData()) {
                // Id of the provider (ex: google.com)
                String providerId = profile.getProviderId();

                // Name, email address, and profile photo Url
                String name = profile.getDisplayName();
                String email = profile.getEmail();
                photoUrl = profile.getPhotoUrl();
                String phone= profile.getPhoneNumber();



            }

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true);
            requestOptions.centerCrop();
            Glide.with(this).load(photoUrl).apply(requestOptions).apply(circleCropTransform()).transition(withCrossFade()).into(imageView);


        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //tester la connection reseau

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if(networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()) {
        }
        else{
            Toast toast = Toast.makeText(this, getString(R.string.aucuneConnexion), Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }
    private void executeGetEventOfUser(String idUser){
        EventOfUser eventOfUser = retrofit.create(EventOfUser.class);

        Call<Example> call = eventOfUser.getEventUser(idUser);
        call.enqueue(new Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {

                if(response.body() != null){
                    if(BuildConfig.DEBUG) {
                        Toast.makeText(ProfilActivity.this, "Succes", Toast.LENGTH_SHORT).show();
                    }
                    evenements = response.body().getData();
                    if(BuildConfig.DEBUG) {
                        Log.e("ProfilLIST", response.body().getData().toString() + "taile:" + evenements.size());
                    }
                    if(evenements.isEmpty()){
                        textViewListeVide.setVisibility(View.VISIBLE);
                    }else {
                        textViewListeVide.setVisibility(View.GONE);
                    }
                    setUp();
                }
                else {
                    Toast.makeText(ProfilActivity.this, getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();

                }


            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {

                Toast.makeText(ProfilActivity.this, getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();

                if(BuildConfig.DEBUG) {
                    Toast.makeText(ProfilActivity.this, "Error", Toast.LENGTH_SHORT).show();
                }
                if(BuildConfig.DEBUG) {
                    Log.i("profil", call.toString());
                    Log.i("profil", t.toString());
                }
            }
        });
    }

    public void setUp(){
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        AdapterProfil adapterProfil = new AdapterProfil(this, evenements, new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                Intent myIntent = new Intent(ProfilActivity.this, DescriptionEvent.class);
                myIntent.putExtra("id", "" + evenements.get(position).getId());
                myIntent.putExtra("nom", evenements.get(position).getNomEvenement());
                myIntent.putExtra("type", evenements.get(position).getType() + "");
                myIntent.putExtra("adresse", evenements.get(position).getAdresse() + "");
                myIntent.putExtra("adresseComplet", evenements.get(position).getAdresseComplet()) ;
                myIntent.putExtra("codepostal", evenements.get(position).getCodePostale() + "");
                myIntent.putExtra("ville", evenements.get(position).getVille() + "");
                myIntent.putExtra("departement", evenements.get(position).getDepartement());
                myIntent.putExtra("region", evenements.get(position).getRegion());
                myIntent.putExtra("paysNom", evenements.get(position).getPaysNom());
                myIntent.putExtra("paysCode", evenements.get(position).getPaysCode());
                myIntent.putExtra("latitude", evenements.get(position).getLatitude());
                myIntent.putExtra("lontitude", evenements.get(position).getLontitude());
                myIntent.putExtra("datedeb", evenements.get(position).getDatedeb() + "");
                myIntent.putExtra("datefin", evenements.get(position).getDatefin());
                myIntent.putExtra("telephone", evenements.get(position).getTelephone());
                myIntent.putExtra("typeTarif", evenements.get(position).getTypeTarif());
                myIntent.putExtra("description", evenements.get(position).getDescription());
                myIntent.putExtra("image", evenements.get(position).getImageUrl());
                startActivity(myIntent);
            }
        });
        recyclerView.setAdapter(adapterProfil);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_desc_event, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();

        }

        return super.onOptionsItemSelected(item);
    }

    private void refresh(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                executeGetEventOfUser(session.getIdUser()+"");
                swipeRefresh.setRefreshing(false);
            }
        },0);

    }
}
