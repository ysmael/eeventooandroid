package com.eeventoo.app.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.eeventoo.app.BuildConfig;
import com.eeventoo.app.InterfaceRetrofit.I_Authentification;
import com.eeventoo.app.R;
import com.eeventoo.app.class_.ReponseLogin;
import com.eeventoo.app.class_.ReponseRegister;
import com.eeventoo.app.class_.ServiceGenerator;
import com.eeventoo.app.class_.UserSessionManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.ActionCodeSettings;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.eeventoo.app.class_.Constants.USER_EEVENTOO;

public class Register extends Activity {
    EditText nom,prenom,mp,confirm_mp,email,adresse,cp,ville,tel;
    Button buttonReg;
    ProgressDialog progressDialog;
    AlertDialog.Builder builder;
    UserSessionManager session;
    private FirebaseAuth mAuth;
    FirebaseAuth.AuthStateListener mAuthStateListener;
    ActionCodeSettings actionCodeSettings;



    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        mAuth.addAuthStateListener(mAuthStateListener);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);




        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();


        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

                if(firebaseAuth.getCurrentUser() != null){


                    UserProfileChangeRequest.Builder u = new UserProfileChangeRequest.Builder()
                            .setDisplayName(prenom.getText().toString() +" "+ nom.getText().toString())
                            .setPhotoUri((Uri.parse("https://eeventoo.com/webapp/UserPhotoProfilDef/defaultphoto.png")));
                    firebaseAuth.getCurrentUser().updateProfile(u.build());
//                    Intent intent = new Intent(Register.this,LoginActivity.class);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(intent);
                }
            }
        };


        session = new UserSessionManager(getApplicationContext());
        nom =(EditText) findViewById  (R.id.nom_reg);
        prenom =(EditText) findViewById  (R.id.prenom_reg);
        mp  =(EditText) findViewById  (R.id.mp_reg);
        confirm_mp =(EditText) findViewById  (R.id.confirm_mp_reg);
        email=(EditText) findViewById  (R.id.email_reg);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.patientez));


        buttonReg= (Button) findViewById(R.id.register_button);

        //tester la connection reseau

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        actionCodeSettings =
                ActionCodeSettings.newBuilder()
                        // URL you want to redirect back to. The domain (www.example.com) for this
                        // URL must be whitelisted in the Firebase Console.
                        .setUrl("https://eeventoo.com")
                        // This must be true
                        .setHandleCodeInApp(true)
                        .setAndroidPackageName(
                                "com.eeventoo.app",
                                true, /* installIfNotAvailable */
                                "21"    /* minimumVersion */)
                        .build();


        buttonReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(nom.getText().toString().equals("")||
                        prenom.getText().toString().equals("")||
                        mp.getText().toString().equals("")||
                        confirm_mp.getText().toString().equals("")||
                        email.getText().toString().equals("")){

                    builder= new AlertDialog.Builder(Register.this);
                    builder.setTitle(getString(R.string.quelqueChoseMalPasse));
                    builder.setMessage(getString(R.string.remplissezToutChamp));
                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }else if ( mp.getText().toString().length() <= 5){
                    builder= new AlertDialog.Builder(Register.this);
                    builder.setTitle(getString(R.string.quelqueChoseMalPasse));
                    builder.setMessage(getString(R.string.password6lettre));
                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
                else if (!(mp.getText().toString().equals(confirm_mp.getText().toString())))
                {
                    builder= new AlertDialog.Builder(Register.this);
                    builder.setTitle(getString(R.string.quelqueChoseMalPasse));
                    builder.setMessage(getString(R.string.mpCorrespondPas));
                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            mp.setText("");
                            confirm_mp.setText("");
                        }
                    });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
                else
                {
                    String emailSansEspace=email.getText().toString().replaceAll(" ","");
                    if (isValidEmail(emailSansEspace)) {

                        registerFirebase(emailSansEspace,mp.getText().toString());
                        showProgressLoad();

                    }else {
                        builder = new AlertDialog.Builder(Register .this);
                        builder.setTitle(getString(R.string.quelqueChoseMalPasse));
                        builder.setMessage(getString(R.string.emailIncorrect));
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                    }

                }
            }
        });

    }



    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    private void registration( final String email, String displayName){
        I_Authentification iAuthentification =ServiceGenerator.createService(I_Authentification.class);
        Call<ReponseRegister> call = iAuthentification.registration(email, displayName);
        call.enqueue(new Callback<ReponseRegister>() {
            @Override
            public void onResponse(Call<ReponseRegister> call, Response<ReponseRegister> response) {
                if(response.body() != null){

                    if (response.body().isSuccess()) {
                        String title= getString(R.string.app_name);
                        String message= getString(R.string.demandeConfirmation);
                        builder = new AlertDialog.Builder(Register.this);
                        builder.setTitle(title);
                        builder.setMessage(message);
                        builder.setPositiveButton(getString(R.string.affiche_se_connecter), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(Register.this, LoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);

                            }
                        });

                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();

                    }
                    else {
                        showDialog(getString(R.string.inscriptionEchoue),response.body().getMessage());
                    }
                }

                else {
                    Toast.makeText(Register.this, getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ReponseRegister> call, Throwable t) {
                String title=getString(R.string.ooops);
                String message =getString(R.string.erreurServeur);
                builder = new AlertDialog.Builder(Register.this);
                builder.setTitle(title);
                builder.setMessage(message);
                builder.setPositiveButton(getString(R.string.affiche_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        progressDialog.dismiss();
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });

    }

    private void login(String email, String mp){
        progressDialog = new ProgressDialog(Register.this);
        progressDialog.show();
        progressDialog.setMessage(getString(R.string.affiche_connection));
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        I_Authentification i_authentification= ServiceGenerator.createService(I_Authentification.class);
        Call<ReponseLogin> call = i_authentification.checkLogin(email,mp);
        call.enqueue(new Callback<ReponseLogin>() {
            @Override
            public void onResponse(Call<ReponseLogin> call, Response<ReponseLogin> response) {
                progressDialog.dismiss();
                if(response.body() != null){

                    if (response.body().isSuccess()) {
                        int id = response.body().getId();
                        String prenomU =  response.body().getPrenomU();
                        String nomU= response.body().getNomU();
                        String ppU= response.body().getPpU();
                        String emailOfUser= response.body().getEmailOfUser();
                        String adresseU= response.body().getAdresseU();
                        String cpU= response.body().getCpU();
                        String villeU= response.body().getVilleU();
                        String telU= response.body().getTelU();

                        session.createUserLoginSession(id,emailOfUser,nomU,prenomU,ppU,adresseU,cpU,villeU,telU, USER_EEVENTOO);
                        Intent intent = new Intent(Register.this,MainActivity.class);

                        startActivity(intent);
                    }
                    else  {
                        String msg ;
                        Integer status =response.body().getStatus();
                        if (status==0){
                            msg=getString(R.string.utilisateurExistant);
                        }
                        else{

                            msg=getString(R.string.erreurServeur);
                        }
                        showDialog(getString(R.string.loginEchoue),msg);
                    }
                }

                else {
                    Toast.makeText(Register.this, getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<ReponseLogin> call, Throwable t) {
                showDialog(getString(R.string.ooops),getString(R.string.erreurServeur));
            }
        });
    }
    public void showDialog(String title, String message){
        builder = new AlertDialog.Builder(Register.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.affiche_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }


    private void registerFirebase(String email, String password){
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                           hideProgressLoad();
                            // Sign in success, update UI with the signed-in user's information
                            if(BuildConfig.DEBUG) {
                                Log.d("REGISTER", "createUserWithEmail:success");

                            }
                            if(mAuth.getCurrentUser()!=null){
                                mAuth.getCurrentUser().sendEmailVerification()
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    registration(email, prenom.getText().toString() +" "+ nom.getText().toString());

                                                }
                                                else {
                                                    Toast.makeText(Register.this, getString(R.string.quelqueChoseMalPasse), Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        });
                            }


//                            sendEmailVerification(email);


//                            FirebaseUser user = mAuth.getCurrentUser();
//                            mAuth.addAuthStateListener(mAuthStateListener);
                        } else {
                            hideProgressLoad();
                            // If sign in fails, display a message to the user.
                            if(BuildConfig.DEBUG) {
                                Log.w("REGISTER", "createUserWithEmail:failure", task.getException());
                            }
                            Toast.makeText(Register.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                });

    }

    private void sendEmailVerification(String email){
        mAuth.sendSignInLinkToEmail(email, actionCodeSettings)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            if(BuildConfig.DEBUG) {
                                Log.d("REGISTER", "Email sent.");
                            }
                        }
                        else {
                            if(BuildConfig.DEBUG) {
                                Log.d("REGISTER", task.getException().getMessage());
                            }
                        }
                    }
                });



    }

    public void hideProgressLoad(){
        progressDialog.dismiss();
    }

    public void showProgressLoad(){
        progressDialog.show();    }


}


