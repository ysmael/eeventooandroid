package com.eeventoo.app.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.core.view.MenuItemCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import com.android.volley.toolbox.JsonArrayRequest;
import com.eeventoo.app.Adapter.AdapterSearch;
import com.eeventoo.app.Adapter.AdapterSearchSubmit;
import com.eeventoo.app.class_.Constants;
import com.eeventoo.app.interfaceListener.CustomItemClickListener;
import com.eeventoo.app.class_.Evenement;
import com.eeventoo.app.class_.MyRequestQueue;
import com.eeventoo.app.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity implements SearchView.OnQueryTextListener  {
    SearchView mSearchView;
    RecyclerView recyclerView;
    RecyclerView recyclerViewSubmit;
    AdapterSearch adapter;
    AdapterSearchSubmit adapterSubmit;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<Evenement> evenements = new ArrayList<>();
    String search;
    LayoutInflater inflater;
    boolean qC;
    View view;
    AlertDialog.Builder builder;
    private TextView textVTrier;
    private Spinner spinnerTrierPar;
    ArrayAdapter<CharSequence> adapterSpinner;
    private AutoCompleteTextView region;
private ArrayAdapter<String> villeAdapter;
    private AutoCompleteTextView  ville;
    private Spinner departement;
    static  final String textQC="QueryChange";
    static  final String textQS="QuerySubmit";
    ArrayList<String>  listRegion = new ArrayList<>();
    ArrayList<String>  listDepartement = new ArrayList<>();
    ArrayList<String>  listVille = new ArrayList<>();
    ArrayAdapter<String> deptAdapter;
    ArrayAdapter<String> detAdapterReinit;
private String textSaveRegion="";
    private String textSaveVille="";
    private int numberSaveTrierPar=0;
    private int numberSaveDept=0;
    private  ArrayList<String> listDeptReinit = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        getSupportActionBar().setTitle("");
        //a louverture de lactivité on revoie tous les  evenements
        inflater=SearchActivity.this.getLayoutInflater();
        listDeptReinit.add("Departement");

        searchSubmit();
        getData(" ");
        getRegion();

    }

    //fonction permettant de recuperer toutes les evenements lorsque lorque l'on fait un submit car apres un submit le recyclerview change en recyclerviewsubmit



    //fonction permettant de recuperer toutes les evenements lorsque lorque l'on fait un submit et changer le recyclerview en  recyclerviewsubmit
public void searchSubmit(){
    Log.i("searchSubmiit()", "searchSubmit: ");
    recyclerViewSubmit = (RecyclerView) findViewById(R.id.recyclerSearch);
    layoutManager = new LinearLayoutManager(this);
    recyclerViewSubmit.setLayoutManager(layoutManager);
    recyclerViewSubmit.setHasFixedSize(true);
    adapterSubmit = new AdapterSearchSubmit(this, evenements, new CustomItemClickListener() {
        @Override
        public void onItemClick(View v, int position) {
            hideKeyboard();
            Intent myIntent = new Intent(SearchActivity.this, DescriptionEvent.class);
            myIntent.putExtra("id",""+ evenements.get(position).getId());
            Log.i("RECYCLERVIEW", "IDEVENT: "+ evenements.get(position).getId());
            myIntent.putExtra("nom", evenements.get(position).getNomEvenement());
            myIntent.putExtra("type", evenements.get(position).getType() + "");
            myIntent.putExtra("adresse", evenements.get(position).getAdresse() + "");
            myIntent.putExtra("codepostal", evenements.get(position).getCodePostale() + "");
            myIntent.putExtra("ville", evenements.get(position).getVille() + "");
            myIntent.putExtra("datedeb", evenements.get(position).getDatedeb() + "");
            myIntent.putExtra("datefin", evenements.get(position).getDatefin());
            myIntent.putExtra("telephone", evenements.get(position).getTelephone());
            myIntent.putExtra("typeTarif", evenements.get(position).getTypeTarif());
            myIntent.putExtra("description", evenements.get(position).getDescription());
            myIntent.putExtra("image",evenements.get(position).getImageUrl());

            startActivity(myIntent);
            SearchActivity.this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    });

    recyclerViewSubmit.setAdapter(adapterSubmit);
}




    //Cette methode permet de recuperer le resultat json et de le mettre dans larraylist
    private void parseData(JSONArray array) {
        Log.i("parse data", "parseData: ");
        evenements.clear();
        for (int i = 0; i < array.length(); i++) {
            //Creation dun evenement
            Evenement evenement_ = new Evenement();
            JSONObject json = null;
            try {
                //Getting json
                json = array.getJSONObject(i);

                //Adding data to the superhero object
                evenement_.setImageUrl(json.getString(Constants.TAG_IMAGE_URL));
                evenement_.setId(json.getString(Constants.ROW_ID));
                evenement_.setNomEvenement(json.getString(Constants.NOM_EVENEMENT));
                evenement_.setType(json.getString(Constants.TYPE));
                evenement_.setAdresse (json.getString(Constants.ADRESSE));
                evenement_.setCodePostale (json.getString(Constants.CODE_POSTAL));
                evenement_.setVille (json.getString(Constants.VILLE));
                evenement_.setDatedeb (json.getString(Constants.DATE_DEB));
                evenement_.setDatefin (json.getString(Constants.DATE_FIN));
                evenement_.setTelephone (json.getString(Constants.TELEPHONE));
                evenement_.setTypeTarif (json.getString(Constants.TYPETARIF));
                evenement_.setDescription(json.getString(Constants.DESCRIPTION));
                evenement_.setDepartement(json.getString(Constants.DEPARTEMENT));
                evenement_.setRegion(json.getString(Constants.REGION));
                // evenement_.setPrenomUser(json.getString(Constants.PRENOM_USER));
                //evenement_.setNomUser(json.getString(Constants.NOM_USER));
                //Log.i("PARSEDATA",json+"");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //Adding the superhero object to the list
            evenements.add(evenement_);
            Log.i("LISTEEVENEMENT", "parseData: "+evenements.size());
        }

    adapterSubmit.notifyDataSetChanged();

    }


private void populateArrayRegion(JSONArray jsonArray){
    for (int i=0;i<jsonArray.length();i++ ){
        JSONObject json = null;
        try {
            json=jsonArray.getJSONObject(i);
            listRegion.add(json.getString("region"));
        }catch (JSONException e){
            e.printStackTrace();
        }

    }
}
    private void populateArrayDepartement(JSONArray jsonArray){
        listDepartement.clear();
        listDepartement.add("Departement");
        for (int i=0;i<jsonArray.length();i++ ){
            JSONObject json = null;
            try {
                json=jsonArray.getJSONObject(i);
                listDepartement.add(json.getString("departement"));
            }catch (JSONException e){
                e.printStackTrace();
            }

        }

    }

    private void populateArrayVille(JSONArray jsonArray) {
        listVille.clear();
        Toast.makeText(SearchActivity.this, listVille.size()+"", Toast.LENGTH_SHORT).show();
        for (int i =0;i<jsonArray.length();i++){
            JSONObject json = null;
            try{
                json=jsonArray.getJSONObject(i);
                listVille.add(json.getString("ville")+" "+ json.getString("cp"));

            }catch (JSONException e){
                e.printStackTrace();
            }
        }
        villeAdapter.setNotifyOnChange(true);

        Log.i("POPULATEVILLE", listVille.size()+"");
    }

    private JsonArrayRequest getRegionFromServer(){
        final String url = Constants.GET_REGION_URL;
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        populateArrayRegion(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        return jsonArrayRequest;
    }

//obtenir les donnees que renvoie le serveur
    private JsonArrayRequest getDataFromServer(String recherche){
        Log.i("GETDATFROMSERVER", "getDataFromServer: ");

final String url = Constants.SEARCH_EVENT_URL+recherche;
      
            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Constants.SEARCH_EVENT_URL+recherche,
                    new Response.Listener<JSONArray>() {

                        @Override
                        public void onResponse(JSONArray response) {

                            parseData(response);
                            response.toString();
                            Log.i("ONRESPONSE30submit", "onResponse: "+url);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                            Log.i("JSONERRORsubmit", "jsonerro" );
                            Toast.makeText(SearchActivity.this, "Erreur", Toast.LENGTH_SHORT).show();
                        }
                    }
            );

            return jsonArrayRequest;
        

    }
    private JsonArrayRequest getDepartementFromServer(String rgn){
        final String url = Constants.GET_DEPARTEMENT_URL+rgn;
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Constants.GET_DEPARTEMENT_URL+rgn, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                populateArrayDepartement(response);
                response.toString();
                Log.i("DEPARTEMENTSERVER",response.toString() );
                Log.i("DEPARTEMENTSERVER",url);
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        );
        return jsonArrayRequest;

    }

    private JsonArrayRequest getVilleFromServer(String dept){
        final String url = Constants.GET_VILLE_URL+dept;
        JsonArrayRequest jsonArrayRequest= new JsonArrayRequest(Constants.GET_VILLE_URL + dept, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                populateArrayVille(response);
                Log.i("VILLESERVER",response.toString() );
                Log.i("VILLESERVER",url);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

            }
        });
        return jsonArrayRequest;
    }



    private void getRegion(){
        MyRequestQueue.getInstance(SearchActivity.this).addTorequestqueue(getRegionFromServer());
    }
//fonction qui appel getDataFromServer et le met dans une requesteQueue qui a ete instancié dans la classe MyRequestQueue
    private void getData(String recherche) {
        Log.i("GETDATA", "getData: ");
MyRequestQueue.getInstance(SearchActivity.this).addTorequestqueue(getDataFromServer(recherche));
    }
    private void getDepartement(String reg){
        MyRequestQueue.getInstance(SearchActivity.this).addTorequestqueue(getDepartementFromServer(reg));
    }
private void getVille(String dept){
    MyRequestQueue.getInstance(SearchActivity.this).addTorequestqueue(getVilleFromServer(dept));
}
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);

        MenuItem searchItem = menu.findItem(R.id.button_search);
// with MenuItemCompat instead of your MenuItem

     //mSearchView.setIconified(false);
        //Pour maintenir la barre de recherche toujours ouverte
        searchItem.expandActionView();
       MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
           @Override
           public boolean onMenuItemActionExpand(MenuItem item) {
               return true;
           }

           @Override
           public boolean onMenuItemActionCollapse(MenuItem item) {
               finish();
               return true;
           }
       });
        mSearchView = (SearchView) MenuItemCompat.getActionView(searchItem);
       mSearchView.setOnQueryTextListener(this);




        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.icon_filter:

                view= inflater.inflate(R.layout.dialog_filter_search,null);
                builder=new AlertDialog.Builder(SearchActivity.this);
                textVTrier = (TextView) findViewById(R.id.filterTriePar);




                ArrayList<String> triType = new ArrayList<String>();
                triType.add("Pertinence");
                triType.add("Nombre d'interressés");
                triType.add("Date d'ajout");
                triType.add("Date de début");
                triType.add("Date de fin");



                spinnerTrierPar = (Spinner) view.findViewById(R.id.spinner_Trier_Par);
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, triType);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                // attaching data adapter to spinner
                spinnerTrierPar.setAdapter(dataAdapter);
                spinnerTrierPar.setSelection(numberSaveTrierPar);
                spinnerTrierPar.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        numberSaveTrierPar=i;
                        Toast.makeText(SearchActivity.this, i+"", Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {
                        Toast.makeText(SearchActivity.this, "nothing selected", Toast.LENGTH_SHORT).show();
                    }
                });

spinnerTrierPar.setOnTouchListener(new View.OnTouchListener() {
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        //Pour cacher le clavier
        InputMethodManager imm=(InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        return false;
    }
});


                region = (AutoCompleteTextView) view.findViewById(R.id.filterRegion);
                ArrayAdapter<String> regionAdapter =new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listRegion);
                regionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                region.setAdapter(regionAdapter);
                region.setText(textSaveRegion);

                region.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                  @Override
                                                  public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                                                      String str_rgnAvecEspace= adapterView.getItemAtPosition(i).toString();
                                                      String str_regionSansEspace = str_rgnAvecEspace.replaceAll(" ","+");
                                                      String str_region = str_regionSansEspace.replaceAll("'", "\\\\'");
                                                      Log.i("REGIONSELECTED", str_region);
                                                      Toast.makeText(SearchActivity.this, str_region, Toast.LENGTH_SHORT).show();

                                                      //departement.setAdapter(null);
                                                      //listDepartement.clear();
                                                      //deptAdapter.clear();
                                                      //deptAdapter.notifyDataSetChanged();
                                                      getDepartement(str_region);

                                                        textSaveRegion=str_rgnAvecEspace;
                                                      Toast.makeText(SearchActivity.this,  listDepartement.size()+"", Toast.LENGTH_SHORT).show();
                                                      deptAdapter=new ArrayAdapter<String>(SearchActivity.this,android.R.layout.simple_spinner_item,listDepartement);
                                                      deptAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                                      deptAdapter.notifyDataSetChanged();
                                                      departement.setAdapter(deptAdapter);
                                                      Toast.makeText(SearchActivity.this,  departement.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();

                                                  }
                                              });

//listDepartement.add("Departement");
                        departement = (Spinner) view.findViewById(R.id.spinnerDepartement);
                 deptAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listDepartement);
                deptAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
               deptAdapter.add("Departement");
                departement.setAdapter(deptAdapter);
                departement.setSelection(numberSaveDept);
                departement.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                          @Override
                                                          public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                                            if (!adapterView.getItemAtPosition(i).toString().equals("Departement")) {


                                                                String str_deptAvecEspace = adapterView.getItemAtPosition(i).toString();
                                                                String str_deptSansEspace = str_deptAvecEspace.replaceAll(" ", "+");
                                                                String str_departement = str_deptSansEspace.replaceAll("'", "\\\\'");
                                                                numberSaveDept = i;
                                                                Toast.makeText(SearchActivity.this, str_departement, Toast.LENGTH_SHORT).show();
                                                                getVille(str_departement);

                                                                //Toast.makeText(SearchActivity.this, ""+listVille.size(), Toast.LENGTH_SHORT).show();
                                                                villeAdapter.notifyDataSetChanged();
                                                                villeAdapter = new ArrayAdapter<String>(SearchActivity.this, android.R.layout.simple_spinner_item, listVille);
                                                                ville.setAdapter(villeAdapter);
                                                            }
                                                          }

                                                          @Override
                                                          public void onNothingSelected(AdapterView<?> adapterView) {
                                                              Toast.makeText(SearchActivity.this, "Nothing change", Toast.LENGTH_SHORT).show();
                                                          }
                                                      });
departement.setOnTouchListener(new View.OnTouchListener() {
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        //Pour cacher le clavier
        InputMethodManager imm=(InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        return false;
    }
});


                ville= (AutoCompleteTextView) view.findViewById(R.id.filterVilleCp);
               villeAdapter =new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,listVille);
                villeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                ville.setAdapter(villeAdapter);
ville.setText(textSaveVille);
                ville.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        String str_villeAvecEspace= adapterView.getItemAtPosition(i).toString();
                        String str_villeSansEspace = str_villeAvecEspace.replaceAll(" ","+");
                        String str_ville = str_villeSansEspace.replaceAll("'", "\\\\'");
                        textSaveVille=str_villeAvecEspace;

                    }
                });

                builder.setView(view);
                builder.setTitle("Filtre de recherche");
                builder.setPositiveButton("Appliquer", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {


                    }
                });
                builder.setNegativeButton("Fermer", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                builder.show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {

        String qry= query.replaceAll(" ","");
        if(qry.equals("")){
            return false;
        }
        else {
            hideKeyboard();

            return true;
        }
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        newText = newText.toLowerCase();
        String nText =   newText.replaceAll(" ","+");
        Log.d("Search", "onQueryTextChange: "+nText);
        getData(nText);


        return true;
    }
//permet de cacher le clavier du portable
    private void hideKeyboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}
