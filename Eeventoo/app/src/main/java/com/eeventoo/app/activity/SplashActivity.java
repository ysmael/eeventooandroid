package com.eeventoo.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.eeventoo.app.InterfaceRetrofit.I_Authentification;
import com.eeventoo.app.R;
import com.eeventoo.app.class_.ReponseRegister;
import com.eeventoo.app.class_.ServiceGenerator;
import com.eeventoo.app.class_.UserSessionManager;
import com.google.firebase.auth.FirebaseAuth;

public class SplashActivity extends AppCompatActivity {
    private static int avatarSize;
    private static final long SPLASH_DISPLAY_LENGTH = 1250;
    ImageView imageView;
    UserSessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        /*avatarSize =this.getResources().getDimensionPixelSize(R.dimen.image_splash);
        imageView = (ImageView) findViewById(R.id.imageSplash);
        Picasso.with(this)
                .load(R.mipmap.ic_launcher_app)
                .resize(avatarSize, avatarSize)
                .into(imageView);


*/


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent mainIntent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(mainIntent);
                finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }



}