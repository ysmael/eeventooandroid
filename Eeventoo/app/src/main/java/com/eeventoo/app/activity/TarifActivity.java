package com.eeventoo.app.activity;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.widget.Toast;

import com.eeventoo.app.Adapter.AdapterTarif;
import com.eeventoo.app.InterfaceRetrofit.I_Event;
import com.eeventoo.app.R;
import com.eeventoo.app.class_.Formule;
import com.eeventoo.app.class_.ResponselistFormule;
import com.eeventoo.app.class_.ServiceGenerator;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TarifActivity extends AppCompatActivity {
    private ArrayList<Formule> formules = new ArrayList<>();
    private  String idEvent, nom;
    RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    AdapterTarif adapterTarif;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tarif);
        idEvent =getIntent().getExtras().getString("idEvent");
        nom =getIntent().getExtras().getString("nomEvent");
        getSupportActionBar().setTitle(nom);
        getFormules(idEvent);
        recyclerView=findViewById(R.id.recyclerFormule);
        adapterTarif= new AdapterTarif(TarifActivity.this,formules);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);


        recyclerView.setAdapter(adapterTarif);
    }


    public void getFormules(final String idEvent){

        I_Event i_event = ServiceGenerator.createService(I_Event.class);
        Call<ResponselistFormule> call = i_event.getFomulesEvent(idEvent);

        call.enqueue(new Callback<ResponselistFormule>() {
            @Override
            public void onResponse(Call<ResponselistFormule> call, Response<ResponselistFormule> response) {
                assert response.body() != null;
                if (response.body().isSuccess()) {
                    formules = response.body().getData();
                    Toast.makeText(TarifActivity.this, formules.size()+"", Toast.LENGTH_SHORT).show();
                    adapterTarif= new AdapterTarif(TarifActivity.this,formules);
                    layoutManager = new LinearLayoutManager(TarifActivity.this);
                    recyclerView.setLayoutManager(layoutManager);


                    recyclerView.setAdapter(adapterTarif);

                }
                else{
                    Toast.makeText(TarifActivity.this, getString(R.string.quelqueChoseMalPasse), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<ResponselistFormule> call, Throwable t) {
                Toast.makeText(TarifActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }
}
