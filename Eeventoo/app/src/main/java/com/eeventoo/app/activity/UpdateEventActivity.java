package com.eeventoo.app.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import androidx.annotation.RequiresApi;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.eeventoo.app.BuildConfig;
import com.eeventoo.app.InterfaceRetrofit.FileUploadService;
import com.eeventoo.app.InterfaceRetrofit.I_IndicatifCountry;
import com.eeventoo.app.InterfaceRetrofit.I_TypeEvent;
import com.eeventoo.app.R;
import com.eeventoo.app.class_.FileUtils;
import com.eeventoo.app.class_.PaysIndicatif;
import com.eeventoo.app.class_.PaysMonnaie;
import com.eeventoo.app.class_.ReponseServeur;
import com.eeventoo.app.class_.ResponseIndicatif;
import com.eeventoo.app.class_.ResponseTypeEvent;
import com.eeventoo.app.class_.ServiceGenerator;
import com.eeventoo.app.class_.TypeEvent;
import com.eeventoo.app.class_.UserSessionManager;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;
import static com.bumptech.glide.request.RequestOptions.centerCropTransform;

public class UpdateEventActivity extends AppCompatActivity {

    //variable pour le boutton date debut et heure debut
    int year_x, month_x, day_x, heure_x, min_x;
    //variable pour le boutton date fin et heure fin
    int year_df, month_df, day_df, heure_df, min_df;

    static final int DIALOG_ID = 0;
    static final int DIALOG_ID_DATE_FIN = 1;
    static final int DIALOG_ID_HEURE_DEB = 2;
    static final int DIALOG_ID_HEURE_FIN = 3;
    private String cp;
    private String ville;
    private String departement;
    private String region;
    private String paysNom;
    private String adresseNom;
    private String adresseComplet;
    private String paysCode;
    private double latitude,latitudeE;
    private double lontitude,lontitudeE;
    private String textAdress;
    private String tarif_type_inserer;
    private String  type_event_inserer;
    private boolean bool_payant;
    String idEvent;
    String nomEvent, typeE, adresseE,codepostalE,villeE, datedebE,datefinE,telephoneE, typeTarifE, tarifE,descriptionE,imageURLE,adresseCompletE,departementE,regionE,paysNomE,paysCodeE;
    private ArrayList<PaysIndicatif> listeIndicatif = new ArrayList() ;
    private   ArrayAdapter<PaysIndicatif> indicatifMonnaieArrayAdapter;

    private ArrayList <PaysMonnaie> listeMonnaie = new ArrayList() ;
    private   ArrayAdapter<PaysMonnaie> paysMonnaieArrayAdapter;



    private  ArrayList<TypeEvent> listeTypeEvent= new ArrayList<>();
    private ArrayAdapter<TypeEvent> listeTypeEventArrayAdapter ;
    private static final int TAKE_PICTURE_CAMERA_REQUEST = 2;
    //Editext
    EditText nom_E, tel_E,lieu_E, description_E;
    Spinner spinner_type_E;
    EditText  buttonDateDeb, buttonHeureDeb, buttonDateFin, buttonHeureFin;
    Button buttonSave, btnModifierTarif;
    private ImageView imageEvent;
    AlertDialog.Builder builder;
    String dateDebString, dateFinString, heureDebString, heureFinString;
    CoordinatorLayout coordinatorLayout;
    UserSessionManager session;
    RadioButton radioButtonGratuit, radioButtonPayant;

    Calendar calendarChoisieDateFin = Calendar.getInstance();
    Calendar calendarChoisieDateDebut = Calendar.getInstance();

    private Geocoder mGeocoder;
    private int PICK_IMAGE_REQUEST = 1;
    private Spinner tel_indicatif;

    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 30;
    private Uri filePath;
    private CheckBox checkBoxAddTel;
    String numTelString ="";
    private Button btnRepeterEvent, buttonAddPictureE;



    CollapsingToolbarLayout collapsingToolbarLayout;

    @BindView(R.id.radioGroupE)
    RadioGroup radioGroup;

    private RadioButton radioButton;
    ProgressDialog progressDialog;
    Calendar calendar = Calendar.getInstance(Locale.FRANCE);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upadte_event);


        //tester la connection reseau

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if(networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()) {
        }
        else{
            Toast toast = Toast.makeText(this, getString(R.string.aucuneConnexion), Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }

        year_x = calendar.get(Calendar.YEAR);
        month_x = calendar.get(Calendar.MONTH);
        day_x = calendar.get(Calendar.DAY_OF_MONTH);
        heure_x = calendar.get(Calendar.HOUR_OF_DAY);
        min_x = calendar.get(Calendar.MINUTE);
        year_df = calendar.get(Calendar.YEAR);
        month_df = calendar.get(Calendar.MONTH);
        day_df = calendar.get(Calendar.DAY_OF_MONTH);
        heure_df = calendar.get(Calendar.HOUR_OF_DAY);
        min_df = calendar.get(Calendar.MINUTE);
        nom_E = (EditText) findViewById(R.id.nomE);
        spinner_type_E = (Spinner) findViewById(R.id.typeE);
        buttonDateDeb = (EditText) findViewById(R.id.Buttondatedeb);
        buttonHeureDeb = (EditText) findViewById(R.id.Buttonheuredeb);
        buttonDateFin = (EditText) findViewById(R.id.ButtondatefinE);
        buttonHeureFin = (EditText) findViewById(R.id.ButtonheurefinE);
        buttonAddPictureE = findViewById(R.id.buttonAddPictureE);
        session = new UserSessionManager(getApplicationContext());
        tel_E = (EditText) findViewById(R.id.telephoneE);
        description_E = (EditText) findViewById(R.id.descriptionE);
        buttonSave = (Button) findViewById(R.id.buttonSaveE);
        imageEvent = (ImageView) findViewById(R.id.imageEvenement);
        lieu_E= (EditText) findViewById(R.id.lieuE);
        ButterKnife.bind(this);
        tel_indicatif = (Spinner) findViewById(R.id.telephoneIndicatif);
        radioButtonPayant= findViewById(R.id.radioPayant);
        radioButtonGratuit=findViewById(R.id.radioGratuit);
        checkBoxAddTel = findViewById(R.id.checkboxAddTel);
        mGeocoder = new Geocoder(this, Locale.getDefault());
        btnRepeterEvent=findViewById(R.id.btnRepeterEvent);




        imageURLE=getIntent().getExtras().getString("image");

        idEvent =getIntent().getExtras().getString("id");
        nomEvent  =getIntent().getExtras().getString("nom");
        typeE =getIntent().getExtras().getString("type");
        adresseE=getIntent().getExtras().getString("adresse");
        adresseCompletE=getIntent().getExtras().getString("adresseComplet");
        codepostalE=getIntent().getExtras().getString("codepostal");
        departementE=getIntent().getExtras().getString("departement");
        regionE=getIntent().getExtras().getString("region");
        paysNomE=getIntent().getExtras().getString("paysNom");
        paysCodeE=getIntent().getExtras().getString("paysCode");
        latitudeE=getIntent().getExtras().getDouble("latitude");
        lontitudeE=getIntent().getExtras().getDouble("lontitude");
        villeE=getIntent().getExtras().getString("ville");
        datedebE=getIntent().getExtras().getString("datedeb");
        datefinE=getIntent().getExtras().getString("datefin");
        telephoneE =getIntent().getExtras().getString("telephone");
        typeTarifE =getIntent().getExtras().getString("typeTarif");
        tarifE =getIntent().getExtras().getString("tarif");
        descriptionE=getIntent().getExtras().getString("description");

        //bouton modifier tarifs
        btnModifierTarif = findViewById(R.id.btnModifierTarif);
        btnModifierTarif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UpdateEventActivity.this, UpdateFormuleActivity.class);
                intent.putExtra("idEvent",idEvent);
                intent.putExtra("nomEvent",nomEvent);
                startActivity(intent);
            }
        });
        if(BuildConfig.DEBUG){
            Log.i("TESTLatitudeLonti", latitudeE+" "+lontitudeE);
        }
        try {
            getInformationByCoordinates(latitudeE,lontitudeE);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //image
        RequestOptions requestOptions = new RequestOptions();
        filePath=Uri.parse(imageURLE);
        if(BuildConfig.DEBUG){
            Log.i("URIimage", imageURLE);
        }

        requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true);
        Glide.with(this).load(imageURLE).apply(requestOptions).apply(centerCropTransform()).transition(withCrossFade()).into(imageEvent);


        //nomEvent
        nom_E.setText(nomEvent);
        //type

        //lieu
        lieu_E.setText(adresseE);
        textAdress=adresseE;
        //datedeb
        buttonDateDeb.setText(getDateDebOfDate(datedebE));

        //heuredeb
        buttonHeureDeb.setText(getHeureDebOfDate(datedebE));
        //datefin
        buttonDateFin.setText(getDateFinOfDate(datefinE));

        //heurefin
        buttonHeureFin.setText(getHeureFinOfDate(datefinE));

        //indicatif

        //numero de tel
        final String numTel = telephoneE.substring(telephoneE.lastIndexOf(")")+1);
        if(numTel.equals("NR")){
            tel_E.setText("");
        }
        else {
            tel_E.setText(numTel);
        }



        //type tarif
        String payantstr="Payant";
        if(typeTarifE.contains("y")){

            radioButtonPayant.setChecked(true);

            btnModifierTarif.setVisibility(View.VISIBLE);
            bool_payant=true;
            tarif_type_inserer="Payant";

        }
        else
        {
            radioButtonGratuit.setChecked(true);
            tarif_type_inserer="Gratuit";
            //prix

            bool_payant=false;

        }

        //description
        description_E.setText(descriptionE);



        spinner_type_E.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                //Pour cacher le clavier
                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                return false;
            }
        });
        spinner_type_E.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                typeEventAInserer(i);
                ((TextView) adapterView.getChildAt(0)).setTextColor(Color.WHITE);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        if(telephoneE.equals("NR")){
            checkBoxAddTel.setChecked(false);
        }
        else{
            checkBoxAddTel.setChecked(true);

            tel_indicatif.setVisibility(View.VISIBLE);
            tel_E.setVisibility(View.VISIBLE);
        }

        //ajouter numero de tel
        checkBoxAddTel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkAddTel();
            }
        });




        //indicatif adapter
        recupererIndicatif();
        tel_indicatif.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                //Pour cacher le clavier
                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                return false;
            }
        });

        tel_indicatif.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) adapterView.getChildAt(0)).setTextColor(Color.WHITE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        ArrayAdapter<CharSequence> typeAdapter = ArrayAdapter.createFromResource(UpdateEventActivity.this, R.array.type_eventStringAddEvent, android.R.layout.simple_spinner_item);
        spinner_type_E.setAdapter(typeAdapter);

        //recupererTypeEvent();
        //recupererMonnaie();
       /* spinner_devise.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                return false;
            }
        });

        spinner_devise.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) adapterView.getChildAt(0)).setTextColor(Color.WHITE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorAddE);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_AddE);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar_AddE);
        buttonAddPictureE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSelectImage();

            }
        });

        showDialogOnButtonDateDebClick();
        showDialogOnButtonDateFinClick();
        showDialogOnButtonheureDebClick();
        showDialogOnButtonheureFinClick();

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(getString(R.string.patientez));
        progressDialog.setMessage(getString(R.string.miseAjourEvent));

        lieu_E.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lancerAutocomplete();
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                String titre = getString(R.string.quelqueChoseMalPasse);
                String str_tout_champ = getString(R.string.remplissezToutChamp);
                String str_datef_inf_dateD = getString(R.string.checkDebFin);
                String str_dateD_inf_dateC = getString(R.string.checkDebCourante);
                String str_tel = getString(R.string.numeroTelError);
                String str_imageEvenement = getString(R.string.ajouterImagesvp);
                if(checkBoxAddTel.isChecked()){
                    numTelString = recupererIndicatifSpinner(tel_indicatif.getSelectedItem().toString()) + tel_E.getText().toString();
                }
                else {
                    numTelString="NR";
                }
                if (nom_E.getText().toString().equals("") ||
                        textAdress.equals("") ||
                        description_E.getText().toString().equals("") ||
                        radioGroup.getCheckedRadioButtonId() == -1) {
                    showDialog(titre, str_tout_champ);
                } else {

                    if (!calendarChoisieDateDebut.before(calendar)) {
                        if (calendarChoisieDateDebut.before(calendarChoisieDateFin)) {
                            if (filePath == null) {
                                showDialog(titre, str_imageEvenement);
                            } else {
                                if (!filePath.toString().equals(imageURLE)) {
                                    updateEvent(filePath, idEvent,
                                            session.getIdUser() + "",
                                            replaceApostrophe(nom_E.getText().toString()),
                                            replaceApostrophe(type_event_inserer),
                                            replaceApostrophe(adresseNom),
                                            replaceApostrophe(adresseComplet),
                                            replaceApostrophe(cp),
                                            replaceApostrophe(ville),
                                            replaceApostrophe(departement),
                                            replaceApostrophe(region),
                                            replaceApostrophe(paysNom),
                                            replaceApostrophe(paysCode),
                                            replaceApostrophe(String.valueOf(latitude)),
                                            replaceApostrophe(String.valueOf(lontitude)),
                                            replaceApostrophe(dateDebString + " " + heureDebString),
                                            replaceApostrophe(dateFinString + " " + heureFinString),
                                            numTelString,
                                            replaceApostrophe(tarif_type_inserer),
                                            replaceApostrophe(description_E.getText().toString()));
                                }
                                else {
                                    updateEventSansImage( idEvent,
                                            session.getIdUser() + "",
                                            replaceApostrophe(nom_E.getText().toString()),
                                            replaceApostrophe(type_event_inserer),
                                            replaceApostrophe(adresseNom),
                                            replaceApostrophe(adresseComplet),
                                            replaceApostrophe(cp),
                                            replaceApostrophe(ville),
                                            replaceApostrophe(departement),
                                            replaceApostrophe(region),
                                            replaceApostrophe(paysNom),
                                            replaceApostrophe(paysCode),
                                            replaceApostrophe(String.valueOf(latitude)),
                                            replaceApostrophe(String.valueOf(lontitude)),
                                            replaceApostrophe(dateDebString + " " + heureDebString),
                                            replaceApostrophe(dateFinString + " " + heureFinString),
                                            numTelString,                                            replaceApostrophe(tarif_type_inserer),
                                            replaceApostrophe(description_E.getText().toString()));
                                }
                            }
                        } else {
                            showDialog(titre, str_datef_inf_dateD);
                        }
                    } else {

                        showDialog(titre, str_dateD_inf_dateC);


                    }




                }
            }
        });
    }

    private String getHeureDebOfDate(String dateString) {
        String res =dateString.substring(11);
        heure_x=Integer.parseInt(res.substring(0,res.lastIndexOf(":")));
        min_x=Integer.parseInt(res.substring(res.lastIndexOf(":")+1));
        calendarChoisieDateDebut.set(Calendar.HOUR_OF_DAY,heure_x);
        calendarChoisieDateDebut.set(Calendar.MINUTE,min_x);
        heureDebString=heure_x+":"+min_x+":00";
        return outPutTime(calendarChoisieDateDebut);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //tester la connection reseau

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if(networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()) {
        }
        else{
            Toast toast = Toast.makeText(this, getString(R.string.aucuneConnexion), Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }
    private String getDateDebOfDate(String dateString) {
        String res =dateString.substring(0,10);
        day_x=Integer.parseInt(res.substring(0,2));
        month_x=Integer.parseInt(res.substring(3,5))-1;
        year_x=Integer.parseInt(res.substring(6));
        calendarChoisieDateDebut.set(Calendar.YEAR,year_x);
        calendarChoisieDateDebut.set(Calendar.MONTH,month_x );
        calendarChoisieDateDebut.set(Calendar.DAY_OF_MONTH,day_x);
        dateDebString=year_x+"-"+month_x+"-"+day_x;
        return  outPutDate(calendarChoisieDateDebut);
    }

    private String getHeureFinOfDate(String dateString) {
        String res =dateString.substring(11);
        heure_df=Integer.parseInt(res.substring(0,res.lastIndexOf(":")));
        min_df=Integer.parseInt(res.substring(res.lastIndexOf(":")+1));
        calendarChoisieDateFin.set(Calendar.HOUR_OF_DAY,heure_df);
        calendarChoisieDateFin.set(Calendar.MINUTE,min_df);
        heureFinString=heure_df+":"+min_df+":00";
        return outPutTime(calendarChoisieDateFin);
    }

    private String getDateFinOfDate(String dateString) {
        String res =dateString.substring(0,10);
        day_df=Integer.parseInt(res.substring(0,2));
        month_df=Integer.parseInt(res.substring(3,5))-1;
        year_df=Integer.parseInt(res.substring(6));

        calendarChoisieDateFin.set(Calendar.YEAR,year_df);
        calendarChoisieDateFin.set(Calendar.MONTH,month_df);
        calendarChoisieDateFin.set(Calendar.DAY_OF_MONTH,day_df);
        dateFinString=year_df+"-"+month_df+"-"+day_df;
        return  outPutDate(calendarChoisieDateFin);
    }


    private void getInformationByCoordinates(double lat, double lon) throws IOException {

        List<Address> addresses = mGeocoder.getFromLocation(lat, lon, 1);
        if (addresses != null && addresses.size() > 0) {

            adresseNom = checkNullString(addresses.get(0).getFeatureName()) + " " + checkNullString(addresses.get(0).getThoroughfare());
            cp = checkNullString(addresses.get(0).getPostalCode());
            ville = checkNullString(addresses.get(0).getLocality());
            departement = checkNullString(addresses.get(0).getSubAdminArea());
            region = checkNullString(addresses.get(0).getAdminArea());
            paysNom = checkNullString(addresses.get(0).getCountryName());
            paysCode = checkNullString(addresses.get(0).getCountryCode());
            latitude = lat;
            lontitude = lon;
            adresseComplet = adresseNom + "," + cp + "," + ville + "," + departement + "," + region + "," + paysNom + "," + paysCode + "," + latitude + "," + lontitude;
            if (BuildConfig.DEBUG) {
                Log.i("ADRESSE", "getInformationByCoordinates: " + adresseComplet);
            }
        }

    }

    private String checkNullString(String str) {
        if (str != null) {
            return str;
        } else {
            return "";
        }
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        /*Intent intent = new Intent(Intent.ACTION_PICK,
        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);*/
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }



    private String recupererIndicatifSpinner(String s){

        return s.substring(s.lastIndexOf("("), s.lastIndexOf(")")+1);
    }

    private String recupererSymboleMonnaieSpinner(String s){

        String res ="";
        if(bool_payant) {
            res= s.substring(s.lastIndexOf("("), s.lastIndexOf(")") + 1);
        }
        return  res;
    }





    public void takePictureCamera() {

        Intent pickPhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(pickPhoto, TAKE_PICTURE_CAMERA_REQUEST);

    }

    private void dialogSelectImage() {
        final CharSequence[] items = {getString(R.string.prendrePhoto),getString(R.string.selectPhoto) };
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);

        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals(getString(R.string.prendrePhoto))) {
                    takePictureCamera();
                } else if (items[item].equals(getString(R.string.selectPhoto))) {

                    showFileChooser();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            filePath = data.getData();
            Glide.with(this).load(filePath).apply(new RequestOptions()
                    .centerCrop()).transition(withCrossFade()).into(imageEvent);
        }

        if (requestCode == TAKE_PICTURE_CAMERA_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            filePath = data.getData();
            Glide.with(this).load(filePath).apply(new RequestOptions()
                    .centerCrop()).transition(withCrossFade()).into(imageEvent);
        }
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                try {
                    getInformationByCoordinates(place.getLatLng().latitude, place.getLatLng().longitude);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                textAdress = place.getName() + "";
                lieu_E.setText(place.getName());

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i("PLACEERROR", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }

    }





    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void updateEvent(
            Uri fileUri,
            String idEvent,
            String idUser,
            String nom,
            String type,
            String adresse,
            String adresseComplet,
            String codepostal,
            String ville,
            String departement,
            String region,
            String paysNom,
            String paysCode,
            String latitude,
            String lontitude,
            String datedeb,
            String datefin,
            String telephone,
            String typeTarif,
            String description) {

        progressDialog.show();
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        // create upload service client
        FileUploadService service =
                ServiceGenerator.createService(FileUploadService.class);

        // https://github.com/iPaulPro/aFileChooser/blob/master/aFileChooser/src/com/ipaulpro/afilechooser/utils/FileUtils.java
        // use the FileUtils to get the actual file by uri
        File file = FileUtils.getFile(this, fileUri);


        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse(getContentResolver().getType(fileUri)),
                        file
                );

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file", file.getName(), requestFile);

        RequestBody r_idEvent =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, idEvent);
        RequestBody r_idUser =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, idUser);
        RequestBody r_nom =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, nom);

        RequestBody r_type =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, type);
        RequestBody r_adresse =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, adresse);

        RequestBody r_adresseComplet =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, adresseComplet);

        RequestBody r_codepostal =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, codepostal);

        RequestBody r_departement =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, departement);
        RequestBody r_region =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, region);
        RequestBody r_paysNom =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, paysNom);
        RequestBody r_paysCode =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, paysCode);
        RequestBody r_latitude =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, latitude);
        RequestBody r_lontitude =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, lontitude);

        RequestBody r_ville =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, ville);
        RequestBody r_datedeb =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, datedeb);
        RequestBody r_datefin =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, datefin);
        RequestBody r_telephone =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, telephone);
        RequestBody r_typeTarif =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, typeTarif);


        RequestBody r_description =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, description);


        Call<ReponseServeur> call = service.updateEvent(body, r_idEvent, r_idUser, r_nom, r_type, r_adresse, r_adresseComplet, r_codepostal, r_ville, r_departement, r_region, r_paysNom, r_paysCode, r_latitude, r_lontitude, r_datedeb, r_datefin, r_telephone, r_typeTarif, r_description);
        call.enqueue(new Callback<ReponseServeur>() {
            @Override
            public void onResponse(Call<ReponseServeur> call, Response<ReponseServeur> response) {
                if(response.body() != null){

                    if (response.isSuccessful()) {
                        if (BuildConfig.DEBUG) {
                            Toast.makeText(UpdateEventActivity.this, response.body().getMessage().toString(), Toast.LENGTH_SHORT).show();

                            Snackbar.make(coordinatorLayout, response.body().getMessage().toString(), Snackbar.LENGTH_LONG);
                        }
                        progressDialog.dismiss();
                        Intent intent = new Intent(UpdateEventActivity.this, ProfilActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        if (BuildConfig.DEBUG) {
                            Log.i("Upload", " message :" + response.body().getMessage().toString());
                        }
                    }
                }

                else {
                    Toast.makeText(UpdateEventActivity.this, getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ReponseServeur> call, Throwable t) {
                if (BuildConfig.DEBUG) {
                    Log.e("Upload error:", t.getMessage() + " " + call.toString());
                }
            }

        });
    }

    private void updateEventSansImage(
            String idEvent,
            String idUser,
            String nom,
            String type,
            String adresse,
            String adresseComplet,
            String codepostal,
            String ville,
            String departement,
            String region,
            String paysNom,
            String paysCode,
            String latitude,
            String lontitude,
            String datedeb,
            String datefin,
            String telephone,
            String typeTarif,
            String description) {

        progressDialog.show();
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        // create upload service client
        FileUploadService service =
                ServiceGenerator.createService(FileUploadService.class);


        RequestBody r_idEvent =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, idEvent);
        RequestBody r_idUser =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, idUser);
        RequestBody r_nom =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, nom);

        RequestBody r_type =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, type);
        RequestBody r_adresse =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, adresse);

        RequestBody r_adresseComplet =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, adresseComplet);

        RequestBody r_codepostal =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, codepostal);

        RequestBody r_departement =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, departement);
        RequestBody r_region =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, region);
        RequestBody r_paysNom =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, paysNom);
        RequestBody r_paysCode =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, paysCode);
        RequestBody r_latitude =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, latitude);
        RequestBody r_lontitude =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, lontitude);

        RequestBody r_ville =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, ville);
        RequestBody r_datedeb =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, datedeb);
        RequestBody r_datefin =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, datefin);
        RequestBody r_telephone =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, telephone);
        RequestBody r_typeTarif =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, typeTarif);


        RequestBody r_description =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, description);


        Call<ReponseServeur> call = service.updateEventSansImage(r_idEvent, r_idUser, r_nom, r_type, r_adresse, r_adresseComplet, r_codepostal, r_ville, r_departement, r_region, r_paysNom, r_paysCode, r_latitude, r_lontitude, r_datedeb, r_datefin, r_telephone, r_typeTarif, r_description);
        call.enqueue(new Callback<ReponseServeur>() {
            @Override
            public void onResponse(Call<ReponseServeur> call, Response<ReponseServeur> response) {

                if(response.body() != null){
                    if (response.body().isSuccess()){
                        if (BuildConfig.DEBUG) {
                            Toast.makeText(UpdateEventActivity.this, response.body().getMessage().toString(), Toast.LENGTH_SHORT).show();

                            Snackbar.make(coordinatorLayout, response.body().getMessage().toString(), Snackbar.LENGTH_LONG);
                        }
                        progressDialog.dismiss();
                        Intent intent = new Intent(UpdateEventActivity.this, ProfilActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        if (BuildConfig.DEBUG) {
                            Log.i("Upload", " message :" + response.body().getMessage().toString());
                        }
                    }
                }

                else {
                    Toast.makeText(UpdateEventActivity.this, getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ReponseServeur> call, Throwable t) {
                if (BuildConfig.DEBUG) {
                    Log.e("Upload error:", t.getMessage() + " " + call.toString());
                }
            }

        });
    }



    public void lancerAutocomplete() {

        try

        {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch(
                GooglePlayServicesRepairableException e)

        {
            // TODO: Handle the error.
        } catch(
                GooglePlayServicesNotAvailableException e)

        {
            // TODO: Handle the error.
        }

    }


    public void showDialogOnButtonDateDebClick(){
        buttonDateDeb.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                hideKeyboard();
                showDialog(DIALOG_ID);
            }
        });
    }
    //selection de la date de fin
    public void showDialogOnButtonDateFinClick(){
        buttonDateFin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
                showDialog(DIALOG_ID_DATE_FIN);
            }
        });
    }
    //selection heure et min de debut
    public void showDialogOnButtonheureDebClick(){
        buttonHeureDeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
                showDialog(DIALOG_ID_HEURE_DEB);
            }
        });
    }
    //selection heure et minute de fin
    public void showDialogOnButtonheureFinClick(){
        buttonHeureFin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
                showDialog(DIALOG_ID_HEURE_FIN);
            }
        });
    }
    @Override
    protected Dialog onCreateDialog(int id){
        if (id == DIALOG_ID){
            DatePickerDialog datePickerDialog =new DatePickerDialog(this,datePickerListener,year_x,month_x,day_x);
//            datePickerDialog.setButton(DialogInterface.BUTTON_NEUTRAL, getString(R.string.aujourdhui), new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialogInterface, int i) {
//                    Calendar calendar = Calendar.getInstance(Locale.FRANCE);
//                    year_x = calendar.get(Calendar.YEAR);
//                    month_x=calendar.get(Calendar.MONTH)+1;
//                    day_x=calendar.get(Calendar.DAY_OF_MONTH);
//                    buttonDateDeb.setText(day_x+"-"+month_x+"-"+year_x);
//                    dateDebString=year_x+"-"+month_x+"-"+day_x;
//                    if(BuildConfig.DEBUG) {
//                        Toast.makeText(UpdateEventActivity.this, dateDebString, Toast.LENGTH_SHORT).show();
//                    }
//
//                }
//            }
//            );
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            return  datePickerDialog;
        }
        else if (id==DIALOG_ID_DATE_FIN){
            DatePickerDialog datePickerDialog=  new DatePickerDialog(this,datePickerDateFinListener,year_df,month_df,day_df);
//            datePickerDialog.setButton(DialogInterface.BUTTON_NEUTRAL, getString(R.string.aujourdhui), new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialogInterface, int i) {
//                    Calendar calendar = Calendar.getInstance(Locale.FRANCE);
//                    year_df = calendar.get(Calendar.YEAR);
//                    month_df=calendar.get(Calendar.MONTH)+1;
//                    day_df=calendar.get(Calendar.DAY_OF_MONTH);
//                    buttonDateFin.setText(day_df+"-"+month_df+"-"+year_df);
//                    dateFinString=year_df+"-"+month_df+"-"+day_df;
//
//                    if(BuildConfig.DEBUG) {
//                        Toast.makeText(UpdateEventActivity.this, dateFinString, Toast.LENGTH_SHORT).show();
//                    }
//                }
//            });
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            return  datePickerDialog;
        }
        else if (id==DIALOG_ID_HEURE_DEB){
            TimePickerDialog timePickerDialog =new TimePickerDialog(this,timePickerHeureDebListener,heure_x,min_x,true);

//            timePickerDialog.setButton(DialogInterface.BUTTON_NEUTRAL, getString(R.string.maintenant), new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialogInterface, int i) {
//                    Calendar calendar = Calendar.getInstance();
//                    heure_x = calendar.get(Calendar.HOUR_OF_DAY);
//                    min_x = calendar.get(Calendar.MINUTE);
//
//                    buttonHeureDeb.setText(heure_x + ":" + min_x);
//                    heureDebString = heure_x + ":" + min_x + ":00";
//
//                    if(BuildConfig.DEBUG) {
//                        Toast.makeText(UpdateEventActivity.this, heureDebString, Toast.LENGTH_SHORT).show();
//                    }
//                }
//            });
            return  timePickerDialog;
        }
        else if (id==DIALOG_ID_HEURE_FIN){
            TimePickerDialog timePickerDialog = new TimePickerDialog(this,timePickerHeureFinListener,heure_df,min_df,true);

//            timePickerDialog.setButton(DialogInterface.BUTTON_NEUTRAL, getString(R.string.maintenant), new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialogInterface, int i) {
//                    Calendar calendar = Calendar.getInstance();
//                    heure_df=calendar.get(Calendar.HOUR_OF_DAY);
//                    min_df= calendar.get(Calendar.MINUTE);
//
//                    buttonHeureFin.setText(heure_df+":"+min_df);
//                    heureFinString=heure_df+":"+min_df+":00";
//                    if(BuildConfig.DEBUG) {
//                        Toast.makeText(UpdateEventActivity.this, heureFinString, Toast.LENGTH_SHORT).show();
//                    }
//                }
//            });

            return  timePickerDialog;
        }
        return null;
    }

    //Datepicker de la date de debut
    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {

            calendarChoisieDateDebut.set(Calendar.YEAR,year);
            calendarChoisieDateDebut.set(Calendar.MONTH,monthOfYear);
            calendarChoisieDateDebut.set(Calendar.DAY_OF_MONTH,dayOfMonth);


            if (calendarChoisieDateDebut.before(calendar)){
                year_x = calendar.get(Calendar.YEAR);
                month_x=calendar.get(Calendar.MONTH)+1;
                day_x=calendar.get(Calendar.DAY_OF_MONTH);
                buttonDateDeb.setText(day_x + "-" + month_x + "-" + year_x);
                dateDebString = year_x + "-" + month_x + "-" + day_x;
                if(BuildConfig.DEBUG) {
                    Toast.makeText(UpdateEventActivity.this, dateDebString, Toast.LENGTH_SHORT).show();
                }

            }else {
                year_x = year;
                month_x = monthOfYear + 1;
                day_x = dayOfMonth;
                buttonDateDeb.setText(day_x + "-" + month_x + "-" + year_x);
                dateDebString = year_x + "-" + month_x + "-" + day_x;
                if(BuildConfig.DEBUG) {
                    Toast.makeText(UpdateEventActivity.this, dateDebString, Toast.LENGTH_SHORT).show();
                }
            }
        }
    };
    //Datepicker de la date de fin
    private DatePickerDialog.OnDateSetListener datePickerDateFinListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {

            calendarChoisieDateFin.set(Calendar.YEAR,year);
            calendarChoisieDateFin.set(Calendar.MONTH,monthOfYear);
            calendarChoisieDateFin.set(Calendar.DAY_OF_MONTH,dayOfMonth);


            if (calendarChoisieDateFin.before(calendar)) {
                year_df = calendar.get(Calendar.YEAR);
                month_df=calendar.get(Calendar.MONTH)+1;
                day_df=calendar.get(Calendar.DAY_OF_MONTH);

                buttonDateFin.setText(day_df + "-" + month_df + "-" + year_df);
                dateFinString = year_df + "-" + month_df + "-" + day_df;
                if(BuildConfig.DEBUG) {
                    Toast.makeText(UpdateEventActivity.this, dateFinString, Toast.LENGTH_SHORT).show();
                }
            }
            else {

                year_df = year;
                month_df = monthOfYear + 1;
                day_df = dayOfMonth;
                buttonDateFin.setText(day_df + "-" + month_df + "-" + year_df);
                dateFinString = year_df + "-" + month_df + "-" + day_df;
                if(BuildConfig.DEBUG) {
                    Toast.makeText(UpdateEventActivity.this, dateFinString, Toast.LENGTH_SHORT).show();
                }
            }
        }
    };
    //Timepicker de la heure de debut
    private TimePickerDialog.OnTimeSetListener timePickerHeureDebListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int heure, int min) {
            heure_x=heure;
            min_x=min;
            calendarChoisieDateDebut.set(Calendar.HOUR_OF_DAY,heure_x);
            calendarChoisieDateDebut.set(Calendar.MINUTE,min_x);
            buttonHeureDeb.setText(heure_x+":"+min_x);
            heureDebString=heure_x+":"+min_x+":00";
            if(BuildConfig.DEBUG) {
                Toast.makeText(UpdateEventActivity.this, heureDebString, Toast.LENGTH_SHORT).show();
            }
        }
    };
    //Timepicker de la heure de fin
    private TimePickerDialog.OnTimeSetListener timePickerHeureFinListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int heure, int min) {
            heure_df=heure;
            min_df=min;

            calendarChoisieDateFin.set(Calendar.HOUR_OF_DAY,heure_df);
            calendarChoisieDateFin.set(Calendar.MINUTE,min_df);
            buttonHeureFin.setText(heure_df+":"+min_df);
            heureFinString=heure_df+":"+min_df+":00";
            if(BuildConfig.DEBUG) {
                Toast.makeText(UpdateEventActivity.this, heureFinString, Toast.LENGTH_SHORT).show();
            }

        }
    };
    private void hideKeyboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }





    public void showDialog(String title, String message){
        builder = new AlertDialog.Builder(UpdateEventActivity.this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }



    public String replaceApostrophe(String str){

        String str_=str.replaceAll("'", "\\\\'");
        return str_;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_desc_event, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();

        }

        return super.onOptionsItemSelected(item);
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radioGratuit:
                if (checked)

                    bool_payant=false;
                tarif_type_inserer="Gratuit";
                btnModifierTarif.setVisibility(View.GONE);
                break;
            case R.id.radioPayant:
                if (checked)
                    btnModifierTarif.setVisibility(View.VISIBLE);
                bool_payant=true;
                tarif_type_inserer="Payant";
                break;
        }
    }

    public void typeEventAInserer(int i){
        switch (i){
            case 0:
                type_event_inserer ="Soirées";
                break;
            case 1:
                type_event_inserer ="Concerts";
                break;
            case 2:
                type_event_inserer ="Films";
                break;
            case 3:
                type_event_inserer ="Spectacles";
                break;
            case 4:
                type_event_inserer ="Festivals";
                break;
        }

    }


    private void recupererIndicatif(){
        I_IndicatifCountry service =
                ServiceGenerator.createService(I_IndicatifCountry.class);

        Call<ResponseIndicatif> call = service.getIndicatif();

        call.enqueue(new Callback<ResponseIndicatif>() {
            @Override
            public void onResponse(Call<ResponseIndicatif> call, Response<ResponseIndicatif> response) {
                if(response.body() != null){
                    if (response.body().isSuccess()){
                        listeIndicatif=response.body().getData();

                        indicatifMonnaieArrayAdapter = new ArrayAdapter<PaysIndicatif>(UpdateEventActivity.this,android.R.layout.simple_spinner_item,listeIndicatif);
                        indicatifMonnaieArrayAdapter.setDropDownViewResource(R.layout.spinner_ajout_e);


                        tel_indicatif.setAdapter(indicatifMonnaieArrayAdapter);
                        tel_indicatif.setSelection(recupererIndexIndicatif());
                        if(BuildConfig.DEBUG){
                            Toast.makeText(UpdateEventActivity.this, listeIndicatif.size()+"", Toast.LENGTH_SHORT).show();
                            Log.i("LIST_INDICATIF", listeIndicatif.toString());


                        }
                    }
                    else{
                        if(BuildConfig.DEBUG){
                            Toast.makeText(UpdateEventActivity.this, "error Indicatif succes False", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                else {
                    Toast.makeText(UpdateEventActivity.this, getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseIndicatif> call, Throwable t) {
                if(BuildConfig.DEBUG) {
                    Toast.makeText(UpdateEventActivity.this, "Error Failure", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void recupererTypeEvent(){

        I_TypeEvent service = ServiceGenerator.createService(I_TypeEvent.class);
        Call<ResponseTypeEvent> call = service.getTypeEvent();

        call.enqueue(new Callback<ResponseTypeEvent>() {
            @Override
            public void onResponse(Call<ResponseTypeEvent> call, Response<ResponseTypeEvent> response) {
                if(response.body() != null){
                    if (response.body().isSuccess()){
                        listeTypeEvent=response.body().getData();



                        listeTypeEventArrayAdapter = new ArrayAdapter<TypeEvent>(UpdateEventActivity.this,android.R.layout.simple_spinner_item,listeTypeEvent);
                        listeTypeEventArrayAdapter.setDropDownViewResource(R.layout.spinner_ajout_e);

                        spinner_type_E.setAdapter(listeTypeEventArrayAdapter);
                        if(BuildConfig.DEBUG){
                            Toast.makeText(UpdateEventActivity.this, listeTypeEvent.size()+"", Toast.LENGTH_SHORT).show();
                            Log.i("LIST_TYPE_EVENT", listeTypeEvent.toString());


                        }
                        spinner_type_E.setSelection(recupererIndexTypeTarif());
                    }

                }

                else {
                    Toast.makeText(UpdateEventActivity.this, getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseTypeEvent> call, Throwable t) {

            }
        });
    }



    private  int recupererIndexIndicatif(){
        int res=0;
        if (telephoneE.contains("(")){
            for (int i=0; i<listeIndicatif.size();i++){
                if (listeIndicatif.get(i).toString().contains(recupererIndicatifSpinner(telephoneE))){
                    res=i;
                }
            }
        }

        return res;
    }


    private  int recupererIndexTypeTarif(){
        int res=0;

        for (int i=0; i<listeTypeEvent.size();i++){
            if (listeTypeEvent.get(i).toString().contains(typeE)){
                res=i;
            }
        }


        return res;
    }
    private  int recupererIndexMonnaie(){
        int res=0;

        for (int i=0; i<listeMonnaie.size();i++){
            if (listeMonnaie.get(i).toString().contains(tarifE.substring(tarifE.lastIndexOf("(")))){
                res=i;
            }
        }


        return res;
    }

    public void checkAddTel(){
        if (checkBoxAddTel.isChecked()){
            tel_E.setVisibility(View.VISIBLE);
            tel_indicatif.setVisibility(View.VISIBLE);
        }
        else {
            tel_E.setVisibility(View.GONE);
            tel_indicatif.setVisibility(View.GONE);
            tel_E.setText("");
        }
    }

    public String outPutDate(Calendar calendar){

        int style = DateFormat.MEDIUM;

        DateFormat df =  DateFormat.getDateInstance(style,this.getResources().getConfiguration().locale);;
        return df.format(calendar.getTime());
    }

    public String outPutTime(Calendar calendar){
        String pattern = "HH:mm";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat (pattern);
        return   simpleDateFormat.format(calendar.getTime());
    }
}
