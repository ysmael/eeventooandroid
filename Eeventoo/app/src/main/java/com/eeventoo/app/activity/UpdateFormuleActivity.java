package com.eeventoo.app.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.eeventoo.app.Adapter.AdapterFormule;
import com.eeventoo.app.BuildConfig;
import com.eeventoo.app.InterfaceRetrofit.I_Event;
import com.eeventoo.app.InterfaceRetrofit.I_PaysMonnaie;
import com.eeventoo.app.R;
import com.eeventoo.app.class_.Formule;
import com.eeventoo.app.class_.PaysMonnaie;
import com.eeventoo.app.class_.ReponseServeur;
import com.eeventoo.app.class_.ResponsePaysMonnaie;
import com.eeventoo.app.class_.ResponselistFormule;
import com.eeventoo.app.class_.ServiceGenerator;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateFormuleActivity extends AppCompatActivity {

    private ArrayList<Formule> formules = new ArrayList<>();
    private  String idEvent, nom;
    public AdapterFormule adapterFormule;
    private Spinner spinner_devise;
    private ArrayList <PaysMonnaie> listeMonnaie = new ArrayList() ;
    private ArrayAdapter<PaysMonnaie> paysMonnaieArrayAdapter;
    RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    String tarifE;
    String requete = "";
    String requeteDeleteOlder = "";
    String requeteUpdatePayant ="";
    ProgressBar progressBar;
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_formule);

        spinner_devise= (Spinner)findViewById(R.id.spinner_devise);
        progressBar = (ProgressBar) findViewById(R.id.progressBar1);
        idEvent =getIntent().getExtras().getString("idEvent");
        nom =getIntent().getExtras().getString("nomEvent");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(nom);
        fab = (FloatingActionButton) findViewById(R.id.fab);


        spinner_devise.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                return false;
            }
        });

        spinner_devise.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) adapterView.getChildAt(0)).setTextColor(Color.WHITE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adapterFormule.addItem();
            }
        });

        getFormules(idEvent);
        //recupererMonnaie doit etre apeller apres getFormules car il a besoin de la liste des formules
        recupererMonnaie();

    }


    public void getFormules(final String idEvent){

        I_Event i_event = ServiceGenerator.createService(I_Event.class);
        Call<ResponselistFormule> call = i_event.getFomulesEvent(idEvent);

        call.enqueue(new Callback<ResponselistFormule>() {
            @Override
            public void onResponse(Call<ResponselistFormule> call, Response<ResponselistFormule> response) {
                  if(response.body() != null){
                      if (response.body().isSuccess()) {
                          formules = response.body().getData();

                          Toast.makeText(UpdateFormuleActivity.this, formules.size()+"", Toast.LENGTH_SHORT).show();
                          adapterFormule= new AdapterFormule(UpdateFormuleActivity.this,formules);

                          recyclerView=findViewById(R.id.recyclerFormule);
                          layoutManager = new LinearLayoutManager(UpdateFormuleActivity.this);
                          recyclerView.setLayoutManager(layoutManager);


                          recyclerView.setAdapter(adapterFormule);

                      }
                      else{
                          Toast.makeText(UpdateFormuleActivity.this, getString(R.string.quelqueChoseMalPasse), Toast.LENGTH_SHORT).show();

                      }

                }

                else {
                    Toast.makeText(UpdateFormuleActivity.this, getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponselistFormule> call, Throwable t) {
                Toast.makeText(UpdateFormuleActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private ArrayList<Formule> populateList(){

        ArrayList<Formule> list = new ArrayList<>();

        for(int i = 0; i < 1; i++){
            Formule formule = new Formule();
            list.add(formule);
        }

        return list;
    }

     private void recupererMonnaie(){
        I_PaysMonnaie service = ServiceGenerator.createService(I_PaysMonnaie.class);
        Call<ResponsePaysMonnaie> call = service.getMonnaie();

        call.enqueue(new Callback<ResponsePaysMonnaie>() {
            @Override
            public void onResponse(Call<ResponsePaysMonnaie> call, Response<ResponsePaysMonnaie> response) {
                  if(response.body() != null){
                      if (response.body().isSuccess()) {
                          listeMonnaie = response.body().getData();

                          paysMonnaieArrayAdapter = new ArrayAdapter<PaysMonnaie>(UpdateFormuleActivity.this, android.R.layout.simple_spinner_item, listeMonnaie);
                          paysMonnaieArrayAdapter.setDropDownViewResource(R.layout.spinner_ajout_e);


                          spinner_devise.setAdapter(paysMonnaieArrayAdapter);
                          if (BuildConfig.DEBUG) {
                              Toast.makeText(UpdateFormuleActivity.this, listeMonnaie.size() + "", Toast.LENGTH_SHORT).show();
                              Log.i("LIST_INDICATIF", listeMonnaie.toString());


                          }


                          spinner_devise.setSelection(recupererIndexMonnaie());

                      }
                      else{
                          if(BuildConfig.DEBUG){
                              Toast.makeText(UpdateFormuleActivity.this, "error Indicatif succes False", Toast.LENGTH_SHORT).show();
                          }
                      }
                }

                else {
                    Toast.makeText(UpdateFormuleActivity.this, getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponsePaysMonnaie> call, Throwable t) {
                if(BuildConfig.DEBUG) {
                    Toast.makeText(UpdateFormuleActivity.this, "Error Failure", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private  int recupererIndexMonnaie(){
        int res=0;
if(!formules.isEmpty()) {
    for (int i = 0; i < listeMonnaie.size(); i++) {
        if (listeMonnaie.get(i).toString().contains(formules.get(0).getDevise())) {
            res = i;
        }
    }
}


        return res;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.saveButton) {
            formules=adapterFormule.getListFormules();
            Log.i("FormuleActivity", "onCreate: formules size : "+ formules.size() );

                requeteDeleteOlder += "DELETE FROM `formulesEvent` WHERE idEvent="+idEvent+"; ";
                requeteUpdatePayant +="UPDATE `evenement_tb` SET `typeTarif`=\"Payant\" WHERE id="+idEvent+";";
                for (int j=0; j<formules.size();j++){

                    requete += "INSERT INTO `formulesEvent`(`idEvent`, `description`, `prix`, `devise`) VALUES ("+idEvent+",'"+
                            formules.get(j).getDescription()+"','"+
                            formules.get(j).getPrix()+"','"+
                            recupererSymboleMonnaieSpinner(spinner_devise.getSelectedItem().toString())+"') ;";
                }


            addFormules(requeteDeleteOlder+requeteUpdatePayant + requete);
        }

        Log.i("FORMULEREQUETE", requeteDeleteOlder + requete );
        return super.onOptionsItemSelected(item);
    }

    public void addFormules(String requete){
        I_Event i_event= ServiceGenerator.createService(I_Event.class);
        Call<ReponseServeur>  call = i_event.setFormuleEvent(requete);
        //Displaying Progressbar
        showProgressLoad();
        call.enqueue(new Callback<ReponseServeur>() {
                         @Override
                         public void onResponse(Call<ReponseServeur> call, Response<ReponseServeur> response) {
                             hideProgressLoad();
                               if(response.body() != null){
                                   if(response.body().isSuccess()){
                                       Toast.makeText(UpdateFormuleActivity.this, getString(R.string.modificationSucces), Toast.LENGTH_SHORT).show();
                                       Intent intent = new Intent(UpdateFormuleActivity.this, MainActivity.class);
                                       startActivity(intent);
                                   }
                                   else {
                                       Toast.makeText(UpdateFormuleActivity.this, getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();

                                   }
                }

                else {
                    Toast.makeText(UpdateFormuleActivity.this, getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                }

                         }

                         @Override
                         public void onFailure(Call<ReponseServeur> call, Throwable t) {
                             hideProgressLoad();
                             if(BuildConfig.DEBUG) {
                                 Toast.makeText(UpdateFormuleActivity.this, "Error Failure", Toast.LENGTH_SHORT).show();
                             }
                         }
                     }
        );
    }

    public void hideProgressLoad(){
        progressBar.setVisibility(View.GONE);
    }

    public void showProgressLoad(){
        progressBar.setVisibility(View.VISIBLE);
    }

    private String recupererSymboleMonnaieSpinner(String s){

        String res= s.substring(s.lastIndexOf("("));

        return  res;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_save_formules, menu);

        return true;
    }

}
