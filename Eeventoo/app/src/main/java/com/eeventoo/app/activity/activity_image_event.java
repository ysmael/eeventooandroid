package com.eeventoo.app.activity;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ImageView;

import com.android.volley.toolbox.ImageLoader;
import com.eeventoo.app.R;
import com.bumptech.glide.Glide;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class activity_image_event extends AppCompatActivity {
    private ImageView imageView;
    private ImageLoader imageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_event);
        imageView = (ImageView) findViewById(R.id.imagefull);

        String imageURL = getIntent().getExtras().getString("image");
        String nom = getIntent().getExtras().getString("nom");
        setTitle(nom);
        //imageLoader = CustomVolleyRequest.getInstance(this).getImageLoader();
        //networkImageView.setImageUrl(imageURL, imageLoader);
        Glide.with(this).load(imageURL).transition(withCrossFade()).into(imageView);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();

        }

        return super.onOptionsItemSelected(item);
    }

}