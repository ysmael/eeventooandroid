package com.eeventoo.app.backgroundTask;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import androidx.appcompat.app.AlertDialog;

import com.eeventoo.app.class_.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by isma on 06/07/2016.
 */

public class AddEventTask extends AsyncTask<String,Void,String> {

    String addEvent_url= Constants.IP_URL+"webapp/addEvent.php";
    Context ctx;
    ProgressDialog progressDialog;
    Activity activity;
    AlertDialog.Builder builder;
    @Override
    protected void onPreExecute() {
        builder = new AlertDialog.Builder(activity);
        progressDialog = new ProgressDialog(ctx);
        progressDialog.setTitle("Please Wait");
        progressDialog.setMessage("Connecting to server...");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... params) {
        String method= params[0];
        if(method.equals("addEvent")){
            try {
                URL url= new URL(addEvent_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter= new BufferedWriter(new OutputStreamWriter(outputStream));
                String id = params[1];
                String nom = params[2];
                String type =params[3];
                String adresse = params[4];
                String cp =params[5];
                String ville = params[6];
                String datedeb =params[7];
                String datefin = params[8];
                String tel =params[9];
                String tarifE =params[10];
                String desc =params[11];
                String imag =params[12];

                String data = URLEncoder.encode("id","UTF-8")+"="+URLEncoder.encode(id,"UTF-8")+"&"+
                        URLEncoder.encode("nom","UTF-8")+"="+URLEncoder.encode(nom,"UTF-8")+"&"+
                        URLEncoder.encode("type","UTF-8")+"="+URLEncoder.encode(type,"UTF-8")+"&"+
                        URLEncoder.encode("adresse","UTF-8")+"="+URLEncoder.encode(adresse,"UTF-8")+"&"+
                        URLEncoder.encode("codepostal","UTF-8")+"="+URLEncoder.encode(cp,"UTF-8")+"&"+
                        URLEncoder.encode("ville","UTF-8")+"="+URLEncoder.encode(ville,"UTF-8")+"&"+
                        URLEncoder.encode("datedebut","UTF-8")+"="+URLEncoder.encode(datedeb,"UTF-8")+"&"+
                        URLEncoder.encode("datefin","UTF-8")+"="+URLEncoder.encode(datefin,"UTF-8")+"&"+
                        URLEncoder.encode("telephone","UTF-8")+"="+URLEncoder.encode(tel,"UTF-8")+"&"+
                        URLEncoder.encode("tarifEntree","UTF-8")+"="+URLEncoder.encode(tarifE,"UTF-8")+"&"+
                        URLEncoder.encode("description","UTF-8")+"="+URLEncoder.encode(desc,"UTF-8")+"&"+
                        URLEncoder.encode("image","UTF-8")+"="+URLEncoder.encode(imag,"UTF-8");
                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();

                InputStream inputStream= httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder stringBuilder = new StringBuilder();
                String line="";
                while ((line = bufferedReader.readLine())!=null){
                    stringBuilder.append(line+"\n");
                }
                httpURLConnection.disconnect();
                Thread.sleep(5000);
                return stringBuilder.toString().trim();
            }catch (MalformedURLException e){
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


        return null;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String json) {
        try {
            progressDialog.dismiss();
            JSONObject jsonObject = new JSONObject(json.substring(json.indexOf("{"), json.lastIndexOf("}") + 1));
            JSONArray jsonArray = jsonObject.getJSONArray("server_response");
            JSONObject JO = jsonArray.getJSONObject(0);
            String code = JO.getString("code");
            String message = JO.getString("message");
            if (code.equals("add_true"))
            {
                showDialog("Event Sucess",message,code);
            }
            else if (code.equals("reg_false"))
            {
                showDialog("Event Failed",message,code);
            }
        }       catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void showDialog(String title, String message, String code){
        builder.setTitle(title);
        if (code.equals("add_true")|| code.equals("add_false") ){
            builder.setMessage(message);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    activity.finish();
                }
            });




        }
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
