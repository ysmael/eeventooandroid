package com.eeventoo.app.backgroundTask;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.eeventoo.app.BuildConfig;
import com.eeventoo.app.class_.Constants;
import com.eeventoo.app.class_.UserSessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by isma on 06/09/2016.
 */

public class BackgroundTaskAdd extends AsyncTask<String,Void,String> {

    //Url addfavoris
    String addfavoris_url =  Constants.IP_URL+"webapp/addFavoris.php";
    String addAvis_url=  Constants.IP_URL+"webapp/addAvis.php";
    Activity activity;
    Context ctx;
    AlertDialog.Builder builder;
    UserSessionManager session;

    public BackgroundTaskAdd(Context ctx) {
        this.ctx = ctx;
        activity=(Activity) ctx;
    }
    public BackgroundTaskAdd(Context ctx, UserSessionManager session) {
        this.ctx = ctx;
        activity=(Activity) ctx;
        this.session=session;
    }

    public BackgroundTaskAdd() {

    }

    @Override
    protected String doInBackground(String... params) {
        String method= params[0];
        if(method.equals("favoris")){
            try {
                URL url= new URL(addfavoris_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter= new BufferedWriter(new OutputStreamWriter(outputStream));
                String id = params[1];
               String idUser_ = params[2];
                String idEvent_ = params[3];

                String data = URLEncoder.encode("id","UTF-8")+"="+URLEncoder.encode(id,"UTF-8")+"&"+
                        URLEncoder.encode("idUser","UTF-8")+"="+URLEncoder.encode(idUser_,"UTF-8")+"&"+
                        URLEncoder.encode("idEvent","UTF-8")+"="+URLEncoder.encode(idEvent_,"UTF-8");
                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();

                InputStream inputStream= httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder stringBuilder = new StringBuilder();
                String line="";
                while ((line = bufferedReader.readLine())!=null){
                    stringBuilder.append(line+"\n");
                }
                httpURLConnection.disconnect();
               // Thread.sleep(5000);
                return stringBuilder.toString().trim();
            }catch (MalformedURLException e){
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if (method.equals("avis")){
            try {
                URL url= new URL(addAvis_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter= new BufferedWriter(new OutputStreamWriter(outputStream));
                String id = params[1];
                String idUser_ = params[2];
                String idEvent = params[3];
                String avis = params[4];
                String date_avis=params[5];
                String nbetoiles=params[6];

                String data = URLEncoder.encode("id","UTF-8")+"="+URLEncoder.encode(id,"UTF-8")+"&"+
                        URLEncoder.encode("idUser","UTF-8")+"="+URLEncoder.encode(idUser_,"UTF-8")+"&"+
                        URLEncoder.encode("idEvent","UTF-8")+"="+URLEncoder.encode(idEvent,"UTF-8")+"&"+
                        URLEncoder.encode("avis","UTF-8")+"="+URLEncoder.encode(avis,"UTF-8")+"&"+
                        URLEncoder.encode("date_avis","UTF-8")+"="+URLEncoder.encode(date_avis,"UTF-8")+"&"+
                        URLEncoder.encode("nbetoiles","UTF-8")+"="+URLEncoder.encode(nbetoiles,"UTF-8");
                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();

                InputStream inputStream= httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder stringBuilder = new StringBuilder();
                String line="";
                while ((line = bufferedReader.readLine())!=null){
                    stringBuilder.append(line+"\n");
                }
                httpURLConnection.disconnect();
                // Thread.sleep(5000);
                return stringBuilder.toString().trim();
            }catch (MalformedURLException e){
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    protected void onPostExecute(String json) {
        if (json!=""){
            Log.i("jsonResponse ", "JSON =:"+json+" ");
            String jsonSub= json.substring(json.indexOf("{"), json.lastIndexOf("}") + 1);
            try {

                JSONObject jsonObject = new JSONObject(jsonSub);
                JSONArray jsonArray = jsonObject.getJSONArray("server_response");
                JSONObject JO = jsonArray.getJSONObject(0);
                String code = JO.getString("code");
                String message = JO.getString("message");
                if(BuildConfig.DEBUG) {
                    if (code.equals("fav_true")) {
                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();

                    } else if (code.equals("fav_false")) {
                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                    } else if (code.equals("addAvis_true")) {
                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                    } else if (code.equals("addAvis_false")) {
                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        else {
            Log.i("jsonResponse ", "onPostExecute:"+json+" ");
        }
    }

    public void showDialog(String title, String message, String code){
        builder.setTitle(title);
        if ( code.equals("addAvis_false") ){
            builder.setMessage(message);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    //activity.finish();
                }
            });

        }else if (code.equals("addAvis_true")){
            builder.setMessage(message);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();

                    activity.finish();

                }
            });
        } AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

}
