package com.eeventoo.app.backgroundTask;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.eeventoo.app.Adapter.AdapterFav;
import com.eeventoo.app.class_.Constants;
import com.eeventoo.app.interfaceListener.CustomItemClickListener;
import com.eeventoo.app.activity.DescriptionEvent;
import com.eeventoo.app.class_.Evenement;
import com.eeventoo.app.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by isma on 06/09/2016.
 */

public class BackgroundTaskGetFav extends AsyncTask<String,Evenement,Void> {
    Context ctx;
    Activity activity;
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<Evenement> evenements = new ArrayList<>();
    int ID_user;
    String email_user;
    String getfavoris_url = Constants.IP_URL+"webapp/getFavoris.php";


    public BackgroundTaskGetFav(Context ctx,int ID_user,String email_user){
        this.ctx =ctx;
        activity=(Activity)ctx;
        layoutManager = new LinearLayoutManager(ctx);
        this.ID_user=ID_user;
        this.email_user=email_user;
    }


    @Override
    protected void onPreExecute() {
        recyclerView = (RecyclerView) activity.findViewById(R.id.myrecycler);

    }
    @Override
    protected Void doInBackground(String... params) {
        String method= params[0];
        if(method.equals("favoris")) {
            try {
                URL url = new URL(getfavoris_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream));
                String idUser_ = params[1];

                Log.i("GETFAVVVV", "doInBackground: " + idUser_);
                String data = URLEncoder.encode("idUser", "UTF-8") + "=" + URLEncoder.encode(idUser_, "UTF-8");
                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();

                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder stringBuilder = new StringBuilder();
                String line = "";
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line + "\n");
                }
                httpURLConnection.disconnect();
                String json_string = stringBuilder.toString().trim();
                String jsonSub = json_string.substring(json_string.indexOf("{"), json_string.lastIndexOf("}") + 1);
                JSONObject jsonObject = new JSONObject(jsonSub);
                JSONArray jsonArray = jsonObject.getJSONArray("server_response");
                int count = 0;
                if (jsonArray.length() > 0) {


                    while (count < jsonArray.length()) {
                        JSONObject JO = jsonArray.getJSONObject(count);
                        count++;
                        Evenement evenement = new Evenement();
                        /*JO.getString("id"),
                                JO.getString("nom"),
                                JO.getString("nom"),
                                JO.getString("type"),
                                JO.getString("adresse"),
                                JO.getString("codepostal"),
                                JO.getString("ville"),
                                JO.getString("datedeb"),
                                JO.getString("datefin"),
                                JO.getString("telephone"),
                                JO.getString("typeTarif"),
                                JO.getString("tarifE"),
                                JO.getString("description"),
                                JO.getInt("nbLike"),
                                JO.getInt("nbFavoris"),
                                JO.getInt("nbAvis"));*/
                        publishProgress(evenement);
                    }
                } else {
                    evenements.clear();
                }


                Log.d("JSON-STRING-FAV", json_string);
                Log.i("LENGTH_LIST-FAV", "" + evenements.size());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    protected void onProgressUpdate(Evenement... values) {
        evenements.add(values[0]);
        Log.i("LENGTH_LISTE_FAV",""+ evenements.size());
//        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        //super.onPostExecute(aVoid);
        recyclerView = (RecyclerView) activity.findViewById(R.id.myrecyclerFav);
        layoutManager = new LinearLayoutManager(this.ctx);
        recyclerView.setLayoutManager(new LinearLayoutManager(ctx));
        recyclerView.setHasFixedSize(true);
        adapter = new AdapterFav(activity, evenements,ID_user,email_user, new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                Intent myIntent = new Intent(activity, DescriptionEvent.class);
                myIntent.putExtra("id", evenements.get(position).getId());
                myIntent.putExtra("nom", evenements.get(position).getNomEvenement());
                myIntent.putExtra("type", evenements.get(position).getType() + "");
                myIntent.putExtra("adresse", evenements.get(position).getAdresse() + "");
                myIntent.putExtra("codepostal", evenements.get(position).getCodePostale() + "");
                myIntent.putExtra("ville", evenements.get(position).getVille() + "");
                myIntent.putExtra("datedeb", evenements.get(position).getDatedeb() + "");
                myIntent.putExtra("datefin", evenements.get(position).getDatefin());
                myIntent.putExtra("telephone", evenements.get(position).getTelephone());
                myIntent.putExtra("description", evenements.get(position).getDescription());

                ctx.startActivity(myIntent);
            }
        });

        recyclerView.setAdapter(adapter);
    }
}
