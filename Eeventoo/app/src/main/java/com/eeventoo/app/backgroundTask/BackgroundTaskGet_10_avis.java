package com.eeventoo.app.backgroundTask;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;

import com.eeventoo.app.Adapter.Adapter_10_avis;
import com.eeventoo.app.Adapter.CommentsAdapter;
import com.eeventoo.app.class_.Avis;
import com.eeventoo.app.class_.Constants;
import com.eeventoo.app.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by isma on 11/09/2016.
 */

public class BackgroundTaskGet_10_avis extends AsyncTask<String,Avis,Void>  {
    Context ctx;
    Activity activity;
    RecyclerView recyclerView;
    Adapter_10_avis adapter;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<Avis> list_10_Avis = new ArrayList<>();
    String get_10_avis = Constants.IP_URL+"webapp/get_10_avis.php";
    String activiteAppellante;
    private CommentsAdapter commentsAdapter;
    RecyclerView rvComments;
    public BackgroundTaskGet_10_avis(Context ctx,String activiteAppellante) {
        this.ctx = ctx;
        activity=(Activity) ctx;
    this.activiteAppellante=activiteAppellante;
    }


    protected void onPreExecute() {


    }
    @Override
    protected Void doInBackground(String... params) {
        String method= params[0];
   if (method.equals("getavis")){
       try {
           URL url = new URL(get_10_avis);
           HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
           httpURLConnection.setRequestMethod("POST");
           httpURLConnection.setDoOutput(true);
           httpURLConnection.setDoInput(true);
           OutputStream outputStream = httpURLConnection.getOutputStream();
           BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream));
           String idEvent_ = params[1];

           Log.i("GETAVISS", "idEvent " + idEvent_);
           String data = URLEncoder.encode("idEvent", "UTF-8") + "=" + URLEncoder.encode(idEvent_, "UTF-8");
           bufferedWriter.write(data);
           bufferedWriter.flush();
           bufferedWriter.close();
           outputStream.close();

           InputStream inputStream = httpURLConnection.getInputStream();
           BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
           StringBuilder stringBuilder = new StringBuilder();
           String line = "";
           while ((line = bufferedReader.readLine()) != null) {
               stringBuilder.append(line + "\n");
           }
           httpURLConnection.disconnect();
           String json_string = stringBuilder.toString().trim();
           String jsonSub = json_string.substring(json_string.indexOf("{"), json_string.lastIndexOf("}") + 1);
           JSONObject jsonObject = new JSONObject(jsonSub);
           JSONArray jsonArray = jsonObject.getJSONArray("server_response");
           int count = 0;
           if (jsonArray.length() > 0) {



               while (count<jsonArray.length()){
                   JSONObject JO = jsonArray.getJSONObject(count);
                   count++;
                   Avis avis = new Avis(JO.getString("idavis"),JO.getString("displayName"),JO.getString("avis"),JO.getString("date_avis"),JO.getString("nbetoiles"),JO.getString("photoprofil"));
                   publishProgress(avis);
               }
           } else {
               list_10_Avis.clear();
           }

            Log.d("GetAVIS",json_string);
            Log.i("LENGTH_LIST_AVIS",""+ list_10_Avis.size());



        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
        return null;
    }


    protected void onProgressUpdate(Avis... values) {
        list_10_Avis.add(values[0]);
        Log.i("LENGTH_LISTE_Avis",""+ list_10_Avis.size());
//        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        //super.onPostExecute(aVoid);
        layoutManager = new LinearLayoutManager(this.ctx);
        adapter = new Adapter_10_avis(activity, list_10_Avis);

        if (activiteAppellante.equals("FragmentAvis")) {
            recyclerView = (RecyclerView) activity.findViewById(R.id.myrecycler_frag_avis);
            recyclerView.setLayoutManager(new LinearLayoutManager(ctx));
            recyclerView.setHasFixedSize(true);

            recyclerView.setAdapter(adapter);
        }

      /*  if (activiteAppellante.equals("CommentsActivity")){
            rvComments= (RecyclerView) activity.findViewById(R.id.rvComments);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.ctx);
            rvComments.setLayoutManager(linearLayoutManager);
            rvComments.setHasFixedSize(true);

            commentsAdapter = new CommentsAdapter(activity);
            rvComments.setAdapter(commentsAdapter);
            rvComments.setOverScrollMode(View.OVER_SCROLL_NEVER);
            rvComments.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                        commentsAdapter.setAnimationsLocked(true);
                    }
                }
            });}*/
    }
}
