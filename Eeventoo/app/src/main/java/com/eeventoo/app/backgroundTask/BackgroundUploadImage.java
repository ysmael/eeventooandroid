package com.eeventoo.app.backgroundTask;

import android.os.AsyncTask;
import android.util.Log;

import com.eeventoo.app.class_.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by isma on 12/09/2016.
 */

public class BackgroundUploadImage extends AsyncTask<String,Void,Void> {

    private String upload_url= Constants.IP_URL+"AndroidUploadImage/getImages.php";
    @Override
    protected Void doInBackground(String... params) {
        String method= params[0];
        if(method.equals("addimage")) {
        try {
            URL url = new URL(upload_url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            OutputStream outputStream = httpURLConnection.getOutputStream();
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream));
           String image=params[1];
            String name=params[2];
            Log.i("IMAGE", "doInBackground: " + image);
            String data = URLEncoder.encode("image", "UTF-8") + "=" + URLEncoder.encode(image, "UTF-8")+"&"+
                             URLEncoder.encode("name","UTF-8")+"="+URLEncoder.encode(name,"UTF-8");
            bufferedWriter.write(data);
            bufferedWriter.flush();
            bufferedWriter.close();
            outputStream.close();

            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder stringBuilder = new StringBuilder();
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            httpURLConnection.disconnect();
            String json_string = stringBuilder.toString().trim();
            String jsonSub = json_string.substring(json_string.indexOf("{"), json_string.lastIndexOf("}") + 1);
            JSONObject jsonObject = new JSONObject(jsonSub);
            JSONArray jsonArray = jsonObject.getJSONArray("server_response");
            int count = 0;
            if (jsonArray.length() > 0) {


                while (count < jsonArray.length()) {
                    JSONObject JO = jsonArray.getJSONObject(count);
                    count++;
                    Log.i("ARRAY", "ARRAY: "+ JO.getString("image"));
                }
            } else {
                //evenements.clear();
            }


            Log.d("JSON-STRING-IMAGE", json_string);
            //Log.i("LENGTH_LIST-FAV", "" + evenements.size());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
        return null;
}
}
