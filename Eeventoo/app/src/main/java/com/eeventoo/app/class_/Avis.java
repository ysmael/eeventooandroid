package com.eeventoo.app.class_;

/**
 * Created by isma on 11/09/2016.
 */

public class Avis {
    private String id;
    private String displayNameUserAvis;

    private String dateAvis;
    private String textAvis;
    private String nbStarAvis;
    private String userPP;

    public Avis(String id,String displayNameUserAvis,  String textAvis, String dateAvis, String nbStarAvis,String userPP) {
        this.id=id;
        this.displayNameUserAvis = displayNameUserAvis;
        this.dateAvis = dateAvis;
        this.textAvis = textAvis;
        this.nbStarAvis = nbStarAvis;
        this.userPP=userPP;
    }

    public Avis(){
        this.id="";
        this.displayNameUserAvis="";
        this.dateAvis="";
        this.textAvis="";
        this.nbStarAvis="";

    }

    public String getDisplayNameUserAvis() {
        return displayNameUserAvis;
    }

    public void setDisplayNameUserAvis(String premonUserAvis) {
        this.displayNameUserAvis = premonUserAvis;
    }



    public String getDateAvis() {
        return dateAvis;
    }

    public void setDateAvis(String dateAvis) {
        this.dateAvis = dateAvis;
    }

    public String getTextAvis() {
        return textAvis;
    }

    public void setTextAvis(String textAvis) {
        this.textAvis = textAvis;
    }

    public String getNbStarAvis() {
        return nbStarAvis;
    }

    public void setNbStarAvis(String nbStarAvis) {
        this.nbStarAvis = nbStarAvis;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserPP() {
        return userPP;
    }

    public void setUserPP(String userPP) {
        this.userPP = userPP;
    }
}
