package com.eeventoo.app.class_;

/**
 * Created by isma on 22/06/2016.
 */

public class Constants {

    //Colonne
   public static final String ROW_ID="id";
    public static final String NOM_EVENEMENT="nom";
    public static final String TYPE="type";
    public static final String ADRESSE ="adresse";
    public static final String TELEPHONE="telephone";
    public static final String DATE_DEB="datedeb";
    public static final String DATE_FIN ="datefin";
    public static final String VILLE ="ville";
    public static final String DEPARTEMENT="departement";
    public static final String TARIF="tarifE";
    public static final String CODE_POSTAL="codepostal";
    public static final String DESCRIPTION ="description";
    public static final String PRENOM_USER="prenomUser";
    public static final String NOM_USER="nomUser";
    public static final String REGION="region";

    public static final String IP_URL= "https://eeventoo.com/";
    public static final String ENDPOINT = IP_URL+"webapp/";
    public static final String UPLOAD_URL = IP_URL+"AndroidImageUpload/upload.php";
    public static final String IMAGES_URL = IP_URL+"AndroidImageUpload/getImages.php";
    public static final String TAG_IMAGE_URL="image";
    public static final String DATA_URL=ENDPOINT+"getAllEvent.php?page=";
    public static final String EVENT_URL=ENDPOINT+"getAllEvent.php";
 public static  final String GET_AVIS_URL=IP_URL+"webapp/getAvis.php";

    public static final String  SEARCH_EVENT_URL = IP_URL+"webapp/searchEvent.php?search=";
    public static final String  GET_DEPARTEMENT_URL = IP_URL+"webapp/getDepartement.php?region=";
    public static final String  GET_FAVORIS_URL = IP_URL+"webapp/getFavoris.php?page=";
    public static final String  GET_REGION_URL = IP_URL+"webapp/regionList.php";
    //PROPRIETES DB
    public static final String DB_NAME="db";
    public static final String TB_NAME="evenement";
    public static final int DB_VERSION='1';
    public static final String CREATE_TB="CREATE TABLE " +TB_NAME+" (id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "nom TEXT NOT NULL,"+
            "jour INTEGER NOT NULL ," +
            "mois INTEGER NOT NULL ," +
            "annee INTEGER NOT NULL ," +
            "heureDeb INTEGER NOT NULL ," +
            "heureFin INTEGER NOT NULL ," +
            "ville TEXT NOT NULL," +
            "departement TEXT NOT NULL," +
            "region TEXT NOT NULL," +
            "codePostale INTEGER NOT NULL," +
            "description TEXT NOT NULL);";
    public static final String GET_VILLE_URL =IP_URL+"webapp/getVille.php";
    public static final String GET_ALL_VILLE_URL =IP_URL+"webapp/getAllVille.php" ;
    public static final String SUP_FAV_URL =IP_URL+"webapp/SupFav.php" ;
    public static final String EVENTOFUSER = ENDPOINT+"getEventOfUser.php/" ;
    public static final String TYPETARIF ="typeTarif" ;
    public static final String DELETE_EVENTOFUSER =ENDPOINT+"deleteEventOfUser.php/" ;
    public static final String INTEGER_FAVORIS_OF_USER =ENDPOINT+ "getIntegerFavorisUser.php/";
    public static final String ADDLIKE =ENDPOINT+"addLike.php/" ;
    public static final String SUPLIKE = ENDPOINT+"supLike.php/" ;
    public static final String INTEGER_LIKES_OF_USER =ENDPOINT+ "getIntegerLikesUser.php/" ;
    public static final String GET_ID_LIST_EVENT =ENDPOINT+ "getIdListEvent.php/" ;
    public static final String UPDATE_PP =ENDPOINT+"updatePP.php/" ;
    public static final String UPDATE_EMAIL =ENDPOINT+"updateEmail.php/" ;
    public static final String UPDATE_TEL =ENDPOINT+"updateTel.php/" ;
    public static final String UPDATE_MODIF = ENDPOINT+"updateModif.php/";
    public static final String INFOS_USER = ENDPOINT+"getInfosUser.php/";
    public static final String  LOGIN_URL= ENDPOINT+"login.php";
    public static final String  REGISTER_URL= ENDPOINT+"register.php";
    public static final String GET_ALL_DEP_URL = ENDPOINT+"getAllDepartement.php/";
    public static final String ADDAVIS =ENDPOINT+"addAvis.php";
    public static final String GET_INDICATIF = ENDPOINT+"getIndicatif.php";
    public static final String GET_PAYS_MONNAIE = ENDPOINT+"getPaysMonnaie.php";
    public static final String GET_TYPE_EVENT= ENDPOINT+"getTypeEvent.php";
    public static final String ADRESSE_COMPLET ="adresseComplet" ;
    public static final String PAYS_NOM = "paysNom";
    public static final String PAYS_CODE = "paysCode";
    public static final String LATITUDE = "latitude";
    public static final String LONTITUDE = "lontitude";
    public static final String REGISTER_USER_GOOGLE_IN_MYSQL_URL = ENDPOINT+"registrationUserGoogleInMysql.php" ;
    public static final int NO_USER = 0 ;
    public   static final int USER_GOOGLE = 1;
    public static final String ADD_FORMULE_EVENT = ENDPOINT+"addFormulesEvent.php";
    public static final String GET_FORMULES_EVENT =ENDPOINT+"getFormulesEvent.php" ;
    public static final String INIT_USER_FIREBASE_URL = ENDPOINT+ "initUserFirebase.php" ;
    public static final String INIT_USER_FIREBASE_GOOGLE_FACEBOOK_URL = ENDPOINT+ "initUserFirebaseGoogleFacebook.php" ;
    public static final String UPDATE_DISPLAY_NAME =ENDPOINT+"updateDisplayName.php";
    public static final int ID_REQUEST_LOG_OUT = -1;
    public static int USER_EEVENTOO = 2;
    public static int USER_FACEBOOK =3;
}
 /*


 SET @main_query1="select e.id, e.image,e.nom,e.type,e.adresse,e.codepostal,e.ville, e.datedebut,e.datefin,e.telephone,e.typeTarif,e.tarifentree,e.description,d.nom AS departement,r.nom AS region from evenement_tb e, villes v , regions r , departements d where e.codepostal=v.codepostal and e.ville= v.nom and v.departement_id=d.id and d.region_id=r.id ";

IF (p_dateDebut<>'')THEN
SET @main_query2= concat(@main_query1,@dateDebut);

ELSE
SET @main_query2= @main_query1;

END IF;

IF (p_dateFin<>'')THEN
SET @main_query= concat(@main_query2,@dateFin);

ELSE
SET @main_query = @main_query2;

END IF;
  */