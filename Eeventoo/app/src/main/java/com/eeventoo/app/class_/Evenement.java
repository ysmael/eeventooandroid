package com.eeventoo.app.class_;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by isma on 22/06/2016.
 */

public class Evenement /*implements Parcelable */{
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("image")
    @Expose
    private String imageUrl;

    @SerializedName("nom")
    @Expose
    private String nomEvenement;
    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("adresse")
    @Expose
    private String adresse;
    @SerializedName("codepostale")
    @Expose
    private String codePostale;

    @SerializedName("ville")
    @Expose
    private String ville;

    @SerializedName("datedeb")
    @Expose
    private String datedeb;

    @SerializedName("datefin")
    @Expose
    private String datefin;
    @SerializedName("telephone")
    @Expose
    private String telephone;


    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("prenomUser")
    @Expose
    private String prenomUser;


    @SerializedName("adresseComplet")
    @Expose
    private String adresseComplet;

    @SerializedName("paysNom")
    @Expose
    private String paysNom;

            @SerializedName("paysCode")
    @Expose
    private String paysCode;

    @SerializedName("latitude")
    @Expose
    private double latitude;

    @SerializedName("lontitude")
    @Expose
    private double lontitude;

    @SerializedName("nomUser")
    @Expose
    private  String nomUser;

    @SerializedName("departement")
    @Expose
    private String departement;

    @SerializedName("region")
    @Expose
    private  String region;

    @SerializedName("typeTarif")
    @Expose
    private String typeTarif;

    @SerializedName("nbLike")
    @Expose
    private int nbLike;
    @SerializedName("nbFavoris")
    @Expose
    private int nbFavoris;
    @SerializedName("nbAvis")
    @Expose
    private  int nbAvis;

    public Evenement(String id,String imageUrl,
                     String nomEvenement,
                     String type,
                     String adresse,
                     String adresseComplet,
                     String codePostale,
                     String ville,
                     String departement,
                     String region,
                     String paysNom,
                     String paysCode,
                     double latitude,
                     double lontitude,
                     String datedeb,
                     String datefin,
                     String telephone,
                     String typeTarif,
                     String description,
                     int nbLikes,
                     int nbFavoris,
                     int nbAvis) {
        this.id = id;
        this.imageUrl = imageUrl;
        this.nomEvenement = nomEvenement;
        this.type = type;
        this.adresse = adresse;
        this.codePostale = codePostale;
        this.ville = ville;
        this.datedeb = datedeb;
        this.datefin = datefin;
        this.telephone = telephone;
        this.description = description;
        this.typeTarif = typeTarif;
        this.nbLike = nbLikes;
        this.nbFavoris = nbFavoris;
        this.nbAvis = nbAvis;
        this.adresseComplet= adresseComplet;
        this.departement=departement;
        this.region=region;
        this.paysNom=paysNom;
        this.paysCode=paysCode;
        this.latitude=latitude;
        this.lontitude=lontitude;

    }
public Evenement(){
    this.id = "";
    this.imageUrl="";
    this.nomEvenement = "";
    this.type = "";
    this.adresse = "";
    this.codePostale = "";
    this.ville = "";
    this.datedeb = "";
    this.datefin = "";
    this.telephone = "";
    this.description = "";
   // this.prenomUser="";
    //this.nomUser="";
    this.departement="";
    this.region="";
    this.typeTarif="";

    this.nbLike = 0;
    this.nbFavoris = 0;
    this.nbAvis = 0;
    this.adresseComplet= "";
    this.departement="";
    this.region="";
    this.paysNom="";
    this.paysCode="";
    this.latitude=0;
    this.lontitude=0;
}

    public String getPrenomUser() {
        return prenomUser;
    }

    public void setPrenomUser(String prenomUser) {
        this.prenomUser = prenomUser;
    }

    public String getNomUser() {
        return nomUser;
    }

    public void setNomUser(String nomUser) {
        this.nomUser = nomUser;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNomEvenement() {
        return nomEvenement;
    }

    public void setNomEvenement(String nomEvenement) {
        this.nomEvenement = nomEvenement;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getCodePostale() {
        return codePostale;
    }

    public void setCodePostale(String codePostale) {
        this.codePostale = codePostale;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getDatedeb() {
        return datedeb;
    }

    public void setDatedeb(String datedeb) {
        this.datedeb = datedeb;
    }

    public String getDatefin() {
        return datefin;
    }

    public void setDatefin(String datefin) {
        this.datefin = datefin;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDepartement() {
        return departement;
    }

    public void setDepartement(String departement) {
        this.departement = departement;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getTypeTarif() {
        return typeTarif;
    }

    public void setTypeTarif(String typeTarif) {
        this.typeTarif = typeTarif;
    }

    public int getNbLike() {
        return nbLike;
    }

    public void setNbLike(int nbLike) {
        this.nbLike = nbLike;
    }

    public int getNbFavoris() {
        return nbFavoris;
    }

    public void setNbFavoris(int nbFavoris) {
        this.nbFavoris = nbFavoris;
    }

    public int getNbAvis() {
        return nbAvis;
    }

    public void setNbAvis(int nbAvis) {
        this.nbAvis = nbAvis;
    }

    public String getAdresseComplet() {
        return adresseComplet;
    }

    public void setAdresseComplet(String adresseComplet) {
        this.adresseComplet = adresseComplet;
    }

    public String getPaysNom() {
        return paysNom;
    }

    public void setPaysNom(String paysNom) {
        this.paysNom = paysNom;
    }

    public String getPaysCode() {
        return paysCode;
    }

    public void setPaysCode(String paysCode) {
        this.paysCode = paysCode;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLontitude() {
        return lontitude;
    }

    public void setLontitude(double lontitude) {
        this.lontitude = lontitude;
    }
}
