package com.eeventoo.app.class_;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by isma on 04/02/2018.
 */

public class Formule {

    @SerializedName("prix")
    @Expose
    private int prix;

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("idEvent")
    @Expose
    private String idEvent;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("devise")
    @Expose
    private String devise;

    public Formule(){
        this.prix = 0;
        this.description = "";
    }
    public Formule(int prix, String description) {
        this.prix = prix;
        this.description = description;
    }


    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdEvent() {
        return idEvent;
    }

    public void setIdEvent(String idEvent) {
        this.idEvent = idEvent;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    @Override
    public String toString() {
        return "Formule{" +
                "prix=" + prix +
                ", description='" + description + '\'' +
                ", devise=" + devise +
                '}';
    }
}
