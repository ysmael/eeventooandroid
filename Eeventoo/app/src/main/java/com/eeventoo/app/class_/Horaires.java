package com.eeventoo.app.class_;

/**
 * Created by isma on 22/06/2016.
 */

public class Horaires {
    int heuredeb;
    int Heurefin;

    public Horaires(int heuredeb, int heurefin) {
        this.heuredeb = heuredeb;
        Heurefin = heurefin;
    }

    public int getHeurefin() {
        return Heurefin;
    }

    public void setHeurefin(int heurefin) {
        Heurefin = heurefin;
    }

    public int getHeuredeb() {
        return heuredeb;
    }

    public void setHeuredeb(int heuredeb) {
        this.heuredeb = heuredeb;
    }
}
