package com.eeventoo.app.class_;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by isma on 13/01/2017.
 */

public class MyRequestQueue {
    private static MyRequestQueue mInstance;
    private RequestQueue requestQueue;
    private static Context mCtx;
//consturcteur
    private MyRequestQueue(Context context){
        mCtx=context;
        requestQueue = getRequestQueue();
    }

    public  static synchronized MyRequestQueue getInstance(Context context){
    if (mInstance==null){
        mInstance=new MyRequestQueue(context);
    }
        return mInstance;
    }


    public RequestQueue getRequestQueue(){
        if (requestQueue ==null){
            requestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return requestQueue;
    }

    public <T> void addTorequestqueue(Request<T> request){
        requestQueue.add(request);

    }

}
