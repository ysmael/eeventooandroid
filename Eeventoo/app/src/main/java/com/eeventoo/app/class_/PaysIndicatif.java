package com.eeventoo.app.class_;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by isma on 26/02/2018.
 */

public class PaysIndicatif {
    @SerializedName("nom")
    @Expose
    private String nom;

    @SerializedName("indicatif")
    @Expose
    private String indicatif;

    @Override
    public String toString() {
        return
                 nom + " (" + indicatif + ")";
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getIndicatif() {
        return indicatif;
    }

    public void setIndicatif(String indicatif) {
        this.indicatif = indicatif;
    }
}
