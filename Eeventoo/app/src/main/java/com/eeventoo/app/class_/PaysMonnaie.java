package com.eeventoo.app.class_;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by isma on 28/02/2018.
 */

public class PaysMonnaie {
    @SerializedName("monnaie")
    @Expose
    private String monnaie;

    @SerializedName("symboleMonnaie")
    @Expose
    private String symboleMonnaie;


    @SerializedName("codeMonnaie")
    @Expose
    private String codeMonnaie;


    public String getMonnaie() {
        return monnaie;
    }

    public void setMonnaie(String monnaie) {
        this.monnaie = monnaie;
    }

    public String getSymboleMonnaie() {
        return symboleMonnaie;
    }

    public void setSymboleMonnaie(String symboleMonnaie) {
        this.symboleMonnaie = symboleMonnaie;
    }

    public String getCodeMonnaie() {
        return codeMonnaie;
    }

    public void setCodeMonnaie(String codeMonnaie) {
        this.codeMonnaie = codeMonnaie;
    }

    @Override
    public String toString() {
        return  monnaie +" ("+ symboleMonnaie +") "+ codeMonnaie ;
    }
}
