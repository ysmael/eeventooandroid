package com.eeventoo.app.class_;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by isma on 14/06/2017.
 */

public class ReponseAllDepartement {
    @SerializedName("data")
    @Expose
    private ArrayList<String> listDepartement;

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("message")
    @Expose
    private String message;


    public ArrayList<String> getListDepartement() {
        return listDepartement;
    }

    public void setListDepartement(ArrayList<String> listDepartement) {
        this.listDepartement = listDepartement;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
