package com.eeventoo.app.class_;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by isma on 12/06/2017.
 */

public class ReponseLogin {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("id")
    @Expose
    private int id;


    @SerializedName ("prenom")
    @Expose
    private String prenomU;

    @SerializedName ("nom")
    @Expose
    private  String nomU;

    @SerializedName ("photoprofil")
    @Expose
    private String ppU ;

    @SerializedName ("email")
    @Expose
    private  String emailOfUser;

    @SerializedName ("adresse")
    @Expose
    private String adresseU ;

    @SerializedName ("codepostal")
    @Expose
    private String cpU;

    @SerializedName ("ville")
    @Expose
    private String villeU;

    @SerializedName ("telephone")
    @Expose
    private String telU;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPrenomU() {
        return prenomU;
    }

    public void setPrenomU(String prenomU) {
        this.prenomU = prenomU;
    }

    public String getNomU() {
        return nomU;
    }

    public void setNomU(String nomU) {
        this.nomU = nomU;
    }

    public String getPpU() {
        return ppU;
    }

    public void setPpU(String ppU) {
        this.ppU = ppU;
    }

    public String getEmailOfUser() {
        return emailOfUser;
    }

    public void setEmailOfUser(String emailOfUser) {
        this.emailOfUser = emailOfUser;
    }

    public String getAdresseU() {
        return adresseU;
    }

    public void setAdresseU(String adresseU) {
        this.adresseU = adresseU;
    }

    public String getCpU() {
        return cpU;
    }

    public void setCpU(String cpU) {
        this.cpU = cpU;
    }

    public String getVilleU() {
        return villeU;
    }

    public void setVilleU(String villeU) {
        this.villeU = villeU;
    }

    public String getTelU() {
        return telU;
    }

    public void setTelU(String telU) {
        this.telU = telU;
    }
}
