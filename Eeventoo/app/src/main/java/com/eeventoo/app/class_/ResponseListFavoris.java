package com.eeventoo.app.class_;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by isma on 31/05/2017.
 */

public class ResponseListFavoris {
    @SerializedName("data")
    @Expose
    private ArrayList<Integer> tmpList=null;

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("message")
    @Expose
    private String message;


    public ArrayList<Integer> getData() {
        return tmpList;
    }

    public void setData(ArrayList<Integer> data) {
        this.tmpList = data;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
