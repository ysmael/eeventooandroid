package com.eeventoo.app.class_;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by isma on 17/10/2018.
 */

public class ResponseListIdEvent {


    @SerializedName("data")
    @Expose
    private ArrayList<Integer> tmpList=null;

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;


    public ArrayList<Integer> getData() {
        return tmpList;
    }

    public void setData(ArrayList<Integer> data) {
        this.tmpList = data;
    }


    public Boolean isSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
