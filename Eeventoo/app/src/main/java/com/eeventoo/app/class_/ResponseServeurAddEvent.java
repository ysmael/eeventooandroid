package com.eeventoo.app.class_;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by isma on 16/10/2018.
 */

public class ResponseServeurAddEvent {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("idGroupEvent")
    @Expose
    private String idGroupEvent;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getIdGroupEvent() {
        return idGroupEvent;
    }

    public void setIdGroupEvent(String idGroupEvent) {
        this.idGroupEvent = idGroupEvent;
    }
}
