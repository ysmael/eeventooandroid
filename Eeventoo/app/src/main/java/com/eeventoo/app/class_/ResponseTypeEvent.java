package com.eeventoo.app.class_;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by isma on 03/03/2018.
 */

public class ResponseTypeEvent {

    @SerializedName("status")
    @Expose
    private Integer status;

    @SerializedName("success")
    @Expose
    private boolean success;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private ArrayList<TypeEvent> data=null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<TypeEvent> getData() {
        return data;
    }

    public void setData(ArrayList<TypeEvent> data) {
        this.data = data;
    }
}
