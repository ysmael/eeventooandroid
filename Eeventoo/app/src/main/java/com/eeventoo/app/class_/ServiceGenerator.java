package com.eeventoo.app.class_;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by isma on 24/05/2017.
 */

public class ServiceGenerator {

    private static final String BASE_URL = Constants.ENDPOINT;
    private  static  Gson gson = new GsonBuilder()
            .setLenient()
            .create();

    private  static  final OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .readTimeout(4000, TimeUnit.SECONDS)
            .connectTimeout(4000, TimeUnit.SECONDS)
            .build();

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(BASE_URL).client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create(gson));

    private static Retrofit retrofit = builder.build();

    private static OkHttpClient.Builder httpClient =
            new OkHttpClient.Builder();

    public static <S> S createService(
            Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }
}