package com.eeventoo.app.class_;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by isma on 03/03/2018.
 */

public class TypeEvent {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("nomType")
    @Expose
    private String nomType;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNomType() {
        return nomType;
    }

    public void setNomType(String nomType) {
        this.nomType = nomType;
    }

    @Override
    public String toString() {
        return  nomType ;
    }
}
