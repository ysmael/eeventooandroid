package com.eeventoo.app.class_;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.util.Log;
import android.widget.Toast;

import com.eeventoo.app.BuildConfig;
import com.eeventoo.app.InterfaceRetrofit.IFavorisIntegerUser;
import com.eeventoo.app.InterfaceRetrofit.I_Like;
import com.eeventoo.app.InterfaceRetrofit.I_User;
import com.eeventoo.app.R;
import com.eeventoo.app.activity.CompteActivity;
import com.eeventoo.app.activity.MainActivity;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by isma on 05/09/2016.
 */

public class UserSessionManager {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;


    //sharedPreferences fin name
    private static final String PREFER_NAME = "AndroidExamplePref";

    //All Shared Preferences Keys
    private static final String IS_USER_LOGIN = "IsUserLoggedIn";

    //id de l'utilisateur , mettre en public pour pouvoir y acceder de l'exterieur
    public static final String KEY_ID = "id";
    //email de l'utilisateur , mettre en public pour pouvoir y acceder de l'exterieur
    public static final String KEY_EMAIL = "email";
    public static final String KEY_TYPE_USER = "typeUser";

    //mp de l'utilisateur , mettre en public pour pouvoir y acceder de l'exterieur
    public static final String KEY_NOM = "nom";
    public static final String KEY_PRENOM = "prenom";
    public static final String KEY_URL_PP="photoprofil";
    public static final String KEY_ADRESSE = "adresse";
    public static final String KEY_CP = "codePostal";
    public static final String KEY_VILLE="ville";
    public static final String KEY_TELEPHONE="telephone";

    private  ArrayList<Integer> likedPositions = new ArrayList<>();

    private ArrayList<Integer> interstPositions = new ArrayList<>();


    //constructeur
    public UserSessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    //creation session de loggin
    public void createUserLoginSession(int id,String email, String nom,String prenom,String photoprofil,String adresse, String cp, String ville, String tel, int typeUser) {
        //Stocker la valerur de login a TRUE
        editor.putBoolean(IS_USER_LOGIN, true);
        //stocker l'id de l'utilisateur
        editor.putInt(KEY_ID, id);
        //stocker l'email de l'utilisateur
        editor.putString(KEY_EMAIL, email);
        //stocker nom  de l'utilisateur
        editor.putString(KEY_NOM, nom);
        editor.putInt(KEY_TYPE_USER, typeUser);

        //stocker le prenom de l'utilisateur

        editor.putString(KEY_PRENOM,prenom);

        //stocker l'url du profil de l'utilisateur
        editor.putString(KEY_URL_PP,photoprofil);
        editor.putString(KEY_ADRESSE,adresse);
        editor.putString(KEY_CP,cp);
        editor.putString(KEY_VILLE,ville);
        editor.putString(KEY_TELEPHONE,tel);



        //soumettre les changements
        editor.commit();
    }

    public void createUserLoginSession(int id, String email){
        if(BuildConfig.DEBUG) {
            Log.i("CREATESESSION", "session cree" + id + email);
        }
        //Stocker la valerur de login a TRUE
        editor.putBoolean(IS_USER_LOGIN, true);
        //stocker l'id de l'utilisateur
        editor.putInt(KEY_ID, id);

        editor.putString(KEY_EMAIL, email);
        //soumettre les changements
        editor.commit();
    }

    public void createUserLoginSession(User u){
        //Stocker la valerur de login a TRUE
        editor.putBoolean(IS_USER_LOGIN, true);
        //stocker l'id de l'utilisateur
        editor.putInt(KEY_ID, u.getId());
        //stocker l'email de l'utilisateur
        editor.putString(KEY_EMAIL, u.getEmail());
        //stocker nom  de l'utilisateur
        editor.putString(KEY_NOM, u.getNom());

        //stocker le prenom de l'utilisateur

        editor.putString(KEY_PRENOM,u.getPrenom());

        //stocker l'url du profil de l'utilisateur
        editor.putString(KEY_URL_PP,u.getPhotoProfil());
        editor.putString(KEY_ADRESSE,u.getAdresse());
        editor.putString(KEY_CP,u.getCodepostal());
        editor.putString(KEY_VILLE,u.getVille());
        editor.putString(KEY_TELEPHONE,u.getTelephone());



        //soumettre les changements
        editor.commit();
    }


    public void setId(int id){
        editor.putInt(KEY_ID, id);
        //soumettre les changements
        editor.commit();
    }

    public void setKeyTypeUser(int keyTypeUser){
        editor.putInt(KEY_TYPE_USER, keyTypeUser);
        //soumettre les changements
        editor.commit();
    }

public void setNom(String nom){
    editor.putString(KEY_NOM, nom);
    //soumettre les changements
    editor.commit();
}

    public void setPrenom(String prenom){
        editor.putString(KEY_PRENOM,prenom);
        //soumettre les changements
        editor.commit();
    }
    public void setEmail(String email){
        editor.putString(KEY_EMAIL, email);
        //soumettre les changements
        editor.commit();
    }
    public void setPhotoProfil(String photoprofil){
        editor.putString(KEY_URL_PP,photoprofil);
        //soumettre les changements
        editor.commit();
    }

    public void setAdresse(String adresse){
        editor.putString(KEY_ADRESSE,adresse);
        //soumettre les changements
        editor.commit();
    }
    public void setCp(String cp){
        editor.putString(KEY_CP,cp);
        //soumettre les changements
        editor.commit();
    }

    public void setVille(String ville){
        editor.putString(KEY_VILLE,ville);
        //soumettre les changements
        editor.commit();
    }

    public void setTelephone(String tel){
        editor.putString(KEY_TELEPHONE,tel);
        //soumettre les changements
        editor.commit();
    }

    public boolean isUserLoggedIn() {
        return pref.getBoolean(IS_USER_LOGIN, false);
    }
    /*
    * verifier le statut de la session de l'utilisateur
    * si c'est faux , rediriger la page vers le login activity
    * sinon rien faire
    * */


    public boolean checkLogin() {
        //verifier le statut de la session de l'utilisateur
        if (!this.isUserLoggedIn()) {
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, MainActivity.class);

            // Closing all the Activities from stack
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);

            return true;
        }
        return false;
    }

    /**
     * Stocker les infos de la session
     * */
    public HashMap<String, String> getUserDetails(){

        //Use hashmap to store user credentials
        HashMap<String, String> user = new HashMap<String, String>();

        // user prenom
        user.put(KEY_PRENOM, pref.getString(KEY_PRENOM, null));
// user prenom
        user.put(KEY_NOM, pref.getString(KEY_NOM, null));
        // user email id
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));
        user.put(KEY_URL_PP,pref.getString(KEY_URL_PP,null));

        // return user
        return user;
    }
    /**
     * Clear session details
     * */
    public void logoutUser(){

        // Clearing all user data from Shared Preferences
        editor.clear();
        editor.commit();
       retrieveIntListFav();
        retrieveIntListLike();


    }
    public int getIdUser(){
        return pref.getInt(KEY_ID,0);
    }
    public int getKeyTypeUser(){
        return pref.getInt(KEY_TYPE_USER,0);
    }
    public String getKeyEmail(){
       return pref.getString(KEY_EMAIL, "");
    }
    public String getUserpp(){
        return pref.getString("photoprofil","");
    }
    public String getUserPrenom(){
        return pref.getString(KEY_PRENOM,"");
    }
    public String getUserNom(){
        return pref.getString(KEY_NOM,"");
    }
    public String getAdresse(){
        return pref.getString(KEY_ADRESSE, "");
    }
    public String getCp(){
        return pref.getString(KEY_CP,"");
    }
    public String getKeyVille(){
        return pref.getString(KEY_VILLE,"");
    }
    public String getKeyTelephone(){
        return pref.getString(KEY_TELEPHONE,"");
    }


    public SharedPreferences getPref(){
        return pref;
    }



    public ArrayList<Integer> getLikedPositions() {
        return likedPositions;
    }



    public ArrayList<Integer> getInterstPositions() {
        return interstPositions;
    }

    public void setInterstPositions(ArrayList<Integer> interstPositions) {
        this.interstPositions = interstPositions;
    }

    public void retrieveIntListFav(){
if(isUserLoggedIn()) {

    interstPositions= new ArrayList<>();
    IFavorisIntegerUser favorisIntegerUser = ServiceGenerator.createService(IFavorisIntegerUser.class);
    Call<ResponseListFavoris> call = favorisIntegerUser.getIntegerFavoris(getIdUser() + "");

    call.enqueue(new Callback<ResponseListFavoris>() {
        @Override
        public void onResponse(Call<ResponseListFavoris> call, Response<ResponseListFavoris> response) {
              if(response.body() != null){
                  interstPositions = response.body().getData();
                  if(BuildConfig.DEBUG) {
                      Log.e("FAVLIST", response.body().getData().toString());
                  }

              }

                else {
                    Toast.makeText(_context, _context.getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                }


        }

        @Override
        public void onFailure(Call<ResponseListFavoris> call, Throwable t) {
            if(BuildConfig.DEBUG) {
                Log.e("FAVLIST", t.getMessage());
            }
        }
    });

    }
    else {
    interstPositions= new ArrayList<>();
    }
    }


    public void retrieveIntListLike(){
        if(isUserLoggedIn()) {

            likedPositions = new ArrayList<>();
            I_Like i_like = ServiceGenerator.createService(I_Like.class);
            Call<ResponseListFavoris> call = i_like.getIntegerLikes(getIdUser() + "");

            call.enqueue(new Callback<ResponseListFavoris>() {
                @Override
                public void onResponse(Call<ResponseListFavoris> call, Response<ResponseListFavoris> response) {
                      if(response.body() != null){
                          likedPositions = response.body().getData();
                          if(BuildConfig.DEBUG) {
                              Log.e("LikeLIST", response.body().getData().toString());
                          }
                }

                else {
                    Toast.makeText(_context, _context.getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                }



                }

                @Override
                public void onFailure(Call<ResponseListFavoris> call, Throwable t) {
                    if(BuildConfig.DEBUG) {
                        Log.e("LikeLIST", t.getMessage());
                    }
                }
            });

        }else
        {
            likedPositions = new ArrayList<>();
        }
    }


    public  void refreshSession(){

            I_User iUser = ServiceGenerator.createService(I_User.class);
            Call<ReponseUser> call = iUser.getInfosUser(String.valueOf(getIdUser()));
            call.enqueue(new Callback<ReponseUser>() {
                @Override
                public void onResponse(Call<ReponseUser> call, Response<ReponseUser> response) {
                   /* if(response.isSuccessful()) {
                        Toast.makeText(_context, _context.getResources().getString(R.string.infosUserRecuperer), Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(_context,  _context.getResources().getString(R.string.infosUserError), Toast.LENGTH_SHORT).show();
                    }*/
                      if(response.body() != null){
                          setNom(response.body().getUser().getNom());
                          setPrenom(response.body().getUser().getPrenom());
                          setEmail(response.body().getUser().getEmail());
                          setAdresse(response.body().getUser().getAdresse());
                          setCp(response.body().getUser().getCodepostal());
                          setVille(response.body().getUser().getVille());
                          setPhotoProfil(response.body().getUser().getPhotoProfil());
                          setTelephone(response.body().getUser().getTelephone());

                      }

                else {
                    Toast.makeText(_context, _context.getString(R.string.quelqueChoseMalPasseRessayer), Toast.LENGTH_SHORT).show();
                }


                }

                @Override
                public void onFailure(Call<ReponseUser> call, Throwable t) {

                    if(BuildConfig.DEBUG) {
                        Log.e("Resfresh error:", t.getMessage() + " " + call.toString());
                    }
                }
            });

    }
}


