package com.eeventoo.app.fragment;


import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.eeventoo.app.Adapter.AdapterFav;
import com.eeventoo.app.BuildConfig;
import com.eeventoo.app.R;
import com.eeventoo.app.activity.DescriptionEvent;
import com.eeventoo.app.activity.MainActivity;
import com.eeventoo.app.backgroundTask.BackgroundTaskGetFav;
import com.eeventoo.app.class_.Constants;
import com.eeventoo.app.class_.Evenement;
import com.eeventoo.app.class_.UserSessionManager;
import com.eeventoo.app.interfaceListener.CustomItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;



/**
 * A simple {@link Fragment} subclass.
 */


public class FavorisFragment extends Fragment {

    int idUser;
    String emailUser;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<Evenement> evenements;
    private Cursor cursor;
    private AdapterFav adapter;
    private SwipeRefreshLayout swipeRefresh;
    BackgroundTaskGetFav backgroundTaskGetFav;
    ViewGroup viewGroup;

    //Volley Request Queue
    private RequestQueue requestQueue;
    //The request counter to send ?page=1, ?page=2  requests
    private int requestCount = 1;

    UserSessionManager session;

    TextView textViewListeVide;

    public FavorisFragment() {
        // Required empty public constructor
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        viewGroup= (ViewGroup) inflater.inflate(R.layout.fragment_fav,container,false);



        session = new UserSessionManager(getContext());



        //recuper l'id de l'utilisateur
        if (session.isUserLoggedIn()){
            if(BuildConfig.DEBUG) {
                Log.i("CREATESESSIONFAV", "session cree" + idUser + emailUser);
            }
            idUser = session.getIdUser();
            emailUser=session.getKeyEmail();

        }
        else{
            if(BuildConfig.DEBUG) {
                Log.i("CREATESESSIONFAV", "session not log" );
            }
            idUser=0;
            emailUser="";

        }

        textViewListeVide = (TextView) viewGroup.findViewById(R.id.textViewListVide);
      /*  Log.i("FRAGMENT_Fav", "ID USER: "+idUser+"email :"+emailUser);
        backgroundTaskGetFav = new BackgroundTaskGetFav(getActivity(),idUser,emailUser);
        backgroundTaskGetFav.execute("favoris",""+idUser);
        rv = (RecyclerView) viewGroup.findViewById(R.id.myrecycler);
        retrieve();
*/
        swipeRefresh = (SwipeRefreshLayout) viewGroup.findViewById(R.id.swipeRefrehFav);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {



                refresh();

            }
        });
        recyclerView = (RecyclerView) viewGroup.findViewById(R.id.myrecyclerFav);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        //Initializing our evenement list
        evenements = new ArrayList<>();
        requestQueue = Volley.newRequestQueue(getContext());

        //Calling method to get data to fetch data
        getData();
        Log.i("GET DATA", "GET DATA DE FRAGMENT FAVORIS EST APPELE");
        Log.i("GET DATA", "requestcount= "+requestCount);

        //Adding an scroll change listener to recyclerview
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (isLastItemDisplaying(recyclerView)) {
                    //Calling the method getdata again
                    getData();
                }
            }
        });

        //initializing our adapter
        adapter = new AdapterFav(getActivity(),evenements ,idUser, emailUser, new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                Intent myIntent = new Intent(getContext(), DescriptionEvent.class);
                myIntent.putExtra("id",""+ evenements.get(position).getId());
                Log.i("RECYCLERVIEW", "IDEVENT: "+ evenements.get(position).getId());
                myIntent.putExtra("nom", evenements.get(position).getNomEvenement());
                myIntent.putExtra("type", evenements.get(position).getType() + "");
                myIntent.putExtra("adresse", evenements.get(position).getAdresse() + "");
                myIntent.putExtra("codepostal", evenements.get(position).getCodePostale() + "");
                myIntent.putExtra("ville", evenements.get(position).getVille() + "");
                myIntent.putExtra("datedeb", evenements.get(position).getDatedeb() + "");
                myIntent.putExtra("datefin", evenements.get(position).getDatefin());
                myIntent.putExtra("telephone", evenements.get(position).getTelephone());
                myIntent.putExtra("typeTarif", evenements.get(position).getTypeTarif());
                myIntent.putExtra("description", evenements.get(position).getDescription());
                myIntent.putExtra("idOfUser",""+idUser);
                myIntent.putExtra("image",evenements.get(position).getImageUrl());
                startActivity(myIntent);
            }
        });
        //Adding adapter to recyclerview
        recyclerView.setAdapter(adapter);
        return viewGroup;
    }
    private JsonArrayRequest getDataFromServer(int requestCount) {
        //Initializing ProgressBar
       final ProgressBar progressBar = (ProgressBar) viewGroup.findViewById(R.id.progressBar1);

        //Displaying Progressbar
        progressBar.setVisibility(View.VISIBLE);
        //setProgressBarIndeterminateVisibility(true);

        //JsonArrayRequest of volley
       String url = Constants.GET_FAVORIS_URL + String.valueOf(requestCount)+"&idUser="+idUser;

        Log.i("URLFAV", url);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Constants.GET_FAVORIS_URL + String.valueOf(requestCount)+"&idUser="+idUser,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        //Calling method parseData to parse the json response
                        parseData(response);
                        //Hiding the progressbar
                        progressBar.setVisibility(View.GONE);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                       error.printStackTrace();
                        Log.e("FRAGMENTACCUEIL", Constants.GET_FAVORIS_URL + "1"+"&idUser="+idUser);
                        progressBar.setVisibility(View.GONE);
                        //If an error occurs that means end of the list has reached
                        Toast.makeText(getContext(), "Fin de vos évenements favoris", Toast.LENGTH_SHORT).show();
                    }
                });

        //Returning the request
        return jsonArrayRequest;
    }

    //This method will get data from the web api
    private void getData() {
        assert ((MainActivity)getActivity()) != null;
        ((MainActivity)getActivity()).showProgressLoad();
        //Adding the method to the queue by calling the method getDataFromServer
        requestQueue.add(getDataFromServer(requestCount));
        //Incrementing the request counter
        requestCount++;
    }


    //This method will parse json data
    private void parseData(JSONArray array) {
        for (int i = 0; i < array.length(); i++) {
            //Creating the superhero object
            Evenement evenement_ = new Evenement();
            JSONObject json = null;
            try {
                //Getting json
                json = array.getJSONObject(i);

                //Adding data to the superhero object
                evenement_.setImageUrl(json.getString(Constants.TAG_IMAGE_URL));
                evenement_.setId(json.getString(Constants.ROW_ID));
                evenement_.setNomEvenement(json.getString(Constants.NOM_EVENEMENT));
                evenement_.setType(json.getString(Constants.TYPE));
                evenement_.setAdresse (json.getString(Constants.ADRESSE));
                evenement_.setCodePostale (json.getString(Constants.CODE_POSTAL));
                evenement_.setVille (json.getString(Constants.VILLE));
                evenement_.setDatedeb (json.getString(Constants.DATE_DEB));
                evenement_.setDatefin (json.getString(Constants.DATE_FIN));
                evenement_.setTelephone (json.getString(Constants.TELEPHONE));
                evenement_.setTypeTarif (json.getString(Constants.TYPETARIF));
                evenement_.setDescription(json.getString(Constants.DESCRIPTION));
                evenement_.setDepartement(json.getString(Constants.DEPARTEMENT));
                evenement_.setRegion(json.getString(Constants.REGION));
                evenement_.setNbLike(json.getInt("nbLike"));
                evenement_.setNbFavoris(json.getInt("nbFavoris"));
                evenement_.setNbAvis(json.getInt("nbAvis"));
                evenement_.setAdresseComplet(json.getString(Constants.ADRESSE_COMPLET));
                evenement_.setPaysNom(json.getString(Constants.PAYS_NOM));
                evenement_.setPaysCode(json.getString(Constants.PAYS_CODE));
                evenement_.setLatitude(json.getDouble(Constants.LATITUDE));
                evenement_.setLontitude(json.getDouble(Constants.LONTITUDE));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //Adding the superhero object to the list
            evenements.add(evenement_);
            Log.i("LISTEEVENEMENT", "parseData: "+evenements.size());
        }

        if(((MainActivity)getActivity()) != null){
            ((MainActivity)getActivity()).hideProgressLoad();

        }


        //Notifying the adapter that data has been added or changed
        adapter.notifyDataSetChanged();
        checkList();

    }


    //This method would check that the recyclerview scroll has reached the bottom or not
    private boolean isLastItemDisplaying(RecyclerView recyclerView) {
        if (recyclerView.getAdapter().getItemCount() != 0) {
            int lastVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();
            if (lastVisibleItemPosition != RecyclerView.NO_POSITION && lastVisibleItemPosition == recyclerView.getAdapter().getItemCount() - 1)
                return true;
        }
        return false;
    }

    //Overriden method to detect scrolling
    public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        //Ifscrolled at last then
        if (isLastItemDisplaying(recyclerView)) {
            //Calling the method getdata again
            getData();
        }
    }
 /*   public void onResume(){
        super.onResume();
        Log.i("ON RESUME_FRAG_FAV", "ID USER: "+idUser+"email :"+emailUser);
        BackgroundTaskGetFav backgroundTaskGetFavResume = new BackgroundTaskGetFav(getActivity(),idUser,emailUser);
        backgroundTaskGetFavResume.execute("favoris",""+idUser);

    }

*/
    public void refresh(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(session.isUserLoggedIn()){
                evenements.clear();
                requestCount=1;
                getData();

                }
                else {
                    evenements.clear();
                    adapter.notifyDataSetChanged();
                    checkList();
                }
                swipeRefresh.setRefreshing(false);
            }
        },0);

    }

    public void setSession(int idUser, String emailUser){
        session.createUserLoginSession(idUser,emailUser);
        this.idUser =idUser;
        this.emailUser=emailUser;
        Log.i("FavFrag", "setSession: " + idUser +" "+ emailUser);
    }

    private void checkList(){
        if(evenements.isEmpty()){
            textViewListeVide.setVisibility(View.VISIBLE);
        }
        else {
            textViewListeVide.setVisibility(View.GONE);
        }
    }
}
