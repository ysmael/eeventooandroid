package com.eeventoo.app.fragment;


import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.eeventoo.app.backgroundTask.BackgroundTaskGet_10_avis;
import com.eeventoo.app.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentAvis extends Fragment {
    private SwipeRefreshLayout swipeRefresh;
    BackgroundTaskGet_10_avis backgroundTaskGet_10_avis;
    String idEvent;

    public FragmentAvis() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ViewGroup viewGroup= (ViewGroup)inflater.inflate(R.layout.fragment_avis, container, false);
        //recuper l'id de l'event
        Bundle args = getArguments();
        idEvent = args.getString("IdEvent");
        backgroundTaskGet_10_avis = new BackgroundTaskGet_10_avis(getActivity(),"FragmentAvis");
        backgroundTaskGet_10_avis.execute("getavis",idEvent);
        Toast.makeText(getActivity(),"evenement id :"+idEvent,Toast.LENGTH_SHORT).show();
        Log.i("FRAGMENTAVIS",  "evenement id :"+idEvent);
        return viewGroup;
    }

  public void onResume(){
        super.onResume();

        BackgroundTaskGet_10_avis backgroundTaskGet_10_avisResume = new BackgroundTaskGet_10_avis(getActivity(),"FragmentAvis");
        backgroundTaskGet_10_avisResume.execute("getavis",idEvent);

    }


    private void refresh(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //retrieve();
                BackgroundTaskGet_10_avis backgroundTaskGet_10_avis1 = new BackgroundTaskGet_10_avis(getActivity(),"FragmentAvis");
                backgroundTaskGet_10_avis1.execute("getavis");
                //adapter.notifyDataSetChanged();
                swipeRefresh.setRefreshing(false);
            }
        },0);

    }
}
