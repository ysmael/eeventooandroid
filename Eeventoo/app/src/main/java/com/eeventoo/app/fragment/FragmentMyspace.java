package com.eeventoo.app.fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.eeventoo.app.R;
import com.eeventoo.app.outils.RoundedTransformation;
import com.squareup.picasso.Picasso;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentMyspace extends Fragment {

    ViewGroup viewGroup;
    private ImageView photoProfil;
    private int photoProfilSize;

    public FragmentMyspace() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
      viewGroup= (ViewGroup) inflater.inflate(R.layout.fragment_myspace, container, false);
       photoProfil= (ImageView) viewGroup.findViewById(R.id.photoProfil);
        photoProfilSize  = getResources().getDimensionPixelSize(R.dimen.user_profile_avatar_size);
        Picasso.with(getActivity())
                .load(R.drawable.tigre)
                .centerCrop()
                .resize(photoProfilSize, photoProfilSize)
                .transform(new RoundedTransformation())
                .into(photoProfil);

        return viewGroup;
    }

}
