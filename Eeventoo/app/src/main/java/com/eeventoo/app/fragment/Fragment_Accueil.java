package com.eeventoo.app.fragment;


import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.eeventoo.app.Adapter.MyAdapter;
import com.eeventoo.app.BuildConfig;
import com.eeventoo.app.InterfaceRetrofit.I_Authentification;
import com.eeventoo.app.activity.MainActivity;
import com.eeventoo.app.class_.Constants;
import com.eeventoo.app.class_.ReponseRegister;
import com.eeventoo.app.class_.ServiceGenerator;
import com.eeventoo.app.interfaceListener.CustomItemClickListener;
import com.eeventoo.app.CustomRequest;
import com.eeventoo.app.sqLite.DbAdapter;
import com.eeventoo.app.activity.DescriptionEvent;
import com.eeventoo.app.class_.Evenement;
import com.eeventoo.app.interfaceListener.OnFeedItemClickListener;
import com.eeventoo.app.R;
import com.eeventoo.app.class_.UserSessionManager;
import com.eeventoo.app.activity.CommentsActivity;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;

import static com.android.volley.Request.*;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_Accueil extends Fragment {

private RecyclerView rv;
    private DbAdapter dbAdapter;
    private ArrayList<Evenement> evenements ;
    private Cursor cursor;
    private MyAdapter adapter;
    private SwipeRefreshLayout swipeRefresh;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    int idUser;
    String emailUser;
    ViewGroup viewGroup;
    //Volley Request Queue
    private RequestQueue requestQueue;
    //The request counter to send ?page=1, ?page=2  requests
    private int requestCount = 1;
public boolean ok =false;

    private String searchText="";
    private  String trierPar="datedebut";
    private String type ="Tout";
    private String region ="";
    private String departement ="";
    private String ville ="";
    private String datedebut ="";
    private String datefin ="";

    UserSessionManager session;
    private TextView emptyView;

    Button btnNouvelleRecherche;
    TextView textViewListeVide;
    TextView textViewPasConnexion;

    FirebaseAuth mAuth;

    public Fragment_Accueil() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        viewGroup= (ViewGroup) inflater.inflate(R.layout.content_main,container,false);


        textViewListeVide = (TextView) viewGroup.findViewById(R.id.textViewListVide);
        textViewPasConnexion =(TextView) viewGroup.findViewById(R.id.textViewPasConnexion);
        btnNouvelleRecherche = (Button) viewGroup.findViewById(R.id.btnNouvelleRecherche);

//        mAuth = FirebaseAuth.getInstance();
//
//        if(mAuth.getCurrentUser() != null){
//            initUserFirebase(mAuth.getCurrentUser().getEmail());
//        }



        session = new UserSessionManager(getActivity());
        //recuper l'id de l'utilisateur
        if (session.isUserLoggedIn()) {


            idUser = session.getIdUser();
            emailUser = session.getKeyEmail();
        }
        else{
            idUser=0;
            emailUser="";
            session.retrieveIntListFav();
            session.retrieveIntListLike();

        }





     btnNouvelleRecherche.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View view) {
             ((MainActivity)getActivity()).openDrawerSearch();
         }
     });
        swipeRefresh = (SwipeRefreshLayout) viewGroup.findViewById(R.id.swipeRefreh);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {



            refresh();

        }
    });


        recyclerView = (RecyclerView) viewGroup.findViewById(R.id.myrecycler);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        //Initializing our evenement list
        evenements = new ArrayList<>();
        requestQueue = Volley.newRequestQueue(getContext());
        if(BuildConfig.DEBUG) {
            Log.i("GET DATA", "GET DATA DE FRAGMENT ACCUEIL EST APPELE");
        }
        //Calling method to get data to fetch data
        getData(searchText,trierPar,type,region,departement,ville,datedebut,datefin);
        if(BuildConfig.DEBUG) {
            Log.i("GET DATA", "requestcount= " + requestCount + "et taille liste: " + evenements.size());
        }
        //Adding an scroll change listener to recyclerview
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (isLastItemDisplaying(recyclerView)) {
                    //Calling the method getdata again

                    getData(searchText,trierPar,type,region,departement,ville,datedebut,datefin);
                }
            }
        });

        //initializing our adapter
        adapter = new MyAdapter(getActivity(), evenements, idUser, emailUser, new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                Intent myIntent = new Intent(getContext(), DescriptionEvent.class);
                myIntent.putExtra("id", "" + evenements.get(position).getId());
                myIntent.putExtra("nom", evenements.get(position).getNomEvenement());
                myIntent.putExtra("type", evenements.get(position).getType() + "");
                myIntent.putExtra("adresse", evenements.get(position).getAdresse() + "");
                myIntent.putExtra("codepostal", evenements.get(position).getCodePostale() + "");
                myIntent.putExtra("ville", evenements.get(position).getVille() + "");
                myIntent.putExtra("datedeb", evenements.get(position).getDatedeb() + "");
                myIntent.putExtra("datefin", evenements.get(position).getDatefin());
                myIntent.putExtra("telephone", evenements.get(position).getTelephone());
                myIntent.putExtra("typeTarif", evenements.get(position).getTypeTarif());
                myIntent.putExtra("description", evenements.get(position).getDescription());
                myIntent.putExtra("idOfUser", "" + idUser);
                myIntent.putExtra("image", evenements.get(position).getImageUrl());
                myIntent.putExtra("latitude", evenements.get(position).getLatitude());
                myIntent.putExtra("lontitude", evenements.get(position).getLontitude());


                startActivity(myIntent);
            }
        }, new OnFeedItemClickListener() {
            @Override
            public void onCommentsClick(View v, int position) {
                final Intent intent = new Intent(getContext(), CommentsActivity.class);


                //Get location on screen for tapped view
                int[] startingLocation = new int[2];
                v.getLocationOnScreen(startingLocation);
                intent.putExtra(CommentsActivity.ARG_DRAWING_START_LOCATION, startingLocation[1]);
                intent.putExtra("id", "" + evenements.get(position).getId());
                startActivity(intent);
                //Disable enter transition for new Acitvity
                getActivity().overridePendingTransition(0, 0);
            }
        });
        //Adding adapter to recyclerview
        recyclerView.setAdapter(adapter);

      //afficheAucunEvent();

        return viewGroup;
    }

    private CustomRequest getDataFromServer(String searchText_, String trierPar_, String type_, String region_, String departement_, String ville_, int requestCount, String datedebut_, String datefin_) {
        //Initializing ProgressBar
        final ProgressBar progressBar = (ProgressBar) viewGroup.findViewById(R.id.progressBarAccueil);

        //Displaying Progressbar
        progressBar.setVisibility(View.VISIBLE);
        //setProgressBarIndeterminateVisibility(true);

        final HashMap<String,String> hashMapPostValue = new HashMap<>();
        hashMapPostValue.put("searchText",searchText_);
        hashMapPostValue.put("trierPar",trierPar_);
        hashMapPostValue.put("type",type_);
        hashMapPostValue.put("region",region_);
        hashMapPostValue.put("departement",departement_);
        hashMapPostValue.put("ville",ville_);
        hashMapPostValue.put("page",String.valueOf(requestCount));
        hashMapPostValue.put("datedebut",datedebut_);
        hashMapPostValue.put("datefin",datefin_);

        CustomRequest customRequest = new CustomRequest(Method.POST,Constants.EVENT_URL,hashMapPostValue, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                //Calling method parseData to parse the json response
                parseData(response);


                //Hiding the progressbar
                progressBar.setVisibility(View.GONE);
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();

                        progressBar.setVisibility(View.GONE);
                        //If an error occurs that means end of the list has reached
                        assert ((MainActivity)getActivity()) != null;
                        ((MainActivity)getActivity()).hideProgressLoad();
                        //Toast.makeText(getContext(), getString(R.string.plusEvenementdispo), Toast.LENGTH_SHORT).show();
                    }
                });

        return  customRequest;
    }

    //This method will get data from the web api
    public void getData(String searchText_,String trierPar_, String type_, String region_, String departement_, String ville_, String datedebut_, String datefin_) {
        assert ((MainActivity)getActivity()) != null;
        ((MainActivity)getActivity()).showProgressLoad();


       String newText = searchText_.toLowerCase();
        String nText =   newText.replaceAll(" ","+");
        searchText= nText;
        trierPar=trierPar_;
        type=type_;
        region=region_;
        departement=departement_;
        ville=ville_;
        datedebut=datedebut_;
        datefin=datefin_;

        //Adding the method to the queue by calling the method getDataFromServer
        requestQueue.add(getDataFromServer(searchText_,trierPar_,type_,region_,departement_,ville_,requestCount,datedebut_,datefin_));
        //Incrementing the request counter
        requestCount++;
    }


    //This method will parse json data
    private void parseData(JSONArray array) {
        for (int i = 0; i < array.length(); i++) {
            //Creating the superhero object
            Evenement evenement_ = new Evenement();
            JSONObject json = null;
            try {
                //Getting json
                json = array.getJSONObject(i);

                //Adding data to the superhero object
                evenement_.setImageUrl(json.getString(Constants.TAG_IMAGE_URL));
                evenement_.setId(json.getString(Constants.ROW_ID));
                evenement_.setNomEvenement(json.getString(Constants.NOM_EVENEMENT));
                evenement_.setType(json.getString(Constants.TYPE));
                evenement_.setAdresse (json.getString(Constants.ADRESSE));
                evenement_.setCodePostale (json.getString(Constants.CODE_POSTAL));
                evenement_.setVille (json.getString(Constants.VILLE));
                evenement_.setDatedeb (json.getString(Constants.DATE_DEB));
                evenement_.setDatefin (json.getString(Constants.DATE_FIN));
                evenement_.setTelephone (json.getString(Constants.TELEPHONE));
                evenement_.setTypeTarif (json.getString(Constants.TYPETARIF));
                evenement_.setDescription(json.getString(Constants.DESCRIPTION));
                evenement_.setDepartement(json.getString(Constants.DEPARTEMENT));
                evenement_.setRegion(json.getString(Constants.REGION));
                evenement_.setNbLike(json.getInt("nbLike"));
                evenement_.setNbFavoris(json.getInt("nbFavoris"));
                evenement_.setNbAvis(json.getInt("nbAvis"));
                evenement_.setAdresseComplet(json.getString(Constants.ADRESSE_COMPLET));
                evenement_.setPaysNom(json.getString(Constants.PAYS_NOM));
                evenement_.setPaysCode(json.getString(Constants.PAYS_CODE));
                evenement_.setLatitude(json.getDouble(Constants.LATITUDE));
                evenement_.setLontitude(json.getDouble(Constants.LONTITUDE));
                if(BuildConfig.DEBUG) {
                    Log.i("PARSEDATA", json + "");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //Adding event to the list
            evenements.add(evenement_);
            if(BuildConfig.DEBUG) {
                Log.i("LISTEEVENEMENT", "parseData: " + evenements.size());
            }
        }
        assert ((MainActivity)getActivity()) != null;
if (((MainActivity)getActivity()) != null)      {  ((MainActivity)getActivity()).hideProgressLoad();}
        if(evenements.isEmpty()){
            textViewListeVide.setVisibility(View.VISIBLE);
            btnNouvelleRecherche.setVisibility(View.VISIBLE);
        }
        else {
            textViewListeVide.setVisibility(View.GONE);
            btnNouvelleRecherche.setVisibility(View.GONE);
        }

        //Notifying the adapter that data has been added or changed
        adapter.notifyDataSetChanged();
       // afficheAucunEvent();
    }


    //This method would check that the recyclerview scroll has reached the bottom or not
    private boolean isLastItemDisplaying(RecyclerView recyclerView) {
        if (recyclerView.getAdapter().getItemCount() != 0) {
            int lastVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();
            if (lastVisibleItemPosition != RecyclerView.NO_POSITION && lastVisibleItemPosition == recyclerView.getAdapter().getItemCount() - 1)
                return true;
        }
        return false;
    }

    //Overriden method to detect scrolling
    public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        //Ifscrolled at last then
        if (isLastItemDisplaying(recyclerView)) {
            //Calling the method getdata again

            getData(searchText,trierPar,type,region,departement,ville,datedebut,datefin);
        }
    }

    public void setRequestCount(int requestCount) {
        this.requestCount = requestCount;
        if (requestCount==1){
            evenements.clear();
        }
    }

    public void refresh(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //retrieve();

                if (session.isUserLoggedIn()){
                int idUser_ = session.getIdUser();
                    if(BuildConfig.DEBUG) {
                        Log.i("FRAGMENT_ACCUEIL_SWIPE", "ID USER: " + idUser_ + "email :" + emailUser);
                    }
                }
                evenements.clear();

                adapter.getSession().retrieveIntListLike();
                adapter.getSession().retrieveIntListFav();
                requestCount=1;
                getData(searchText,trierPar,type,region,departement,ville,datedebut,datefin);

adapter.notifyDataSetChanged();

                swipeRefresh.setRefreshing(false);
            }
        },100);

    }

    private String createGet(String searchText_,String trierPar_, String type_, String region_, String departement_, String ville_){
        return "&searchText="+searchText_+"&trierPar="+trierPar_+"&type="+type_+"&region="+region_+"&departement="+departement_+"&ville="+ville_;
    }
    public  void afficheAucunEvent(){
        if (evenements.isEmpty()) {
            if(BuildConfig.DEBUG) {
                Log.i("AFFICHE_AUCUN_EVENT", "afficheAucunEvent: Empty");
            }
            recyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }
        else {
            if(BuildConfig.DEBUG) {
                Log.i("AFFICHE_AUCUN_EVENT", "afficheAucunEvent: Not Empty");
            }
            recyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        }
    }

    public void setSession(int idUser, String emailUser){
        session.createUserLoginSession(idUser,emailUser);
        this.idUser =idUser;
        this.emailUser=emailUser;
        Log.i("FavFrag", "setSession: " + idUser +" "+ emailUser);
    }


}
