package com.eeventoo.app.fragment.dummy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DummyContent {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<FormuleItem> ITEMS = new ArrayList<FormuleItem>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static final Map<String, FormuleItem> ITEM_MAP = new HashMap<String, FormuleItem>();

    private static final int NB_FORMULE_MAX = 1;
    private static final int PREMIERE_FORMULE = 0;

    private int count =1;

  

    private static void addItem(FormuleItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    private static FormuleItem createDummyItem(int position) {
        return new FormuleItem(String.valueOf(position),"Prix (€)" ,  "Description");
    }

    private static String makeDetails(int position) {
        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);
        for (int i = 0; i < position; i++) {
            builder.append("\nMore details information here.");
        }
        return builder.toString();
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    /**
     * A dummy item representing a piece of content.
     */
    public static class FormuleItem {
        public  String id;
        public  String prix;
        public  String description;

        public FormuleItem(String id, String prix, String description) {
            this.id = id;
            this.prix = prix;
            this.description = description;
        }

        public FormuleItem(){

            this.id="";
            this.prix = "Prix (€)";
            this.description = "Description";
        }
        @Override
        public String toString() {
            return prix +" "+ description;
        }

        public String getId() {
            return id;
        }

        public String getPrix() {
            return prix;
        }

        public String getDescription() {
            return description;
        }

        public void setId(String id) {
            this.id = id;
        }

        public void setPrix(String prix) {
            this.prix = prix;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }


}
