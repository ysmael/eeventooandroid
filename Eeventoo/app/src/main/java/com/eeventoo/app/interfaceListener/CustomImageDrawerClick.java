package com.eeventoo.app.interfaceListener;

import android.view.View;

/**
 * Created by isma on 10/02/2017.
 */

public interface CustomImageDrawerClick {
    public void onItemClick(View v, int position);
}
