package com.eeventoo.app.interfaceListener;

import android.view.View;

/**
 * Created by isma on 16/06/2016.
 */
public interface CustomItemClickListener {
    public void onItemClick(View v, int position);


}
