package com.eeventoo.app.interfaceListener;

import android.view.MotionEvent;
import android.view.View;

/**
 * Created by isma on 04/09/2016.
 */

public interface CustomItemLongClickListener {
    public void onItemLongClick(View v, int position, MotionEvent motionEvent);
}
