package com.eeventoo.app.interfaceListener;

import android.view.View;

/**
 * Created by isma on 11/03/2017.
 */
public interface OnFeedItemClickListener {
    public void onCommentsClick(View v, int position);
}
