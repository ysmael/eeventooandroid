package com.eeventoo.app.sqLite;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by isma on 22/06/2016.
 */

public class DbAdapter {
    Context c;
    SQLiteDatabase db;
    DbHelper helper;

    public DbAdapter(Context c) {
        this.c = c;
        helper =new DbHelper((c));
    }

    //open DB
    public DbAdapter openDB(){
        try {
            db=helper.getWritableDatabase();
        }catch (SQLException e){
            e.printStackTrace();
        }
return this;
    }

    //CLOSE DB
     public void closeDB(){
         try {
             helper.close();
         }catch (SQLException e){
             e.printStackTrace();
         }

     }

//INSERT
  /*  public long add(String nom, int jour, int mois , int annee, int hd , int hf, String lieu, String dpt, String reg, int cp, String desc){
        try {
       /*    db.execSQL("CREATE TABLE evenement" +
                   "(id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "nom TEXT NOT NULL,"+
                    "jour INTEGER NOT NULL ," +
                    "mois INTEGER NOT NULL ," +
                    "annee INTEGER NOT NULL ," +
                    "heureDeb INTEGER NOT NULL ," +
                    "heureFin INTEGER NOT NULL ," +
                    "ville TEXT NOT NULL," +
                    "departement TEXT NOT NULL," +
                    "region TEXT NOT NULL," +
                    "codePostale INTEGER NOT NULL," +
                    "description TEXT NOT NULL);");
            ContentValues cv=  new ContentValues();
            cv.put(Constants.NOM_EVENEMENT,nom);
            cv.put(Constants.JOUR,jour);
            cv.put(Constants.MOIS,mois);
            cv.put(Constants.ANNEE,annee);
            cv.put(Constants.HEURE_DEB,hd);
            cv.put(Constants.HEURE_FIN,hf);
            cv.put(Constants.VILLE,lieu);
            cv.put(Constants.DEPARTEMENT,dpt);
            cv.put(Constants.REGION,reg);
            cv.put(Constants.CODE_POSTAL,cp);
            cv.put(Constants.DESCRIPTION,desc);

            return db.insert("evenement", Constants.ROW_ID,cv);
        }catch (SQLException e){
            e.printStackTrace();
        }
        return 0;
    }
//RETRIEVE

    public Cursor getAllEvenement(){

        String[] columns={Constants.ROW_ID, Constants.NOM_EVENEMENT,Constants.JOUR,Constants.MOIS,Constants.ANNEE,Constants.HEURE_DEB,Constants.HEURE_FIN,Constants.VILLE,Constants.DEPARTEMENT,Constants.REGION,Constants.CODE_POSTAL,Constants.DESCRIPTION};
        return db.query(Constants.TB_NAME,columns,null,null,null,null,null,null);
    }*/
}
