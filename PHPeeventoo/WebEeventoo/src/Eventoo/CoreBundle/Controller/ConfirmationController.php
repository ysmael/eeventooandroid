<?php
/**
 * Created by PhpStorm.
 * User: isma
 * Date: 26/10/2017
 * Time: 17:51
 */

namespace Eventoo\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;



class ConfirmationController extends Controller
{
    public function indexAction($key)
    {

        $doctrine =$this->getDoctrine();
        $em= $doctrine->getManager();
        $repo= $em->getRepository('EventooCoreBundle:Confirm_tb')->findOneBy(array('keycript'=>$key));

        if (count($repo)<=0)
        {
            $response="Nous sommes desolé, nous ne pouvons pas confirmer votre email !! ";
            return $this->render('EventooCoreBundle:Confirmation:confirmation.html.twig',array('response'=>$response));
        }
        else
            {
            $id = $repo->getUserid();
            //var_dump($id);

            $repoUser = $em->getRepository('EventooCoreBundle:User_infos')->find($id);

         if ($repoUser->getActive() == false) {

                $repoUser->setActive(1);
                $em->persist($repoUser);
                $em->flush();
                $response = "Eeventoo vous remercie! Votre compte est maintenant activé.";
                return $this->render('EventooCoreBundle:Confirmation:confirmation.html.twig',array('response'=>$response));
            }

            else
            {
                $response = "Votre compte a déjà été activé.";
                return $this->render('EventooCoreBundle:Confirmation:confirmation.html.twig',array('response'=>$response));
            }




        }
    }
}