<?php
/**
 * Created by PhpStorm.
 * User: isma
 * Date: 17/02/2018
 * Time: 02:10
 */

namespace Eventoo\CoreBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PrivacyPolicyController  extends Controller
{
    public function indexAction()
    {
        return $this->render('EventooCoreBundle:PrivacyPolicy:privacypolicy.html.twig');
    }
}