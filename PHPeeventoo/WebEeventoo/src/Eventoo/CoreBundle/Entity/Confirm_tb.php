<?php

namespace Eventoo\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Confirm_tb
 *
 * @ORM\Table(name="confirm_tb")
 * @ORM\Entity(repositoryClass="Eventoo\CoreBundle\Repository\Confirm_tbRepository")
 */
class Confirm_tb
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="userid", type="integer")
     */
    private $userid;

    /**
     * @var string
     *
     * @ORM\Column(name="keycript", type="string", length=250)
     */
    private $keycript;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=250)
     */
    private $email;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userid
     *
     * @param integer $userid
     *
     * @return Confirm_tb
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return int
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Set keycript
     *
     * @param string $keycript
     *
     * @return Confirm_tb
     */
    public function setKeycript($keycript)
    {
        $this->keycript = $keycript;

        return $this;
    }

    /**
     * Get keycript
     *
     * @return string
     */
    public function getKeycript()
    {
        return $this->keycript;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Confirm_tb
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
}
