<?php
/**
 * Created by PhpStorm.
 * User: isma
 * Date: 09/03/2019
 * Time: 13:05
 */

if (isset($_POST["nom"])
    && isset($_POST["type"])
    && isset($_POST["adresse"])
    && isset($_POST["adresseComplet"])
    && isset($_POST["codepostal"])
    && isset($_POST["ville"])
    && isset($_POST["departement"])
    && isset($_POST["region"])
    && isset($_POST["paysNom"])
    && isset($_POST["paysCode"])
    && isset($_POST["latitude"])
    && isset($_POST["lontitude"])
    && isset($_POST["datedeb"])
    && isset($_POST["datefin"])
    && isset($_POST["telephone"])
    && isset($_POST["typeTarif"])
    && isset($_POST["description"])
    && isset($_POST["idUser"])
    && isset($_POST["intTypeEvent"])
    && isset($_POST["description"])) {

    require_once('init.php');


    $target_dir = "EventImageDef/";

    $response = array();



    $target_file_name = $target_dir . "imageEventDefault.jpg";
    $actualpath = "http://eeventoo.com/webapp/$target_file_name";

    $nom = $_POST["nom"];
    $type = $_POST["type"];
    $adresse = $_POST["adresse"];
    $adresseComplet = $_POST["adresseComplet"];
    $codepostal = $_POST["codepostal"];
    $ville = $_POST["ville"];
    $departement = $_POST["departement"];
    $region = $_POST["region"];
    $paysNom = $_POST["paysNom"];
    $paysCode = $_POST["paysCode"];
    $latitude = $_POST["latitude"];
    $lontitude = $_POST["lontitude"];
    $datedeb = $_POST["datedeb"];
    $datefin = $_POST["datefin"];
    $telephone = $_POST["telephone"];
    $description = $_POST["description"];
    $idUser = $_POST["idUser"];
    $typeTarif = $_POST["typeTarif"];
    $typeEventRepetition = $_POST["intTypeEvent"];

    $keyGroupeEvent = $nom . date('Y-m-d H:i:s');
    $keycryptGroupeEvent = md5($keyGroupeEvent);

    if ($typeTarif === "Payant") {
        $boolTypeTarif = 0;
        $success=true;
        $message="payant";
    } else {
        $boolTypeTarif = 1;
        $success=true;
        $message="$typeEventRepetition";
    }






    $nom=str_replace("\'","\\\\\\'",$_POST["nom"]);
    $type=str_replace("\'","\\\\\\'",$_POST["type"]);
    $adresse=str_replace("\'","\\\\\\'",$_POST["adresse"]);
    $adresseComplet=str_replace("\'","\\\\\\'",$_POST["adresseComplet"]);
    $codepostal=str_replace("\'","\\\\\\'",$_POST["codepostal"]);
    $ville =str_replace("\'","\\\\\\'",$_POST["ville"]);
    $departement=str_replace("\'","\\\\\\'",$_POST["departement"]);
    $region=str_replace("\'","\\\\\\'",$_POST["region"]);
    $paysNom=str_replace("\'","\\\\\\'",$_POST["paysNom"]);
    $paysCode=str_replace("\'","\\\\\\'",$_POST["paysCode"]);
    $latitude=str_replace("\'","\\\\\\'",$_POST["latitude"]);
    $lontitude=str_replace("\'","\\\\\\'",$_POST["lontitude"]);
    $datedeb=str_replace("\'","\\\\\\'",$_POST["datedeb"]);
    $datefin=str_replace("\'","\\\\\\'",$_POST["datefin"]);
    $telephone=str_replace("\'","\\\\\\'",$_POST["telephone"]);
    $description=str_replace("\'","\\\\\\'",$_POST["description"]);
    $idUser =str_replace("\'","\\\\\\'",$_POST["idUser"]);
    $typeTarif = str_replace("\'","\\\\\\'",$_POST["typeTarif"]);
    $typeEventRepetition =str_replace("\'","\\\\\\'",$_POST["intTypeEvent"]);




        if ($typeEventRepetition == 1) {
            $key = $nom . date('Y-m-d H:i:s') ;
            $keycrypt = md5($key);

            $path = "http://eeventoo.com/webapp/$target_file_name";

            $sqlOneEvent="call addOneEvent('$idUser',
                '$path',
                '$nom',
                '$type',
                '$adresse',
                '$adresseComplet',
                '$codepostal',
                '$ville',
                '$departement',
                '$region','$paysNom','$paysCode','$latitude','$lontitude','$datedeb','$datefin','$telephone','$typeTarif','$description','$keycryptGroupeEvent','$boolTypeTarif')";


                if ($res = $dbh->query($sqlOneEvent)) {
                    $boolCheckEventRepetition = true;
                    $code=1;
                    $success = true;
                    $message = "Votre évènement a été ajouté avec succès";
                } else {
                    $boolCheckEventRepetition = false;
                    $dbh->rollBack();

                    $success = false;
                    $message = "Quelques erreurs se sont produites lors de la requete sql et les fichiers uploadés ont été supprimés $sqlOneEvent";

                }



        }


}

else {
    $success=false;
    $message= "Element du post maquant";
}

$response["success"]=$success;
$response["message"]=$message ;
$response["idGroupEvent"]=$keycryptGroupeEvent;
echo json_encode($response);


?>