<?php
/**
 * Created by PhpStorm.
 * User: isma
 * Date: 17/10/2018
 * Time: 13:56
 */

require "init.php";
if (isset($_POST["requete"])){
    $requete = $_POST["requete"];

    $res = $dbh->query($requete);

    if (res){
        $response = array();
        $code = "reg_true";
        $success=true;

        $message = "Evenement ajouté!";
        $response["success"]=$success;
        $response["message"] = $message;
        echo json_encode($response);
    }
    else
    {
        $response = array();
        $code = "reg_true";
        $success=false;

        $message = "Une erreur s'est produite, veuillez réessayer!";
        $response["success"]=$success;
        $response["message"] = $message;
        echo json_encode($response);
    }
}