<?php
/**
 * Created by PhpStorm.
 * User: isma
 * Date: 28/05/2017
 * Time: 01:26
 */


if (isset($_POST["idUser"]) && isset($_POST["idEvent"])) {
    require "init.php";
    $idUser = $_POST["idUser"];
    $idEvent = $_POST["idEvent"];

    //Obtenir url image pour pouvoir la supprimer
    $sqlGetImage = "call getImageUrlOfEvent($idUser,$idEvent);";
    $resultImageUrl = $dbh->query($sqlGetImage);

    if ($resultImageUrl) {


        while ($row = $resultImageUrl->fetch()) {
            $urlImage = $row['image'];
        }
    }
    $resultImageUrl->closeCursor();

    //suppresion l'evenement et de l'image
    $query = "call deleteEventUser($idUser,$idEvent);";

    if (strpos($urlImage, 'EventImageDef')) {
        $result = $dbh->query($query);

        if ($result) {
//substr afin d'enlever le http//eeventoo.com/webapp

            $success = true;
            $message = "Votre évènement a été supprimé avec succès Merci  ";
        } else {
            $success = false;
            $message = "Quelques erreurs se sont produites. Essayer ultérieurement ...";
        }
    } else {
        $path = substr($urlImage, 26);
        if (unlink("." . $path)) {
            $result = $dbh->query($query);

            if ($result) {
//substr afin d'enlever le http//eeventoo.com/webapp

                $success = true;
                $message = "Votre évènement a été supprimé avec succès Merci  ";
            } else {
                $success = false;
                $message = "Quelques erreurs se sont produites. Essayer ultérieurement ...";
            }
        } else {

            $success = false;
            $message = "Quelques erreurs se sont produites Essayer encore ...";
        }
    }

}
else {
    $success=false;
    $message= "Element du post maquant";
}


$response["success"]=$success;
$response["message"]=$message;
echo json_encode($response);

