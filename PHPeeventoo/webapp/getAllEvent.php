<?php

	
if (isset($_POST['page'])){

    $searchText = $_POST["searchText"];
    $trierPar = $_POST["trierPar"];
    $type = $_POST["type"];
    $region = $_POST["region"];
    $departement = $_POST["departement"];
    $ville = $_POST["ville"];
	//Getting the page number which is to be displayed  
	$page = $_POST['page'];
	$datedebut= $_POST['datedebut'];
    $datefin= $_POST['datefin'];
	
	//Initially we show the data from 1st row that means the 0th row 
	$start = 0; 
	
	//Limit is 10 that means we will show 3 items at once
	$limit = 10;
	
	//Importing the database connection 
	require_once('init.php');

	//Counting the total item available in the database 
	$allid= $dbh->query("SELECT COUNT(id) from evenement_tb ");
	//$total = mysqli_num_rows(mysqli_query($con, "SELECT id from evenement_tb "));
	$total=$allid->fetchColumn();

		//We can go atmost to page number total/limit
	$page_limit = $total/$limit+1; 
	
	//If the page number is more than the limit we cannot show anything 
	if($page<=$page_limit) {

        //Calculating start for every given page number
        $start = ($page - 1) * $limit;
        $recherche = str_replace(' ', '%', $searchText);
        $rechercheFinal = "%$recherche%";
        //SQL query to fetch data of a range
        //$sql = "SELECT * from evenement_tb order by id desc limit $start, $limit";
//$sql="select e.id, e.image,e.nom,e.type,e.adresse,e.codepostal,e.ville, e.datedebut,e.datefin,e.telephone,e.tarifentree,e.description,d.nom AS departement,r.nom AS region from evenement_tb e, villes v , regions r , departements d where e.codepostal=v.codepostal and e.ville= v.nom and v.departement_id=d.id and d.region_id=r.id order by e.id desc limit $start, $limit";
        $sql = "call searchproc('$rechercheFinal','$trierPar','$type','$region','".$departement."','$ville',$start,$limit,'$datedebut','$datefin');";



        //Getting result
        //$result = mysqli_query($con,$sql);
        $result = $dbh->query($sql);

        if ($result) {
            //Adding results to an array
            $res = array();

            while ($row = $result->fetch()) {

                $datedeb = date_create($row['datedebut']);
                $datefin = date_create($row['datefin']);

                array_push($res, array(
                        "id" => $row['id'],
                        "image" => $row['image'],
                        "nom" => $row['nom'],
                        "type" => $row['type'],
                        "adresse" => $row['adresse'],
                        "codepostal" => $row['codepostal'],
                        "ville" => $row['ville'],
                        "datedeb" => date_format($datedeb, 'd/m/Y H:i'),
                        "datefin" => date_format($datefin, 'd/m/Y H:i'),
                        "telephone" => $row['telephone'],
                        "typeTarif"=>$row['typeTarif'],
                        "description" => $row['description'],
                        "departement" => $row['departement'],
                        "region" => $row['region'],
                        "nbFavoris"=> $row['nbFavoris'],
                        "nbLike"=>$row['nbLike'],
                        "nbAvis"=>$row['nbAvis'],
                        "adresseComplet"=>$row['adresseComplet'],
                        "paysNom"=>$row['paysNom'],
                        "paysCode"=>$row['paysCode'],
                        "latitude"=>$row['latitude'],
                        "lontitude"=>$row['lontitude']

                    )
                );
            }
            //Displaying the array in json format
            echo json_encode($res);
        } else {
            echo "over";

        }
    }
    $dbh=null;
}

	?>