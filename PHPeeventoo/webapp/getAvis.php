<?php
/**
 * Created by PhpStorm.
 * User: isma
 * Date: 13/03/2017
 * Time: 01:21
 */

if (isset($_POST["idEvent"]) && isset($_POST["page"])) {
    require_once('init.php');
    $idEvent = $_POST["idEvent"];
    $page = $_POST["page"];
    //Getting the page number which is to be displayed
    $page = $_POST['page'];

    //Initially we show the data from 1st row that means the 0th row
    $start = 0;

    //Limit is 10 that means we will show 3 items at once
    $limit = 10;

    //Counting the total item available in the database
    $allid = $dbh->query("SELECT COUNT(id) from  avis_tb where idEvent='".$idEvent. "'");
    $total = $allid->fetchColumn();

    //We can go atmost to page number total/limit
    $page_limit = $total / $limit + 1;

    //If the page number is more than the limit we cannot show anything
    if ($page <= $page_limit) {

        //Calculating start for every given page number
        $start = ($page - 1) * $limit;

        $sql = "select A.id ,U.displayName, A.avis, A.date_avis,nbetoiles,U.photoprofil from avis_tb A, evenement_tb E , user_infos U where A.idEvent = E.id and  A.idUser=U.id and A.idEvent=$idEvent order by A.date_avis asc limit $start, $limit";

        $result = $dbh->query($sql);

        if ($result) {
            //Adding results to an array
            $res = array();

            while ($row = $result->fetch()) {

                $dateavis= date_create($row['date_avis']);
               array_push($res,array('idAvis' => $row['id'],'displayName' => $row['displayName'],
                   'avis' => $row['avis'],
                   'date_avis' => date_format($dateavis,'d/m/Y'),'nbetoiles'=>$row['nbetoiles'],"photoprofil"=>$row['photoprofil']));
            }
            //Displaying the array in json format
            echo json_encode($res);
        } else {
            echo "over";
        }
    }


    $dbh = null;
}
?>