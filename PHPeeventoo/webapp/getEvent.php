<?php
/**
 * Created by PhpStorm.
 * User: isma
 * Date: 02/02/2017
 * Time: 21:45
 */

require "init.php";
$searchText = $_POST["searchText"];
$trierPar = $_POST["trierPar"];
$type = $_POST["type"];
$region = $_POST["region"];
$departement = $_POST["departement"];
$ville = $_POST["ville"];
$start = $_POST["start"];
$limit = $_POST["limit"];


$query="call searchproc('$searchText','$trierPar','$type','$region','$departement','$ville',$start,$limit);";
echo  $query;
$result = $dbh->query($query);

if ($result) {
    $res = array();

    while($row =  $result->fetch()){

        $datedeb = date_create($row['datedebut']);
        $datefin = date_create($row['datefin']);

        array_push($res, array(
                "id"=>$row['id'],
                "image"=>$row['image'],
                "nom"=>$row['nom'],
                "type"=>$row['type'],
                "adresse"=>$row['adresse'],
                "codepostal"=>$row['codepostal'],
                "ville"=>$row['ville'],
                "datedeb"=>date_format($datedeb,'d/m/Y H:i'),
                "datefin"=>date_format($datefin,'d/m/Y H:i'),
                "telephone"=>$row['telephone'],
                "description"=>$row['description'],
                "departement"=>$row['departement'],
                "region"=>$row['region']
            )
        );
    }
    echo json_encode($res);


}