<?php
/**
 * Created by PhpStorm.
 * User: isma
 * Date: 21/05/2017
 * Time: 17:50
 */

if (isset($_POST["idUser"])) {
    require_once('init.php');
$idUser= $_POST["idUser"];
    $query ="call getEventUser($idUser);";

    $result = $dbh->query($query);

    if ($result) {
        //Adding results to an array
        $res = array();

        while ($row = $result->fetch()) {

            $datedeb = date_create($row['datedebut']);
            $datefin = date_create($row['datefin']);

            array_push($res, array(
                    "id" => $row['id'],
                    "image" => $row['image'],
                    "nom" => $row['nom'],
                    "type" => $row['type'],
                    "adresse" => $row['adresse'],
                    "codepostal" => $row['codepostal'],
                    "ville" => $row['ville'],
                    "datedeb" => date_format($datedeb, 'd/m/Y H:i'),
                    "datefin" => date_format($datefin, 'd/m/Y H:i'),
                    "telephone" => $row['telephone'],
                    "typeTarif"=>$row['typeTarif'],
                    "description" => $row['description'],
                    "departement" => $row['departement'],
                    "region" => $row['region'],
                    "nbFavoris"=> $row['nbFavoris'],
                    "nbLike"=>$row['nbLike'],
                    "nbAvis"=>$row['nbAvis'],
                    "adresseComplet"=>$row['adresseComplet'],
                    "paysNom"=>$row['paysNom'],
                    "paysCode"=>$row['paysCode'],
                    "latitude"=>$row['latitude'],
                    "lontitude"=>$row['lontitude']
                )
            );
        }
        //Displaying the array in json format
        echo json_encode(array('data' =>$res));
    } else {
        echo "over";
    }

    $dbh=null;
}