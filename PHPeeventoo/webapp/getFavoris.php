<?php
if (isset($_GET['page']) && isset($_GET["idUser"])) {
//Getting the page number which is to be displayed  
    $page = $_GET['page'];
    $idUser = $_GET["idUser"];

//Importing the database connection 
    require_once('init.php');


    //Initially we show the data from 1st row that means the 0th row
    $start = 0;

    //Limit is 3 that means we will show 3 items at once
    $limit = 4;

    //Counting the total item available in the database
    //$total = mysqli_num_rows(mysqli_query($con, "SELECT id from favoris where idUser='".$idUser."'"));
    $idFav = $dbh->query("SELECT id from favoris where idUser='" . $idUser . "'");
    $total = $idFav->fetchColumn();

//We can go atmost to page number total/limit
    $page_limit = $total / $limit + 1;

    //If the page number is more than the limit we cannot show anything
    if ($page <= $page_limit) {

        //Calculating start for every given page number
        $start = ($page - 1) * $limit;

        //SQL query to fetch data of a range
        //$sql ="select idEvent,image,nom,type,adresse,codepostal,ville,datedebut,datefin,telephone,tarifentree,description from evenement_tb E,favoris F where E.id=F.idEvent and idUser='".$idUser."' ORDER BY `F`.`id` DESC limit $start, $limit;";
        $sql = "call getFavorisUser($idUser ,$start, $limit)";
//Getting result 
        //$result = mysqli_query($con,$sql);
        $result = $dbh->query($sql);
        if ($result) {
        //Adding results to an array
        $res = array();

        while ($row = $result->fetch()) {

            $datedeb = date_create($row['datedebut']);
            $datefin = date_create($row['datefin']);

            array_push($res, array(
                    "id" => $row['idEvent'],
                    "image" => $row['image'],
                    "nom" => $row['nom'],
                    "type" => $row['type'],
                    "adresse" => $row['adresse'],
                    "codepostal" => $row['codepostal'],
                    "ville" => $row['ville'],
                    "datedeb" => date_format($datedeb, 'd/m/Y H:i'),
                    "datefin" => date_format($datefin, 'd/m/Y H:i'),
                    "telephone" => $row['telephone'],
                    "typeTarif"=>$row['typeTarif'],
                    "description" => $row['description'],
                    "departement" => $row['departement'],
                    "region" => $row['region'],
                    "nbFavoris"=> $row['nbFavoris'],
                    "nbLike"=>$row['nbLike'],
                    "nbAvis"=>$row['nbAvis']

                )
            );
        }
        //Displaying the array in json format
        echo json_encode($res);
    } else {

        echo "over";
    }
}
    $dbh=null;
}

?>