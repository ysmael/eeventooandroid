<?php
/**
 * Created by PhpStorm.
 * User: isma
 * Date: 18/10/2018
 * Time: 01:07
 */

if (isset($_POST["idEvent"])) {
    require_once('init.php');
    $idEvent= $_POST["idEvent"];


    $query = "call getFormulesEvent($idEvent);";

    $result = $dbh->query($query);

    if ($result) {
        $success = true;
        $response = array();
        $res = array();
        while ($donnees = $result->fetch()) {
            array_push($res, array(

                "id" => $donnees['id'],
                "idEvent" => $donnees['idEvent'],
                "description" => $donnees['description'],
                "prix" => $donnees['prix'],
                "devise" => $donnees['devise']
            ));
        }
        $response['success'] = $success;
        $response['data'] = $res;

        echo json_encode($response);

    } else {
        $success = false;
        $response = array();
        $response["success"] = $success;

        echo json_encode($response);

    }


    $dbh = null;
}