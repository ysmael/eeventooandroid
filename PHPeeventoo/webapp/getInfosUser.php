<?php
/**
 * Created by PhpStorm.
 * User: isma
 * Date: 06/06/2017
 * Time: 03:21
 */

if (isset($_POST["idUser"])) {
    require_once('init.php');
    $idUser= $_POST["idUser"];


    $query ="call getUserInfos('$idUser');";
    $res = array();
    $result = $dbh->query($query);
    if ($result) {

        $response = array();
        while ($row = $result->fetch()) {
            $res["nom"] = $row['nom'];
            $res["prenom"] = $row['prenom'];
            $res["adresse"] = $row['adresse'];
            $res["codepostal"] =$row['codepostal'];
            $res["ville"] = $row['ville'];
             $res["telephone"] =$row['telephone'];
            $res["photoprofil"] = $row['photoprofil'];
            $res["email"]= $row['email'];

        }
        $code = true;
        $message = "Les informations de l'utilisateur ont été recupéré";
        $response["success"]=$code;
        $response["message"]=$message;
        $response["user"]=$res;
        echo json_encode($response);
    }
    else {
        $response = array();
        $code = false;
        $message = "Les informations de l'utilisateur n'ont pas été recupéré. Essayer encore!!!";
        $response["success"]=$code;
        $response["message"]=$message;
        $response["user"]=$res;
        echo json_encode($response);

    }

    $dbh = null;
}