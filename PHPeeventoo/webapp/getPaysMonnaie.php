<?php
/**
 * Created by PhpStorm.
 * User: isma
 * Date: 26/02/2018
 * Time: 22:22
 */
require_once('init.php');
$query ="call getPaysMonnaie();";

$result = $dbh->query($query);

if ($result) {
    $success=true;
    $response = array();
    $res = array();
    while ($donnees = $result->fetch()) {
        array_push($res, array(

            "codeMonnaie"=>$donnees['codeMonnaie'],
            "monnaie"=>$donnees['monnaie'],
            "symboleMonnaie"=>$donnees['symboleMonnaie']
        ));
    }
    $response['success']=$success;
    $response['data']=$res;

    echo json_encode($response);

}
else {
    $success=false;
    $response = array();
    $response["success"]=$success;

    echo json_encode($response);

}
$dbh = null;