<?php
/**
 * Created by PhpStorm.
 * User: isma
 * Date: 02/03/2018
 * Time: 21:47
 */

require_once('init.php');
$query ="call getTypeEvent();";

$result = $dbh->query($query);

if ($result) {
    $success=true;
    $response = array();
    $res = array();
    while ($donnees = $result->fetch()) {
        array_push($res, array(

            "id"=>$donnees['id'],
            "nomType"=>$donnees['nom'],

        ));
    }
    $response['success']=$success;
    $response['data']=$res;

    echo json_encode($response);

}
else {
    $success=false;
    $response = array();
    $response["success"]=$success;

    echo json_encode($response);

}
$dbh = null;