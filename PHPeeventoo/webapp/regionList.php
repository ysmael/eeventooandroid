<?php
/**
 * Created by PhpStorm.
 * User: isma
 * Date: 21/01/2017
 * Time: 01:12
 */

require "init.php";
$query= "SELECT nom from regions";
$result = $dbh->query($query);

if ($result) {
    $res = array();

    while ($row = $result->fetch()) {

        array_push($res, array(
            'region' => $row['nom']
        ));
    }
    echo json_encode($res);


    $dbh = null;
}

?>