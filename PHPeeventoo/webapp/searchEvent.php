<?php
/**
 * Created by PhpStorm.
 * User: isma
 * Date: 13/01/2017
 * Time: 14:42
 */
require "init.php";
if (isset($_GET["search"])) {

    $search = $_GET["search"];
    $recherche = str_replace(' ', '%', $search);

    $query = "SELECT * FROM evenement_tb WHERE nom LIKE '%" . $recherche. "%'";

    $result = $dbh->query($query);

    if ($result) {
        $res = array();

        while ($row = $result->fetch()) {

            $datedeb = date_create($row['datedebut']);
            $datefin = date_create($row['datefin']);

            array_push($res, array(
                    "id" => $row['id'],
                    "image" => $row['image'],
                    "nom" => $row['nom'],
                    "type" => $row['type'],
                    "adresse" => $row['adresse'],
                    "codepostal" => $row['codepostal'],
                    "ville" => $row['ville'],
                    "datedeb" => date_format($datedeb, 'd/m/Y H:i'),
                    "datefin" => date_format($datefin, 'd/m/Y H:i'),
                    "telephone" => $row['telephone'],
                    "description" => $row['description'])
            );
        }
        //Displaying the array in json format
        echo json_encode($res);


        $dbh = null;
    }
}


?>