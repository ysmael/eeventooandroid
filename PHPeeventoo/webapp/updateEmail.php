<?php
/**
 * Created by PhpStorm.
 * User: isma
 * Date: 05/06/2017
 * Time: 23:35
 */

if (isset($_POST["idUser"])&& isset($_POST["email"])) {
    require_once('init.php');
    $idUser= $_POST["idUser"];

    $email=$_POST["email"];
    $query ="call updateEmail('$idUser','$email');";

    $result = $dbh->query($query);
    if ($result) {
        $response = array();
        $code = "UpdateEmail_true";
        $message = "Email mise à jour";
        $response["success"]=true;
        $response["message"]=$message;
        echo json_encode($response);
    }
    else {
        $response = array();
        $code = "UpdateEmail_false";
        $message = "La mise à jour de votre email n'a pas eté effectué suite à un problème. Essayer encore!!!";
        $response["success"]=false;
        $response["message"]=$message;
        echo json_encode($response);

    }

    $dbh = null;
}