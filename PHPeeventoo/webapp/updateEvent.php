<?php
/**
 * Created by PhpStorm.
 * User: isma
 * Date: 02/03/2018
 * Time: 19:26
 */

if (isset($_POST["nom"])&&
    isset($_POST["idEvent"])
    && isset($_POST["type"])
    && isset($_POST["adresse"])
    && isset($_POST["adresseComplet"])
    && isset($_POST["codepostal"])
    && isset($_POST["ville"])
    && isset($_POST["departement"])
    && isset($_POST["region"])
    && isset($_POST["paysNom"])
    && isset($_POST["paysCode"])
    && isset($_POST["latitude"])
    && isset($_POST["lontitude"])
    && isset($_POST["datedeb"])
    && isset($_POST["datefin"])
    && isset($_POST["telephone"])
    && isset($_POST["typeTarif"])
    && isset($_POST["description"])
    && isset($_POST["idUser"])
    && isset($_POST["description"]) && isset($_FILES['file'])) {

    require_once('init.php');



    $id = $_POST["idEvent"];

    $target_dir = "image/";
    $file_name=$target_dir.basename($_FILES['file']['name']);
    $response = array();

    $fichier=$_FILES["file"]["tmp_name"];
    $typeFile=substr($file_name,strrpos($file_name,'.')+0);

    $target_file_name=$target_dir.$id.$typeFile;
    $actualpath = "https://eeventoo.com/webapp/$target_file_name";

    $nom=$_POST["nom"];
    $idEvent=$_POST["idEvent"];
    $type=$_POST["type"];
    $adresse=$_POST["adresse"];
    $adresseComplet=$_POST["adresseComplet"];
    $codepostal=$_POST["codepostal"];
    $ville =$_POST["ville"];
    $departement=$_POST["departement"];
    $region=$_POST["region"];
    $paysNom=$_POST["paysNom"];
    $paysCode=$_POST["paysCode"];
    $latitude=$_POST["latitude"];
    $lontitude=$_POST["lontitude"];
    $datedeb=$_POST["datedeb"];
    $datefin=$_POST["datefin"];
    $telephone=$_POST["telephone"];
    $description=$_POST["description"];
    $idUser =$_POST["idUser"];
    $typeTarif = $_POST["typeTarif"];


    $sql="call updateEvent('$idEvent', '$idUser','$actualpath',
'$nom',
'$type',
'$adresse',
'$adresseComplet',
'$codepostal',
'$ville',
'$departement',
'$region',
'$paysNom',
'$paysCode',
'$latitude',
'$lontitude',
'$datedeb',
'$datefin',
'$telephone',
'$typeTarif',
'$description')";


    $res = $dbh->query($sql);
    if (move_uploaded_file($fichier,$target_file_name) && $res){
        $success=true;
        $message= "Votre évènement a été modifié avec succès Merci ";
    }
    else{
        $dbh->rollBack();
        $success=false;
        $message= "Quelques erreurs se sont produites Essayer encore ... ";
    }


}
else {
    $success=false;
    $message= "Element du post maquant ";
}


$response["success"]=$success;
$response["message"]=$message;
echo json_encode($response);

?>