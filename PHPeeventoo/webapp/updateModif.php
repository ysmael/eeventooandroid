<?php
/**
 * Created by PhpStorm.
 * User: isma
 * Date: 06/06/2017
 * Time: 00:29
 */

if (isset($_POST["idUser"])&& isset($_POST["prenom"]) && isset($_POST["nom"])&& isset($_POST["adresse"])&& isset($_POST["codepostal"])&& isset($_POST["ville"])) {
    require_once('init.php');
    $idUser= $_POST["idUser"];

    $prenom=$_POST["prenom"];
    $nom =$_POST["nom"];
    $adresse=$_POST["adresse"];
    $codepostal =$_POST["codepostal"];
    $ville=$_POST["ville"];
    $departement=$_POST["departement"];
    $region=$_POST["region"];
    $paysNom=$_POST["paysNom"];
    $paysCode=$_POST["paysCode"];
    $latitude=$_POST["latitude"];
    $lontitude=$_POST["lontitude"];
    $adresseComplet=$_POST["adresseComplet"];
    $query ="call updateModif('$idUser','$prenom','$nom','$adresse','$adresseComplet','$codepostal','$ville','$departement','$region','$paysNom','$paysCode','$latitude','$lontitude');";
    $result = $dbh->query($query);
    if ($result) {
        $response = array();
        $code = true;
        $message = "Mise à jour éffectué";
        $response["success"]=$code;
        $response["message"]=$message;
        echo json_encode($response);
    }
    else {
        $response = array();
        $code = false;
        $message = "La mise à jour n'a pas eté effectué suite à un problème. Essayer encore!!!";
        $response["success"]=$code;
        $response["message"]=$message;
        echo json_encode($response);

    }

    $dbh = null;
}