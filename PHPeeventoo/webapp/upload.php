<?php
/**
 * Created by PhpStorm.
 * User: isma
 * Date: 09/03/2019
 * Time: 13:05
 */
require "init.php";

if (isset($_POST["nom"])
    && isset($_POST["type"])
    && isset($_POST["adresse"])
    && isset($_POST["adresseComplet"])
    && isset($_POST["codepostal"])
    && isset($_POST["ville"])
    && isset($_POST["departement"])
    && isset($_POST["region"])
    && isset($_POST["paysNom"])
    && isset($_POST["paysCode"])
    && isset($_POST["latitude"])
    && isset($_POST["lontitude"])
    && isset($_POST["datedeb"])
    && isset($_POST["datefin"])
    && isset($_POST["telephone"])
    && isset($_POST["typeTarif"])
    && isset($_POST["description"])
    && isset($_POST["idUser"])
    && isset($_POST["intTypeEvent"])
    && isset($_POST["description"])
    && isset($_FILES['file'])) {


    $sql = "SELECT id FROM evenement_tb ORDER BY id ASC";

    //$res = mysqli_query($con,$sql);
    $res = $dbh->query($sql);
    $success= false;
    $message ="";

    $keycryptGroupeEvent="";
    $id = 0;

    while ($row = $res->fetch()) {
        $id = $row['id'];
    }

    $target_dir = "image/";
    $target_dir_tmp= "imageTmp/";
    $file_name = $target_dir . basename($_FILES['file']['name']);
    $response = array();

    $fichier = $_FILES["file"]["tmp_name"];
    $typeFile = substr($file_name, strrpos($file_name, '.') + 0);

    $key = $nom . date('mY h:i:s');
    $keycrypt = md5($key);

    $target_file_name = $target_dir_tmp . $keycrypt . $typeFile;
    $actualpath = "http://eeventoo.com/webapp/$target_file_name";

    $nom = $_POST["nom"];
    $type = $_POST["type"];
    $adresse = $_POST["adresse"];
    $adresseComplet = $_POST["adresseComplet"];
    $codepostal = $_POST["codepostal"];
    $ville = $_POST["ville"];
    $departement = $_POST["departement"];
    $region = $_POST["region"];
    $paysNom = $_POST["paysNom"];
    $paysCode = $_POST["paysCode"];
    $latitude = $_POST["latitude"];
    $lontitude = $_POST["lontitude"];
    $datedeb = $_POST["datedeb"];
    $datefin = $_POST["datefin"];
    $telephone = $_POST["telephone"];
    $description = $_POST["description"];
    $idUser = $_POST["idUser"];
    $typeTarif = $_POST["typeTarif"];
    $typeEventRepetition = $_POST["intTypeEvent"];

    $keyGroupeEvent = $nom . date('Y-m-d H:i:s');
    $keycryptGroupeEvent = md5($keyGroupeEvent);

    if ($typeTarif === "Payant") {
        $boolTypeTarif = 0;
        $success=true;
        $message="payant";
    } else {
        $boolTypeTarif = 1;
        $success=true;
        $message="$typeEventRepetition";
    }






    $nom=str_replace("\'","\\\\\\'",$_POST["nom"]);
    $type=str_replace("\'","\\\\\\'",$_POST["type"]);
    $adresse=str_replace("\'","\\\\\\'",$_POST["adresse"]);
    $adresseComplet=str_replace("\'","\\\\\\'",$_POST["adresseComplet"]);
    $codepostal=str_replace("\'","\\\\\\'",$_POST["codepostal"]);
    $ville =str_replace("\'","\\\\\\'",$_POST["ville"]);
    $departement=str_replace("\'","\\\\\\'",$_POST["departement"]);
    $region=str_replace("\'","\\\\\\'",$_POST["region"]);
    $paysNom=str_replace("\'","\\\\\\'",$_POST["paysNom"]);
    $paysCode=str_replace("\'","\\\\\\'",$_POST["paysCode"]);
    $latitude=str_replace("\'","\\\\\\'",$_POST["latitude"]);
    $lontitude=str_replace("\'","\\\\\\'",$_POST["lontitude"]);
    $datedeb=str_replace("\'","\\\\\\'",$_POST["datedeb"]);
    $datefin=str_replace("\'","\\\\\\'",$_POST["datefin"]);
    $telephone=str_replace("\'","\\\\\\'",$_POST["telephone"]);
    $description=str_replace("\'","\\\\\\'",$_POST["description"]);
    $idUser =str_replace("\'","\\\\\\'",$_POST["idUser"]);
    $typeTarif = str_replace("\'","\\\\\\'",$_POST["typeTarif"]);
    $typeEventRepetition =str_replace("\'","\\\\\\'",$_POST["intTypeEvent"]);


    if (move_uploaded_file($fichier,$target_file_name)) {


        if ($typeEventRepetition == 1) {
            $key = $nom . date('Y-m-d H:i:s') ;
            $keycrypt = md5($key);

            $target_file = $target_dir . $keycrypt . $typeFile;
            $path = "http://eeventoo.com/webapp/$target_file";

            $sqlOneEvent="call addOneEvent('$idUser',
                '$path',
                '$nom',
                '$type',
                '$adresse',
                '$adresseComplet',
                '$codepostal',
                '$ville',
                '$departement',
                '$region','$paysNom','$paysCode','$latitude','$lontitude','$datedeb','$datefin','$telephone','$typeTarif','$description','$keycryptGroupeEvent','$boolTypeTarif')";

            if (copy($target_file_name, $target_file)) {

                if ($res = $dbh->query($sqlOneEvent)) {
                    $boolCheckEventRepetition = true;
                    $code=1;
                    $success = true;
                    $message = "Votre évènement a été ajouté avec succès";
                } else {
                    $boolCheckEventRepetition = false;
                    $dbh->rollBack();
                    /* if(unlink("."."/".$target_file_name)){
                         $success = false;
                         $message = "Quelques erreurs se sont produites lors de la requete sql et les fichiers uploadés ont été supprimés $sqlOneEvent";
                     }
                     else{
                         $success = false;
                         $message = "Quelques erreurs se sont produites lors de la requete sql et les fichiers uploadés ont été supprimés $sqlOneEvent";
                     }*/
                    $success = false;
                    $message = "Quelques erreurs se sont produites lors de la requete sql et les fichiers uploadés ont été supprimés $sqlOneEvent";

                }
            }
            else{


                $boolCheckEventRepetition = false;
                $success=false;
                $message= "Echec upload fichier";
            }


        }

        if ($typeEventRepetition == 2) {
            $tousLes = $_POST["intTousLes"];
            $nbJoursRestant = $_POST["nbjoursRestant"];
            $y = $nbJoursRestant;
            $tousLesSql = $tousLes;
            $i = 0;




            while ($i < $y) {
                //create a random key
                $key = $nom . date('Y-m-d H:i:s') . $tousLesSql;
                $keycrypt = md5($key);

                $target_file = $target_dir . $keycrypt . $typeFile;
                $path = "http://eeventoo.com/webapp/$target_file";



                if (copy($target_file_name, $target_file)) {
                    $sql2 = "call evenementQuotidien('$idUser',
                '$path',
                '$nom',
                '$type',
                '$adresse',
                '$adresseComplet',
                '$codepostal','$ville','$departement','$region','$paysNom','$paysCode','$latitude','$lontitude','$datedeb','$datefin','$telephone','$typeTarif','$description','$boolTypeTarif','$keycryptGroupeEvent','$tousLesSql')";

                    $result = $dbh->query($sql2);
                    $i = $i + $tousLes;
                    $tousLesSql = $tousLesSql + $tousLes;
                } else {
                    $dbh->rollBack();
                    $success = false;
                    $message = "Quelques erreurs se sont produites Essayer encore ...";
                }


            }

            $key = $nom . date('Y-m-d H:i:s') . $tousLesSql;
            $keycrypt = md5($key);

            $target_file = $target_dir . $keycrypt . $typeFile;
            $path = "http://eeventoo.com/webapp/$target_file";

            $sqlOneEvent="call addOneEvent('$idUser',
                '$path',
                '$nom',
                '$type',
                '$adresse',
                '$adresseComplet',
                '$codepostal',
                '$ville',
                '$departement',
                '$region','$paysNom','$paysCode','$latitude','$lontitude','$datedeb','$datefin','$telephone','$typeTarif','$description','$keycryptGroupeEvent','$boolTypeTarif')";

            if (copy($target_file_name, $target_file)) {
                if ($res = $dbh->query($sqlOneEvent)) {
                    $boolCheckEventRepetition = true;
                    $success = true;
                    $message = "Votre évènement a été ajouté avec succès";
                } else {
                    $boolCheckEventRepetition = false;
                    $success = false;
                    $message = "Une erreur s'est produite";
                }
            }
            else{
                $dbh->rollBack();
                $boolCheckEventRepetition = false;
                $success=false;
                $message= "Quelques erreurs se sont produites Essayer encore ...";
            }
        }


        //event hebdo
        if ($typeEventRepetition == 3) {
            $tousLes = $_POST["intTousLes"];
            $nbJoursRestant = $_POST["nbjoursRestant"];
            $z=7; //les 7 jours de la semaine
            $y = $nbJoursRestant /$z ;
            $tousLesSql = $tousLes;
            $jourLundi = $_POST["jourLundi"];
            $jourMardi = $_POST["jourMardi"];
            $jourMercredi = $_POST["jourMercredi"];
            $jourJeudi = $_POST["jourJeudi"];
            $jourVendredi = $_POST["jourVendredi"];
            $jourSamedi = $_POST["jourSamedi"];
            $jourDimanche = $_POST["jourDimanche"];
            $intervalleDateDebFin = $_POST["nbjourDiffDebFin"];

            $boolJourLundi = false;
            $boolJourMardi =false;
            $boolJourMercredi=false;
            $boolJourJeudi =false;
            $boolJourVendredi=false;
            $boolJourSamedi =false;
            $boolJourDimanche=false;

            if ($jourLundi != 0) {
                $i = 0;
                $tousLesSql = $tousLes;

                while ($i < $y) {
                    //create a random key
                    $rand =random_int ( 0 , 10000000 );
                    $key = $nom . date('Y-m-d H:i:s') . $tousLesSql.$rand;
                    $keycrypt = md5($key);

                    $target_file = $target_dir . $keycrypt . $typeFile;
                    $path = "http://eeventoo.com/webapp/$target_file";


                    if (copy($target_file_name, $target_file)) {
                        if($i==0){
                            $sql3 = "call addEventIntervalle('$idUser',
                '$path',
                '$nom',
                '$type',
                '$adresse',
                '$adresseComplet',
                '$codepostal',
                '$ville',
                '$departement',
                '$region',
                '$paysNom',
                '$paysCode',
                '$latitude',
                '$lontitude',
                '$jourLundi',
                '$datefin',
                '$telephone',
                '$typeTarif ',
                '$description',
                '$boolTypeTarif',
                '$keycryptGroupeEvent',
                '$intervalleDateDebFin')";

                            $result = $dbh->query($sql3);
                            $i = $i + $tousLes;
                        }
                        else{
                            $sql2 = "call evenementHebdomadaire('$idUser',
                '$path',
                '$nom',
                '$type',
                '$adresse',
                '$adresseComplet',
                '$codepostal',
                '$ville',
                '$departement',
                '$region',
                '$paysNom',
                '$paysCode',
                '$latitude',
                '$lontitude',
                '$jourLundi',
                '$datefin',
                '$telephone',
                '$typeTarif ',
                '$description',
                '$boolTypeTarif',
                '$keycryptGroupeEvent',
                '$intervalleDateDebFin',
                '$tousLesSql')";

                            $result = $dbh->query($sql2);
                            $i = $i + $tousLes;
                            $tousLesSql = $tousLesSql + $tousLes;
                        }

                        $boolJourLundi=true;
                    } else {
                        $dbh->rollBack();
                        $success = false;
                        $message = "Quelques erreurs se sont produites Essayer encore ...";
                    }
                }

            }
            else {
                $boolJourLundi=true;

            }

            if ($jourMardi != 0) {
                $i = 0;
                $tousLesSql = $tousLes;

                while ($i < $y) {
                    //create a random key
                    $rand =random_int ( 0 , 10000000 );
                    $key = $nom . date('Y-m-d H:i:s') . $tousLesSql.$rand;
                    $keycrypt = md5($key);

                    $target_file = $target_dir . $keycrypt . $typeFile;
                    $path = "http://eeventoo.com/webapp/$target_file";


                    if (copy($target_file_name, $target_file)) {
                        if($i==0){
                            $sql3 = "call addEventIntervalle('$idUser',
                '$path',
                '$nom',
                '$type',
                '$adresse',
                '$adresseComplet',
                '$codepostal',
                '$ville',
                '$departement',
                '$region',
                '$paysNom',
                '$paysCode',
                '$latitude',
                '$lontitude',
                '$jourMardi',
                '$datefin',
                '$telephone',
                '$typeTarif ',
                '$description',
                '$boolTypeTarif',
                '$keycryptGroupeEvent',
                '$intervalleDateDebFin')";
                            $result = $dbh->query($sql3);
                            $i = $i + $tousLes;
                        }
                        else{
                            $sql2 = "call evenementHebdomadaire('$idUser',
                '$path',
                '$nom',
                '$type',
                '$adresse',
                '$adresseComplet',
                '$codepostal',
                '$ville',
                '$departement',
                '$region',
                '$paysNom',
                '$paysCode',
                '$latitude',
                '$lontitude',
                '$jourMardi',
                '$datefin',
                '$telephone',
                '$typeTarif ',
                '$description',
                '$boolTypeTarif',
                '$keycryptGroupeEvent',
                '$intervalleDateDebFin',
                '$tousLesSql')";
                            $result = $dbh->query($sql2);
                            $i = $i + $tousLes;
                            $tousLesSql = $tousLesSql + $tousLes;
                        }

                        $boolJourMardii=true;
                    } else {
                        $dbh->rollBack();
                        $success = false;
                        $message = "Quelques erreurs se sont produites Essayer encore ...";
                    }
                }

            }
            else {
                $boolJourMardi=true;

            }

            if ($jourMercredi != 0) {
                $i = 0;
                $tousLesSql = $tousLes;

                while ($i < $y) {
                    //create a random key
                    $rand =random_int ( 0 , 10000000 );
                    $key = $nom . date('Y-m-d H:i:s') . $tousLesSql.$rand;
                    $keycrypt = md5($key);

                    $target_file = $target_dir . $keycrypt . $typeFile;
                    $path = "http://eeventoo.com/webapp/$target_file";


                    if (copy($target_file_name, $target_file)) {
                        if($i==0){
                            $sql3 = "call addEventIntervalle('$idUser',
                '$path',
                '$nom',
                '$type',
                '$adresse',
                '$adresseComplet',
                '$codepostal',
                '$ville',
                '$departement',
                '$region',
                '$paysNom',
                '$paysCode',
                '$latitude',
                '$lontitude',
                '$jourMercredi',
                '$datefin',
                '$telephone',
                '$typeTarif ',
                '$description',
                '$boolTypeTarif',
                '$keycryptGroupeEvent',
                '$intervalleDateDebFin')";
                            $result = $dbh->query($sql3);
                            $i = $i + $tousLes;
                        }
                        else{
                            $sql2 = "call evenementHebdomadaire('$idUser',
                '$path',
                '$nom',
                '$type',
                '$adresse',
                '$adresseComplet',
                '$codepostal',
                '$ville',
                '$departement',
                '$region',
                '$paysNom',
                '$paysCode',
                '$latitude',
                '$lontitude',
                '$jourMercredi',
                '$datefin',
                '$telephone',
                '$typeTarif ',
                '$description',
                '$boolTypeTarif',
                '$keycryptGroupeEvent',
                '$intervalleDateDebFin',
                '$tousLesSql')";
                            $result = $dbh->query($sql2);
                            $i = $i + $tousLes;
                            $tousLesSql = $tousLesSql + $tousLes;
                        }

                        $boolJourMercredi=true;
                    } else {
                        $dbh->rollBack();
                        $success = false;
                        $message = "Quelques erreurs se sont produites Essayer encore ...";
                    }
                }

            }
            else {
                $boolJourMercredi=true;

            }


            if ($jourJeudi != 0) {
                $i = 0;
                $tousLesSql = $tousLes;

                while ($i < $y) {
                    //create a random key
                    $rand =random_int ( 0 , 10000000 );
                    $key = $nom . date('Y-m-d H:i:s') . $tousLesSql.$rand;
                    $keycrypt = md5($key);

                    $target_file = $target_dir . $keycrypt . $typeFile;
                    $path = "http://eeventoo.com/webapp/$target_file";


                    if (copy($target_file_name, $target_file)) {
                        if($i==0){
                            $sql3 = "call addEventIntervalle('$idUser',
                '$path',
                '$nom',
                '$type',
                '$adresse',
                '$adresseComplet',
                '$codepostal',
                '$ville',
                '$departement',
                '$region',
                '$paysNom',
                '$paysCode',
                '$latitude',
                '$lontitude',
                '$jourJeudi',
                '$datefin',
                '$telephone',
                '$typeTarif ',
                '$description',
                '$boolTypeTarif',
                '$keycryptGroupeEvent',
                '$intervalleDateDebFin')";
                            $result = $dbh->query($sql3);
                            $i = $i + $tousLes;
                        }
                        else{
                            $sql2 = "call evenementHebdomadaire('$idUser',
                '$path',
                '$nom',
                '$type',
                '$adresse',
                '$adresseComplet',
                '$codepostal',
                '$ville',
                '$departement',
                '$region',
                '$paysNom',
                '$paysCode',
                '$latitude',
                '$lontitude',
                '$jourJeudi',
                '$datefin',
                '$telephone',
                '$typeTarif ',
                '$description',
                '$boolTypeTarif',
                '$keycryptGroupeEvent',
                '$intervalleDateDebFin',
                '$tousLesSql')";
                            $result = $dbh->query($sql2);
                            $i = $i + $tousLes;
                            $tousLesSql = $tousLesSql + $tousLes;
                        }

                        $boolJourJeudi=true;
                    } else {
                        $dbh->rollBack();
                        $success = false;
                        $message = "Quelques erreurs se sont produites Essayer encore ...";
                    }
                }

            }
            else {
                $boolJourJeudi=true;

            }

            if ($jourVendredi != 0) {
                $i = 0;
                $tousLesSql = $tousLes;

                while ($i < $y) {
                    //create a random key
                    $rand =random_int ( 0 , 10000000 );
                    $key = $nom . date('Y-m-d H:i:s') . $tousLesSql.$rand;
                    $keycrypt = md5($key);

                    $target_file = $target_dir . $keycrypt . $typeFile;
                    $path = "http://eeventoo.com/webapp/$target_file";


                    if (copy($target_file_name, $target_file)) {
                        if($i==0){
                            $sql3 = "call addEventIntervalle('$idUser',
                '$path',
                '$nom',
                '$type',
                '$adresse',
                '$adresseComplet',
                '$codepostal',
                '$ville',
                '$departement',
                '$region',
                '$paysNom',
                '$paysCode',
                '$latitude',
                '$lontitude',
                '$jourVendredi',
                '$datefin',
                '$telephone',
                '$typeTarif ',
                '$description',
                '$boolTypeTarif',
                '$keycryptGroupeEvent',
                '$intervalleDateDebFin')";
                            $result = $dbh->query($sql3);
                            $i = $i + $tousLes;
                        }
                        else{
                            $sql2 = "call evenementHebdomadaire('$idUser',
                '$path',
                '$nom',
                '$type',
                '$adresse',
                '$adresseComplet',
                '$codepostal',
                '$ville',
                '$departement',
                '$region',
                '$paysNom',
                '$paysCode',
                '$latitude',
                '$lontitude',
                '$jourVendredi',
                '$datefin',
                '$telephone',
                '$typeTarif ',
                '$description',
                '$boolTypeTarif',
                '$keycryptGroupeEvent',
                '$intervalleDateDebFin',
                '$tousLesSql')";
                            $result = $dbh->query($sql2);
                            $i = $i + $tousLes;
                            $tousLesSql = $tousLesSql + $tousLes;
                        }

                        $boolJourVendredi=true;
                    } else {
                        $dbh->rollBack();
                        $success = false;
                        $message = "Quelques erreurs se sont produites Essayer encore ...";
                    }
                }

            }
            else {
                $boolJourVendredi=true;

            }

            if ($jourSamedi != 0) {
                $i = 0;
                $tousLesSql = $tousLes;

                while ($i < $y) {
                    //create a random key
                    $rand =random_int ( 0 , 10000000 );
                    $key = $nom . date('Y-m-d H:i:s') . $tousLesSql.$rand;
                    $keycrypt = md5($key);

                    $target_file = $target_dir . $keycrypt . $typeFile;
                    $path = "http://eeventoo.com/webapp/$target_file";


                    if (copy($target_file_name, $target_file)) {
                        if($i==0){
                            $sql3 = "call addEventIntervalle('$idUser',
                '$path',
                '$nom',
                '$type',
                '$adresse',
                '$adresseComplet',
                '$codepostal',
                '$ville',
                '$departement',
                '$region',
                '$paysNom',
                '$paysCode',
                '$latitude',
                '$lontitude',
                '$jourSamedi',
                '$datefin',
                '$telephone',
                '$typeTarif ',
                '$description',
                '$boolTypeTarif',
                '$keycryptGroupeEvent',
                '$intervalleDateDebFin')";
                            $result = $dbh->query($sql3);
                            $i = $i + $tousLes;
                        }
                        else{
                            $sql2 = "call evenementHebdomadaire('$idUser',
                '$path',
                '$nom',
                '$type',
                '$adresse',
                '$adresseComplet',
                '$codepostal',
                '$ville',
                '$departement',
                '$region',
                '$paysNom',
                '$paysCode',
                '$latitude',
                '$lontitude',
                '$jourSamedi',
                '$datefin',
                '$telephone',
                '$typeTarif ',
                '$description',
                '$boolTypeTarif',
                '$keycryptGroupeEvent',
                '$intervalleDateDebFin',
                '$tousLesSql')";
                            $result = $dbh->query($sql2);
                            $i = $i + $tousLes;
                            $tousLesSql = $tousLesSql + $tousLes;
                        }

                        $boolJourSamedi=true;
                    } else {
                        $dbh->rollBack();
                        $success = false;
                        $message = "Quelques erreurs se sont produites Essayer encore ...";
                    }
                }

            }
            else {
                $boolJourSamedi=true;

            }

            if ($jourDimanche != 0) {
                $i = 0;
                $tousLesSql = $tousLes;

                while ($i < $y) {
                    //create a random key
                    $rand =random_int ( 0 , 10000000 );
                    $key = $nom . date('Y-m-d H:i:s') . $tousLesSql.$rand;
                    $keycrypt = md5($key);

                    $target_file = $target_dir . $keycrypt . $typeFile;
                    $path = "http://eeventoo.com/webapp/$target_file";


                    if (copy($target_file_name, $target_file)) {
                        if($i==0){
                            $sql3 = "call addEventIntervalle('$idUser',
                '$path',
                '$nom',
                '$type',
                '$adresse',
                '$adresseComplet',
                '$codepostal',
                '$ville',
                '$departement',
                '$region',
                '$paysNom',
                '$paysCode',
                '$latitude',
                '$lontitude',
                '$jourDimanche',
                '$datefin',
                '$telephone',
                '$typeTarif ',
                '$description',
                '$boolTypeTarif',
                '$keycryptGroupeEvent',
                '$intervalleDateDebFin')";
                            $result = $dbh->query($sql3);
                            $i = $i + $tousLes;
                        }
                        else{
                            $sql2 = "call evenementHebdomadaire('$idUser',
                '$path',
                '$nom',
                '$type',
                '$adresse',
                '$adresseComplet',
                '$codepostal',
                '$ville',
                '$departement',
                '$region',
                '$paysNom',
                '$paysCode',
                '$latitude',
                '$lontitude',
                '$jourDimanche',
                '$datefin',
                '$telephone',
                '$typeTarif ',
                '$description',
                '$boolTypeTarif',
                '$keycryptGroupeEvent',
                '$intervalleDateDebFin',
                '$tousLesSql')";
                            $result = $dbh->query($sql2);
                            $i = $i + $tousLes;
                            $tousLesSql = $tousLesSql + $tousLes ;
                        }

                        $boolJourDimanche=true;
                    } else {
                        $dbh->rollBack();
                        $success = false;
                        $message = "Quelques erreurs se sont produites. Essayer encore ...";
                    }
                }

            }
            else {
                $boolJourDimanche=true;

            }



            if($boolJourLundi && $boolJourMardi && $boolJourMercredi && $boolJourJeudi && $boolJourVendredi && $boolJourSamedi && $boolJourDimanche) {

                $key = $nom . date('Y-m-d H:i:s') . $tousLesSql;
                $keycrypt = md5($key);

                $target_file = $target_dir . $keycrypt . $typeFile;
                $path = "http://eeventoo.com/webapp/$target_file";

                $sqlOneEvent="call addOneEvent('$idUser',
                '$path',
                '$nom',
                '$type',
                '$adresse',
                '$adresseComplet',
                '$codepostal',
                '$ville',
                '$departement',
                '$region','$paysNom','$paysCode','$latitude','$lontitude','$datedeb','$datefin','$telephone','$typeTarif','$description','$keycryptGroupeEvent','$boolTypeTarif')";

                if (copy($target_file_name, $target_file)) {
                    if ($res = $dbh->query($sqlOneEvent)) {
                        $boolCheckEventRepetition = true;
                        $success = true;
                        $message = "Votre évènement a été ajouté avec succès ";
                    } else {
                        $boolCheckEventRepetition = false;
                        $success = false;
                        $message = "Une erreur s'est produite";
                    }
                }
                else{
                    $dbh->rollBack();
                    $boolCheckEventRepetition = false;
                    $success=false;
                    $message= "Quelques erreurs se sont produites Essayer encore ...";
                }
            }
            else
            {
                $success = false;
                $message = "Des erreurs se sont produites, veuillez essayer à nouveau ";
            }

        }


        //event mensuel

        if($typeEventRepetition == 4){
            $intTypeEventMensuel = $_POST["intTypeEventMensuel"];
            $tousLes = $_POST["intTousLes"];
            $nbJoursRestant = $_POST["nbjoursRestant"];
            $datejusqua=$_POST["datejusqua"];
            $queryNbMoisDiff= " call getDiffMonth('$datedeb','$datejusqua')";
            $result = $dbh->query($queryNbMoisDiff);
            $intNbMois=0;

            if($result){
                while ($donnees = $result->fetch()) {
                    $intNbMois =$donnees['datediffmois'];
                }
            }

            if ($intTypeEventMensuel==1){
                $tousLesSql = $tousLes;
                $i=0;
                $y=$intNbMois;
                while ($i <= $y) {
                    //create a random key
                    $key = $nom . date('Y-m-d H:i:s') . $tousLesSql;
                    $keycrypt = md5($key);

                    $target_file = $target_dir . $keycrypt . $typeFile;
                    $path = "http://eeventoo.com/webapp/$target_file";

                    if (copy($target_file_name, $target_file)) {
                        $sql2 = "call evenementMensuelXjoursMois('$idUser',
                '$path',
                '$nom',
                '$type',
                '$adresse',
                '$adresseComplet',
                '$codepostal',
                '$ville',
                '$departement',
                '$region',
                '$paysNom',
                '$paysCode',
                '$latitude',
                '$lontitude',
                '$datedeb',
                '$datefin',
                '$telephone',
                '$typeTarif ',
                '$description',
                '$boolTypeTarif',
                '$keycryptGroupeEvent',
                '$tousLesSql')";

                        $result = $dbh->query($sql2);
                        $i = $i + $tousLes;
                        $tousLesSql = $tousLesSql + $tousLes;
                    } else {
                        $dbh->rollBack();
                        $success = false;
                        $message = "Quelques erreurs se sont produites Essayer encore ...";
                    }
                }

                $key = $nom . date('Y-m-d H:i:s') . $tousLesSql;
                $keycrypt = md5($key);

                $target_file = $target_dir . $keycrypt . $typeFile;
                $path = "http://eeventoo.com/webapp/$target_file";

                $sqlOneEvent="call addOneEvent('$idUser',
                '$path',
                '$nom',
                '$type',
                '$adresse',
                '$adresseComplet',
                '$codepostal',
                '$ville',
                '$departement',
                '$region','$paysNom','$paysCode','$latitude','$lontitude','$datedeb','$datefin','$telephone','$typeTarif','$description','$keycryptGroupeEvent','$boolTypeTarif')";

                if (copy($target_file_name, $target_file)) {
                    if ($res = $dbh->query($sqlOneEvent)) {
                        $boolCheckEventRepetition = true;
                        $success = true;
                        $message = "Votre évènement a été ajouté avec succès $sql2";
                    } else {
                        $boolCheckEventRepetition = false;
                        $success = false;
                        $message = "Une erreur s'est produite $sql2";
                    }
                }
                else{
                    $dbh->rollBack();
                    $boolCheckEventRepetition = false;
                    $success=false;
                    $message= "Quelques erreurs se sont produites Essayer encore ...";
                }

            }

            if ($intTypeEventMensuel==2){
                $intervalleDateDebFin = $_POST["nbjourDiffDebFin"];
                $nieme=$_POST["niemeJour"];
                $jourSemaine=$_POST["jourSemaine"];
                $tousLesSql = $tousLes;
                $i=0;
                $y=$intNbMois;
                $str_res="";
                while ($i <= $y) {
                    //create a random key
                    $key = $nom . date('Y-m-d H:i:s') . $tousLesSql;
                    $keycrypt = md5($key);

                    $target_file = $target_dir . $keycrypt . $typeFile;
                    $path = "http://eeventoo.com/webapp/$target_file";


                    if (copy($target_file_name, $target_file)) {
                        $sql2 = "call evenementMensuelXemejoursMois('$idUser',
                '$path',
                '$nom',
                '$type',
                '$adresse',
                '$adresseComplet',
                '$codepostal',
                '$ville',
                '$departement',
                '$region',
                '$paysNom',
                '$paysCode',
                '$latitude',
                '$lontitude',
                '$datedeb',
                '$datefin',
                '$telephone',
                '$typeTarif ',
                '$description',
                '$boolTypeTarif',
                '$keycryptGroupeEvent',
                '$tousLesSql',
                '$nieme',
                '$jourSemaine',
                '$intervalleDateDebFin')";

                        $result = $dbh->query($sql2);
                        $i = $i + $tousLes;
                        if($i = 0){ $str_res= "tous les sql = "+ $tousLesSql + $sql2;}
                        $tousLesSql = $tousLesSql + $tousLes;
                    } else {
                        $dbh->rollBack();
                        $success = false;
                        $message = "Quelques erreurs se sont produites Essayer encore ... $sql2";
                    }
                }




                $key = $nom . date('Y-m-d H:i:s') . $tousLesSql;
                $keycrypt = md5($key);

                $target_file = $target_dir . $keycrypt . $typeFile;
                $path = "http://eeventoo.com/webapp/$target_file";

                $sqlOneEvent="call addOneEvent('$idUser',
                '$path',
                '$nom',
                '$type',
                '$adresse',
                '$adresseComplet',
                '$codepostal',
                '$ville',
                '$departement',
                '$region','$paysNom','$paysCode','$latitude','$lontitude','$datedeb','$datefin','$telephone','$typeTarif','$description','$keycryptGroupeEvent','$boolTypeTarif')";

                if (copy($target_file_name, $target_file)) {
                    if ($res = $dbh->query($sqlOneEvent)) {
                        $boolCheckEventRepetition = true;
                        $success = true;
                        $message = "Votre évènement a été ajouté avec succès  $sql2 $str_res";
                    } else {
                        $boolCheckEventRepetition = false;
                        $success = false;
                        $message = "Une erreur s'est produite";
                    }
                }
                else{
                    $dbh->rollBack();
                    $boolCheckEventRepetition = false;
                    $success=false;
                    $message= "Quelques erreurs se sont produites Essayer encore ...$sql2";
                }

            }

        }

    }
    else{

        $success=false;
        $message= "Erreur upload file temp";
    }














/*
    if ($typeEventRepetition == 5) {

        $tousLes = $_POST["intTousLes"];
        $nbJoursRestant = $_POST["nbjoursRestant"];
        $y = $nbJoursRestant / $tousLes;
        $tousLesSql = $tousLes;
        $i = 0;
        while ($i <= $y) {
            //create a random key
            $key = $nom . date('Y-m-d H:i:s') . $tousLesSql;
            $keycrypt = md5($key);

            $target_file = $target_dir . $keycrypt . $typeFile;
            $path = "http://eeventoo.com/webapp/$target_file";


            if (copy($target_file_name, $target_file)) {
                $sql2 = "call evenementAnnuel('$idUser',
                '$path',
                '$nom',
                '$type',
                '$adresse',
                '$adresseComplet',
                '$codepostal'
                ,'$ville',
                '$departement',
                '$region',
                '$paysNom',
                '$paysCode',
                '$latitude',
                '$lontitude',
                '$datedeb',
                '$datefin',
                '$telephone',
                '$typeTarif ',
                '$description',
                '$boolTypeTarif',
                '$keycryptGroupeEvent',
                '$tousLesSql')";

                $result = $dbh->query($sql2);
                $i = $i + $tousLes;
                $tousLesSql = $tousLesSql + $tousLes;
            } else {
                $dbh->rollBack();
                $success = false;
                $message = "Quelques erreurs se sont produites Essayer encore ...";
            }
        }
        if (move_uploaded_file($fichier,$target_file_name)) {
            if ($res = $dbh->query($sqlOneEvent)) {
                $boolCheckEventRepetition = true;
                $success = true;
                $message = "Votre évènement a été ajouté avec succès";
            } else {
                $boolCheckEventRepetition = false;
                $success = false;
                $message = "Une erreur s'est produite";
            }
        }
        else{
            $dbh->rollBack();
            $boolCheckEventRepetition = false;
            $success=false;
            $message= "Quelques erreurs se sont produites Essayer encore ...";
        }



    }


*/


}

else {
    $success=false;
    $message= "Element du post maquant";
}

$response["success"]=$success;
$response["message"]=$message ;
$response["idGroupEvent"]=$keycryptGroupeEvent;
echo json_encode($response);


?>