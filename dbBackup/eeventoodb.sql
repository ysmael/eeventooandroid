-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 05, 2019 at 10:33 AM
-- Server version: 10.1.38-MariaDB-0+deb9u1
-- PHP Version: 7.0.33-0+deb9u3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eeventoodb`
--
CREATE DATABASE IF NOT EXISTS `eeventoodb` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;
USE `eeventoodb`;

DELIMITER $$
--
-- Procedures
--
DROP PROCEDURE IF EXISTS `addEventIntervalle`$$
CREATE DEFINER=`ismael`@`%` PROCEDURE `addEventIntervalle` (IN `idUser` INT, IN `actualpath` VARCHAR(255), IN `nom` VARCHAR(255), IN `type` VARCHAR(255), IN `adresse` VARCHAR(255), IN `adresseComplet` VARCHAR(255), IN `codepostal` VARCHAR(5), IN `ville` VARCHAR(255), IN `departement` VARCHAR(255), IN `region` VARCHAR(255), IN `paysNom` VARCHAR(255), IN `paysCode` VARCHAR(255), IN `latitude` DOUBLE, IN `lontitude` DOUBLE, IN `datedeb` DATETIME, IN `datefin` DATETIME, IN `telephone` VARCHAR(255), IN `typeTarif` VARCHAR(255), IN `description` LONGTEXT, IN `active` BOOLEAN, IN `idGroupEvent` VARCHAR(255), IN `intervalleDateDebFin` INT(20))  NO SQL
BEGIN

set @time =  TIME(datefin);

set @dateOfdateDeb = DATE(datedeb);

set @str_datefin = concat (@dateOfdateDeb," ",@time);

set @dateFinFinal = concat("STR_TO_DATE('",@str_datefin,"', '%Y-%m-%d %H:%i:%s') + INTERVAL ",intervalleDateDebFin," DAY");

set @requete = concat("Insert into evenement_tb ( `idUser`, `image`, `nom`, `type`, `adresse`, `adresseComplet`, `codepostal`, `ville`, `departement`, `region`, `paysNom`, `paysCode`, `latitude`, `lontitude`, `datedebut`, `datefin`, `telephone`, `typeTarif`, `description`,`active`, `idGroupEvent`) values (",idUser,",'",actualpath,"','",nom,"','",type,"','",adresse,"','",adresseComplet,"','",codepostal,"','",ville,"','",departement,"','",region,"','",paysNom,"','",paysCode,"','",latitude,"','",lontitude,"','",datedeb,"',",@dateFinFinal,",'",telephone,"','",typeTarif,"','",description,"','",active,"','",idGroupEvent,"');");

PREPARE test FROM @requete;

EXECUTE test;







END$$

DROP PROCEDURE IF EXISTS `addOneEvent`$$
CREATE DEFINER=`ismael`@`%` PROCEDURE `addOneEvent` (IN `idUser` INT, IN `actualpath` VARCHAR(255), IN `nom` VARCHAR(255), IN `type` VARCHAR(255), IN `adresse` VARCHAR(255), IN `adresseComplet` VARCHAR(255), IN `codepostal` VARCHAR(5), IN `ville` VARCHAR(255), IN `departement` VARCHAR(255), IN `region` VARCHAR(255), IN `paysNom` VARCHAR(255), IN `paysCode` VARCHAR(255), IN `latitude` DOUBLE, IN `lontitude` DOUBLE, IN `datedeb` DATETIME, IN `datefin` DATETIME, IN `telephone` VARCHAR(255), IN `typeTarif` VARCHAR(255), IN `description` LONGTEXT, IN `idGroupEvent` VARCHAR(255), IN `active` BOOLEAN)  NO SQL
BEGIN





set @requete = concat("Insert into evenement_tb ( `idUser`, `image`, `nom`, `type`, `adresse`, `adresseComplet`, `codepostal`, `ville`, `departement`, `region`, `paysNom`, `paysCode`, `latitude`, `lontitude`, `datedebut`, `datefin`, `telephone`, `typeTarif`,  `description`, `idGroupEvent`, `active`) values (",idUser,",'",actualpath,"','",nom,"','",type,"','",adresse,"','",adresseComplet,"','",codepostal,"','",ville,"','",departement,"','",region,"','",paysNom,"','",paysCode,"','",latitude,"','",lontitude,"','",datedeb,"','",datefin,"','",telephone,"','",typeTarif,"','",description,"','",idGroupEvent,"',",active,");");





PREPARE test FROM @requete;

EXECUTE test;







END$$

DROP PROCEDURE IF EXISTS `deleteEventUser`$$
CREATE DEFINER=`ismael`@`%` PROCEDURE `deleteEventUser` (IN `p_idUser` INT(10), IN `p_idEvent` INT(10))  NO SQL
DELETE FROM `evenement_tb` WHERE id=p_idEvent AND idUser=p_idUser$$

DROP PROCEDURE IF EXISTS `evenementAnnuel`$$
CREATE DEFINER=`ismael`@`%` PROCEDURE `evenementAnnuel` (IN `idUser` INT, IN `actualpath` VARCHAR(255), IN `nom` VARCHAR(255), IN `type` VARCHAR(255), IN `adresse` VARCHAR(255), IN `adresseComplet` VARCHAR(255), IN `codepostal` VARCHAR(5), IN `ville` VARCHAR(255), IN `departement` VARCHAR(255), IN `region` VARCHAR(255), IN `paysNom` VARCHAR(255), IN `paysCode` VARCHAR(255), IN `latitude` DOUBLE, IN `lontitude` DOUBLE, IN `datedeb` DATETIME, IN `datefin` DATETIME, IN `telephone` VARCHAR(255), IN `typeTarif` VARCHAR(255), IN `description` LONGTEXT, IN `active` BOOLEAN, IN `idGroupEvent` VARCHAR(255), IN `tousLes` INT(20))  NO SQL
BEGIN



set @dateDebFinal = concat("STR_TO_DATE('",datedeb,"' , '%Y-%m-%d %H:%i:%s') + INTERVAL ",tousLes," YEAR");

set @dateFinFinal = concat("STR_TO_DATE('",datefin,"', '%Y-%m-%d %H:%i:%s') + INTERVAL ",tousLes," YEAR");

set @requete = concat("Insert into evenement_tb ( `idUser`, `image`, `nom`, `type`, `adresse`, `adresseComplet`, `codepostal`, `ville`, `departement`, `region`, `paysNom`, `paysCode`, `latitude`, `lontitude`, `datedebut`, `datefin`, `telephone`, `typeTarif`,  `description`, `idGroupEvent`, `active`) values (",idUser,",'",actualpath,"','",nom,"','",type,"','",adresse,"','",adresseComplet,"','",codepostal,"','",ville,"','",departement,"','",region,"','",paysNom,"','",paysCode,"','",latitude,"','",lontitude,"',",@dateDebFinal,",",@dateFinFinal,",'",telephone,"','",typeTarif,"','",description,"','",idGroupEvent,"','",active,"');");

PREPARE test FROM @requete;

EXECUTE test;







END$$

DROP PROCEDURE IF EXISTS `evenementHebdomadaire`$$
CREATE DEFINER=`ismael`@`%` PROCEDURE `evenementHebdomadaire` (IN `idUser` INT, IN `actualpath` VARCHAR(255), IN `nom` VARCHAR(255), IN `type` VARCHAR(255), IN `adresse` VARCHAR(255), IN `adresseComplet` VARCHAR(255), IN `codepostal` VARCHAR(5), IN `ville` VARCHAR(255), IN `departement` VARCHAR(255), IN `region` VARCHAR(255), IN `paysNom` VARCHAR(255), IN `paysCode` VARCHAR(255), IN `latitude` DOUBLE, IN `lontitude` DOUBLE, IN `datedeb` DATETIME, IN `datefin` DATETIME, IN `telephone` VARCHAR(255), IN `typeTarif` VARCHAR(255), IN `description` LONGTEXT, IN `active` BOOLEAN, IN `idGroupEvent` VARCHAR(255), IN `intervalleDateDebFin` INT(20), IN `tousLes` INT(20))  NO SQL
BEGIN

set @time =  TIME(datefin);

set @dateOfdateDeb = DATE(datedeb);

set @str_datefin = concat (@dateOfdateDeb," ",@time);

set @dateFinFinal = concat("STR_TO_DATE('",@str_datefin,"' , '%Y-%m-%d %H:%i:%s') + INTERVAL ",tousLes," WEEK");

set @dateDebFinal = concat("STR_TO_DATE('",datedeb,"' , '%Y-%m-%d %H:%i:%s') + INTERVAL ",tousLes," WEEK");

set @dateFinFinal = concat("STR_TO_DATE((",@dateFinFinal,"), '%Y-%m-%d %H:%i:%s') + INTERVAL ",intervalleDateDebFin," DAY");

set @requete = concat("Insert into evenement_tb ( `idUser`, `image`, `nom`, `type`, `adresse`, `adresseComplet`, `codepostal`, `ville`, `departement`, `region`, `paysNom`, `paysCode`, `latitude`, `lontitude`, `datedebut`, `datefin`, `telephone`, `typeTarif`, `description`,`active`, `idGroupEvent`) values (",idUser,",'",actualpath,"','",nom,"','",type,"','",adresse,"','",adresseComplet,"','",codepostal,"','",ville,"','",departement,"','",region,"','",paysNom,"','",paysCode,"','",latitude,"','",lontitude,"',",@dateDebFinal,",",@dateFinFinal,",'",telephone,"','",typeTarif,"','",description,"','",active,"','",idGroupEvent,"');");

PREPARE test FROM @requete;

EXECUTE test;









END$$

DROP PROCEDURE IF EXISTS `evenementMensuelXemejoursMois`$$
CREATE DEFINER=`ismael`@`%` PROCEDURE `evenementMensuelXemejoursMois` (IN `idUser` INT, IN `actualpath` VARCHAR(255), IN `nom` VARCHAR(255), IN `type` VARCHAR(255), IN `adresse` VARCHAR(255), IN `adresseComplet` VARCHAR(255), IN `codepostal` VARCHAR(5), IN `ville` VARCHAR(255), IN `departement` VARCHAR(255), IN `region` VARCHAR(255), IN `paysNom` VARCHAR(255), IN `paysCode` VARCHAR(255), IN `latitude` DOUBLE, IN `lontitude` DOUBLE, IN `datedeb` DATETIME, IN `datefin` DATETIME, IN `telephone` VARCHAR(255), IN `typeTarif` VARCHAR(255), IN `description` LONGTEXT, IN `active` BOOLEAN, IN `idGroupEvent` VARCHAR(255), IN `tousLes` INT(20), IN `nieme` INT, IN `jourSemaine` INT, IN `intervalleDebFin` INT)  NO SQL
BEGIN

SET @datedebTemp = concat( YEAR(datedeb),"-", MONTH(datedeb),"-15 ",  TIME(datedeb),":", HOUR(datedeb),":", MINUTE(datedeb),"-", SECOND(datedeb));







set @dateDebTempInter = STR_TO_DATE(@datedebTemp , '%Y-%m-%d %H:%i:%s');



set @dateDebTempFinal = @dateDebTempInter + INTERVAL tousLes MONTH;



CALL niemeJoursMois(@dateDebTempFinal,nieme,jourSemaine, @dateDebResult);









set @dateFinTemp = @dateDebResult + INTERVAL intervalleDebFin DAY ;







set @time =  TIME(datefin);

set @dateOfdateFin = DATE(@dateFinTemp);

set @dateFinFinal = concat (@dateOfdateFin," ",@time);

set @requete = concat("Insert into evenement_tb ( `idUser`, `image`, `nom`, `type`, `adresse`, `adresseComplet`, `codepostal`, `ville`, `departement`, `region`, `paysNom`, `paysCode`, `latitude`, `lontitude`, `datedebut`, `datefin`, `telephone`, `typeTarif`, `description`, `idGroupEvent`, `active`) values (",idUser,",'",actualpath,"','",nom,"','",type,"','",adresse,"','",adresseComplet,"','",codepostal,"','",ville,"','",departement,"','",region,"','",paysNom,"','",paysCode,"','",latitude,"','",lontitude,"','",@dateDebResult,"','",@dateFinFinal,"','",telephone,"','",typeTarif,"','",description,"','",idGroupEvent,"','",active,"');");



PREPARE test FROM @requete;

EXECUTE test;







END$$

DROP PROCEDURE IF EXISTS `evenementMensuelXjoursMois`$$
CREATE DEFINER=`ismael`@`%` PROCEDURE `evenementMensuelXjoursMois` (IN `idUser` INT, IN `actualpath` VARCHAR(255), IN `nom` VARCHAR(255), IN `type` VARCHAR(255), IN `adresse` VARCHAR(255), IN `adresseComplet` VARCHAR(255), IN `codepostal` VARCHAR(5), IN `ville` VARCHAR(255), IN `departement` VARCHAR(255), IN `region` VARCHAR(255), IN `paysNom` VARCHAR(255), IN `paysCode` VARCHAR(255), IN `latitude` DOUBLE, IN `lontitude` DOUBLE, IN `datedeb` DATETIME, IN `datefin` DATETIME, IN `telephone` VARCHAR(255), IN `typeTarif` VARCHAR(255), IN `description` LONGTEXT, IN `active` BOOLEAN, IN `idGroupEvent` VARCHAR(255), IN `tousLes` INT(20))  NO SQL
BEGIN



set @dateDebFinal = concat("STR_TO_DATE('",datedeb,"' , '%Y-%m-%d %H:%i:%s') + INTERVAL ",tousLes," MONTH");

set @dateFinFinal = concat("STR_TO_DATE('",datefin,"', '%Y-%m-%d %H:%i:%s') + INTERVAL ",tousLes," MONTH");

set @requete = concat("Insert into evenement_tb ( `idUser`, `image`, `nom`, `type`, `adresse`, `adresseComplet`, `codepostal`, `ville`, `departement`, `region`, `paysNom`, `paysCode`, `latitude`, `lontitude`, `datedebut`, `datefin`, `telephone`, `typeTarif`, `description`, `idGroupEvent`, `active`) values (",idUser,",'",actualpath,"','",nom,"','",type,"','",adresse,"','",adresseComplet,"','",codepostal,"','",ville,"','",departement,"','",region,"','",paysNom,"','",paysCode,"','",latitude,"','",lontitude,"',",@dateDebFinal,",",@dateFinFinal,",'",telephone,"','",typeTarif,"','",description,"','",idGroupEvent,"','",active,"');");

PREPARE test FROM @requete;

EXECUTE test;





END$$

DROP PROCEDURE IF EXISTS `evenementQuotidien`$$
CREATE DEFINER=`ismael`@`%` PROCEDURE `evenementQuotidien` (IN `idUser` INT, IN `actualpath` VARCHAR(255), IN `nom` VARCHAR(255), IN `type` VARCHAR(255), IN `adresse` VARCHAR(255), IN `adresseComplet` VARCHAR(255), IN `codepostal` VARCHAR(5), IN `ville` VARCHAR(255), IN `departement` VARCHAR(255), IN `region` VARCHAR(255), IN `paysNom` VARCHAR(255), IN `paysCode` VARCHAR(255), IN `latitude` DOUBLE, IN `lontitude` DOUBLE, IN `datedeb` DATETIME, IN `datefin` DATETIME, IN `telephone` VARCHAR(255), IN `typeTarif` VARCHAR(255), IN `description` LONGTEXT, IN `active` BOOLEAN, IN `idGroupEvent` VARCHAR(255), IN `tousLes` INT(20))  NO SQL
BEGIN



set @dateDebFinal = concat("STR_TO_DATE('",datedeb,"' , '%Y-%m-%d %H:%i:%s') + INTERVAL ",tousLes," DAY");

set @dateFinFinal = concat("STR_TO_DATE('",datefin,"', '%Y-%m-%d %H:%i:%s') + INTERVAL ",tousLes," DAY");

set @requete = concat("Insert into evenement_tb ( `idUser`, `image`, `nom`, `type`, `adresse`, `adresseComplet`, `codepostal`, `ville`, `departement`, `region`, `paysNom`, `paysCode`, `latitude`, `lontitude`, `datedebut`, `datefin`, `telephone`, `typeTarif`,  `description`,`active`, `idGroupEvent`) values (",idUser,",'",actualpath,"','",nom,"','",type,"','",adresse,"','",adresseComplet,"','",codepostal,"','",ville,"','",departement,"','",region,"','",paysNom,"','",paysCode,"','",latitude,"','",lontitude,"',",@dateDebFinal,",",@dateFinFinal,",'",telephone,"','",typeTarif,"','",description,"','",active,"','",idGroupEvent,"');");

PREPARE test FROM @requete;

EXECUTE test;







END$$

DROP PROCEDURE IF EXISTS `getAllDepartement`$$
CREATE DEFINER=`ismael`@`%` PROCEDURE `getAllDepartement` ()  NO SQL
SELECT * from departements$$

DROP PROCEDURE IF EXISTS `getDiffMonth`$$
CREATE DEFINER=`ismael`@`%` PROCEDURE `getDiffMonth` (IN `startDate` DATETIME, IN `endDate` DATETIME)  NO SQL
BEGIN



set @startdate = DATE(startDate);

set @enddate = DATE(endDate);

set @requete = concat ("SELECT TIMESTAMPDIFF(MONTH,'",@startdate,"','",@enddate,"') as datediffmois");

PREPARE test from @requete;

execute test;

END$$

DROP PROCEDURE IF EXISTS `getEventUser`$$
CREATE DEFINER=`ismael`@`%` PROCEDURE `getEventUser` (IN `p_idUser` INT(100))  NO SQL
BEGIN

set @query= concat("select e.id, e.image,e.nom,e.type,e.adresse,e.codepostal,e.ville,e.adresseComplet,e.paysNom,e.paysCode,e.latitude,e.lontitude,e.datedebut,e.datefin,e.telephone,e.typeTarif,e.description,e.departement,e.region,IFNULL((SELECT COUNT(*)  from favoris f WHERE f.idEvent=e.id GROUP by f.idEvent),0)as nbFavoris, IFNULL((SELECT COUNT(*)from like_tb l WHERE l.idEvent=e.id GROUP by l.idEvent),0)as nbLike,IFNULL((SELECT COUNT(*)from avis_tb a WHERE a.idEvent=e.id GROUP by a.idEvent),0)as nbAvis from evenement_tb e  where e.active=1 and e.idUser = '",p_idUser,"';");

 PREPARE test FROM @query;

 EXECUTE test;

END$$

DROP PROCEDURE IF EXISTS `getFavorisUser`$$
CREATE DEFINER=`ismael`@`%` PROCEDURE `getFavorisUser` (IN `p_idUser` INT(10), IN `p_start` INT(10), IN `p_limit` INT(10))  NO SQL
select f.idEvent,e.image,e.nom,e.type,e.adresse,e.codepostal,e.ville, e.datedebut,e.datefin,e.telephone,e.typeTarif,e.description,e.departement,e.region, IFNULL((SELECT COUNT(*)  from favoris f WHERE f.idEvent=e.id GROUP by f.idEvent),0)as nbFavoris, IFNULL((SELECT COUNT(*)from like_tb l WHERE l.idEvent=e.id GROUP by l.idEvent),0)as nbLike,IFNULL((SELECT COUNT(*)from avis_tb a WHERE a.idEvent=e.id GROUP by a.idEvent),0)as nbAvis from evenement_tb e,favoris f where e.id=f.idEvent and e.datedebut >= CURRENT_DATE() and  f.idUser=p_idUser order by e.datedebut ASC limit p_start, p_limit$$

DROP PROCEDURE IF EXISTS `getFormulesEvent`$$
CREATE DEFINER=`ismael`@`%` PROCEDURE `getFormulesEvent` (IN `p_idEvent` INT)  NO SQL
SELECT f.id, f.idEvent, f.description , f.devise, f.prix

 FROM formulesEvent f

 where f.idEvent= p_idEvent$$

DROP PROCEDURE IF EXISTS `getIdListEvent`$$
CREATE DEFINER=`ismael`@`%` PROCEDURE `getIdListEvent` (IN `p_idUser` INT(10), IN `p_idGroupEvent` VARCHAR(255))  NO SQL
SELECT id from evenement_tb l where l.idUser=p_idUser and l.idGroupEvent =p_idGroupEvent$$

DROP PROCEDURE IF EXISTS `getImageUrlOfEvent`$$
CREATE DEFINER=`ismael`@`%` PROCEDURE `getImageUrlOfEvent` (IN `p_idUser` INT(10), IN `p_idEvent` INT(10))  NO SQL
SELECT image FROM evenement_tb where id =p_idEvent AND idUser=p_idUser$$

DROP PROCEDURE IF EXISTS `getInfosUserAvecEmail`$$
CREATE DEFINER=`ismael`@`%` PROCEDURE `getInfosUserAvecEmail` (IN `p_email` VARCHAR(150))  NO SQL
BEGIN
set @query= concat("select * from user_infos u WHERE u.email = '",p_email,"';");
 PREPARE test FROM @query;
 EXECUTE test;
END$$

DROP PROCEDURE IF EXISTS `getIntegerFavorisUser`$$
CREATE DEFINER=`ismael`@`%` PROCEDURE `getIntegerFavorisUser` (IN `p_idUser` INT(10))  NO SQL
SELECT idEvent from favoris f where f.idUser=p_idUser$$

DROP PROCEDURE IF EXISTS `getIntegerLikesUser`$$
CREATE DEFINER=`ismael`@`%` PROCEDURE `getIntegerLikesUser` (IN `p_idUser` INT(10))  NO SQL
SELECT idEvent from like_tb l where l.idUser=p_idUser$$

DROP PROCEDURE IF EXISTS `getPaysIndicatif`$$
CREATE DEFINER=`ismael`@`%` PROCEDURE `getPaysIndicatif` ()  NO SQL
SELECT c.name as nom, c.dial_code as indicatif
FROM country c$$

DROP PROCEDURE IF EXISTS `getPaysMonnaie`$$
CREATE DEFINER=`ismael`@`%` PROCEDURE `getPaysMonnaie` ()  NO SQL
SELECT * FROM `devise_view` ORDER BY devise_priorite DESC$$

DROP PROCEDURE IF EXISTS `getTypeEvent`$$
CREATE DEFINER=`ismael`@`%` PROCEDURE `getTypeEvent` ()  NO SQL
SELECT * FROM typeEvent_tb$$

DROP PROCEDURE IF EXISTS `getUserInfos`$$
CREATE DEFINER=`ismael`@`%` PROCEDURE `getUserInfos` (IN `p_idUser` INT(10))  NO SQL
SELECT u.nom,u.prenom,u.email,u.adresse,u.codepostal,u.ville,u.telephone,u.photoprofil from user_infos u WHERE u.id=p_idUser$$

DROP PROCEDURE IF EXISTS `getVilleProc`$$
CREATE DEFINER=`ismael`@`%` PROCEDURE `getVilleProc` (IN `p_cpORville` VARCHAR(100))  NO SQL
BEGIN

set @query= concat("select * from villes v WHERE v.nom LIKE '",p_cpORville,"%'","OR v.codepostal LIKE'",p_cpORville,"%'");
 PREPARE test FROM @query;
 EXECUTE test;
END$$

DROP PROCEDURE IF EXISTS `lastDayOfMonth`$$
CREATE DEFINER=`ismael`@`%` PROCEDURE `lastDayOfMonth` (IN `date` DATETIME, IN `jourSemaine` INT)  NO SQL
SELECT LAST_DAY(date) - INTERVAL (7 - jourSemaine + WEEKDAY(LAST_DAY(date))) % 7 DAY as date$$

DROP PROCEDURE IF EXISTS `niemeJoursMois`$$
CREATE DEFINER=`ismael`@`%` PROCEDURE `niemeJoursMois` (IN `ref_date` DATETIME, IN `n` INT, IN `week_day` INT, OUT `dateResult` DATETIME)  NO SQL
BEGIN



SELECT DATE_SUB( DATE_ADD( DATE_SUB( ref_date, INTERVAL DAYOFMONTH(ref_date) - 1 DAY),  INTERVAL n WEEK), INTERVAL ( 7 - ((week_day - ((DAYOFWEEK(DATE_SUB( ref_date, INTERVAL DAYOFMONTH(ref_date) - 1 DAY)) - 2) % 7) + 7) % 7) ) DAY) into dateResult ;







END$$

DROP PROCEDURE IF EXISTS `searchproc`$$
CREATE DEFINER=`ismael`@`%` PROCEDURE `searchproc` (IN `p_searchText` VARCHAR(200), IN `p_trier_par` VARCHAR(100), IN `p_type` VARCHAR(100), IN `p_region` VARCHAR(100), IN `p_departement` VARCHAR(100), IN `p_ville` VARCHAR(100), IN `p_start` INT(10), IN `p_limit` INT(10), IN `p_dateDebut` VARCHAR(100), IN `p_dateFin` VARCHAR(100))  BEGIN



set @searcText= concat(" and e.nom LIKE '",p_searchText,"'");

SET @region = concat(" and e.region LIKE '%",p_region,"%'");

SET @start_limit=concat(' limit ',p_start,',',p_limit);

SET @type = concat(" and e.type = '", p_type,"'");

SET @trierPar= concat(" order by ",p_trier_par," asc");

SET @departement = concat(" and e.departement LIKE '%",p_departement,"%'");

SET @ville = concat(" and e.ville LIKE '%",p_ville,"%'");

SET @dateDebut= concat( " where e.active = 1 and date(e.datedebut) ='",p_dateDebut,"'");

SET @dateFin = concat(" and date(e.datefin) ='",p_dateFin,"'");

set @dateDebutAll= "where e.active = 1 and e.datedebut >= CURRENT_DATE()";



SET @main_query="select e.id, e.image,e.nom,e.type,e.adresse,e.codepostal,e.ville,e.adresseComplet,e.paysNom,e.paysCode,e.latitude,e.lontitude,e.datedebut,e.datefin,e.telephone,e.typeTarif,e.description,e.departement AS departement,e.region AS region ,IFNULL((SELECT COUNT(*)  from favoris f WHERE f.idEvent=e.id GROUP by f.idEvent),0)as nbFavoris, IFNULL((SELECT COUNT(*)from like_tb l WHERE l.idEvent=e.id GROUP by l.idEvent),0)as nbLike,IFNULL((SELECT COUNT(*)from avis_tb a WHERE a.idEvent=e.id GROUP by a.idEvent),0)as nbAvis from evenement_tb e  ";





IF (p_dateDebut<>'')THEN

SET @main_query= concat(@main_query,@dateDebut);

ELSE

SET @main_query= concat(@main_query,@dateDebutAll);

END IF;



IF (p_dateFin<>'')THEN

SET @main_query= concat(@main_query,@dateFin);

END IF;





IF (p_type='Tout') THEN

if (p_ville <>'')THEN



	SET @final_query = concat(@main_query,@ville,@searcText,@trierPar,@start_limit,";");

ELSE

	IF (p_departement <>'') THEN

    

    SET @final_query = concat(@main_query,@departement,@searcText,@trierPar,@start_limit,";");

    ELSE

    	IF(p_region <>'') THEN

        SET @final_query = concat(@main_query, @region,@searcText,@trierPar,@start_limit,";");

        ELSE

        SET @final_query = concat(@main_query, @searcText,@trierPar,@start_limit,";");

        

        

        END IF;

    

    END IF;

END IF;



ELSE







if (p_ville <>'')THEN



	SET @final_query = concat(@main_query,@ville,@type,@searcText,@trierPar,@start_limit,";");

ELSE

	IF (p_departement <>'') THEN

     

    SET @final_query = concat(@main_query, 		@departement,@type,@searcText,@trierPar,@start_limit,";");

    ELSE

    	IF(p_region <>'') THEN

        SET @final_query = concat(@main_query, @region, @type,@searcText,@trierPar,@start_limit,";");

        ELSE

        SET @final_query = concat(@main_query, @type,@searcText,@trierPar,@start_limit,";");

        

        

        END IF;

    

    END IF;

END IF;



END IF;


PREPARE test FROM @final_query;

 EXECUTE test;

END$$

DROP PROCEDURE IF EXISTS `SupFav_proc`$$
CREATE DEFINER=`ismael`@`%` PROCEDURE `SupFav_proc` (IN `p_idEvent` INT(10), IN `p_idUser` INT(10))  NO SQL
DELETE FROM `favoris` WHERE idEvent=p_idEvent AND idUser=p_idUser$$

DROP PROCEDURE IF EXISTS `SupLike_proc`$$
CREATE DEFINER=`ismael`@`%` PROCEDURE `SupLike_proc` (IN `p_idEvent` INT(10), IN `p_idUser` INT(10))  NO SQL
DELETE FROM like_tb WHERE idEvent=p_idEvent AND idUser=p_idUser$$

DROP PROCEDURE IF EXISTS `updateEmail`$$
CREATE DEFINER=`ismael`@`%` PROCEDURE `updateEmail` (IN `p_idUser` INT(10), IN `p_email` VARCHAR(200))  NO SQL
UPDATE  user_infos u set u.email=p_email  WHERE u.id=p_idUser$$

DROP PROCEDURE IF EXISTS `updateEvent`$$
CREATE DEFINER=`ismael`@`%` PROCEDURE `updateEvent` (IN `p_idEvent` INT(200), IN `p_idUser` INT(200), IN `p_actualpath` VARCHAR(255), IN `p_nom` VARCHAR(255), IN `p_type` VARCHAR(255), IN `p_adresse` VARCHAR(255), IN `p_adresseComplet` VARCHAR(255), IN `p_codePostal` VARCHAR(255), IN `p_ville` VARCHAR(255), IN `p_departement` VARCHAR(255), IN `p_region` VARCHAR(255), IN `p_paysNom` VARCHAR(255), IN `p_paysCode` VARCHAR(255), IN `p_latitude` DOUBLE, IN `p_lontitude` DOUBLE, IN `p_datedeb` DATETIME, IN `p_datefin` DATETIME, IN `p_telephone` VARCHAR(255), IN `p_typeTarif` VARCHAR(255), IN `p_description` LONGTEXT)  NO SQL
UPDATE evenement_tb e

SET e.image=p_actualpath, e.nom=p_nom, e.type=p_type,e.adresse=p_adresse,e.adresseComplet=p_adresseComplet,

e.codepostal=p_codePostal,e.ville=p_ville,e.departement=p_departement,

e.region=p_region,e.paysNom=p_paysNom,e.paysCode=p_paysCode,e.latitude=p_latitude,e.lontitude=p_lontitude,e.datedebut=p_datedeb,e.datefin=p_datefin,e.telephone=p_telephone,e.typeTarif=p_typeTarif,e.description=p_description 

WHERE e.id=p_idEvent AND e.idUser=p_idUser$$

DROP PROCEDURE IF EXISTS `updateEventSansImage`$$
CREATE DEFINER=`ismael`@`%` PROCEDURE `updateEventSansImage` (IN `p_idEvent` INT, IN `p_idUser` INT, IN `p_nom` VARCHAR(255), IN `p_type` VARCHAR(255), IN `p_adresse` VARCHAR(255), IN `p_adresseComplet` VARCHAR(255), IN `p_codePostal` VARCHAR(255), IN `p_ville` VARCHAR(255), IN `p_departement` VARCHAR(255), IN `p_region` VARCHAR(255), IN `p_paysNom` VARCHAR(255), IN `p_paysCode` VARCHAR(255), IN `p_latitude` DOUBLE, IN `p_lontitude` DOUBLE, IN `p_datedeb` DATETIME, IN `p_datefin` DATETIME, IN `p_telephone` VARCHAR(255), IN `p_typeTarif` VARCHAR(255), IN `p_description` LONGTEXT)  NO SQL
UPDATE evenement_tb e

SET  e.nom=p_nom, e.type=p_type,e.adresse=p_adresse,e.adresseComplet=p_adresseComplet,

e.codepostal=p_codePostal,e.ville=p_ville,e.departement=p_departement,

e.region=p_region,e.paysNom=p_paysNom,e.paysCode=p_paysCode,e.latitude=p_latitude,e.lontitude=p_lontitude,e.datedebut=p_datedeb,e.datefin=p_datefin,e.telephone=p_telephone,e.typeTarif=p_typeTarif,e.description=p_description 

WHERE e.id=p_idEvent AND e.idUser=p_idUser$$

DROP PROCEDURE IF EXISTS `updateModif`$$
CREATE DEFINER=`ismael`@`%` PROCEDURE `updateModif` (IN `p_idUser` INT(10), IN `p_prenom` VARCHAR(100), IN `p_nom` VARCHAR(100), IN `p_adresse` VARCHAR(100), IN `p_cp` VARCHAR(5), IN `p_ville` VARCHAR(100), IN `p_adresseComplet` VARCHAR(255), IN `p_departement` VARCHAR(255), IN `p_region` VARCHAR(255), IN `p_paysNom` VARCHAR(255), IN `p_paysCode` VARCHAR(255), IN `p_latitude` VARCHAR(255), IN `p_lontitude` VARCHAR(255))  NO SQL
update user_infos u set u.nom=p_nom, u.prenom=p_prenom, u.adresse =p_adresse,u.codepostal =p_cp , u.ville= p_ville , u.adresseComplet=p_adresseComplet, u.departement=p_departement, u.region=p_region, u.paysNom=p_paysNom, u.paysCode=p_paysCode,u.latitude=p_latitude,u.lontitude=p_lontitude  where u.id=p_idUser$$

DROP PROCEDURE IF EXISTS `updatePP`$$
CREATE DEFINER=`ismael`@`%` PROCEDURE `updatePP` (IN `p_idUser` INT(10), IN `p_url` VARCHAR(200))  NO SQL
UPDATE  user_infos u set u.photoprofil=p_url  WHERE u.id=p_idUser$$

DROP PROCEDURE IF EXISTS `updateTel`$$
CREATE DEFINER=`ismael`@`%` PROCEDURE `updateTel` (IN `p_idUser` INT(10), IN `p_tel` VARCHAR(255))  NO SQL
UPDATE  user_infos u set u.telephone=p_tel  WHERE u.id=p_idUser$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `avis_tb`
--

DROP TABLE IF EXISTS `avis_tb`;
CREATE TABLE `avis_tb` (
  `id` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `idEvent` int(11) NOT NULL,
  `avis` varchar(250) COLLATE utf8mb4_bin DEFAULT NULL,
  `date_avis` datetime NOT NULL,
  `nbetoiles` varchar(200) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `confirm_tb`
--

DROP TABLE IF EXISTS `confirm_tb`;
CREATE TABLE `confirm_tb` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `keycript` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `confirm_tb`
--

INSERT INTO `confirm_tb` (`id`, `userid`, `keycript`, `email`) VALUES
(2, 2, '66e56f7bfc6672b7b12a7c03c15b411b', 'foussbak@hotmail.com'),
(3, 3, 'e60d7b086d25478af5099c0a05d9e997', 'assanfatnet@gmail.com'),
(4, 4, '211b01c0032d21b0a996fcca4bb677e2', 'assanfatouma@yahoo.fr'),
(7, 5, '6a66ca12aa8bee33df3b44d2173b9018', 'fredericsanogo@gmail.com'),
(8, 6, 'aa2606dc156107c8cc5f62a3c845de91', 'assaicha@yahoo.fr'),
(12, 10, '237cdafaf5f41ee58f56ba6ccda3b5a4', 'amoross@outlook.fr'),
(36, 52, 'f1e27a7da1975118bdca840942d1769d', 'sempere982@gmail.com'),
(37, 53, 'a414e13c17e8d459d78ce277727b6058', 'L645387249@gmail.com'),
(43, 59, '5c52c450ab5bbc87a7d30f7357747cbf', 'ismabak@hotmail.fr');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
CREATE TABLE `country` (
  `ID` int(5) NOT NULL,
  `name` varchar(20) NOT NULL,
  `code` varchar(2) NOT NULL,
  `dial_code` varchar(5) NOT NULL,
  `currency_name` varchar(20) NOT NULL,
  `currency_symbol` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `currency_code` varchar(10) NOT NULL,
  `devise_priorite` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`ID`, `name`, `code`, `dial_code`, `currency_name`, `currency_symbol`, `currency_code`, `devise_priorite`) VALUES
(1, 'Afghanistan', 'AF', '+93', 'Afghan afghani', '؋', 'AFN', 0),
(2, 'Aland Islands', 'AX', '+358', '', '', '', 0),
(3, 'Albania', 'AL', '+355', 'Albanian lek', 'L', 'ALL', 0),
(4, 'Algeria', 'DZ', '+213', 'Algerian dinar', 'د.ج', 'DZD', 0),
(5, 'AmericanSamoa', 'AS', '+1684', '', '', '', 0),
(6, 'Andorra', 'AD', '+376', 'Euro', '€', 'EUR', 0),
(7, 'Angola', 'AO', '+244', 'Angolan kwanza', 'Kz', 'AOA', 0),
(8, 'Anguilla', 'AI', '+1264', 'East Caribbean dolla', '$', 'XCD', 0),
(9, 'Antarctica', 'AQ', '+672', '', '', '', 0),
(10, 'Antigua and Barbuda', 'AG', '+1268', 'East Caribbean dolla', '$', 'XCD', 0),
(11, 'Argentina', 'AR', '+54', 'Argentine peso', '$', 'ARS', 0),
(12, 'Armenia', 'AM', '+374', 'Armenian dram', '', 'AMD', 0),
(13, 'Aruba', 'AW', '+297', 'Aruban florin', 'ƒ', 'AWG', 0),
(14, 'Australia', 'AU', '+61', 'Australian dollar', '$', 'AUD', 0),
(15, 'Austria', 'AT', '+43', 'Euro', '€', 'EUR', 0),
(16, 'Azerbaijan', 'AZ', '+994', 'Azerbaijani manat', '', 'AZN', 0),
(17, 'Bahamas', 'BS', '+1242', '', '', '', 0),
(18, 'Bahrain', 'BH', '+973', 'Bahraini dinar', '.د.ب', 'BHD', 0),
(19, 'Bangladesh', 'BD', '+880', 'Bangladeshi taka', '৳', 'BDT', 0),
(20, 'Barbados', 'BB', '+1246', 'Barbadian dollar', '$', 'BBD', 0),
(21, 'Belarus', 'BY', '+375', 'Belarusian ruble', 'Br', 'BYR', 0),
(22, 'Belgium', 'BE', '+32', 'Euro', '€', 'EUR', 0),
(23, 'Belize', 'BZ', '+501', 'Belize dollar', '$', 'BZD', 0),
(24, 'Benin', 'BJ', '+229', 'West African CFA fra', 'Fr', 'XOF', 0),
(25, 'Bermuda', 'BM', '+1441', 'Bermudian dollar', '$', 'BMD', 0),
(26, 'Bhutan', 'BT', '+975', 'Bhutanese ngultrum', 'Nu.', 'BTN', 0),
(27, 'Bolivia, Plurination', 'BO', '+591', '', '', '', 0),
(28, 'Bosnia and Herzegovi', 'BA', '+387', '', '', '', 0),
(29, 'Botswana', 'BW', '+267', 'Botswana pula', 'P', 'BWP', 0),
(30, 'Brazil', 'BR', '+55', 'Brazilian real', 'R$', 'BRL', 0),
(31, 'British Indian Ocean', 'IO', '+246', '', '', '', 0),
(32, 'Brunei Darussalam', 'BN', '+673', '', '', '', 0),
(33, 'Bulgaria', 'BG', '+359', 'Bulgarian lev', 'лв', 'BGN', 0),
(34, 'Burkina Faso', 'BF', '+226', 'West African CFA fra', 'Fr', 'XOF', 0),
(35, 'Burundi', 'BI', '+257', 'Burundian franc', 'Fr', 'BIF', 0),
(36, 'Cambodia', 'KH', '+855', 'Cambodian riel', '៛', 'KHR', 0),
(37, 'Cameroon', 'CM', '+237', 'Central African CFA ', 'Fr', 'XAF', 0),
(38, 'Canada', 'CA', '+1', 'Canadian dollar', '$', 'CAD', 0),
(39, 'Cape Verde', 'CV', '+238', 'Cape Verdean escudo', 'Esc or $', 'CVE', 0),
(40, 'Cayman Islands', 'KY', '+ 345', 'Cayman Islands dolla', '$', 'KYD', 0),
(41, 'Central African Repu', 'CF', '+236', '', '', '', 0),
(42, 'Chad', 'TD', '+235', 'Central African CFA ', 'Fr', 'XAF', 0),
(43, 'Chile', 'CL', '+56', 'Chilean peso', '$', 'CLP', 0),
(44, 'China', 'CN', '+86', 'Chinese yuan', '¥ or 元', 'CNY', 0),
(45, 'Christmas Island', 'CX', '+61', '', '', '', 0),
(46, 'Cocos (Keeling) Isla', 'CC', '+61', '', '', '', 0),
(47, 'Colombia', 'CO', '+57', 'Colombian peso', '$', 'COP', 0),
(48, 'Comoros', 'KM', '+269', 'Comorian franc', 'Fr', 'KMF', 0),
(49, 'Congo', 'CG', '+242', '', '', '', 0),
(50, 'Congo, The Democrati', 'CD', '+243', '', '', '', 0),
(51, 'Cook Islands', 'CK', '+682', 'New Zealand dollar', '$', 'NZD', 0),
(52, 'Costa Rica', 'CR', '+506', 'Costa Rican colón', '₡', 'CRC', 0),
(53, 'Cote d\'Ivoire', 'CI', '+225', 'West African CFA fra', 'Fr', 'XOF', 0),
(54, 'Croatia', 'HR', '+385', 'Croatian kuna', 'kn', 'HRK', 0),
(55, 'Cuba', 'CU', '+53', 'Cuban convertible pe', '$', 'CUC', 0),
(56, 'Cyprus', 'CY', '+357', 'Euro', '€', 'EUR', 0),
(57, 'Czech Republic', 'CZ', '+420', 'Czech koruna', 'Kč', 'CZK', 0),
(58, 'Denmark', 'DK', '+45', 'Danish krone', 'kr', 'DKK', 0),
(59, 'Djibouti', 'DJ', '+253', 'Djiboutian franc', 'Fr', 'DJF', 0),
(60, 'Dominica', 'DM', '+1767', 'East Caribbean dolla', '$', 'XCD', 0),
(61, 'Dominican Republic', 'DO', '+1849', 'Dominican peso', '$', 'DOP', 0),
(62, 'Ecuador', 'EC', '+593', 'United States dollar', '$', 'USD', 0),
(63, 'Egypt', 'EG', '+20', 'Egyptian pound', '£ or ج.م', 'EGP', 0),
(64, 'El Salvador', 'SV', '+503', 'United States dollar', '$', 'USD', 0),
(65, 'Equatorial Guinea', 'GQ', '+240', 'Central African CFA ', 'Fr', 'XAF', 0),
(66, 'Eritrea', 'ER', '+291', 'Eritrean nakfa', 'Nfk', 'ERN', 0),
(67, 'Estonia', 'EE', '+372', 'Euro', '€', 'EUR', 0),
(68, 'Ethiopia', 'ET', '+251', 'Ethiopian birr', 'Br', 'ETB', 0),
(69, 'Falkland Islands (Ma', 'FK', '+500', '', '', '', 0),
(70, 'Faroe Islands', 'FO', '+298', 'Danish krone', 'kr', 'DKK', 0),
(71, 'Fiji', 'FJ', '+679', 'Fijian dollar', '$', 'FJD', 0),
(72, 'Finland', 'FI', '+358', 'Euro', '€', 'EUR', 0),
(73, 'France', 'FR', '+33', 'Euro', '€', 'EUR', 1),
(74, 'French Guiana', 'GF', '+594', '', '', '', 0),
(75, 'French Polynesia', 'PF', '+689', 'CFP franc', 'Fr', 'XPF', 0),
(76, 'Gabon', 'GA', '+241', 'Central African CFA ', 'Fr', 'XAF', 0),
(77, 'Gambia', 'GM', '+220', '', '', '', 0),
(78, 'Georgia', 'GE', '+995', 'Georgian lari', 'ლ', 'GEL', 0),
(79, 'Germany', 'DE', '+49', 'Euro', '€', 'EUR', 0),
(80, 'Ghana', 'GH', '+233', 'Ghana cedi', '₵', 'GHS', 0),
(81, 'Gibraltar', 'GI', '+350', 'Gibraltar pound', '£', 'GIP', 0),
(82, 'Greece', 'GR', '+30', 'Euro', '€', 'EUR', 0),
(83, 'Greenland', 'GL', '+299', '', '', '', 0),
(84, 'Grenada', 'GD', '+1473', 'East Caribbean dolla', '$', 'XCD', 0),
(85, 'Guadeloupe', 'GP', '+590', '', '', '', 0),
(86, 'Guam', 'GU', '+1671', '', '', '', 0),
(87, 'Guatemala', 'GT', '+502', 'Guatemalan quetzal', 'Q', 'GTQ', 0),
(88, 'Guernsey', 'GG', '+44', 'British pound', '£', 'GBP', 0),
(89, 'Guinea', 'GN', '+224', 'Guinean franc', 'Fr', 'GNF', 0),
(90, 'Guinea-Bissau', 'GW', '+245', 'West African CFA fra', 'Fr', 'XOF', 0),
(91, 'Guyana', 'GY', '+595', 'Guyanese dollar', '$', 'GYD', 0),
(92, 'Haiti', 'HT', '+509', 'Haitian gourde', 'G', 'HTG', 0),
(93, 'Holy See (Vatican Ci', 'VA', '+379', '', '', '', 0),
(94, 'Honduras', 'HN', '+504', 'Honduran lempira', 'L', 'HNL', 0),
(95, 'Hong Kong', 'HK', '+852', 'Hong Kong dollar', '$', 'HKD', 0),
(96, 'Hungary', 'HU', '+36', 'Hungarian forint', 'Ft', 'HUF', 0),
(97, 'Iceland', 'IS', '+354', 'Icelandic króna', 'kr', 'ISK', 0),
(98, 'India', 'IN', '+91', 'Indian rupee', '₹', 'INR', 0),
(99, 'Indonesia', 'ID', '+62', 'Indonesian rupiah', 'Rp', 'IDR', 0),
(100, 'Iran, Islamic Republ', 'IR', '+98', '', '', '', 0),
(101, 'Iraq', 'IQ', '+964', 'Iraqi dinar', 'ع.د', 'IQD', 0),
(102, 'Ireland', 'IE', '+353', 'Euro', '€', 'EUR', 0),
(103, 'Isle of Man', 'IM', '+44', 'British pound', '£', 'GBP', 0),
(104, 'Israel', 'IL', '+972', 'Israeli new shekel', '₪', 'ILS', 0),
(105, 'Italy', 'IT', '+39', 'Euro', '€', 'EUR', 0),
(106, 'Jamaica', 'JM', '+1876', 'Jamaican dollar', '$', 'JMD', 0),
(107, 'Japan', 'JP', '+81', 'Japanese yen', '¥', 'JPY', 0),
(108, 'Jersey', 'JE', '+44', 'British pound', '£', 'GBP', 0),
(109, 'Jordan', 'JO', '+962', 'Jordanian dinar', 'د.ا', 'JOD', 0),
(110, 'Kazakhstan', 'KZ', '+7 7', 'Kazakhstani tenge', '', 'KZT', 0),
(111, 'Kenya', 'KE', '+254', 'Kenyan shilling', 'Sh', 'KES', 0),
(112, 'Kiribati', 'KI', '+686', 'Australian dollar', '$', 'AUD', 0),
(113, 'Korea, Democratic Pe', 'KP', '+850', '', '', '', 0),
(114, 'Korea, Republic of S', 'KR', '+82', '', '', '', 0),
(115, 'Kuwait', 'KW', '+965', 'Kuwaiti dinar', 'د.ك', 'KWD', 0),
(116, 'Kyrgyzstan', 'KG', '+996', 'Kyrgyzstani som', 'лв', 'KGS', 0),
(117, 'Laos', 'LA', '+856', 'Lao kip', '₭', 'LAK', 0),
(118, 'Latvia', 'LV', '+371', 'Euro', '€', 'EUR', 0),
(119, 'Lebanon', 'LB', '+961', 'Lebanese pound', 'ل.ل', 'LBP', 0),
(120, 'Lesotho', 'LS', '+266', 'Lesotho loti', 'L', 'LSL', 0),
(121, 'Liberia', 'LR', '+231', 'Liberian dollar', '$', 'LRD', 0),
(122, 'Libyan Arab Jamahiri', 'LY', '+218', '', '', '', 0),
(123, 'Liechtenstein', 'LI', '+423', 'Swiss franc', 'Fr', 'CHF', 0),
(124, 'Lithuania', 'LT', '+370', 'Euro', '€', 'EUR', 0),
(125, 'Luxembourg', 'LU', '+352', 'Euro', '€', 'EUR', 0),
(126, 'Macao', 'MO', '+853', '', '', '', 0),
(127, 'Macedonia', 'MK', '+389', '', '', '', 0),
(128, 'Madagascar', 'MG', '+261', 'Malagasy ariary', 'Ar', 'MGA', 0),
(129, 'Malawi', 'MW', '+265', 'Malawian kwacha', 'MK', 'MWK', 0),
(130, 'Malaysia', 'MY', '+60', 'Malaysian ringgit', 'RM', 'MYR', 0),
(131, 'Maldives', 'MV', '+960', 'Maldivian rufiyaa', '.ރ', 'MVR', 0),
(132, 'Mali', 'ML', '+223', 'West African CFA fra', 'Fr', 'XOF', 0),
(133, 'Malta', 'MT', '+356', 'Euro', '€', 'EUR', 0),
(134, 'Marshall Islands', 'MH', '+692', 'United States dollar', '$', 'USD', 0),
(135, 'Martinique', 'MQ', '+596', '', '', '', 0),
(136, 'Mauritania', 'MR', '+222', 'Mauritanian ouguiya', 'UM', 'MRO', 0),
(137, 'Mauritius', 'MU', '+230', 'Mauritian rupee', '₨', 'MUR', 0),
(138, 'Mayotte', 'YT', '+262', '', '', '', 0),
(139, 'Mexico', 'MX', '+52', 'Mexican peso', '$', 'MXN', 0),
(140, 'Micronesia, Federate', 'FM', '+691', '', '', '', 0),
(141, 'Moldova', 'MD', '+373', 'Moldovan leu', 'L', 'MDL', 0),
(142, 'Monaco', 'MC', '+377', 'Euro', '€', 'EUR', 0),
(143, 'Mongolia', 'MN', '+976', 'Mongolian tögrög', '₮', 'MNT', 0),
(144, 'Montenegro', 'ME', '+382', 'Euro', '€', 'EUR', 0),
(145, 'Montserrat', 'MS', '+1664', 'East Caribbean dolla', '$', 'XCD', 0),
(146, 'Morocco', 'MA', '+212', 'Moroccan dirham', 'د.م.', 'MAD', 0),
(147, 'Mozambique', 'MZ', '+258', 'Mozambican metical', 'MT', 'MZN', 0),
(148, 'Myanmar', 'MM', '+95', 'Burmese kyat', 'Ks', 'MMK', 0),
(149, 'Namibia', 'NA', '+264', 'Namibian dollar', '$', 'NAD', 0),
(150, 'Nauru', 'NR', '+674', 'Australian dollar', '$', 'AUD', 0),
(151, 'Nepal', 'NP', '+977', 'Nepalese rupee', '₨', 'NPR', 0),
(152, 'Netherlands', 'NL', '+31', 'Euro', '€', 'EUR', 0),
(153, 'Netherlands Antilles', 'AN', '+599', '', '', '', 0),
(154, 'New Caledonia', 'NC', '+687', 'CFP franc', 'Fr', 'XPF', 0),
(155, 'New Zealand', 'NZ', '+64', 'New Zealand dollar', '$', 'NZD', 0),
(156, 'Nicaragua', 'NI', '+505', 'Nicaraguan córdoba', 'C$', 'NIO', 0),
(157, 'Niger', 'NE', '+227', 'West African CFA fra', 'Fr', 'XOF', 0),
(158, 'Nigeria', 'NG', '+234', 'Nigerian naira', '₦', 'NGN', 0),
(159, 'Niue', 'NU', '+683', 'New Zealand dollar', '$', 'NZD', 0),
(160, 'Norfolk Island', 'NF', '+672', '', '', '', 0),
(161, 'Northern Mariana Isl', 'MP', '+1670', '', '', '', 0),
(162, 'Norway', 'NO', '+47', 'Norwegian krone', 'kr', 'NOK', 0),
(163, 'Oman', 'OM', '+968', 'Omani rial', 'ر.ع.', 'OMR', 0),
(164, 'Pakistan', 'PK', '+92', 'Pakistani rupee', '₨', 'PKR', 0),
(165, 'Palau', 'PW', '+680', 'Palauan dollar', '$', '', 0),
(166, 'Palestinian Territor', 'PS', '+970', '', '', '', 0),
(167, 'Panama', 'PA', '+507', 'Panamanian balboa', 'B/.', 'PAB', 0),
(168, 'Papua New Guinea', 'PG', '+675', 'Papua New Guinean ki', 'K', 'PGK', 0),
(169, 'Paraguay', 'PY', '+595', 'Paraguayan guaraní', '₲', 'PYG', 0),
(170, 'Peru', 'PE', '+51', 'Peruvian nuevo sol', 'S/.', 'PEN', 0),
(171, 'Philippines', 'PH', '+63', 'Philippine peso', '₱', 'PHP', 0),
(172, 'Pitcairn', 'PN', '+872', '', '', '', 0),
(173, 'Poland', 'PL', '+48', 'Polish z?oty', 'zł', 'PLN', 0),
(174, 'Portugal', 'PT', '+351', 'Euro', '€', 'EUR', 0),
(175, 'Puerto Rico', 'PR', '+1939', '', '', '', 0),
(176, 'Qatar', 'QA', '+974', 'Qatari riyal', 'ر.ق', 'QAR', 0),
(177, 'Romania', 'RO', '+40', 'Romanian leu', 'lei', 'RON', 0),
(178, 'Russia', 'RU', '+7', 'Russian ruble', '', 'RUB', 0),
(179, 'Rwanda', 'RW', '+250', 'Rwandan franc', 'Fr', 'RWF', 0),
(180, 'Reunion', 'RE', '+262', '', '', '', 0),
(181, 'Saint Barthelemy', 'BL', '+590', '', '', '', 0),
(182, 'Saint Helena, Ascens', 'SH', '+290', '', '', '', 0),
(183, 'Saint Kitts and Nevi', 'KN', '+1869', '', '', '', 0),
(184, 'Saint Lucia', 'LC', '+1758', 'East Caribbean dolla', '$', 'XCD', 0),
(185, 'Saint Martin', 'MF', '+590', '', '', '', 0),
(186, 'Saint Pierre and Miq', 'PM', '+508', '', '', '', 0),
(187, 'Saint Vincent and th', 'VC', '+1784', '', '', '', 0),
(188, 'Samoa', 'WS', '+685', 'Samoan t?l?', 'T', 'WST', 0),
(189, 'San Marino', 'SM', '+378', 'Euro', '€', 'EUR', 0),
(190, 'Sao Tome and Princip', 'ST', '+239', '', '', '', 0),
(191, 'Saudi Arabia', 'SA', '+966', 'Saudi riyal', 'ر.س', 'SAR', 0),
(192, 'Senegal', 'SN', '+221', 'West African CFA fra', 'Fr', 'XOF', 0),
(193, 'Serbia', 'RS', '+381', 'Serbian dinar', 'дин. or din.', 'RSD', 0),
(194, 'Seychelles', 'SC', '+248', 'Seychellois rupee', '₨', 'SCR', 0),
(195, 'Sierra Leone', 'SL', '+232', 'Sierra Leonean leone', 'Le', 'SLL', 0),
(196, 'Singapore', 'SG', '+65', 'Brunei dollar', '$', 'BND', 0),
(197, 'Slovakia', 'SK', '+421', 'Euro', '€', 'EUR', 0),
(198, 'Slovenia', 'SI', '+386', 'Euro', '€', 'EUR', 0),
(199, 'Solomon Islands', 'SB', '+677', 'Solomon Islands doll', '$', 'SBD', 0),
(200, 'Somalia', 'SO', '+252', 'Somali shilling', 'Sh', 'SOS', 0),
(201, 'South Africa', 'ZA', '+27', 'South African rand', 'R', 'ZAR', 0),
(202, 'South Georgia and th', 'GS', '+500', '', '', '', 0),
(203, 'Spain', 'ES', '+34', 'Euro', '€', 'EUR', 0),
(204, 'Sri Lanka', 'LK', '+94', 'Sri Lankan rupee', 'Rs or රු', 'LKR', 0),
(205, 'Sudan', 'SD', '+249', 'Sudanese pound', 'ج.س.', 'SDG', 0),
(206, 'Suriname', 'SR', '+597', 'Surinamese dollar', '$', 'SRD', 0),
(207, 'Svalbard and Jan May', 'SJ', '+47', '', '', '', 0),
(208, 'Swaziland', 'SZ', '+268', 'Swazi lilangeni', 'L', 'SZL', 0),
(209, 'Sweden', 'SE', '+46', 'Swedish krona', 'kr', 'SEK', 0),
(210, 'Switzerland', 'CH', '+41', 'Swiss franc', 'Fr', 'CHF', 0),
(211, 'Syrian Arab Republic', 'SY', '+963', '', '', '', 0),
(212, 'Taiwan', 'TW', '+886', 'New Taiwan dollar', '$', 'TWD', 0),
(213, 'Tajikistan', 'TJ', '+992', 'Tajikistani somoni', 'ЅМ', 'TJS', 0),
(214, 'Tanzania, United Rep', 'TZ', '+255', '', '', '', 0),
(215, 'Thailand', 'TH', '+66', 'Thai baht', '฿', 'THB', 0),
(216, 'Timor-Leste', 'TL', '+670', '', '', '', 0),
(217, 'Togo', 'TG', '+228', 'West African CFA fra', 'Fr', 'XOF', 0),
(218, 'Tokelau', 'TK', '+690', '', '', '', 0),
(219, 'Tonga', 'TO', '+676', 'Tongan pa?anga', 'T$', 'TOP', 0),
(220, 'Trinidad and Tobago', 'TT', '+1868', 'Trinidad and Tobago ', '$', 'TTD', 0),
(221, 'Tunisia', 'TN', '+216', 'Tunisian dinar', 'د.ت', 'TND', 0),
(222, 'Turkey', 'TR', '+90', 'Turkish lira', '', 'TRY', 0),
(223, 'Turkmenistan', 'TM', '+993', 'Turkmenistan manat', 'm', 'TMT', 0),
(224, 'Turks and Caicos Isl', 'TC', '+1649', '', '', '', 0),
(225, 'Tuvalu', 'TV', '+688', 'Australian dollar', '$', 'AUD', 0),
(226, 'Uganda', 'UG', '+256', 'Ugandan shilling', 'Sh', 'UGX', 0),
(227, 'Ukraine', 'UA', '+380', 'Ukrainian hryvnia', '₴', 'UAH', 0),
(228, 'United Arab Emirates', 'AE', '+971', 'United Arab Emirates', 'د.إ', 'AED', 0),
(229, 'United Kingdom', 'GB', '+44', 'British pound', '£', 'GBP', 0),
(230, 'United States', 'US', '+1', 'United States dollar', '$', 'USD', 1),
(231, 'Uruguay', 'UY', '+598', 'Uruguayan peso', '$', 'UYU', 0),
(232, 'Uzbekistan', 'UZ', '+998', 'Uzbekistani som', '', 'UZS', 0),
(233, 'Vanuatu', 'VU', '+678', 'Vanuatu vatu', 'Vt', 'VUV', 0),
(234, 'Venezuela, Bolivaria', 'VE', '+58', '', '', '', 0),
(235, 'Vietnam', 'VN', '+84', 'Vietnamese ??ng', '₫', 'VND', 0),
(236, 'Virgin Islands, Brit', 'VG', '+1284', '', '', '', 0),
(237, 'Virgin Islands, U.S.', 'VI', '+1340', '', '', '', 0),
(238, 'Wallis and Futuna', 'WF', '+681', 'CFP franc', 'Fr', 'XPF', 0),
(239, 'Yemen', 'YE', '+967', 'Yemeni rial', '﷼', 'YER', 0),
(240, 'Zambia', 'ZM', '+260', 'Zambian kwacha', 'ZK', 'ZMW', 0),
(241, 'Zimbabwe', 'ZW', '+263', 'Botswana pula', 'P', 'BWP', 0);

-- --------------------------------------------------------

--
-- Stand-in structure for view `devise_view`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `devise_view`;
CREATE TABLE `devise_view` (
`monnaie` varchar(20)
,`symboleMonnaie` varchar(20)
,`codeMonnaie` varchar(10)
,`devise_priorite` decimal(32,0)
);

-- --------------------------------------------------------

--
-- Table structure for table `evenement_tb`
--

DROP TABLE IF EXISTS `evenement_tb`;
CREATE TABLE `evenement_tb` (
  `id` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `image` varchar(300) COLLATE utf8mb4_bin NOT NULL,
  `nom` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `type` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `adresse` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `adresseComplet` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `codepostal` varchar(5) COLLATE utf8mb4_bin NOT NULL,
  `ville` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `departement` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `region` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `paysNom` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `paysCode` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `lontitude` double DEFAULT NULL,
  `datedebut` datetime NOT NULL,
  `datefin` datetime NOT NULL,
  `telephone` varchar(30) COLLATE utf8mb4_bin NOT NULL,
  `typeTarif` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `description` longtext COLLATE utf8mb4_bin NOT NULL,
  `idGroupEvent` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `active` binary(1) DEFAULT '\0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `favoris`
--

DROP TABLE IF EXISTS `favoris`;
CREATE TABLE `favoris` (
  `id` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `idEvent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `formulesEvent`
--

DROP TABLE IF EXISTS `formulesEvent`;
CREATE TABLE `formulesEvent` (
  `id` int(11) NOT NULL,
  `idEvent` int(11) NOT NULL,
  `description` text NOT NULL,
  `prix` int(11) NOT NULL,
  `devise` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `like_tb`
--

DROP TABLE IF EXISTS `like_tb`;
CREATE TABLE `like_tb` (
  `id` int(30) NOT NULL,
  `idUser` int(30) NOT NULL,
  `idEvent` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_infos`
--

DROP TABLE IF EXISTS `user_infos`;
CREATE TABLE `user_infos` (
  `id` int(11) NOT NULL,
  `nom` varchar(75) NOT NULL,
  `prenom` varchar(75) NOT NULL,
  `motdepasse` varchar(75) NOT NULL,
  `email` varchar(75) NOT NULL,
  `adresse` varchar(100) DEFAULT NULL,
  `adresseComplet` varchar(255) DEFAULT NULL,
  `codepostal` varchar(5) DEFAULT NULL,
  `ville` varchar(75) DEFAULT NULL,
  `departement` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `paysNom` varchar(255) DEFAULT NULL,
  `paysCode` varchar(255) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `lontitude` double DEFAULT NULL,
  `telephone` varchar(30) DEFAULT NULL,
  `photoprofil` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'https://eeventoo.com/webapp/UserPhotoProfilDef/defaultphoto.png',
  `active` binary(1) NOT NULL DEFAULT '\0',
  `idOtherUser` varchar(255) DEFAULT NULL,
  `typeInscription` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_infos`
--

INSERT INTO `user_infos` (`id`, `nom`, `prenom`, `motdepasse`, `email`, `adresse`, `adresseComplet`, `codepostal`, `ville`, `departement`, `region`, `paysNom`, `paysCode`, `latitude`, `lontitude`, `telephone`, `photoprofil`, `active`, `idOtherUser`, `typeInscription`) VALUES
(2, 'Bak', 'Fouss', 'f71dbe52628a3f83a77ab494817525c6', 'foussbak@hotmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://eeventoo.com/webapp/UserPhotoProfilDef/defaultphoto.png', 0x31, '', ''),
(3, 'assan', 'fatouma', '4fab4e8397e9849c96546713c9a1ed31', 'assanfatnet@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://eeventoo.com/webapp/UserPhotoProfilDef/defaultphoto.png', 0x31, '', ''),
(4, 'fatouma', 'assan ', '592bbc6b916c57aa1195eb126530b5c7', 'assanfatouma@yahoo.fr', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://eeventoo.com/webapp/UserPhotoProfilDef/defaultphoto.png', 0x31, '', ''),
(5, 'sanogo', 'frederic', 'f979387414f88e8c70f4f6070c21eeea', 'fredericsanogo@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://eeventoo.com/webapp/UserPhotoProfilDef/defaultphoto.png', 0x31, '', ''),
(6, 'assan', 'aicha', '9601115388c2ad1ad0a3568aaac70232', 'assaicha@yahoo.fr', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://eeventoo.com/webapp/UserPhotoProfilDef/defaultphoto.png', 0x31, '', ''),
(10, 'Diabate', 'amara', '07385fdfaadaae33ffc8350e0ebd594f', 'amoross@outlook.fr', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://eeventoo.com/webapp/UserPhotoProfilDef/defaultphoto.png', 0x31, '', ''),
(52, 'l', 'v', '33728272ed1634bd366dd71eb3befd16', 'sempere982@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://eeventoo.com/webapp/UserPhotoProfilDef/defaultphoto.png', 0x30, NULL, NULL),
(53, 'D', 'Loulou ', '6d9df66cf86bea5ae751c508268d9576', 'L645387249@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://eeventoo.com/webapp/UserPhotoProfilDef/defaultphoto.png', 0x30, NULL, NULL),
(59, 'bak', 'isma', 'b65cb932ad941c2de600ff3c5c0c9a56', 'ismabak@hotmail.fr', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://eeventoo.com/webapp/UserPhotoProfilDef/defaultphoto.png', 0x30, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure for view `devise_view` exported as a table
--
DROP TABLE IF EXISTS `devise_view`;
CREATE TABLE`devise_view`(
    `monnaie` varchar(20) COLLATE latin1_swedish_ci NOT NULL,
    `symboleMonnaie` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
    `codeMonnaie` varchar(10) COLLATE latin1_swedish_ci NOT NULL,
    `devise_priorite` decimal(32,0) DEFAULT NULL
);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `avis_tb`
--
ALTER TABLE `avis_tb`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_AidEvent` (`idEvent`),
  ADD KEY `FK_AidUser` (`idUser`);

--
-- Indexes for table `confirm_tb`
--
ALTER TABLE `confirm_tb`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_userid` (`userid`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `evenement_tb`
--
ALTER TABLE `evenement_tb`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favoris`
--
ALTER TABLE `favoris`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_IdEvent` (`idEvent`),
  ADD KEY `FK_IdUser` (`idUser`);

--
-- Indexes for table `formulesEvent`
--
ALTER TABLE `formulesEvent`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_formule_idEvent` (`idEvent`);

--
-- Indexes for table `like_tb`
--
ALTER TABLE `like_tb`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_IdEvent_like` (`idEvent`),
  ADD KEY `fk_IdUser_like` (`idUser`);

--
-- Indexes for table `user_infos`
--
ALTER TABLE `user_infos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `avis_tb`
--
ALTER TABLE `avis_tb`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `confirm_tb`
--
ALTER TABLE `confirm_tb`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=242;
--
-- AUTO_INCREMENT for table `evenement_tb`
--
ALTER TABLE `evenement_tb`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `favoris`
--
ALTER TABLE `favoris`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `formulesEvent`
--
ALTER TABLE `formulesEvent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `like_tb`
--
ALTER TABLE `like_tb`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_infos`
--
ALTER TABLE `user_infos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `avis_tb`
--
ALTER TABLE `avis_tb`
  ADD CONSTRAINT `FK_AidEvent` FOREIGN KEY (`idEvent`) REFERENCES `evenement_tb` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_AidUser` FOREIGN KEY (`idUser`) REFERENCES `user_infos` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `confirm_tb`
--
ALTER TABLE `confirm_tb`
  ADD CONSTRAINT `FK_userid` FOREIGN KEY (`userid`) REFERENCES `user_infos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `favoris`
--
ALTER TABLE `favoris`
  ADD CONSTRAINT `FK_IdEvent` FOREIGN KEY (`idEvent`) REFERENCES `evenement_tb` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_IdUser` FOREIGN KEY (`idUser`) REFERENCES `user_infos` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `formulesEvent`
--
ALTER TABLE `formulesEvent`
  ADD CONSTRAINT `fk_formule_idEvent` FOREIGN KEY (`idEvent`) REFERENCES `evenement_tb` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `like_tb`
--
ALTER TABLE `like_tb`
  ADD CONSTRAINT `fk_IdEvent_like` FOREIGN KEY (`idEvent`) REFERENCES `evenement_tb` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_IdUser_like` FOREIGN KEY (`idUser`) REFERENCES `user_infos` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
